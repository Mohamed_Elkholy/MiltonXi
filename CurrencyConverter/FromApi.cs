﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CurrencyConverter
{
   public class FromApi
    {
        
        public static HttpClient ApiClient { get; set; } = new HttpClient();
        public static void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.BaseAddress = new Uri("");
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
        
        public static async Task<double> CurrConv(string FromCurrencyISO,string ToCurrencyISO,double Amount, string apiKey)
        {
            string FromToEx = $"{ FromCurrencyISO }_{ ToCurrencyISO}";
            string requestString = $@"https://free.currconv.com/api/v7/convert?q={FromToEx}&compact=ultra&apiKey={apiKey}";

            using (HttpResponseMessage res = await ApiClient.GetAsync(requestString))
            {
                if (res.IsSuccessStatusCode)
                {
                  string   jsonresult = await res.Content.ReadAsStringAsync();
                    var serializer = new JavaScriptSerializer();
                    Dictionary<string, string> values = serializer.Deserialize<Dictionary<string, string>>(jsonresult);
                    string rate = values[FromToEx];
                  double  Exrate = System.Convert.ToDouble(rate);
                   double  ConvertedValue = Amount * Exrate;
                    return ConvertedValue;
                }
                return 0;

            }

            //HttpClient client = new HttpClient();
            //string FromToEx = $"{ FromCurrencyISO }_{ ToCurrencyISO}";

            //string requestString = $@"https://free.currconv.com/api/v7/convert?q={FromToEx}&compact=ultra&apiKey={apiKey}";

            //HttpResponseMessage response = await client.GetAsync(requestString);
            //response.EnsureSuccessStatusCode();
            //string responseBody = await response.Content.ReadAsStringAsync();
            //string result = responseBody;
            //return 0;
        }

        public void temp()
        {
            //string FromToEx = $"{ invoiceHeader.Currency }_{ Session.Defaults.Currency}";
            //string requestString = $@"https://free.currconv.com/api/v7/convert?q={FromToEx}&compact=ultra&apiKey={Session.Defaults.CurrencyExchangeAPIKey}";
            //var client = new RestClient(requestString);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.GET);
            //IRestResponse response = client.Execute(request);

            //Console.WriteLine(response.Content);
            //string data = JObject.Parse(response.Content)[FromToEx].ToString();
            //Exrate = Convert.ToDouble(data);
        }
    }
}
