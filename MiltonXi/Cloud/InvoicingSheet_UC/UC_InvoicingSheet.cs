﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;

namespace MiltonXi.InvoicingSheet_UC
{
    public partial class UC_InvoicingSheet : Forms.frm_Master
    {
        
        public UC_InvoicingSheet()
        {
            InitializeComponent();
            Entities ctx = new Entities();
            lst = ctx.InvoicingSheets.ToList() ;
            gridControl1.DataSource = lst;
        }
        List<InvoicingSheet> lst = new List<InvoicingSheet>();
        public override void GetData()
        {
            base.GetData();
            List<string> children = Utils.Utilities.FromByteArray<List<String>>(lst[11].ChildInvoices);
        }
    }
}
