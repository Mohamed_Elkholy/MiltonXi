﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Utils
{
    class FireStore
    {
        public static string projectID;
        private static FirestoreDb db = null;
        public static string bucketName { get { return projectID + ".appspot.com"; } }
        public static readonly string Drivers = "Drivers";
        public static readonly string Trucks = "Trucks";
        public static readonly string DispatchForms = "DispatchForms";
        public static readonly string DispatchFormsStatus = "DispatchFormsStatus";
        public static readonly string Trailers = "Trailers";
        public static readonly string MultiStopRountPlan = "MultiStopRountPlans";
        public static readonly string Users = "Users";
        public static readonly string SystemUsers = "SystemUser";
        public static readonly string TransportOrderInvoice = "TransportOrderInvoice";
        public static readonly string Expense = "Expenses";
        public static readonly string Currencies = "Currencies";
        public static readonly string Countries = "Countries";
        public static readonly string ExpenseType = "ExpenseType";
        public static readonly string DispatchFromCounter = "DispatchFromCounter";
        public static FirestoreDb getFireStoreDB()
        {
            try
            {
                if (db == null)
                {
                    db = FirestoreDb.Create(projectID);
                    return db;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException().Message);
                throw;
            }
            
            return db;
        }
    }
}
