﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Utils
{
    class Utilities
    {
        //Date Time Helpers
        public static DateTime setToLocalTime(DateTimeOffset DT)
        {
            return TimeZone.CurrentTimeZone.ToLocalTime(DT.DateTime);

        }
        public static DateTime? setToLocalTime(DateTimeOffset? DT)
        {
            if (DT == null)
                return null;
            else if (DT.Value.Date.Year == 1)
                return null;
            else
                return TimeZone.CurrentTimeZone.ToLocalTime(DT.Value.DateTime);

        }
        public static DateTimeOffset ConvertDateTimeToOffset(DateTime dt)
        {
            dt = DateTime.SpecifyKind(dt, DateTimeKind.Local);
            DateTimeOffset utcTime2 = dt;
            return utcTime2;
        }
        public static DateTimeOffset? ConvertDateTimeToOffset(DateTime? dt)
        {
            if (dt == null)
            {
                return null;
            }
            dt = DateTime.SpecifyKind(dt.Value.Date, DateTimeKind.Local);
            DateTimeOffset utcTime2 = dt.Value.Date;
            return utcTime2;
        }
        public static DateTime? convertToDateTimeNullable(object d)
        {
            DateTime result;
            if (DateTime.TryParse((d ?? "").ToString(), out result))
            {
                return result;
            }
            return null;
        }

        //Converters
        public static T FromByteArray<T>(byte[] data)
        {
            if (data == null)
            {
                return default(T);
            }

            using (MemoryStream stream = new MemoryStream(data))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (T)formatter.Deserialize(stream);
            }

        }
        public static byte[] ToByteArray<T>(object data)
        {
            if (data == null)
            {
                return null;
            }
            using (MemoryStream mStream = new MemoryStream())
            {
                BinaryFormatter binFormatter = new BinaryFormatter();
                binFormatter.Serialize(mStream, data);
                return mStream.ToArray();
            }
        }
        public static DbGeography CreatePoint(double lat, double lon)
        {
            string wkt = String.Format("POINT({0} {1})", lon, lat);
            return DbGeography.FromText(wkt);
        }
        public static int ToInt(object var)
        {
            var value = (var == null) ? "":var;
            int result = 0;
            if (String.IsNullOrWhiteSpace(value.ToString()))
            {
                return result;
            }
            else
            {
                int.TryParse(value.ToString(), out result);
            }
            return result;
        }
    }
}
