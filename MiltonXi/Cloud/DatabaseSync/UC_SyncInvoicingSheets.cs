﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Forms;
using MiltonXi.DataModel.FireStoreDataModel;
using MiltonXi.Utils;
using Google.Cloud.Firestore;

namespace MiltonXi.Forms
{
    public partial class UC_SyncInvoicingSheets : frm_Master
    {
       
        public UC_SyncInvoicingSheets()
        {
            InitializeComponent();
           
            btn_New.Enabled = false;
            dateEdit1.EditValue = DateTime.Now;
            dateEdit2.EditValue = DateTime.Now;
        }
        private List<InvoicingSheetsModel> modelList = new List<InvoicingSheetsModel>();
        private async void LoadDataAsync()
        {
            modelList = new List<InvoicingSheetsModel>();
            CollectionReference col = FireStore.getFireStoreDB().Collection(FireStore.TransportOrderInvoice);
            DateTime dateForm = dateEdit1.DateTime;
            var firstDayOfMonth = new DateTime(dateForm.Year, dateForm.Month, 1, 0, 0, 0);

            DateTime dateTo = dateEdit2.DateTime;
            int lastDay = DateTime.DaysInMonth(dateTo.Year, dateTo.Month);
            var lastDayOfMonth = new DateTime(dateForm.Year, dateForm.Month, lastDay, 23, 59, 59);
            Query query = col.WhereGreaterThanOrEqualTo("CreatedDate", Utilities.ConvertDateTimeToOffset(dateForm.AddHours(0).AddMinutes(0)))
                .WhereLessThanOrEqualTo("CreatedDate", Utilities.ConvertDateTimeToOffset(dateTo).AddHours(23).AddMinutes(59));

            QuerySnapshot documentSnapshots = await query.GetSnapshotAsync();
            foreach (var item in documentSnapshots)
            {

                modelList.Add(item.ConvertTo<InvoicingSheetsModel>());
            }
            gridControl1.DataSource = null;
            gridControl1.DataSource = modelList;
        }
        private void SaveToSQL()
        {
            Forms.DataSync.ToSQL(modelList);
        }


        public override void GetData()
        {
            base.GetData();
            LoadDataAsync();
        }

        public override void Save()
        {
            base.Save();
            if (DialogResult.Yes == XtraMessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                SaveToSQL();
            }
        }

        private void gridView1_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            gridView1.IndicatorWidth = 30;
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }
    }
}
