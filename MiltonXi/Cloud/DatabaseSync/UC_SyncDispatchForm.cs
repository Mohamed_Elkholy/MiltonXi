﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.DataModel.FireStoreDataModel;
using Google.Cloud.Firestore;
using MiltonXi.Utils;
using MiltonXi.Forms;

namespace MiltonXi.Forms
{
    public partial class UC_SyncDispatchForm : frm_Master
    {
        public UC_SyncDispatchForm()
        {
            InitializeComponent();

            btn_New.Enabled = false;
            dateEdit1.EditValue = DateTime.Now;
            dateEdit2.EditValue = DateTime.Now;
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                FieldName = "TransportOrderNumber",
                ColumnEditName = "TransportOrderNumber",
                Caption = "Transport Order Number",
                VisibleIndex = 0,
            });
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                FieldName = "MultiStopPlanReference",
                Caption = "Multi Stop Reference",
                VisibleIndex = 1,
            });
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                FieldName = "TruckName",
                Caption = "Truck Name",
                VisibleIndex = 2,
            });
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                FieldName = "CreatedDate",
                Caption = "Date Created",
                VisibleIndex = 3,
            });
            gridView1.OptionsSelection.MultiSelect = true;
            gridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 30;
            gridView1.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            gridView1.OptionsView.ColumnAutoWidth = true;
            gridView1.IndicatorWidth = 50;
        }

        private List<DispatchFormModel> dispatchFormsList = new List<DispatchFormModel>();

        private async void LoadDataAsync()
        {
            dispatchFormsList = new List<DispatchFormModel>();
            CollectionReference col = FireStore.getFireStoreDB().Collection(FireStore.DispatchForms);
            DateTime dateForm = dateEdit1.DateTime;
            var firstDayOfMonth = new DateTime(dateForm.Year, dateForm.Month, 1, 0, 0, 0);

            DateTime dateTo = dateEdit2.DateTime;
            int lastDay = DateTime.DaysInMonth(dateTo.Year, dateTo.Month);
            var lastDayOfMonth = new DateTime(dateForm.Year, dateForm.Month, lastDay, 23, 59, 59);
            Query query = col.WhereGreaterThanOrEqualTo("CreatedDate", Utilities.ConvertDateTimeToOffset(dateForm.AddHours(0).AddMinutes(0)))
                .WhereLessThanOrEqualTo("CreatedDate", Utilities.ConvertDateTimeToOffset(dateTo).AddHours(23).AddMinutes(59));

            QuerySnapshot documentSnapshots = await query.GetSnapshotAsync();
            foreach (var item in documentSnapshots)
            {
                if (item.ConvertTo<DispatchFormModel>().TransportOrderNumber.Contains("TS"))
                {
                    continue;
                }
                dispatchFormsList.Add(item.ConvertTo<DispatchFormModel>());
            }
            gridControl1.DataSource = null;
            gridControl1.DataSource = dispatchFormsList;
        }
        private async void SaveToSQL()
        {
            Forms.DataSync.ToSQL(dispatchFormsList);
        }


        public override void GetData()
        {
            base.GetData();
            LoadDataAsync();
        }
        
        public override void Save()
        {
            base.Save();
            if (DialogResult.Yes == XtraMessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                SaveToSQL();
            }
        }

        private void gridView1_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            gridView1.IndicatorWidth = 30;
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void UC_SyncDispatchForm_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.KeyCode);
        }

        private void UC_SyncDispatchForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Console.WriteLine(e.KeyCode);
        }
       
    }
}