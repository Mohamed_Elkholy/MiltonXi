﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Spatial;
using System.Linq;
using MiltonXi._DAL;
using MiltonXi.DataModel.FireStoreDataModel;
using MiltonXi.Utils;

namespace MiltonXi.Forms
{
    class DataSync
    {
        public static void ToSQL(List<DispatchFormModel> model)
        {
            try
            {
                using (Entities ctx = new Entities())
                {

                    foreach (var item in model)
                    {
                        var Entity = (from s in ctx.DispatchForms.Where(d => d.TransportOrderNumber == item.TransportOrderNumber) select s).FirstOrDefault() ?? new DispatchForm();

                        Entity.TransportOrderNumber = item.TransportOrderNumber;
                        Entity.ExternalReference = item.ExternalReference;
                        var truck = ctx.Trucks.Where(x => x.FirebaseDocumentId == item.TruckID).FirstOrDefault() ?? new Truck();
                        if (truck == null || truck.Id == 0)
                        {
                            Console.WriteLine("something wrong");
                            truck = new Truck();
                            truck.Id = 0;
                            truck.TruckName = item.TruckID;
                        }
                        Entity.TruckID = truck.Id;
                        Entity.TruckName = truck.TruckName;
                        var trailer = ctx.Trailers.Where(x => x.TrailerFullName == item.TrailerName || x.TrailerName == item.TrailerName || item.TrailerName.Contains(x.TrailerName)).FirstOrDefault() ?? new Trailer();
                        if (trailer == null || trailer.Id == 0)
                        {
                            Console.WriteLine("someting wrong");
                            trailer = new Trailer();
                            trailer.Id = 0;
                            trailer.TrailerName = item.TrailerName;
                        }

                        Entity.TrailerID = trailer.Id;
                        Entity.TrailerName = trailer.TrailerName;
                        var driver = ctx.Drivers.Where(x => x.FirebaseDocumentId == item.DriverID).FirstOrDefault() ?? new Driver();
                        if (driver == null || driver.Id == 0)
                        {
                            Console.WriteLine("somethingwrong");
                            driver = new Driver();
                            driver.Id = 0;
                            driver.DriverName = item.DriverID;
                        }
                        Entity.DriverID = driver.Id;
                        Entity.DriverName = driver.DriverName;
                        Entity.CurrentStatus = item.CurrentStatus;
                        Entity.LoadingAddress = item.LoadingAddress;
                        Entity.LoadingDateTimeFrom = Utilities.setToLocalTime(item.LoadingDateTimeFrom);
                        Entity.LoadingDateTimeTo = Utilities.setToLocalTime(item.LoadingDateTimeTo);
                        Entity.LoadingReferenceNumber = item.LoadingReferenceNumber;
                        Entity.UnloadingAddress = item.UnloadingAddress;
                        Entity.UnloadingDateTimeFrom = Utilities.setToLocalTime(item.UnloadingDateTimeFrom);
                        Entity.UnloadingDateTimeTo = Utilities.setToLocalTime(item.UnloadingDateTimeTo);
                        Entity.UnloadingReferenceNumber = item.UnloadingReferenceNumber;
                        Entity.NumberOfLoads = item.NumberOfLoads;
                        Entity.TotalWeight = item.TotalWeight;
                        Entity.LoadDetails = item.LoadDetails;
                        Entity.RouteInformation = item.RouteInformation;
                        Entity.CustomsClearance = item.CustomsClearance;
                        Entity.TollRoads = item.TollRoads;
                        Entity.VignettesNeeded = item.VignettesNeeded;
                        Entity.FerryInformation = item.FerryInformation;
                        Entity.ContactInformation = item.ContactInformation;
                        Entity.MultiStopPlanReference = item.MultiStopPlanReference;
                        Entity.Notes = item.Notes;
                        Entity.LoadedDateTime = Utilities.setToLocalTime(item.LoadedDateTime);
                        Entity.UnloadedDateTime = Utilities.setToLocalTime(item.UnloadedDateTime);
                        Entity.DriverLastReadDateTime = Utilities.setToLocalTime(item.DriverLastReadDateTime);
                        Entity.ADRCategory21 = item.ADRCategory21;
                        Entity.ADRCategory22 = item.ADRCategory22;
                        Entity.ADRCategory23 = item.ADRCategory23;
                        Entity.ADRCategory3 = item.ADRCategory3;
                        Entity.ADRCategory41 = item.ADRCategory41;
                        Entity.ADRCategory42 = item.ADRCategory42;
                        Entity.ADRCategory43 = item.ADRCategory43;
                        Entity.ADRCategory51 = item.ADRCategory51;
                        Entity.ADRCategory52 = item.ADRCategory52;
                        Entity.ADRCategory61 = item.ADRCategory61;
                        Entity.ADRCategory62 = item.ADRCategory62;
                        Entity.ADRCategory8 = item.ADRCategory8;
                        Entity.ADRCategory9 = item.ADRCategory9;
                        Entity.NotifyDriver = item.NotifyDriver;
                        Entity.Loaded = item.Loaded;
                        Entity.Unloaded = item.Unloaded;
                        Entity.CreatedBy = 1;
                        Entity.ModifiedBy = 1;
                        Entity.DateCreated = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.DateModified = Utilities.setToLocalTime(item.LastUpdateDate);
                        Entity.FirebaseDocumentId = item.DocumentID;
                        Entity.FirebaseDocumentRef = item.Reference.Path;
                        Entity.Archived = item.Archived;
                        if (item.LoadedImageURL != null)
                        {
                            foreach (var loadedimg in item.LoadedImageURL)
                            {
                                var li = (from s in ctx.Documents.Where(d => d.FileCloudName == loadedimg.Key.ToString()) select s).FirstOrDefault() ?? new Document();
                                li.TransportOrderNumber = item.TransportOrderNumber;
                                li.FileCloudName = loadedimg.Key.ToString();
                                li.FileCloudURL = loadedimg.Value.ToString();
                                li.Type = "Load";
                                li.TypeId = 1;
                                li.FirebaseDocumentId = item.DocumentID;
                                li.FirebaseDocumentRef = item.Reference.Path;
                                Entity.Documents.Add(li);
                                
                            }
                        }


                        if (item.UnloadedImageURL != null)
                        {
                            foreach (var loadedimg in item.UnloadedImageURL)
                            {
                                var li = (from s in ctx.Documents.Where(d => d.FileCloudName == loadedimg.Key.ToString()) select s).FirstOrDefault() ?? new Document();
                                //   li.DispatchFormId = Entity.Id;
                                li.TransportOrderNumber = item.TransportOrderNumber;
                                li.FileCloudName = loadedimg.Key.ToString();
                                li.FileCloudURL = loadedimg.Value.ToString();
                                li.Type = "Unload";
                                li.TypeId = 2;
                                li.FirebaseDocumentId = item.DocumentID;
                                li.FirebaseDocumentRef = item.Reference.Path;
                                Entity.Documents.Add(li);
                                // ctx.Documents.AddOrUpdate(li);
                            }
                        }
                        
                        ctx.DispatchForms.AddOrUpdate(Entity);

                    }

                    int count = ctx.SaveChanges();
                    Console.WriteLine("Dispatch Form Data Saved! " + count);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException().Message);
                Console.WriteLine(ex.GetBaseException().Message);
            }
        }
        public static void ToSQL(List<DriverModel> driversList)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    foreach (var item in driversList)
                    {
                        var Entity = (from s in ctx.Drivers.Where(d => d.DriverName == item.DriverName) select s).FirstOrDefault() ?? new Driver();
                        Entity.DriverName = item.DriverName;
                        Entity.PassportNumber = item.PassportNumber;
                        Entity.PassportExpiryDate = Utilities.setToLocalTime(item.PassportExpiryDate);
                        Entity.DrivingLicenseNumber = item.DrivingLicenseNumber;
                        Entity.DrivingLicenseExpiryDate = Utilities.setToLocalTime(item.DrivingLicenseExpiryDate);
                        Entity.TachoCardNumber = item.TachoCardNumber;
                        Entity.TachoCardExpiryDate = Utilities.setToLocalTime(item.TachoCardExpiryDate);
                        Entity.PhoneNumber = item.PhoneNumber;
                        Entity.ADRCertificateNumber = item.ADRCertificateNumber;
                        Entity.ADRCertificateExpiryDate = Utilities.setToLocalTime(item.ADRCertificateExpiryDate);
                        Entity.CPCCardNumber = item.CPCCardNumber;
                        Entity.CPCCardExpiryDate = Utilities.setToLocalTime(item.CPCCardExpiryDate);
                        Dictionary<string, DateTime> localDelegation = new Dictionary<string, DateTime>();
                        foreach (var v in item.DriverDelegations)
                        {
                            localDelegation.Add(v.Key, Utilities.setToLocalTime(v.Value));
                        }
                        Entity.DriverDelegations = Utilities.ToByteArray<Dictionary<string, DateTime>>(localDelegation);
                        Entity.DrivingLicenseCategoryB = item.DrivingLicenseCategoryB;
                        Entity.DrivingLicenseCategoryBE = item.DrivingLicenseCategoryBE;
                        Entity.DrivingLicenseCategoryC = item.DrivingLicenseCategoryC;
                        Entity.CreatedBy = 1;
                        Entity.ModifiedBy = 1;
                        Entity.DateCreated = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.DateModified = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.Archived = item.Archived;
                        Entity.FirebaseDocumentId = item.DocumentID;
                        Entity.FirebaseDocumentRef = item.Reference.Path;
                        ctx.Drivers.AddOrUpdate(Entity);
                    }
                    int count = ctx.SaveChanges();
                    Console.WriteLine("Driver Data Saved! " + count);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException().Message);
            }

        }
        public static void ToSQL(List<TrailerModel> trailerList)
        {
            try
            {
                using (Entities ctx = new Entities())
                {

                    foreach (var item in trailerList)
                    {
                        var Entity = (from s in ctx.Trailers.Where(d => d.TrailerFullName == item.TrailerFullName) select s).DefaultIfEmpty().FirstOrDefault() ?? new Trailer();

                        Entity.TrailerName = item.TrailerName;
                        Entity.TrailerRegistrationNumber = item.TrailerRegistrationNumber;
                        Entity.TrailerFullName = item.TrailerFullName;
                        Entity.CargoHeight = item.CargoHeight;
                        Entity.CargoWidth = item.CargoWidth;
                        Entity.CargoLength = item.CargoLength;
                        Entity.GrossWeight = item.GrossWeight;
                        Entity.LoadingWeight = item.LoadingWeight;
                        Entity.TrailerTechnicalInspectionExpiryDate = Utilities.setToLocalTime(item.TrailerTechnicalInspectionExpiryDate);
                        Entity.CreatedBy = 1;
                        Entity.ModifiedBy = 1;
                        Entity.DateCreated = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.DateModified = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.Archived = item.Archived;
                        Entity.FirebaseDocumentId = item.DocumentID;
                        Entity.FirebaseDocumentRef = item.Reference.Path;
                        ctx.Trailers.AddOrUpdate(Entity);

                    }
                    int count = ctx.SaveChanges();
                    Console.WriteLine("Trailer Data Saved! " + count);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException().Message);
            }

        }
        public static void ToSQL(List<TruckModel> truckList)
        {
            try
            {
                using (Entities ctx = new Entities())
                {

                    foreach (var item in truckList)
                    {
                        var Entity = (from s in ctx.Trucks.Where(d => d.TruckFullName == item.TruckFullName) select s).DefaultIfEmpty().FirstOrDefault() ?? new Truck();
                        Entity.TruckName = item.TruckName;
                        Entity.TruckFullName = item.TruckFullName;
                        Entity.TruckRegistrationNumber = item.TruckRegistrationNumber;
                        Entity.EuroCateogry = item.EuroCateogry;
                        Dictionary<string, DateTime> localVignettes = new Dictionary<string, DateTime>();
                        foreach (var v in item.Vignettes)
                        {
                            localVignettes.Add(v.Key, Utilities.setToLocalTime(v.Value));
                        }
                        Entity.Vignettes = Utilities.ToByteArray<Dictionary<string, DateTime>>(localVignettes);
                        Entity.PhoneNumber = item.PhoneNumber;
                        Entity.CargoHeight = item.CargoHeight;
                        Entity.CargoWidth = item.CargoWidth;
                        Entity.CargoLength = item.CargoLength;
                        Entity.AustriaLSticker = item.AustriaLSticker;
                        Entity.GrossWeight = item.GrossWeight;
                        Entity.LoadingWeight = item.LoadingWeight;
                        Entity.TechnicalInspectionExpiryDate = Utilities.setToLocalTime(item.TruckTechnicalInspectionExpiryDate);
                        Entity.MaximumTrailerGrossWeight = item.MaximumTrailerGrossWeight;
                        Entity.LondonLowEmissionZone = item.LondonLowEmissionZone;
                        Entity.FranceCritAir = item.FranceCritAir;
                        Entity.ViennaLowEmissionZone = item.ViennaLowEmissionZone;
                        Entity.TrailerName = item.TrailerFullName;
                        Entity.TrailerID = ctx.Trailers.Where(x => x.TrailerFullName == item.TrailerFullName).FirstOrDefault().Id;
                        Entity.DriverName = item.DriverName;
                        Entity.DriverID = ctx.Drivers.Where(x => x.DriverName == item.DriverName).FirstOrDefault().Id;
                        Entity.CreatedBy = 1;
                        Entity.ModifiedBy = 1;
                        Entity.DateCreated = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.DateModified = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.FirebaseDocumentId = item.DocumentID;
                        Entity.FirebaseDocumentRef = item.Reference.Path;
                        Entity.Archived = item.Archived;
                        ctx.Trucks.AddOrUpdate(Entity);
                    }
                    int count = ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException().Message);
            }

        }
        public static void ToSQL(List<ExpenseModel> expensesList)
        {
            int i = 0;
            try
            {

                Entities ctx = new Entities();


                foreach (var item in expensesList)
                {
                    i++;
                    var Entity = (from s in ctx.TruckExpenses.Where(d => d.FirebaseDocumentId == item.DocumentId) select s).DefaultIfEmpty().FirstOrDefault() ?? new TruckExpens();
                    Entity.FirebaseDocumentId = item.DocumentId;
                    Entity.FirebaseDocumentRef = item.Reference.Path;
                    Entity.Type = item.expenseType;
                    Entity.DateCreated = Utilities.setToLocalTime(item.dateTime);
                    Entity.UserInputDate = Utilities.setToLocalTime(item.userInputDate);
                    Entity.Location = item.location;
                    Entity.LocationGeo = item.locationGeo != null ? Utilities.CreatePoint(item.locationGeo.Value.Latitude, item.locationGeo.Value.Longitude) : null;
                    Entity.AmountOfLiters = item.amountOfLiters;
                    Entity.TotalCost = item.totalCost;
                    Entity.Currency = item.currency.Trim();
                    Entity.ReceiptNumber = item.receiptNumber;
                    Entity.PaymentMethod = item.paymentMethod;
                    Entity.Description = item.expenseDescription;
                    Entity.CountryOfToll = item.countryOfToll;
                    Entity.TruckName = item.truckName;
                    Entity.TruckId = ctx.Trucks.Where(x => x.TruckFullName.Contains(item.truckName)).Select(x => x.Id).FirstOrDefault();
                    Entity.Comment = item.comment;
                    double odometer;
                    Double.TryParse(item.odometer, out odometer);
                    Entity.Odometer = odometer;
                    double distance;
                    Double.TryParse(item.distanceTravelled, out distance);
                    Entity.DistanceTravelled = distance;
                    double consumption;
                    Double.TryParse(item.consumption, out consumption);
                    Entity.Consumption = consumption;
                    Entity.GUID = item.guid;
                    ctx.TruckExpenses.AddOrUpdate(Entity);
                }
                int count = ctx.SaveChanges();


            }
            catch (Exception ex)
            {
                Console.WriteLine("count " + i);
                Console.WriteLine(ex.GetBaseException().Message);
            }
        }
        public static void ToSQL(List<InvoicingSheetsModel> InvoicingList)
        {
            int i = 0;
            try
            {

                using (Entities ctx = new Entities())
                {
                    foreach (var item in InvoicingList)
                    {
                        i++;
                        if (i == 180)
                        {
                            Console.WriteLine("here");
                        }
                        if (item.TransportOrderNumber == @"11\9\2020\FM")
                        {
                            Console.WriteLine("here");
                        }
                        if (item.DateTimeCompleted == null || item.DateTimeCompleted == new DateTimeOffset(0001, 01, 01, 12, 0, 0, new TimeSpan()))
                        {
                            Console.WriteLine("here");
                        }
                        var Entity = (from s in ctx.InvoicingSheets.Where(d => d.FirebaseDocumentId == item.DocumentID) select s).DefaultIfEmpty().FirstOrDefault() ?? new InvoicingSheet();
                        Entity.TruckFireStoreRef = item.TruckID;
                        Truck TID = (from t in ctx.Trucks.Where(tr => tr.TruckFullName.Contains(item.TruckName)) select t).FirstOrDefault();
                        Entity.TruckId = ((TID != null) ? TID.Id : 0);
                        Entity.TruckName = ((TID != null) ? TID.TruckName : item.TruckName);
                        Entity.TransportOrderNumber = item.TransportOrderNumber;
                        Entity.ExternalReference = item.ExternalReference;
                        Entity.DateCreated = Utilities.setToLocalTime(item.CreatedDate);
                        Entity.DepartureLocation = item.DepartureLocation;
                        Entity.ArrivalLocation = item.ArrivalLocation;
                        Entity.DistanceKM = item.DistanceKM;
                        Entity.BuyerName = item.Buyer;
                        Entity.BuyerAddress = item.BuyerAddress;
                        Entity.TotalOrderWorthNET = item.TotalOrderWorthNET;
                        Entity.TotalOrderWorthGROSS = item.TotalOrderWorthGROSS;
                        Entity.RatePerKM = item.RatePerKM;
                        Entity.Currency = item.Currency;
                        Entity.DateTimeCompleted = Utilities.setToLocalTime(item.DateTimeCompleted);
                        Entity.DateTimeCMRSent = Utilities.setToLocalTime(item.DateTimeCMRSent);
                        Entity.InvoiceNumber = item.InvoiceNumber;
                        Entity.InvoiceIssuedDate = Utilities.setToLocalTime(item.InvoiceIssuedDate);
                        Entity.InvoiceLink = item.InvoiceLink;
                        Entity.PaymentDeadline = item.PaymentDeadline;
                        Entity.DatePaymentReceived = Utilities.setToLocalTime(item.DatePaymentRecieved);
                        Entity.FreightExchange = item.FrieghtExchange;
                        Entity.Notes = item.Notes;
                        Entity.Currency = item.Currency;
                        Entity.CurrencyId = null;
                        Entity.VATPercentage = item.VATPercentage;
                        Entity.NIPEUVATNumber = item.NIPEUVATNumber;
                        Entity.DeadLine = item.DeadLine;
                        Entity.DepartureLocation = item.DepartureLocation;
                        Entity.ArrivalLocation = item.ArrivalLocation;
                        Entity.DistanceKM = item.DistanceKM;
                        Entity.TotalOrderWorthNET = item.TotalOrderWorthNET;
                        Entity.TotalOrderWorthGROSS = item.TotalOrderWorthGROSS;
                        Entity.RatePerKM = item.RatePerKM;
                        Entity.DateTimeCompleted = Utilities.setToLocalTime(item.DateTimeCompleted);
                        Entity.DateTimeCMRSent = Utilities.setToLocalTime(item.DateTimeCMRSent);
                        Entity.InvoiceNumber = item.InvoiceNumber;
                        Entity.InvoiceIssuedDate = Utilities.setToLocalTime(item.InvoiceIssuedDate);
                        Entity.InvoiceLink = item.InvoiceLink;
                        Entity.PaymentDeadline = item.PaymentDeadline;
                        Entity.PaymentDeadlineDate = Utilities.setToLocalTime(item.PaymentDeadlineDate);
                        Entity.DatePaymentReceived = Utilities.setToLocalTime(item.DatePaymentRecieved);
                        Entity.Notes = item.Notes;
                        Entity.ParentInvoice = item.ParentInvoice;
                        Entity.ChildInvoices = ((item.ChildInvoice??new List<string>()).Count() > 0) ? Utilities.ToByteArray<List<string>>(item.ChildInvoice) : null;
                        Entity.FactoringAgent = item.FactoringAgent;
                        Entity.FirebaseDocumentId = item.DocumentID;
                        Entity.FirebaseDocumentRef = item.Reference.Path;
                        Entity.Archived = item.Archived;
                        ctx.InvoicingSheets.AddOrUpdate(Entity);
                    }
                    int count = ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("count " + i);
                Console.WriteLine(ex.GetBaseException().Message);
            }
        }
    }
}
