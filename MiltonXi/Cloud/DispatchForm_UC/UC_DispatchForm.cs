﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using MiltonXi._DAL;
using MiltonXi.Forms;
using MiltonXi.Utils;

namespace MiltonXi.Forms
{
    public partial class UC_DispatchForm : frm_Master, IUC_DispatchForm
    {
         private static readonly log4net.ILog log =  log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Variables
        public DateTime? CreatedDate { get; set; }
        public bool? Archived { get { return switch_Archived.IsOn; } set { switch_Archived.IsOn = value.Value; } }
        public string ContactInformation { get { return txt_ContactInformation.Text; } set { txt_ContactInformation.Text = value; } }
        public string TrailerName { get { return cmbox_Trailer.Text; } set { cmbox_Trailer.Text = value; } }
        public int? TrailerId { get; private set; }
        public int? DriverID { get { return Utilities.ToInt(cmbox_Driver.EditValue); } set { cmbox_Driver.EditValue = value; } }
        public string ExternalReference { get { return txt_ExternalReference.Text; } set { txt_ExternalReference.Text = value; } }
        public string FerryInformation { get { return txt_FerryInformation.Text; } set { txt_FerryInformation.Text = value; } }
        public string LandingAddress { get { return txt_LoandingAddress.Text; } set { txt_LoandingAddress.Text = value; } }
        public DateTime? LoadingDateTimeFrom { get { return dtp_LoadingDateTimeFrom.DateTime; } set { dtp_LoadingDateTimeFrom.DateTime = value.Value; } }
        public DateTime? LoadingDateTimeTo { get { return dtp_LoadingDateTimeTo.DateTime; } set { dtp_LoadingDateTimeTo.DateTime = value.Value; } }
        public string LoadingReferenceNumber { get { return txt_LoadingReferenceNumber.Text; } set { txt_LoadingReferenceNumber.Text = value; } }
        public string MultiStopPlanReference { get { return txt_MultiStopPlanReference.Text; } set { txt_MultiStopPlanReference.Text = value; } }
        public int? NumberOfLoads { get { return returnInt(txt_TotalWeight.Text); } set { txt_TotalWeight.Text = value.ToString(); } }
        public int? TotalWeight { get { return returnInt(txt_NumberOfLoads.Text); } set { txt_NumberOfLoads.Text = value.ToString(); } }
        public bool? TollRoads { get { return checkBoxTollRoads.Checked; } set { checkBoxTollRoads.Checked = value.Value; } }
        public string RouteInformation { get { return txt_RouteInformation.Text; } set { txt_RouteInformation.Text = value; } }
        public string TransportOrderNumber { get { return txt_TransportOrderNumber.Text; } set { txt_TransportOrderNumber.Text = value; } }
        public int? TruckID { get { return Utilities.ToInt(cmbox_Truck.EditValue); } set { cmbox_Truck.EditValue = value; } }
        public string TruckName { get { return cmbox_Truck.Properties.GetDisplayValueByKeyValue(cmbox_Truck.EditValue).ToString(); } set { cmbox_Truck.Text = value; } }
        public string UnloadingAddress { get { return txt_UnloadingAddress.Text; } set { txt_UnloadingAddress.Text = value; } }
        public DateTime? UnloadingDateTimeFrom { get { return dtp_UnloadingDateTimeFrom.DateTime; } set { dtp_UnloadingDateTimeFrom.DateTime = value.Value; } }
        public DateTime? UnloadingDateTimeTo { get { return dtp_UnloadingDateTimeTo.DateTime; } set { dtp_UnloadingDateTimeTo.DateTime = value.Value; } }
        public string UnloadingReferenceNumber { get { return txt_UnloadingReferenceNumber.Text; } set { txt_UnloadingReferenceNumber.Text = value; } }
        public bool? VignettesNeeded { get { return checkBoxVignettesNeeded.Checked; } set { checkBoxVignettesNeeded.Checked = value.Value; } }
        public string LoadDetails { get { return txt_LoadDetails.Text; } set { txt_LoadDetails.Text = value; } }
        public bool? ADRCategory21 { get { return checkBoxADRCategory21.Checked; } set { checkBoxADRCategory21.Checked = value.Value; } }
        public bool? ADRCategory22 { get { return checkBoxADRCategory22.Checked; } set { checkBoxADRCategory22.Checked = value.Value; } }
        public bool? ADRCategory23 { get { return checkBoxADRCategory23.Checked; } set { checkBoxADRCategory23.Checked = value.Value; } }
        public bool? ADRCategory3 { get { return checkBoxADRCategory3.Checked; } set { checkBoxADRCategory3.Checked = value.Value; } }
        public bool? ADRCategory41 { get { return checkBoxADRCategory41.Checked; } set { checkBoxADRCategory41.Checked = value.Value; } }
        public bool? ADRCategory42 { get { return checkBoxADRCategory42.Checked; } set { checkBoxADRCategory42.Checked = value.Value; } }
        public bool? ADRCategory43 { get { return checkBoxADRCategory43.Checked; } set { checkBoxADRCategory43.Checked = value.Value; } }
        public bool? ADRCategory51 { get { return checkBoxADRCategory51.Checked; } set { checkBoxADRCategory51.Checked = value.Value; } }
        public bool? ADRCategory52 { get { return checkBoxADRCategory52.Checked; } set { checkBoxADRCategory52.Checked = value.Value; } }
        public bool? ADRCategory61 { get { return checkBoxADRCategory61.Checked; } set { checkBoxADRCategory61.Checked = value.Value; } }
        public bool? ADRCategory62 { get { return checkBoxADRCategory62.Checked; } set { checkBoxADRCategory62.Checked = value.Value; } }
        public bool? ADRCategory8 { get { return checkBoxADRCategory8.Checked; } set { checkBoxADRCategory8.Checked = value.Value; } }
        public bool? ADRCategory9 { get { return checkBoxADRCategory9.Checked; } set { checkBoxADRCategory9.Checked = value.Value; } }
        public string Notes { get { return richTextBoxNotes.Text; } set { richTextBoxNotes.Text = value; } }
        public string DriverLastReadDateTime { get { return lbl_DriverLastReadDateTime.Text; } set { lbl_DriverLastReadDateTime.Text = lbl_DriverLastReadDateTime.Text + value; } }
        public int? CurrentStatus { get { return Convert.ToInt16(cmbox_Status.EditValue); } set { cmbox_Status.EditValue = value; } }
        public string CustomsClearance { get { return txt_CustomsClearance.Text; } set { txt_CustomsClearance.Text = value; } }

        DispatchForm _model;
        UC_DispatchFormPresenter presenter;

        #endregion 
      

        public UC_DispatchForm()
        {
            InitializeComponent();
            InitializePrivateComponents();
            _model = new DispatchForm();
            presenter = new UC_DispatchFormPresenter(_model,this);
        }
        public UC_DispatchForm(DispatchForm model)
        {
            InitializeComponent();
            InitializePrivateComponents();
            _model = model;
            presenter = new UC_DispatchFormPresenter(_model, this);
            GetValue(_model);
        }


        public override void Save()
        {
            base.Save();
            presenter.save();
        }

        public override void New()
        {
            base.New();
        }

        public override void Delete()
        {
            base.Delete();
        }


        public int returnInt(string text)
        {
            int value;
            int.TryParse(text, out value);
            return value;

        }

        public void SaveCompleted()
        {
            XtraMessageBox.Show("Data saved!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void NoChangesSaved()
        {
            XtraMessageBox.Show("No changes to be saved!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        

        private void repositoryItemPictureEdit1_PopupMenuShowing(object sender, DevExpress.XtraEditors.Events.PopupMenuShowingEventArgs e)
        {
            e.PopupMenu.Items["Cut"].Visible = false;
            e.PopupMenu.Items["Copy"].Visible = false;
            e.PopupMenu.Items["Paste"].Visible = false;
            e.PopupMenu.Items["Delete"].Visible = false;
            e.PopupMenu.Items["Load"].Visible = false;
            //((DevExpress.Utils.Menu.DXSubMenuItem)(e.PopupMenu.Items[8])).Items[4].Enabled = true;
            //((DevExpress.Utils.Menu.DXSubMenuItem)(e.PopupMenu.Items[8])).Items[4].Visible = true;
            //  repositoryItemPictureEdit1.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            try
            {
                var editresult = e.PopupMenu.Items.Where(x => x.Caption == "Edit").FirstOrDefault();
                if (editresult != null)
                {
                    editresult.Caption = "Open";
                }
            }
            catch (Exception)
            {


            }


            string parentname = (sender as PictureEdit).Parent.Name;
            DevExpress.XtraGrid.GridControl parentgird = (sender as PictureEdit).Parent as DevExpress.XtraGrid.GridControl;
            GridView parentgridview = parentgird.MainView as GridView;

            var result = e.PopupMenu.Items.Where(x => x.Caption == "Open in browser").FirstOrDefault();
            if (result == null)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("Open in browser");
                //item.Click += openinbrowser;
                SubscribeClick(item, parentname, parentgridview);
                e.PopupMenu.Items.Add(item);
            }
        }
        private void SubscribeClick(DevExpress.Utils.Menu.DXMenuItem button, string parentName, GridView parentgridview)
        {
            button.Click += new EventHandler(
                (sender, e) => OpenInBrowser(sender, e, parentName, parentgridview)
            );
        }
        public void OpenInBrowser(object sender, EventArgs e, string parentName, GridView parentgridview)
        {
            if (!string.IsNullOrWhiteSpace(parentgridview.GetFocusedRowCellValue("URL").ToString()))
            {
                System.Diagnostics.Process.Start(parentgridview.GetFocusedRowCellValue("URL").ToString());
            }
            else
            {
                XtraMessageBox.Show("URL is not available", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        void comboboxes()
        {
            cmbox_Truck.Properties.DataSource = new Entities().Trucks.Where(x => x.Archived != true).ToList();
            cmbox_Truck.Properties.DisplayMember = "TruckName";
            cmbox_Truck.Properties.ValueMember = "Id";
            cmbox_Truck.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id",2));
            cmbox_Truck.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TruckName", "Name"));
            cmbox_Truck.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TrailerName", "Trailer"));
            cmbox_Truck.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DriverName", "Driver"));

            cmbox_Driver.Properties.DataSource = new Entities().Drivers.Where(x=>x.Archived!=true).ToList();
            cmbox_Driver.Properties.DisplayMember = "DriverName";
            cmbox_Driver.Properties.ValueMember = "Id";
            cmbox_Driver.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 2));
            cmbox_Driver.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DriverName", "Name"));

            cmbox_Trailer.Properties.DataSource = new Entities().Trailers.Where(x => x.Archived != true).ToList();
            cmbox_Trailer.Properties.DisplayMember = "TrailerName";
            cmbox_Trailer.Properties.ValueMember = "Id";
            cmbox_Trailer.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 2));
            cmbox_Trailer.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TrailerName", "Name"));
        }
       public void GetValue(DispatchForm _model)
        {
           
            CreatedDate = _model.DateCreated;
            Archived = _model.Archived;
            ContactInformation = _model.ContactInformation;
            TrailerName = _model.TrailerName;
            TrailerId = _model.TrailerID;
            DriverID = _model.DriverID;
            ExternalReference = _model.ExternalReference;
            FerryInformation = _model.FerryInformation;
            LandingAddress = _model.LoadingAddress;
            LoadingDateTimeFrom = _model.LoadingDateTimeFrom;
            LoadingDateTimeTo = _model.LoadingDateTimeTo;
            LoadingReferenceNumber = _model.LoadingReferenceNumber;
            MultiStopPlanReference = _model.MultiStopPlanReference;
            NumberOfLoads = _model.NumberOfLoads;
            TotalWeight = _model.TotalWeight;
            TollRoads = _model.TollRoads;
            RouteInformation = _model.RouteInformation;
            TransportOrderNumber = _model.TransportOrderNumber;
            TruckID = _model.TruckID;
            TruckName = _model.TruckName;
            UnloadingAddress = _model.UnloadingAddress;
            UnloadingDateTimeFrom = _model.UnloadingDateTimeFrom;
            UnloadingDateTimeTo = _model.UnloadingDateTimeTo;
            UnloadingReferenceNumber = _model.UnloadingReferenceNumber;
            VignettesNeeded = _model.VignettesNeeded;
            LoadDetails = _model.LoadDetails;
            ADRCategory21 = _model.ADRCategory21;
            ADRCategory22 = _model.ADRCategory22;
            ADRCategory23 = _model.ADRCategory23;
            ADRCategory3 = _model.ADRCategory3;
            ADRCategory41 = _model.ADRCategory41;
            ADRCategory42 = _model.ADRCategory42;
            ADRCategory43 = _model.ADRCategory43;
            ADRCategory51 = _model.ADRCategory51;
            ADRCategory52 = _model.ADRCategory52;
            ADRCategory61 = _model.ADRCategory61;
            ADRCategory62 = _model.ADRCategory62;
            ADRCategory8 = _model.ADRCategory8;
            ADRCategory9 = _model.ADRCategory9;
            Notes = _model.Notes;
            DriverLastReadDateTime = _model.DriverLastReadDateTime.ToString();
            CurrentStatus = _model.CurrentStatus;
            CustomsClearance = _model.CustomsClearance;
        }


      public  void SetValue(DispatchForm _model)
        {
            _model.DateCreated = CreatedDate;
            _model.Archived = Archived;
            _model.ContactInformation = ContactInformation;
            _model.TrailerName = TrailerName;
            _model.TrailerID = TrailerId;
            _model.DriverID = DriverID;
            _model.ExternalReference = ExternalReference;
            _model.FerryInformation = FerryInformation;
            _model.LoadingAddress = LandingAddress;
            _model.LoadingDateTimeFrom = LoadingDateTimeFrom;
            _model.LoadingDateTimeTo = LoadingDateTimeTo;
            _model.LoadingReferenceNumber = LoadingReferenceNumber;
            _model.MultiStopPlanReference = MultiStopPlanReference;
            _model.NumberOfLoads = NumberOfLoads;
            _model.TotalWeight = TotalWeight;
            _model.TollRoads = TollRoads;
            _model.RouteInformation = RouteInformation;
            _model.TransportOrderNumber = TransportOrderNumber;
            _model.TruckID = TruckID;
            _model.TruckName = TruckName;
            _model.UnloadingAddress = UnloadingAddress;
            _model.UnloadingDateTimeFrom = UnloadingDateTimeFrom;
            _model.UnloadingDateTimeTo = UnloadingDateTimeTo;
            _model.UnloadingReferenceNumber = UnloadingReferenceNumber;
            _model.VignettesNeeded = VignettesNeeded;
            _model.LoadDetails = LoadDetails;
            _model.ADRCategory21 = ADRCategory21;
            _model.ADRCategory22 = ADRCategory22;
            _model.ADRCategory23 = ADRCategory23;
            _model.ADRCategory3 = ADRCategory3;
            _model.ADRCategory41 = ADRCategory41;
            _model.ADRCategory42 = ADRCategory42;
            _model.ADRCategory43 = ADRCategory43;
            _model.ADRCategory51 = ADRCategory51;
            _model.ADRCategory52 = ADRCategory52;
            _model.ADRCategory61 = ADRCategory61;
            _model.ADRCategory62 = ADRCategory62;
            _model.ADRCategory8 = ADRCategory8;
            _model.ADRCategory9 = ADRCategory9;
            _model.Notes = Notes;
            _model.CurrentStatus = CurrentStatus;
            _model.CustomsClearance = CustomsClearance;
            this._model = _model;

        }
        void InitializePrivateComponents()
        {
            comboboxes();
        }

       
    }

}
