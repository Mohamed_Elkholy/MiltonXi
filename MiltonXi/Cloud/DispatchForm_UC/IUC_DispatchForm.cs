﻿using System;
using DevExpress.XtraGrid.Views.Grid;
using MiltonXi._DAL;

namespace MiltonXi.Forms
{
    public interface IUC_DispatchForm
    {
        bool? ADRCategory21 { get; set; }
        bool? ADRCategory22 { get; set; }
        bool? ADRCategory23 { get; set; }
        bool? ADRCategory3 { get; set; }
        bool? ADRCategory41 { get; set; }
        bool? ADRCategory42 { get; set; }
        bool? ADRCategory43 { get; set; }
        bool? ADRCategory51 { get; set; }
        bool? ADRCategory52 { get; set; }
        bool? ADRCategory61 { get; set; }
        bool? ADRCategory62 { get; set; }
        bool? ADRCategory8 { get; set; }
        bool? ADRCategory9 { get; set; }
        bool? Archived { get; set; }
        string ContactInformation { get; set; }
        DateTime? CreatedDate { get; set; }
        int? CurrentStatus { get; set; }
        string CustomsClearance { get; set; }
        int? DriverID { get; set; }
        string DriverLastReadDateTime { get; set; }
        string ExternalReference { get; set; }
        string FerryInformation { get; set; }
        string LandingAddress { get; set; }
        string LoadDetails { get; set; }
        DateTime? LoadingDateTimeFrom { get; set; }
        DateTime? LoadingDateTimeTo { get; set; }
        string LoadingReferenceNumber { get; set; }
        string MultiStopPlanReference { get; set; }
        string Notes { get; set; }
        int? NumberOfLoads { get; set; }
        string RouteInformation { get; set; }
        bool? TollRoads { get; set; }
        int? TotalWeight { get; set; }
        int? TrailerId { get; }
        string TrailerName { get; set; }
        string TransportOrderNumber { get; set; }
        int? TruckID { get; set; }
        string TruckName { get; set; }
        string UnloadingAddress { get; set; }
        DateTime? UnloadingDateTimeFrom { get; set; }
        DateTime? UnloadingDateTimeTo { get; set; }
        string UnloadingReferenceNumber { get; set; }
        bool? VignettesNeeded { get; set; }
        void Delete();
        void New();
        void NoChangesSaved();
        void OpenInBrowser(object sender, EventArgs e, string parentName, GridView parentgridview);
        int returnInt(string text);
        void Save();
        void SaveCompleted();
        void SetValue(DispatchForm _model);
        void GetValue(DispatchForm _model);
    }
}