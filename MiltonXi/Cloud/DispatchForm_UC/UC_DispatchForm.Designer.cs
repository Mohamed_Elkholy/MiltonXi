﻿namespace MiltonXi.Forms
{
    partial class UC_DispatchForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.switch_Archived = new DevExpress.XtraEditors.ToggleSwitch();
            this.gridControlUnloadedImageURL = new DevExpress.XtraGrid.GridControl();
            this.gridViewUnloadedImageURL = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkBoxVignettesNeeded = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxTollRoads = new DevExpress.XtraEditors.CheckEdit();
            this.gridControlLoadedImageURL = new DevExpress.XtraGrid.GridControl();
            this.gridViewLoadedImageURL = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt_TransportOrderNumber = new DevExpress.XtraEditors.TextEdit();
            this.cmbox_Driver = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_ExternalReference = new DevExpress.XtraEditors.TextEdit();
            this.txt_RouteInformation = new DevExpress.XtraEditors.TextEdit();
            this.cmbox_Status = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_FerryInformation = new DevExpress.XtraEditors.TextEdit();
            this.txt_LoadDetails = new DevExpress.XtraEditors.TextEdit();
            this.richTextBoxNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txt_NumberOfLoads = new DevExpress.XtraEditors.TextEdit();
            this.txt_TotalWeight = new DevExpress.XtraEditors.TextEdit();
            this.txt_CustomsClearance = new DevExpress.XtraEditors.TextEdit();
            this.txt_ContactInformation = new DevExpress.XtraEditors.TextEdit();
            this.cmbox_Truck = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_LoandingAddress = new DevExpress.XtraEditors.MemoEdit();
            this.dtp_LoadingDateTimeFrom = new DevExpress.XtraEditors.DateEdit();
            this.dtp_LoadingDateTimeTo = new DevExpress.XtraEditors.DateEdit();
            this.txt_LoadingReferenceNumber = new DevExpress.XtraEditors.TextEdit();
            this.txt_UnloadingAddress = new DevExpress.XtraEditors.MemoEdit();
            this.dtp_UnloadingDateTimeFrom = new DevExpress.XtraEditors.DateEdit();
            this.dtp_UnloadingDateTimeTo = new DevExpress.XtraEditors.DateEdit();
            this.txt_UnloadingReferenceNumber = new DevExpress.XtraEditors.TextEdit();
            this.txt_MultiStopPlanReference = new DevExpress.XtraEditors.TextEdit();
            this.checkBoxADRCategory21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory61 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory41 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory51 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory52 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory62 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkBoxADRCategory9 = new DevExpress.XtraEditors.CheckEdit();
            this.cmbox_Trailer = new DevExpress.XtraEditors.LookUpEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl_DriverLastReadDateTime = new DevExpress.XtraLayout.SimpleLabelItem();
            this.groupboxLoading = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupboxUnLoading = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.btn_CustomizeLayout = new DevExpress.XtraBars.BarButtonItem();
            this.errorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.switch_Archived.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnloadedImageURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnloadedImageURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxVignettesNeeded.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxTollRoads.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLoadedImageURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoadedImageURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TransportOrderNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Driver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ExternalReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_RouteInformation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_FerryInformation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoadDetails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NumberOfLoads.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TotalWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CustomsClearance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ContactInformation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Truck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoandingAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoadingReferenceNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UnloadingAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UnloadingReferenceNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_MultiStopPlanReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Trailer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_DriverLastReadDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupboxLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupboxUnLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.switch_Archived);
            this.layoutControl1.Controls.Add(this.gridControlUnloadedImageURL);
            this.layoutControl1.Controls.Add(this.checkBoxVignettesNeeded);
            this.layoutControl1.Controls.Add(this.checkBoxTollRoads);
            this.layoutControl1.Controls.Add(this.gridControlLoadedImageURL);
            this.layoutControl1.Controls.Add(this.txt_TransportOrderNumber);
            this.layoutControl1.Controls.Add(this.cmbox_Driver);
            this.layoutControl1.Controls.Add(this.txt_ExternalReference);
            this.layoutControl1.Controls.Add(this.txt_RouteInformation);
            this.layoutControl1.Controls.Add(this.cmbox_Status);
            this.layoutControl1.Controls.Add(this.txt_FerryInformation);
            this.layoutControl1.Controls.Add(this.txt_LoadDetails);
            this.layoutControl1.Controls.Add(this.richTextBoxNotes);
            this.layoutControl1.Controls.Add(this.txt_NumberOfLoads);
            this.layoutControl1.Controls.Add(this.txt_TotalWeight);
            this.layoutControl1.Controls.Add(this.txt_CustomsClearance);
            this.layoutControl1.Controls.Add(this.txt_ContactInformation);
            this.layoutControl1.Controls.Add(this.cmbox_Truck);
            this.layoutControl1.Controls.Add(this.txt_LoandingAddress);
            this.layoutControl1.Controls.Add(this.dtp_LoadingDateTimeFrom);
            this.layoutControl1.Controls.Add(this.dtp_LoadingDateTimeTo);
            this.layoutControl1.Controls.Add(this.txt_LoadingReferenceNumber);
            this.layoutControl1.Controls.Add(this.txt_UnloadingAddress);
            this.layoutControl1.Controls.Add(this.dtp_UnloadingDateTimeFrom);
            this.layoutControl1.Controls.Add(this.dtp_UnloadingDateTimeTo);
            this.layoutControl1.Controls.Add(this.txt_UnloadingReferenceNumber);
            this.layoutControl1.Controls.Add(this.txt_MultiStopPlanReference);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory21);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory3);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory42);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory61);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory22);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory23);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory43);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory41);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory51);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory52);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory62);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory8);
            this.layoutControl1.Controls.Add(this.checkBoxADRCategory9);
            this.layoutControl1.Controls.Add(this.cmbox_Trailer);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 31);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1270, 485, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(971, 1057);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // switch_Archived
            // 
            this.switch_Archived.Location = new System.Drawing.Point(629, 999);
            this.switch_Archived.MenuManager = this._barManager;
            this.switch_Archived.Name = "switch_Archived";
            this.switch_Archived.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.switch_Archived.Properties.OffText = "Active";
            this.switch_Archived.Properties.OnText = "Archived";
            this.switch_Archived.Size = new System.Drawing.Size(318, 24);
            this.switch_Archived.StyleController = this.layoutControl1;
            this.switch_Archived.TabIndex = 42;
            // 
            // gridControlUnloadedImageURL
            // 
            this.gridControlUnloadedImageURL.Location = new System.Drawing.Point(497, 483);
            this.gridControlUnloadedImageURL.MainView = this.gridViewUnloadedImageURL;
            this.gridControlUnloadedImageURL.Name = "gridControlUnloadedImageURL";
            this.gridControlUnloadedImageURL.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit2});
            this.gridControlUnloadedImageURL.Size = new System.Drawing.Size(450, 332);
            this.gridControlUnloadedImageURL.TabIndex = 41;
            this.gridControlUnloadedImageURL.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUnloadedImageURL});
            // 
            // gridViewUnloadedImageURL
            // 
            this.gridViewUnloadedImageURL.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4});
            this.gridViewUnloadedImageURL.GridControl = this.gridControlUnloadedImageURL;
            this.gridViewUnloadedImageURL.Name = "gridViewUnloadedImageURL";
            this.gridViewUnloadedImageURL.OptionsView.ShowGroupPanel = false;
            this.gridViewUnloadedImageURL.RowHeight = 300;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Image";
            this.gridColumn3.ColumnEdit = this.repositoryItemPictureEdit2;
            this.gridColumn3.FieldName = "Image";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.AllowAnimationOnValueChanged = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit2.CustomHeight = 250;
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ShowEditMenuItem = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit2.ShowScrollBars = true;
            this.repositoryItemPictureEdit2.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit2.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "URL";
            this.gridColumn4.FieldName = "URL";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // checkBoxVignettesNeeded
            // 
            this.checkBoxVignettesNeeded.Location = new System.Drawing.Point(24, 354);
            this.checkBoxVignettesNeeded.Name = "checkBoxVignettesNeeded";
            this.checkBoxVignettesNeeded.Properties.Caption = "Vignettes Needed";
            this.checkBoxVignettesNeeded.Size = new System.Drawing.Size(457, 19);
            this.checkBoxVignettesNeeded.StyleController = this.layoutControl1;
            this.checkBoxVignettesNeeded.TabIndex = 40;
            // 
            // checkBoxTollRoads
            // 
            this.checkBoxTollRoads.Location = new System.Drawing.Point(24, 377);
            this.checkBoxTollRoads.Name = "checkBoxTollRoads";
            this.checkBoxTollRoads.Properties.Caption = "Toll Roads";
            this.checkBoxTollRoads.Size = new System.Drawing.Size(457, 19);
            this.checkBoxTollRoads.StyleController = this.layoutControl1;
            this.checkBoxTollRoads.TabIndex = 39;
            // 
            // gridControlLoadedImageURL
            // 
            this.gridControlLoadedImageURL.Location = new System.Drawing.Point(24, 483);
            this.gridControlLoadedImageURL.MainView = this.gridViewLoadedImageURL;
            this.gridControlLoadedImageURL.Name = "gridControlLoadedImageURL";
            this.gridControlLoadedImageURL.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.gridControlLoadedImageURL.Size = new System.Drawing.Size(445, 332);
            this.gridControlLoadedImageURL.TabIndex = 37;
            this.gridControlLoadedImageURL.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLoadedImageURL});
            // 
            // gridViewLoadedImageURL
            // 
            this.gridViewLoadedImageURL.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridViewLoadedImageURL.GridControl = this.gridControlLoadedImageURL;
            this.gridViewLoadedImageURL.Name = "gridViewLoadedImageURL";
            this.gridViewLoadedImageURL.OptionsView.ShowGroupPanel = false;
            this.gridViewLoadedImageURL.RowHeight = 300;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Image";
            this.gridColumn1.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumn1.FieldName = "Image";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.AllowAnimationOnValueChanged = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit1.CustomHeight = 250;
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ShowEditMenuItem = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit1.ShowScrollBars = true;
            this.repositoryItemPictureEdit1.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.repositoryItemPictureEdit1.PopupMenuShowing += new DevExpress.XtraEditors.Events.PopupMenuShowingEventHandler(this.repositoryItemPictureEdit1_PopupMenuShowing);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "URL";
            this.gridColumn2.FieldName = "URL";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // txt_TransportOrderNumber
            // 
            this.txt_TransportOrderNumber.Location = new System.Drawing.Point(167, 42);
            this.txt_TransportOrderNumber.Name = "txt_TransportOrderNumber";
            this.txt_TransportOrderNumber.Properties.MaxLength = 30;
            this.txt_TransportOrderNumber.Properties.UseReadOnlyAppearance = false;
            this.txt_TransportOrderNumber.Size = new System.Drawing.Size(314, 20);
            this.txt_TransportOrderNumber.StyleController = this.layoutControl1;
            this.txt_TransportOrderNumber.TabIndex = 0;
            // 
            // cmbox_Driver
            // 
            this.cmbox_Driver.Location = new System.Drawing.Point(167, 114);
            this.cmbox_Driver.Name = "cmbox_Driver";
            this.cmbox_Driver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbox_Driver.Properties.NullText = "";
            this.cmbox_Driver.Size = new System.Drawing.Size(314, 20);
            this.cmbox_Driver.StyleController = this.layoutControl1;
            this.cmbox_Driver.TabIndex = 17;
            // 
            // txt_ExternalReference
            // 
            this.txt_ExternalReference.Location = new System.Drawing.Point(167, 66);
            this.txt_ExternalReference.Name = "txt_ExternalReference";
            this.txt_ExternalReference.Properties.MaxLength = 500;
            this.txt_ExternalReference.Size = new System.Drawing.Size(314, 20);
            this.txt_ExternalReference.StyleController = this.layoutControl1;
            this.txt_ExternalReference.TabIndex = 15;
            // 
            // txt_RouteInformation
            // 
            this.txt_RouteInformation.Location = new System.Drawing.Point(167, 186);
            this.txt_RouteInformation.Name = "txt_RouteInformation";
            this.txt_RouteInformation.Size = new System.Drawing.Size(314, 20);
            this.txt_RouteInformation.StyleController = this.layoutControl1;
            this.txt_RouteInformation.TabIndex = 20;
            // 
            // cmbox_Status
            // 
            this.cmbox_Status.Location = new System.Drawing.Point(167, 162);
            this.cmbox_Status.Name = "cmbox_Status";
            this.cmbox_Status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbox_Status.Properties.NullText = "";
            this.cmbox_Status.Size = new System.Drawing.Size(314, 20);
            this.cmbox_Status.StyleController = this.layoutControl1;
            this.cmbox_Status.TabIndex = 19;
            // 
            // txt_FerryInformation
            // 
            this.txt_FerryInformation.Location = new System.Drawing.Point(167, 210);
            this.txt_FerryInformation.Name = "txt_FerryInformation";
            this.txt_FerryInformation.Size = new System.Drawing.Size(314, 20);
            this.txt_FerryInformation.StyleController = this.layoutControl1;
            this.txt_FerryInformation.TabIndex = 21;
            // 
            // txt_LoadDetails
            // 
            this.txt_LoadDetails.Location = new System.Drawing.Point(167, 282);
            this.txt_LoadDetails.Name = "txt_LoadDetails";
            this.txt_LoadDetails.Size = new System.Drawing.Size(314, 20);
            this.txt_LoadDetails.StyleController = this.layoutControl1;
            this.txt_LoadDetails.TabIndex = 24;
            // 
            // richTextBoxNotes
            // 
            this.richTextBoxNotes.Location = new System.Drawing.Point(36, 891);
            this.richTextBoxNotes.Name = "richTextBoxNotes";
            this.richTextBoxNotes.Size = new System.Drawing.Size(162, 130);
            this.richTextBoxNotes.StyleController = this.layoutControl1;
            this.richTextBoxNotes.TabIndex = 27;
            // 
            // txt_NumberOfLoads
            // 
            this.txt_NumberOfLoads.EditValue = "0";
            this.txt_NumberOfLoads.Location = new System.Drawing.Point(167, 234);
            this.txt_NumberOfLoads.Name = "txt_NumberOfLoads";
            this.txt_NumberOfLoads.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_NumberOfLoads.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_NumberOfLoads.Properties.Mask.EditMask = "################";
            this.txt_NumberOfLoads.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_NumberOfLoads.Size = new System.Drawing.Size(314, 20);
            this.txt_NumberOfLoads.StyleController = this.layoutControl1;
            this.txt_NumberOfLoads.TabIndex = 22;
            // 
            // txt_TotalWeight
            // 
            this.txt_TotalWeight.EditValue = "0";
            this.txt_TotalWeight.Location = new System.Drawing.Point(167, 258);
            this.txt_TotalWeight.Name = "txt_TotalWeight";
            this.txt_TotalWeight.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_TotalWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_TotalWeight.Properties.Mask.EditMask = "################";
            this.txt_TotalWeight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt_TotalWeight.Size = new System.Drawing.Size(314, 20);
            this.txt_TotalWeight.StyleController = this.layoutControl1;
            this.txt_TotalWeight.TabIndex = 23;
            // 
            // txt_CustomsClearance
            // 
            this.txt_CustomsClearance.Location = new System.Drawing.Point(167, 306);
            this.txt_CustomsClearance.Name = "txt_CustomsClearance";
            this.txt_CustomsClearance.Size = new System.Drawing.Size(314, 20);
            this.txt_CustomsClearance.StyleController = this.layoutControl1;
            this.txt_CustomsClearance.TabIndex = 25;
            // 
            // txt_ContactInformation
            // 
            this.txt_ContactInformation.Location = new System.Drawing.Point(167, 330);
            this.txt_ContactInformation.Name = "txt_ContactInformation";
            this.txt_ContactInformation.Size = new System.Drawing.Size(314, 20);
            this.txt_ContactInformation.StyleController = this.layoutControl1;
            this.txt_ContactInformation.TabIndex = 26;
            // 
            // cmbox_Truck
            // 
            this.cmbox_Truck.EditValue = "";
            this.cmbox_Truck.Location = new System.Drawing.Point(167, 90);
            this.cmbox_Truck.Name = "cmbox_Truck";
            this.cmbox_Truck.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbox_Truck.Properties.NullText = "";
            this.cmbox_Truck.Size = new System.Drawing.Size(314, 20);
            this.cmbox_Truck.StyleController = this.layoutControl1;
            this.cmbox_Truck.TabIndex = 16;
            // 
            // txt_LoandingAddress
            // 
            this.txt_LoandingAddress.Location = new System.Drawing.Point(640, 72);
            this.txt_LoandingAddress.Name = "txt_LoandingAddress";
            this.txt_LoandingAddress.Size = new System.Drawing.Size(295, 81);
            this.txt_LoandingAddress.StyleController = this.layoutControl1;
            this.txt_LoandingAddress.TabIndex = 29;
            // 
            // dtp_LoadingDateTimeFrom
            // 
            this.dtp_LoadingDateTimeFrom.EditValue = null;
            this.dtp_LoadingDateTimeFrom.Location = new System.Drawing.Point(640, 157);
            this.dtp_LoadingDateTimeFrom.Name = "dtp_LoadingDateTimeFrom";
            this.dtp_LoadingDateTimeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeProperties.Mask.EditMask = "HH:mm";
            this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_LoadingDateTimeFrom.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.dtp_LoadingDateTimeFrom.Properties.DisplayFormat.FormatString = "";
            this.dtp_LoadingDateTimeFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_LoadingDateTimeFrom.Properties.EditFormat.FormatString = "";
            this.dtp_LoadingDateTimeFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_LoadingDateTimeFrom.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtp_LoadingDateTimeFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_LoadingDateTimeFrom.Properties.ShowToday = false;
            this.dtp_LoadingDateTimeFrom.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtp_LoadingDateTimeFrom.Size = new System.Drawing.Size(295, 20);
            this.dtp_LoadingDateTimeFrom.StyleController = this.layoutControl1;
            this.dtp_LoadingDateTimeFrom.TabIndex = 30;
            // 
            // dtp_LoadingDateTimeTo
            // 
            this.dtp_LoadingDateTimeTo.EditValue = null;
            this.dtp_LoadingDateTimeTo.Location = new System.Drawing.Point(640, 181);
            this.dtp_LoadingDateTimeTo.Name = "dtp_LoadingDateTimeTo";
            this.dtp_LoadingDateTimeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_LoadingDateTimeTo.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtp_LoadingDateTimeTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_LoadingDateTimeTo.Properties.CalendarTimeProperties.Mask.EditMask = "HH:mm";
            this.dtp_LoadingDateTimeTo.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_LoadingDateTimeTo.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.dtp_LoadingDateTimeTo.Properties.DisplayFormat.FormatString = "";
            this.dtp_LoadingDateTimeTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_LoadingDateTimeTo.Properties.EditFormat.FormatString = "";
            this.dtp_LoadingDateTimeTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_LoadingDateTimeTo.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtp_LoadingDateTimeTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_LoadingDateTimeTo.Properties.ShowToday = false;
            this.dtp_LoadingDateTimeTo.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtp_LoadingDateTimeTo.Size = new System.Drawing.Size(295, 20);
            this.dtp_LoadingDateTimeTo.StyleController = this.layoutControl1;
            this.dtp_LoadingDateTimeTo.TabIndex = 31;
            // 
            // txt_LoadingReferenceNumber
            // 
            this.txt_LoadingReferenceNumber.Location = new System.Drawing.Point(640, 205);
            this.txt_LoadingReferenceNumber.Name = "txt_LoadingReferenceNumber";
            this.txt_LoadingReferenceNumber.Size = new System.Drawing.Size(295, 20);
            this.txt_LoadingReferenceNumber.StyleController = this.layoutControl1;
            this.txt_LoadingReferenceNumber.TabIndex = 32;
            // 
            // txt_UnloadingAddress
            // 
            this.txt_UnloadingAddress.Location = new System.Drawing.Point(640, 271);
            this.txt_UnloadingAddress.Name = "txt_UnloadingAddress";
            this.txt_UnloadingAddress.Size = new System.Drawing.Size(295, 82);
            this.txt_UnloadingAddress.StyleController = this.layoutControl1;
            this.txt_UnloadingAddress.TabIndex = 33;
            // 
            // dtp_UnloadingDateTimeFrom
            // 
            this.dtp_UnloadingDateTimeFrom.EditValue = null;
            this.dtp_UnloadingDateTimeFrom.Location = new System.Drawing.Point(640, 357);
            this.dtp_UnloadingDateTimeFrom.Name = "dtp_UnloadingDateTimeFrom";
            this.dtp_UnloadingDateTimeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeProperties.Mask.EditMask = "HH:mm";
            this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_UnloadingDateTimeFrom.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.dtp_UnloadingDateTimeFrom.Properties.DisplayFormat.FormatString = "";
            this.dtp_UnloadingDateTimeFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_UnloadingDateTimeFrom.Properties.EditFormat.FormatString = "";
            this.dtp_UnloadingDateTimeFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_UnloadingDateTimeFrom.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtp_UnloadingDateTimeFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_UnloadingDateTimeFrom.Properties.ShowToday = false;
            this.dtp_UnloadingDateTimeFrom.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtp_UnloadingDateTimeFrom.Size = new System.Drawing.Size(295, 20);
            this.dtp_UnloadingDateTimeFrom.StyleController = this.layoutControl1;
            this.dtp_UnloadingDateTimeFrom.TabIndex = 34;
            // 
            // dtp_UnloadingDateTimeTo
            // 
            this.dtp_UnloadingDateTimeTo.EditValue = null;
            this.dtp_UnloadingDateTimeTo.Location = new System.Drawing.Point(640, 381);
            this.dtp_UnloadingDateTimeTo.Name = "dtp_UnloadingDateTimeTo";
            this.dtp_UnloadingDateTimeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeProperties.Mask.EditMask = "HH:mm";
            this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_UnloadingDateTimeTo.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.dtp_UnloadingDateTimeTo.Properties.DisplayFormat.FormatString = "";
            this.dtp_UnloadingDateTimeTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_UnloadingDateTimeTo.Properties.EditFormat.FormatString = "";
            this.dtp_UnloadingDateTimeTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtp_UnloadingDateTimeTo.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dtp_UnloadingDateTimeTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtp_UnloadingDateTimeTo.Properties.ShowToday = false;
            this.dtp_UnloadingDateTimeTo.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtp_UnloadingDateTimeTo.Size = new System.Drawing.Size(295, 20);
            this.dtp_UnloadingDateTimeTo.StyleController = this.layoutControl1;
            this.dtp_UnloadingDateTimeTo.TabIndex = 35;
            // 
            // txt_UnloadingReferenceNumber
            // 
            this.txt_UnloadingReferenceNumber.Location = new System.Drawing.Point(640, 405);
            this.txt_UnloadingReferenceNumber.Name = "txt_UnloadingReferenceNumber";
            this.txt_UnloadingReferenceNumber.Size = new System.Drawing.Size(295, 20);
            this.txt_UnloadingReferenceNumber.StyleController = this.layoutControl1;
            this.txt_UnloadingReferenceNumber.TabIndex = 36;
            // 
            // txt_MultiStopPlanReference
            // 
            this.txt_MultiStopPlanReference.Enabled = false;
            this.txt_MultiStopPlanReference.Location = new System.Drawing.Point(167, 400);
            this.txt_MultiStopPlanReference.Name = "txt_MultiStopPlanReference";
            this.txt_MultiStopPlanReference.Size = new System.Drawing.Size(314, 20);
            this.txt_MultiStopPlanReference.StyleController = this.layoutControl1;
            this.txt_MultiStopPlanReference.TabIndex = 28;
            // 
            // checkBoxADRCategory21
            // 
            this.checkBoxADRCategory21.Location = new System.Drawing.Point(214, 861);
            this.checkBoxADRCategory21.Name = "checkBoxADRCategory21";
            this.checkBoxADRCategory21.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory21.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory21.Properties.Caption = "Class 2.1 – Flammable gases";
            this.checkBoxADRCategory21.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory21.StyleController = this.layoutControl1;
            this.checkBoxADRCategory21.TabIndex = 2;
            // 
            // checkBoxADRCategory3
            // 
            this.checkBoxADRCategory3.Location = new System.Drawing.Point(629, 884);
            this.checkBoxADRCategory3.Name = "checkBoxADRCategory3";
            this.checkBoxADRCategory3.Properties.Caption = "Class 3 – Flammable liquids";
            this.checkBoxADRCategory3.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory3.StyleController = this.layoutControl1;
            this.checkBoxADRCategory3.TabIndex = 5;
            // 
            // checkBoxADRCategory42
            // 
            this.checkBoxADRCategory42.Location = new System.Drawing.Point(629, 907);
            this.checkBoxADRCategory42.Name = "checkBoxADRCategory42";
            this.checkBoxADRCategory42.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory42.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory42.Properties.Caption = "Class 4.2 – Substances liable to spontaneous combustion";
            this.checkBoxADRCategory42.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory42.StyleController = this.layoutControl1;
            this.checkBoxADRCategory42.TabIndex = 7;
            // 
            // checkBoxADRCategory61
            // 
            this.checkBoxADRCategory61.Location = new System.Drawing.Point(629, 953);
            this.checkBoxADRCategory61.Name = "checkBoxADRCategory61";
            this.checkBoxADRCategory61.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory61.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory61.Properties.Caption = "Class 6.1 – Toxic substances";
            this.checkBoxADRCategory61.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory61.StyleController = this.layoutControl1;
            this.checkBoxADRCategory61.TabIndex = 11;
            // 
            // checkBoxADRCategory22
            // 
            this.checkBoxADRCategory22.Location = new System.Drawing.Point(629, 861);
            this.checkBoxADRCategory22.Name = "checkBoxADRCategory22";
            this.checkBoxADRCategory22.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory22.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory22.Properties.Caption = "Class 2.2 – Non-flammable,non-toxic gases";
            this.checkBoxADRCategory22.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory22.StyleController = this.layoutControl1;
            this.checkBoxADRCategory22.TabIndex = 3;
            // 
            // checkBoxADRCategory23
            // 
            this.checkBoxADRCategory23.Location = new System.Drawing.Point(214, 884);
            this.checkBoxADRCategory23.Name = "checkBoxADRCategory23";
            this.checkBoxADRCategory23.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory23.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory23.Properties.Caption = "Class 2.3 – Toxic gasses";
            this.checkBoxADRCategory23.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory23.StyleController = this.layoutControl1;
            this.checkBoxADRCategory23.TabIndex = 4;
            // 
            // checkBoxADRCategory43
            // 
            this.checkBoxADRCategory43.Location = new System.Drawing.Point(214, 930);
            this.checkBoxADRCategory43.Name = "checkBoxADRCategory43";
            this.checkBoxADRCategory43.Properties.Caption = "Class 4.3 – Substances which, in contact with water, emit flammable gases";
            this.checkBoxADRCategory43.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkBoxADRCategory43.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory43.StyleController = this.layoutControl1;
            this.checkBoxADRCategory43.TabIndex = 8;
            // 
            // checkBoxADRCategory41
            // 
            this.checkBoxADRCategory41.Location = new System.Drawing.Point(214, 907);
            this.checkBoxADRCategory41.Name = "checkBoxADRCategory41";
            this.checkBoxADRCategory41.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory41.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory41.Properties.Caption = "Class 4.1 – Flammable solids";
            this.checkBoxADRCategory41.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory41.StyleController = this.layoutControl1;
            this.checkBoxADRCategory41.TabIndex = 6;
            // 
            // checkBoxADRCategory51
            // 
            this.checkBoxADRCategory51.Location = new System.Drawing.Point(629, 930);
            this.checkBoxADRCategory51.Name = "checkBoxADRCategory51";
            this.checkBoxADRCategory51.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory51.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory51.Properties.Caption = "Class 5.1 – Oxidizing substances";
            this.checkBoxADRCategory51.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory51.StyleController = this.layoutControl1;
            this.checkBoxADRCategory51.TabIndex = 9;
            // 
            // checkBoxADRCategory52
            // 
            this.checkBoxADRCategory52.Location = new System.Drawing.Point(214, 953);
            this.checkBoxADRCategory52.Name = "checkBoxADRCategory52";
            this.checkBoxADRCategory52.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory52.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory52.Properties.Caption = "Class 5.2 – Organic peroxides";
            this.checkBoxADRCategory52.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory52.StyleController = this.layoutControl1;
            this.checkBoxADRCategory52.TabIndex = 10;
            // 
            // checkBoxADRCategory62
            // 
            this.checkBoxADRCategory62.Location = new System.Drawing.Point(214, 976);
            this.checkBoxADRCategory62.Name = "checkBoxADRCategory62";
            this.checkBoxADRCategory62.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory62.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory62.Properties.Caption = "Class 6.2 – Infectious substances";
            this.checkBoxADRCategory62.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory62.StyleController = this.layoutControl1;
            this.checkBoxADRCategory62.TabIndex = 12;
            // 
            // checkBoxADRCategory8
            // 
            this.checkBoxADRCategory8.Location = new System.Drawing.Point(629, 976);
            this.checkBoxADRCategory8.Name = "checkBoxADRCategory8";
            this.checkBoxADRCategory8.Properties.Caption = "Class 8 – Corrosive substances";
            this.checkBoxADRCategory8.Size = new System.Drawing.Size(318, 19);
            this.checkBoxADRCategory8.StyleController = this.layoutControl1;
            this.checkBoxADRCategory8.TabIndex = 13;
            // 
            // checkBoxADRCategory9
            // 
            this.checkBoxADRCategory9.Location = new System.Drawing.Point(214, 999);
            this.checkBoxADRCategory9.Name = "checkBoxADRCategory9";
            this.checkBoxADRCategory9.Properties.Appearance.Options.UseTextOptions = true;
            this.checkBoxADRCategory9.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkBoxADRCategory9.Properties.Caption = "Class 9 – Miscellaneous dangerous substances and articles";
            this.checkBoxADRCategory9.Size = new System.Drawing.Size(411, 19);
            this.checkBoxADRCategory9.StyleController = this.layoutControl1;
            this.checkBoxADRCategory9.TabIndex = 14;
            // 
            // cmbox_Trailer
            // 
            this.cmbox_Trailer.Location = new System.Drawing.Point(167, 138);
            this.cmbox_Trailer.Name = "cmbox_Trailer";
            this.cmbox_Trailer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbox_Trailer.Properties.NullText = "";
            this.cmbox_Trailer.Properties.UseReadOnlyAppearance = false;
            this.cmbox_Trailer.Size = new System.Drawing.Size(314, 20);
            this.cmbox_Trailer.StyleController = this.layoutControl1;
            this.cmbox_Trailer.TabIndex = 18;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup6,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(971, 1057);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem15,
            this.layoutControlItem24,
            this.layoutControlItem13,
            this.layoutControlItem16,
            this.layoutControlItem42,
            this.layoutControlItem21,
            this.layoutControlItem27,
            this.layoutControlItem9,
            this.layoutControlItem5,
            this.layoutControlItem12,
            this.lbl_DriverLastReadDateTime,
            this.groupboxLoading,
            this.groupboxUnLoading,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem19});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(951, 441);
            this.layoutControlGroup1.Text = "Basic Info";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt_ExternalReference;
            this.layoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem8.CustomizationFormText = "External Reference";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem8.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem8.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem8.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem8.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem8.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem8.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem8.Text = "External Reference";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmbox_Truck;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem6.CustomizationFormText = "Truck";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem6.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem6.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem6.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem6.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem6.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem6.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem6.Text = "Truck";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cmbox_Driver;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem7.CustomizationFormText = "Driver";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem7.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem7.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem7.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem7.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem7.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem7.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem7.Text = "Driver";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cmbox_Trailer;
            this.layoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem15.CustomizationFormText = "Trailer";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem15.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem15.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem15.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem15.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem15.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem15.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem15.Text = "Trailer";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.cmbox_Status;
            this.layoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem24.CustomizationFormText = "Status";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem24.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem24.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem24.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem24.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem24.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem24.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem24.Text = "Status";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt_RouteInformation;
            this.layoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem13.CustomizationFormText = "Route Information";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem13.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem13.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem13.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem13.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem13.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem13.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem13.Text = "Route Information";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt_FerryInformation;
            this.layoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem16.CustomizationFormText = "FerryInformation";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem16.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem16.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem16.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem16.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem16.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem16.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem16.Text = "FerryInformation";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.txt_NumberOfLoads;
            this.layoutControlItem42.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem42.CustomizationFormText = "Number Of Loads";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem42.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem42.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem42.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem42.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem42.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem42.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem42.Text = "Number Of Loads";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txt_LoadDetails;
            this.layoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem21.CustomizationFormText = "Details";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem21.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem21.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem21.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem21.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem21.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem21.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem21.Text = "Details";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.txt_CustomsClearance;
            this.layoutControlItem27.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem27.CustomizationFormText = "Customs Clearance";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 264);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem27.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem27.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem27.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem27.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem27.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem27.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem27.Text = "Customs Clearance";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt_ContactInformation;
            this.layoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem9.CustomizationFormText = "Contact Information";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 288);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem9.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem9.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem9.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem9.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem9.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem9.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem9.Text = "Contact Information";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt_TransportOrderNumber;
            this.layoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem5.CustomizationFormText = "Transport Order Number";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem5.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem5.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem5.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem5.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem5.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem5.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem5.Text = "Transport Order Number";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt_MultiStopPlanReference;
            this.layoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem12.CustomizationFormText = "Multi Stop Reference";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 358);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem12.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem12.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem12.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem12.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem12.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem12.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem12.Text = "Multi Stop Reference";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(140, 13);
            // 
            // lbl_DriverLastReadDateTime
            // 
            this.lbl_DriverLastReadDateTime.AllowHotTrack = false;
            this.lbl_DriverLastReadDateTime.Location = new System.Drawing.Point(0, 382);
            this.lbl_DriverLastReadDateTime.Name = "lbl_DriverLastReadDateTime";
            this.lbl_DriverLastReadDateTime.Size = new System.Drawing.Size(461, 17);
            this.lbl_DriverLastReadDateTime.Text = "Driver Read at : ";
            this.lbl_DriverLastReadDateTime.TextSize = new System.Drawing.Size(140, 13);
            // 
            // groupboxLoading
            // 
            this.groupboxLoading.CustomizationFormText = "Loading";
            this.groupboxLoading.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48});
            this.groupboxLoading.Location = new System.Drawing.Point(461, 0);
            this.groupboxLoading.Name = "groupboxLoading";
            this.groupboxLoading.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxLoading.OptionsPrint.AppearanceGroupCaption.Options.UseFont = true;
            this.groupboxLoading.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxLoading.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.groupboxLoading.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxLoading.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.groupboxLoading.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxLoading.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.groupboxLoading.Size = new System.Drawing.Size(466, 199);
            this.groupboxLoading.Text = "Loading";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txt_LoandingAddress;
            this.layoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem26.CustomizationFormText = "Loading Address";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem26.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem26.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem26.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem26.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem26.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem26.Size = new System.Drawing.Size(442, 85);
            this.layoutControlItem26.Text = "Loading Address";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.dtp_LoadingDateTimeFrom;
            this.layoutControlItem46.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem46.CustomizationFormText = "Created Date From";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 85);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem46.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem46.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem46.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem46.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem46.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem46.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem46.Text = "Loading Date Time From";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.dtp_LoadingDateTimeTo;
            this.layoutControlItem47.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem47.CustomizationFormText = "Created Date From";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 109);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem47.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem47.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem47.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem47.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem47.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem47.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem47.Text = "Loading Date Time to";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.txt_LoadingReferenceNumber;
            this.layoutControlItem48.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 133);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem48.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem48.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem48.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem48.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem48.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem48.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem48.Text = "Loading Reference Number";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(140, 13);
            // 
            // groupboxUnLoading
            // 
            this.groupboxUnLoading.CustomizationFormText = "Unloading";
            this.groupboxUnLoading.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem52,
            this.layoutControlItem53});
            this.groupboxUnLoading.Location = new System.Drawing.Point(461, 199);
            this.groupboxUnLoading.Name = "groupboxUnLoading";
            this.groupboxUnLoading.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxUnLoading.OptionsPrint.AppearanceGroupCaption.Options.UseFont = true;
            this.groupboxUnLoading.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxUnLoading.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.groupboxUnLoading.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxUnLoading.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.groupboxUnLoading.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupboxUnLoading.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.groupboxUnLoading.Size = new System.Drawing.Size(466, 200);
            this.groupboxUnLoading.Text = "Unloading";
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txt_UnloadingAddress;
            this.layoutControlItem49.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem49.CustomizationFormText = "Unloading Address";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem49.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem49.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem49.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem49.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem49.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem49.Size = new System.Drawing.Size(442, 86);
            this.layoutControlItem49.Text = "Unloading Address";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.dtp_UnloadingDateTimeFrom;
            this.layoutControlItem50.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem50.CustomizationFormText = "Unloading Date Time From";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem50.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem50.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem50.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem50.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem50.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem50.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem50.Text = "Unloading Date Time From";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.dtp_UnloadingDateTimeTo;
            this.layoutControlItem52.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem52.CustomizationFormText = "Unloading Date Time To";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem52.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem52.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem52.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem52.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem52.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem52.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem52.Text = "Unloading Date Time To";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.txt_UnloadingReferenceNumber;
            this.layoutControlItem53.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem53.CustomizationFormText = "Unloading Reference Number";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 134);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem53.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem53.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem53.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem53.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem53.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem53.Size = new System.Drawing.Size(442, 24);
            this.layoutControlItem53.Text = "Unloading Reference Number";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkBoxVignettesNeeded;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 312);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(461, 23);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkBoxTollRoads;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 335);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(461, 23);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txt_TotalWeight;
            this.layoutControlItem19.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem19.CustomizationFormText = "Number Of Loads";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem19.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem19.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem19.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem19.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem19.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem19.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem19.Text = "Total Weight";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.switch_Archived;
            this.layoutControlItem10.Location = new System.Drawing.Point(605, 138);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(322, 38);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "ADR Category";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem35,
            this.layoutControlItem29,
            this.layoutControlItem39,
            this.layoutControlItem33,
            this.layoutControlItem37,
            this.layoutControlItem31,
            this.layoutControlItem41,
            this.layoutControlItem34,
            this.layoutControlItem32,
            this.layoutControlItem30,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.layoutControlItem40,
            this.layoutControlGroup2,
            this.layoutControlItem10});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 819);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroup6.OptionsPrint.AppearanceGroupCaption.Options.UseFont = true;
            this.layoutControlGroup6.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroup6.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlGroup6.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroup6.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlGroup6.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroup6.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlGroup6.Size = new System.Drawing.Size(951, 218);
            this.layoutControlGroup6.Text = "ADR Category";
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.checkBoxADRCategory21;
            this.layoutControlItem35.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(190, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem35.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem35.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem35.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem35.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem35.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem35.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.checkBoxADRCategory43;
            this.layoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(190, 69);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem29.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem29.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem29.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem29.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem29.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem29.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.checkBoxADRCategory62;
            this.layoutControlItem39.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
            this.layoutControlItem39.Location = new System.Drawing.Point(190, 115);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem39.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem39.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem39.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem39.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem39.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem39.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.checkBoxADRCategory23;
            this.layoutControlItem33.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(190, 23);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem33.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem33.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem33.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem33.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem33.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem33.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.checkBoxADRCategory52;
            this.layoutControlItem37.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
            this.layoutControlItem37.Location = new System.Drawing.Point(190, 92);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem37.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem37.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem37.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem37.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem37.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem37.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.checkBoxADRCategory41;
            this.layoutControlItem31.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(190, 46);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem31.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem31.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem31.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem31.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem31.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem31.Size = new System.Drawing.Size(415, 23);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.checkBoxADRCategory9;
            this.layoutControlItem41.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem41.CustomizationFormText = "layoutControlItem41";
            this.layoutControlItem41.Location = new System.Drawing.Point(190, 138);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem41.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem41.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem41.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem41.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem41.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem41.Size = new System.Drawing.Size(415, 38);
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.checkBoxADRCategory22;
            this.layoutControlItem34.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(605, 0);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem34.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem34.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem34.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem34.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem34.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem34.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.checkBoxADRCategory3;
            this.layoutControlItem32.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(605, 23);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem32.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem32.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem32.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem32.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem32.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem32.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.checkBoxADRCategory42;
            this.layoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(605, 46);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem30.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem30.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem30.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem30.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem30.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem30.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.checkBoxADRCategory51;
            this.layoutControlItem36.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(605, 69);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem36.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem36.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem36.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem36.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem36.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem36.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.checkBoxADRCategory61;
            this.layoutControlItem38.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(605, 92);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem38.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItem.Options.UseTextOptions = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItem.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem38.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItemControl.Options.UseTextOptions = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItemControl.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem38.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItemText.Options.UseTextOptions = true;
            this.layoutControlItem38.OptionsPrint.AppearanceItemText.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem38.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.checkBoxADRCategory8;
            this.layoutControlItem40.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem40";
            this.layoutControlItem40.Location = new System.Drawing.Point(605, 115);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem40.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem40.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem40.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem40.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem40.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem40.Size = new System.Drawing.Size(322, 23);
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(190, 176);
            this.layoutControlGroup2.Text = "Note";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.richTextBoxNotes;
            this.layoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem23.CustomizationFormText = "Note";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(166, 0);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(166, 20);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.OptionsPrint.AppearanceItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem23.OptionsPrint.AppearanceItem.Options.UseFont = true;
            this.layoutControlItem23.OptionsPrint.AppearanceItemControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem23.OptionsPrint.AppearanceItemControl.Options.UseFont = true;
            this.layoutControlItem23.OptionsPrint.AppearanceItemText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlItem23.OptionsPrint.AppearanceItemText.Options.UseFont = true;
            this.layoutControlItem23.Size = new System.Drawing.Size(166, 134);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "Note";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 441);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(473, 378);
            this.layoutControlGroup4.Text = "Loading Images";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlLoadedImageURL;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 336);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 336);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(449, 336);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(473, 441);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(478, 378);
            this.layoutControlGroup5.Text = "Unloading";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlUnloadedImageURL;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(454, 336);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "View In Browser";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Save Image";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // btn_CustomizeLayout
            // 
            this.btn_CustomizeLayout.Name = "btn_CustomizeLayout";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // UC_DispatchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.layoutControl1);
            this.Name = "UC_DispatchForm";
            this.Size = new System.Drawing.Size(971, 1088);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.switch_Archived.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnloadedImageURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnloadedImageURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxVignettesNeeded.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxTollRoads.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLoadedImageURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoadedImageURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TransportOrderNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Driver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ExternalReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_RouteInformation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_FerryInformation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoadDetails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_NumberOfLoads.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TotalWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CustomsClearance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ContactInformation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Truck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoandingAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_LoadingDateTimeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LoadingReferenceNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UnloadingAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp_UnloadingDateTimeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UnloadingReferenceNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_MultiStopPlanReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxADRCategory9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbox_Trailer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_DriverLastReadDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupboxLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupboxUnLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorProvider;
        private DevExpress.XtraGrid.GridControl gridControlLoadedImageURL;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLoadedImageURL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.TextEdit txt_TransportOrderNumber;
        private DevExpress.XtraEditors.LookUpEdit cmbox_Driver;
        private DevExpress.XtraEditors.TextEdit txt_ExternalReference;
        private DevExpress.XtraEditors.TextEdit txt_RouteInformation;
        private DevExpress.XtraEditors.LookUpEdit cmbox_Status;
        private DevExpress.XtraEditors.TextEdit txt_FerryInformation;
        private DevExpress.XtraEditors.TextEdit txt_LoadDetails;
        private DevExpress.XtraEditors.MemoEdit richTextBoxNotes;
        private DevExpress.XtraEditors.TextEdit txt_NumberOfLoads;
        private DevExpress.XtraEditors.TextEdit txt_TotalWeight;
        private DevExpress.XtraEditors.TextEdit txt_CustomsClearance;
        private DevExpress.XtraEditors.TextEdit txt_ContactInformation;
        private DevExpress.XtraEditors.LookUpEdit cmbox_Truck;
        private DevExpress.XtraEditors.MemoEdit txt_LoandingAddress;
        private DevExpress.XtraEditors.DateEdit dtp_LoadingDateTimeFrom;
        private DevExpress.XtraEditors.DateEdit dtp_LoadingDateTimeTo;
        private DevExpress.XtraEditors.TextEdit txt_LoadingReferenceNumber;
        private DevExpress.XtraEditors.MemoEdit txt_UnloadingAddress;
        private DevExpress.XtraEditors.DateEdit dtp_UnloadingDateTimeFrom;
        private DevExpress.XtraEditors.DateEdit dtp_UnloadingDateTimeTo;
        private DevExpress.XtraEditors.TextEdit txt_UnloadingReferenceNumber;
        private DevExpress.XtraEditors.TextEdit txt_MultiStopPlanReference;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory21;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory3;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory42;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory61;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory22;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory23;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory43;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory41;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory51;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory52;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory62;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory8;
        private DevExpress.XtraEditors.CheckEdit checkBoxADRCategory9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.SimpleLabelItem lbl_DriverLastReadDateTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlGroup groupboxLoading;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlGroup groupboxUnLoading;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.CheckEdit checkBoxTollRoads;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit checkBoxVignettesNeeded;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem btn_CustomizeLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraGrid.GridControl gridControlUnloadedImageURL;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUnloadedImageURL;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraEditors.LookUpEdit cmbox_Trailer;
        private DevExpress.XtraEditors.ToggleSwitch switch_Archived;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
