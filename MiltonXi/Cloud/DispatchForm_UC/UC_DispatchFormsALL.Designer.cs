﻿namespace MiltonXi.Forms
{
    partial class UC_DispatchFormsALL
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceDispatchForms = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransportOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExternalReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTruckName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrailerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMultiStopPlanReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnloadedDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverLastReadDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoaded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnloaded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDispatchForms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bindingSourceDispatchForms;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 24);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(942, 371);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // bindingSourceDispatchForms
            // 
            this.bindingSourceDispatchForms.DataSource = typeof(MiltonXi._DAL.DispatchForm);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransportOrderNumber,
            this.colExternalReference,
            this.colTruckName,
            this.colTrailerName,
            this.colDriverName,
            this.colCurrentStatus,
            this.colMultiStopPlanReference,
            this.colLoadedDateTime,
            this.colUnloadedDateTime,
            this.colDriverLastReadDateTime,
            this.colLoaded,
            this.colUnloaded,
            this.colCreatedBy,
            this.colModifiedBy,
            this.colDateCreated,
            this.colDateModified,
            this.colLoadDetails,
            this.colArchived});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplaceHideCurrentRow;
            this.gridView1.OptionsEditForm.EditFormColumnCount = 2;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowFooter = true;
            // 
            // colTransportOrderNumber
            // 
            this.colTransportOrderNumber.FieldName = "TransportOrderNumber";
            this.colTransportOrderNumber.Name = "colTransportOrderNumber";
            this.colTransportOrderNumber.Visible = true;
            this.colTransportOrderNumber.VisibleIndex = 0;
            // 
            // colExternalReference
            // 
            this.colExternalReference.FieldName = "ExternalReference";
            this.colExternalReference.Name = "colExternalReference";
            this.colExternalReference.Visible = true;
            this.colExternalReference.VisibleIndex = 1;
            // 
            // colTruckName
            // 
            this.colTruckName.FieldName = "TruckName";
            this.colTruckName.Name = "colTruckName";
            this.colTruckName.Visible = true;
            this.colTruckName.VisibleIndex = 2;
            // 
            // colTrailerName
            // 
            this.colTrailerName.FieldName = "TrailerName";
            this.colTrailerName.Name = "colTrailerName";
            this.colTrailerName.Visible = true;
            this.colTrailerName.VisibleIndex = 3;
            // 
            // colDriverName
            // 
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 4;
            // 
            // colCurrentStatus
            // 
            this.colCurrentStatus.FieldName = "CurrentStatus";
            this.colCurrentStatus.Name = "colCurrentStatus";
            this.colCurrentStatus.Visible = true;
            this.colCurrentStatus.VisibleIndex = 5;
            // 
            // colMultiStopPlanReference
            // 
            this.colMultiStopPlanReference.FieldName = "MultiStopPlanReference";
            this.colMultiStopPlanReference.Name = "colMultiStopPlanReference";
            this.colMultiStopPlanReference.Visible = true;
            this.colMultiStopPlanReference.VisibleIndex = 6;
            // 
            // colLoadedDateTime
            // 
            this.colLoadedDateTime.FieldName = "LoadedDateTime";
            this.colLoadedDateTime.Name = "colLoadedDateTime";
            this.colLoadedDateTime.Visible = true;
            this.colLoadedDateTime.VisibleIndex = 7;
            // 
            // colUnloadedDateTime
            // 
            this.colUnloadedDateTime.FieldName = "UnloadedDateTime";
            this.colUnloadedDateTime.Name = "colUnloadedDateTime";
            this.colUnloadedDateTime.Visible = true;
            this.colUnloadedDateTime.VisibleIndex = 8;
            // 
            // colDriverLastReadDateTime
            // 
            this.colDriverLastReadDateTime.FieldName = "DriverLastReadDateTime";
            this.colDriverLastReadDateTime.Name = "colDriverLastReadDateTime";
            this.colDriverLastReadDateTime.Visible = true;
            this.colDriverLastReadDateTime.VisibleIndex = 9;
            // 
            // colLoaded
            // 
            this.colLoaded.FieldName = "Loaded";
            this.colLoaded.Name = "colLoaded";
            this.colLoaded.Visible = true;
            this.colLoaded.VisibleIndex = 10;
            // 
            // colUnloaded
            // 
            this.colUnloaded.FieldName = "Unloaded";
            this.colUnloaded.Name = "colUnloaded";
            this.colUnloaded.Visible = true;
            this.colUnloaded.VisibleIndex = 11;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 12;
            // 
            // colModifiedBy
            // 
            this.colModifiedBy.FieldName = "ModifiedBy";
            this.colModifiedBy.Name = "colModifiedBy";
            this.colModifiedBy.Visible = true;
            this.colModifiedBy.VisibleIndex = 13;
            // 
            // colDateCreated
            // 
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 14;
            // 
            // colDateModified
            // 
            this.colDateModified.FieldName = "DateModified";
            this.colDateModified.Name = "colDateModified";
            this.colDateModified.Visible = true;
            this.colDateModified.VisibleIndex = 15;
            // 
            // colArchived
            // 
            this.colArchived.FieldName = "Archived";
            this.colArchived.Name = "colArchived";
            this.colArchived.Visible = true;
            this.colArchived.VisibleIndex = 16;
            // 
            // colLoadDetails
            // 
            this.colLoadDetails.FieldName = "LoadDetails";
            this.colLoadDetails.Name = "colLoadDetails";
            this.colLoadDetails.Visible = true;
            this.colLoadDetails.VisibleIndex = 17;
            // 
            // UC_DispatchFormsALL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 418);
            this.Controls.Add(this.gridControl1);
            this.Name = "UC_DispatchFormsALL";
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDispatchForms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource bindingSourceDispatchForms;
        private DevExpress.XtraGrid.Columns.GridColumn colTransportOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalReference;
        private DevExpress.XtraGrid.Columns.GridColumn colTruckName;
        private DevExpress.XtraGrid.Columns.GridColumn colTrailerName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMultiStopPlanReference;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colUnloadedDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverLastReadDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colLoaded;
        private DevExpress.XtraGrid.Columns.GridColumn colUnloaded;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDateModified;
        private DevExpress.XtraGrid.Columns.GridColumn colArchived;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadDetails;
    }
}
