﻿using DevExpress.XtraEditors;
using MiltonXi.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiltonXi.Class
{
    public static class Utilities
    {
        public static void OpenFormInMain(Form form)
        {
          //  frm_MainForm frm = Application.OpenForms.Cast<Form>().Single(f => f.Name == "frm_MainForm") as frm_MainForm;
            frm_MainForm frm = frm_MainForm.Instance;
            frm.OpenFormWithPermission(form);
        }

        public static void RefreshGrid(string FormName)
        {
            frm_Master frm = Application.OpenForms.Cast<Form>().Where(f => f.Name == FormName).FirstOrDefault() as frm_Master;
            if (frm != null)
            {
                frm.RefreshData();
            }
        }

        public static void SetPageName(string FormName,string tag , string NewName)
        {
            frm_MainForm frm = Application.OpenForms.Cast<Form>().Single(f => f.Name == "frm_MainForm") as frm_MainForm;
          var page=  frm.TabFormControl.Pages.Where(x => (string)x.Tag == tag &&
            x.Name == FormName 
            ).FirstOrDefault();
            if (page != null)
                page.Text = NewName;
            
        }

 
    }
}
