﻿using MiltonXi._DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Class
{
    public static class Session
    {

        public static string connectionString { get => System.Configuration.ConfigurationManager.ConnectionStrings["DBWatcher"].ConnectionString; }



        public static class Defaults
        {
            public static int Drawer { get => 1; }
            public static int Customer { get => 1; }
            public static int Vendor { get => 2; }
            public static int Store { get => 1; }
            public static int RawStore { get => 1; }
            public static int DiscountReceivedAccountId { get => 1; }
            public static int DiscountAllowedAccountId { get => 2; }
            public static int SalesTaxId { get => 3; }
            public static int PurchaseTaxId { get => 4; }
            public static int PurchaseExpenseId { get => 5; }
            public static string Currency { get => "PLN"; }
            public static string CurrencyExchangeAPIKey { get => "aabad6fe5e7ce9ee40aa"; }
        }
        private static CompanyInfo _companyInfo;
        public static CompanyInfo CompanyInfo
        {
            get
            {
                if (_companyInfo == null)
                {
                    using (var db = new Entities())
                    {
                        _companyInfo = db.CompanyInfoes.First();
                    }
                }
                return _companyInfo;
            }
        }

        private static BindingList<_DAL.Product> _products;
        public static BindingList<_DAL.Product> Products
        {
            get
            {

                if (_products == null)
                {

                    using (var db = new Entities())
                    {
                        _products = new BindingList<Product>(db.Products.ToList());
                    }
                    DatabaseWatcher.Products = new TableDependency.SqlClient.SqlTableDependency<Product>(connectionString);
                    DatabaseWatcher.Products.OnChanged += DatabaseWatcher.Products_Changed;
                    DatabaseWatcher.Products.Start();
                }
                return _products;
            }
        }




        private static BindingList<ProductViewClass> productViewClasses;
        public static BindingList<ProductViewClass> ProductsView
        {
            get
            {
                if (productViewClasses == null)
                {
                    using (var db = new Entities())
                    {
                        var data = from pr in Session.Products
                                   join cg in db.ProductCategories on pr.CategoryId equals cg.Id
                                   select new ProductViewClass
                                   {
                                       Id = pr.Id,
                                       Code = pr.Code,
                                       Name = pr.Name,
                                       CategoryName = cg.Name,
                                       Description = pr.Descreption,
                                       Inactive = pr.Inactive,
                                       Type = pr.Type,
                                       NonTaxable = pr.Nontaxable,
                                       Units = (from u in db.ProductUnits
                                                where u.ProductId == pr.Id
                                                join un in db.UnitNames on u.UnitId equals un.Id
                                                select new ProductViewClass.ProductUOMView
                                                {
                                                    UnitId = u.UnitId,
                                                    UnitName = un.Name,
                                                    Factor = u.Factor,
                                                    SellPrice = u.SellPrice,
                                                    BuyPrice = u.BuyPrice,
                                                    Barcode = u.Barcode,
                                                    
                                                }).ToList()
                                   };
                        productViewClasses = new BindingList<ProductViewClass>(data.ToList());
                    }
                }
                return productViewClasses;
            }
        }

        public class ProductViewClass
        {
            public static ProductViewClass GetProduct(int Id)
            {
                ProductViewClass obj;
                using (var db = new _DAL.Entities())
                {
                    var data = from pr in Session.Products
                               where pr.Id == Id
                               join cg in db.ProductCategories on pr.CategoryId equals cg.Id
                               select new ProductViewClass
                               {
                                   Id = pr.Id,
                                   Code = pr.Code,
                                   Name = pr.Name,
                                   CategoryName = cg.Name,
                                   Description = pr.Descreption,
                                   Inactive = pr.Inactive,
                                   Type = pr.Type,
                                   Units = (from u in db.ProductUnits
                                            where u.ProductId == pr.Id
                                            join un in db.UnitNames on u.UnitId equals un.Id
                                            select new ProductViewClass.ProductUOMView
                                            {
                                                UnitId = u.UnitId,
                                                UnitName = un.Name,
                                                Factor = u.Factor,
                                                SellPrice = u.SellPrice,
                                                BuyPrice = u.BuyPrice,
                                                Barcode = u.Barcode,
                                            }).ToList()
                               };
                    obj = data.First();
                };
                return obj;
            }
            public int Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string CategoryName { get; set; }
            public string Description { get; set; }
            public Boolean Inactive { get; set; }
            public Boolean NonTaxable { get; set; }
            public byte Type { get; set; }
            public List<ProductUOMView> Units { get; set; }
            public class ProductUOMView
            {
                public int UnitId { get; set; }
                public string UnitName { get; set; }
                public Double Factor { get; set; }
                public Double SellPrice { get; set; }
                public Double BuyPrice { get; set; }
                public string Barcode { get; set; }
            }
        }

        /// <summary>
        /// /////////////
        /// 
        /// </summary>
        private static BindingList<ProductBalanceClass> _productsBalance;
        public static BindingList<ProductBalanceClass> ProductsBalance
        {
            get
            {
                if (_productsBalance == null)
                {
                    using (var db = new Entities())
                    {
                        var data = from sl in db.StoreLogs
                                   group sl by new { sl.ProductId, sl.StoreId } into g
                                   select new ProductBalanceClass
                                   {
                                       Balance =( g.Where(x => x.IsInTransaction == true).Sum(x => (double?)x.Qty) ?? 0) -
                                       (g.Where(x => x.IsInTransaction == false).Sum(x => (double?)x.Qty) ?? 0),
                                       ProductId = g.Key.ProductId,
                                       StoreId = g.Key.StoreId,
                                   };

                        _productsBalance = new BindingList<ProductBalanceClass>(data.ToList());
                    }
                    DatabaseWatcher.StoreLog = new TableDependency.SqlClient.SqlTableDependency<StoreLog>(connectionString);
                    DatabaseWatcher.StoreLog.OnChanged += DatabaseWatcher.StoreLog_Changed;
                    DatabaseWatcher.StoreLog.Start();
                }
                return _productsBalance;
            }
        }

        public class ProductBalanceClass
        {
            public int ProductId { get; set; }
            public int StoreId { get; set; }
            public double Balance { get; set; }
        }


        private static BindingList<_DAL.CustomerAndVendor> _vendors;
        public static BindingList<_DAL.CustomerAndVendor> Vendors
        {
            get
            {

                if (_vendors == null)
                {
                    using (var db = new Entities())
                    {
                        _vendors = new BindingList<CustomerAndVendor>(db.CustomerAndVendors.Where(x => x.IsCustomer == false).ToList());
                    }
                    DatabaseWatcher.Vendors = new TableDependency.SqlClient.SqlTableDependency<CustomerAndVendor>(connectionString, filter: new DatabaseWatcher.VendorsOnly());
                    DatabaseWatcher.Vendors.OnChanged += DatabaseWatcher.Vendors_Changed;
                    DatabaseWatcher.Vendors.Start();
                }
                return _vendors;
            }
        }


        private static BindingList<_DAL.CustomerAndVendor> _customers;
        public static BindingList<_DAL.CustomerAndVendor> Customers
        {
            get
            {
                if (_customers == null)
                {
                    using (var db = new Entities())
                    {
                        _customers = new BindingList<CustomerAndVendor>(db.CustomerAndVendors.Where(x => x.IsCustomer == true).ToList());
                    }
                    DatabaseWatcher.Customers = new TableDependency.SqlClient.SqlTableDependency<CustomerAndVendor>(connectionString, filter: new DatabaseWatcher.VendorsOnly());
                    DatabaseWatcher.Customers.OnChanged += DatabaseWatcher.Customers_Changed;
                    DatabaseWatcher.Customers.Start();
                }
                return _customers;
            }
        }


        private static BindingList<_DAL.Drawer> _drawer;
        public static BindingList<_DAL.Drawer> Drawer
        {
            get
            {
                if (_drawer == null)
                {
                    using (var db = new Entities())
                    {
                        _drawer = new BindingList<Drawer>(db.Drawers.ToList());

                    }
                }
                return _drawer;
            }
        }


        private static BindingList<_DAL.Store> _store;
        public static BindingList<_DAL.Store> Store
        {
            get
            {
                if (_store == null)
                {
                    using (var db = new Entities())
                    {
                        _store = new BindingList<Store>(db.Stores.ToList());

                    }
                }
                return _store;
            }
        }

        private static BindingList<UnitName> _unitNames;
        public static BindingList<UnitName> UnitNames
        {
            get
            {
                if (_unitNames == null)
                {
                    using (Entities ctx = new Entities())
                    {
                        _unitNames = new BindingList<UnitName>(ctx.UnitNames.ToList());
                    }
                }
                return _unitNames;
            }
        }


        public static class CurrentSettings
        {
            public static Master.PrintMode InvoicePrintMode { get => Master.PrintMode.ShowPreview; }

        }
        private static UserSettingsTemplate _userSettings;
        public static UserSettingsTemplate UserSettings
        {
            get
            {
                if (_userSettings == null)
                    _userSettings = new UserSettingsTemplate(User.SettingsProfileId);
                return _userSettings;
            }
        }
        //private static BindingList<_DAL.UserSetttingsProfileProperty> _profileProperties;
        //public static BindingList<_DAL.UserSetttingsProfileProperty> profileProperties
        //{
        //    get
        //    {
        //        if (_profileProperties == null)
        //        {
        //            using (var db = new Entities())
        //            {
        //                _profileProperties = new BindingList<UserSetttingsProfileProperty>(db.UserSetttingsProfileProperties.ToList());

        //            }
        //        }
        //        return _profileProperties;
        //    }
        //}

        private static _DAL.User _user;
        public static _DAL.User User { get { return _user; } }
        public static void SetUser(_DAL.User user)
        {
            _user = user;
            using(var db = new Entities())
            _screenAccess = (from s in Screens.GetScreens
                             from d in db.UserAccessProfileDetails
                             .Where(x => x.UserAccessProfileId == user.ScreenProfileId && x.ScreenId == s.ScreenId).DefaultIfEmpty()
                             select new ScreenAccessProfile(s.ScreenName)
                             {
                                 ScreenName = s.ScreenName,
                                 ParentScreenId = s.ParentScreenId,
                                 ScreenCaption = s.ScreenCaption,
                                 ScreenId = s.ScreenId,
                                 Actions = s.Actions,
                                 CanAdd = (d == null) ? true : d.CanAdd,
                                 CanDelete = (d == null) ? true : d.CanDelete,
                                 CanOpen = (d == null) ? true : d.CanOpen,
                                 CanPrint = (d == null) ? true : d.CanPrint,
                                 CanEdit = (d == null) ? true : d.CanEdit,
                                 CanShow = (d == null) ? true : d.CanShow,
                             }).ToList();
        }
        private static List<Class.ScreenAccessProfile> _screenAccess;
        public static List<Class.ScreenAccessProfile> ScreenAccess
        {
            get
            {
                //if(User.UserType == (byte)Master.UserType.SystemAdmin)
                //{
                //    return Screens.GetScreens;
                //}
                return _screenAccess;
            }
        }

        public static class GlobalSettings
        {
            public static bool ReadFromScaleBarcode { get => true; }
            public static string ScaleBarcodePrefix { get => "20"; }
            public static byte BarcodeLength { get => 13; }
            public static byte ProductCodeLength { get => 5; }
            public static byte ValueCodeLength { get => 5; }
            public static ReadValueMode ReadMode { get => ReadValueMode.Weight; }
            public static bool IgnoreCheckDigit { get => true; }
            public static byte DivideValueBy { get => 3; }
            public enum ReadValueMode
            {
                Weight,
                Price
            }

        }
    }

}