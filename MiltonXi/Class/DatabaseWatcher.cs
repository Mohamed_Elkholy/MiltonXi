﻿using MiltonXi._DAL;
using MiltonXi.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base.Abstracts;
using TableDependency.SqlClient.Base.Enums;
using TableDependency.SqlClient.Base.EventArgs;

namespace MiltonXi.Class
{
    public static class DatabaseWatcher
    {
        public static SqlTableDependency<Product> Products;
        public static void Products_Changed(object sender, RecordChangedEventArgs<Product> e)
        {

            Application.OpenForms[0].Invoke(new Action(() =>
            {
                if (e.ChangeType == ChangeType.Insert)
                {
                    Session.Products.Add(e.Entity);
                    Session.ProductsView.Add(Session.ProductViewClass.GetProduct(e.Entity.Id));
                }
                else if (e.ChangeType == ChangeType.Update)
                {
                    var index = Session.Products.IndexOf(Session.Products.Single(x => x.Id == e.Entity.Id));
                    Session.Products.Remove(Session.Products.Single(x => x.Id == e.Entity.Id));
                    Session.Products.Insert(index, e.Entity);

                    var viewIndex = Session.ProductsView.IndexOf(Session.ProductsView.Single(x => x.Id == e.Entity.Id));
                    Session.ProductsView.Remove(Session.ProductsView.Single(x => x.Id == e.Entity.Id));
                    Session.ProductsView.Insert(viewIndex, Session.ProductViewClass.GetProduct(e.Entity.Id));

                }
                else if (e.ChangeType == ChangeType.Delete)
                {
                    Session.Products.Remove(Session.Products.Single(x => x.Id == e.Entity.Id));
                    Session.ProductsView.Remove(Session.ProductsView.Single(x => x.Id == e.Entity.Id));
                }
            }));
        }


        public static SqlTableDependency<StoreLog> StoreLog;
        public static void StoreLog_Changed(object sender, RecordChangedEventArgs<StoreLog> e)
        {

            frm_MainForm.Instance.Invoke(new Action(() =>
            {
                var balance = MasterInventory.GetProductBalanceInStore(e.Entity.ProductId, e.Entity.StoreId);
                    Session.ProductsBalance.Remove(Session.ProductsBalance.FirstOrDefault(x => x.ProductId == e.Entity.ProductId &&
                    x.StoreId == e.Entity.StoreId) );
                    Session.ProductsBalance.Add(new Session.ProductBalanceClass()
                    {
                        Balance = balance,
                        ProductId = e.Entity.ProductId,
                        StoreId = e.Entity.StoreId,
                    });


               
        
            }));
        }


        public class VendorsOnly : ITableDependencyFilter
        {
            public string Translate()
            {
                return "[IsCustomer] = 0";
            }
        }
        public static SqlTableDependency<CustomerAndVendor> Vendors;
        public static void Vendors_Changed(object sender,RecordChangedEventArgs<CustomerAndVendor> e)
        {
            Application.OpenForms[0].Invoke(new Action(() =>
            {
                switch (e.ChangeType)
                {
                    case ChangeType.None:
                        break;
                    case ChangeType.Delete:
                        Session.Vendors.Remove(Session.Vendors.Single(x => x.Id == e.Entity.Id));
                        break;
                    case ChangeType.Insert:
                        Session.Vendors.Add(e.Entity);
                        break;
                    case ChangeType.Update:
                        var index = Session.Vendors.IndexOf(Session.Vendors.Single(x => x.Id == e.Entity.Id));
                        Session.Vendors.Remove(Session.Vendors.Single(x => x.Id == e.Entity.Id));
                        Session.Vendors.Insert(index, e.Entity);
                        break;
                    default:
                        break;
                }
            }));
        }

        public class CustomersOnly : ITableDependencyFilter
        {
            public string Translate()
            {
                return "[IsCustomer] = 1";
            }
        }
        public static SqlTableDependency<CustomerAndVendor> Customers;
        public static void Customers_Changed(object sender, RecordChangedEventArgs<CustomerAndVendor> e)
        {
            Application.OpenForms[0].Invoke(new Action(() =>
            {
                switch (e.ChangeType)
                {
                    case ChangeType.None:
                        break;
                    case ChangeType.Delete:
                        Session.Vendors.Remove(Session.Customers.Single(x => x.Id == e.Entity.Id));
                        break;
                    case ChangeType.Insert:
                        Session.Vendors.Add(e.Entity);
                        break;
                    case ChangeType.Update:
                        var index = Session.Customers.IndexOf(Session.Customers.Single(x => x.Id == e.Entity.Id));
                        Session.Customers.Remove(Session.Customers.Single(x => x.Id == e.Entity.Id));
                        Session.Customers.Insert(index, e.Entity);
                        break;
                    default:
                        break;
                }
            }));
        }

    }
}
