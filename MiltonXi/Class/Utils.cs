﻿using DevExpress.LookAndFeel;
using DevExpress.Utils.Svg;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Class
{
    public static class Utils
    {
        public static void AddButtonToGroupHeader(this GridView view, SvgImage img, EventHandler eventHandler)
        {
            var customClass = new CustomHeaderButtonClass(view, img, eventHandler);

        }
        public class CustomHeaderButtonClass
        {
            public CustomHeaderButtonClass(GridView _view, SvgImage _img, EventHandler _eventHandler)
            {
                view = _view;
                svgImage = _img;
                Event = _eventHandler;
                view.CustomDrawGroupPanel += View_CustomDrawGroupPanel;
                view.MouseMove += View_MouseMove;
                view.Click += View_Click;
            }

            private void View_Click(object sender, EventArgs e)
            {
                DevExpress.Utils.DXMouseEventArgs ea = e as DevExpress.Utils.DXMouseEventArgs;
                if (r.Contains(ea.Location))
                    Event(sender, e);
            }

            private void View_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                IsInRectangle = r.Contains(e.Location);
                view.Invalidate();
            }

            GridView view;
            SvgImage svgImage;
            EventHandler Event;
            int svgSize = 16;
            bool IsInRectangle;
            Rectangle r;
            private void View_CustomDrawGroupPanel(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
            {

                //Brush brush2 = e.Cache.GetGradientBrush(e.Bounds, Color.LightYellow, Color.WhiteSmoke,
                //    System.Drawing.Drawing2D.LinearGradientMode.Horizontal);
                //e.Cache.FillRectangle(brush2, e.Bounds);
                SvgBitmap bitmap = SvgBitmap.Create(svgImage);
                r = new Rectangle(e.Bounds.X + e.Bounds.Width - (svgSize * 3),
                   e.Bounds.Y + ((e.Bounds.Height - svgSize) / 2), svgSize, svgSize);
                var palette = SvgPaletteHelper.GetSvgPalette(UserLookAndFeel.Default,
                    DevExpress.Utils.Drawing.ObjectState.Normal);
                e.Cache.DrawImage(bitmap.Render(palette), r);
                int thickness = IsInRectangle ? 2 : 1;
                int Offset = thickness + 1;
                e.Cache.DrawRectangle(r.X - Offset, r.Y - Offset, r.Width + (Offset * 2), r.Height + (Offset * 2), Color.Black, thickness);



                e.Handled = true;
            }
        }

        public static void AddClearValueButton(this PopupBaseEdit edit)
        {
            edit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)});
            edit.ButtonClick += (sender, e) =>
            {
                if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
                    ((PopupBaseEdit)sender).EditValue = null;
            };
        }

    }
}
