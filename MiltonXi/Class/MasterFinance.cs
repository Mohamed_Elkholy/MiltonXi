﻿using MiltonXi._DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Class
{
    public static class MasterFinance
    {
        public static AccountBalance GetAccountBalance(int accountId)
        {
            using (var db = new Entities())
            {
                var query = db.Journals.Where(x => x.AccountId == accountId).Select(x => new { x.Debit, x.Credit });
                var totalCredit = query.Sum(x => (double?)x.Credit) ?? 0;
                var totalDebit = query.Sum(x => (double?)x.Debit) ?? 0;
                var account = db.Accounts.Single(x => x.Id == accountId);
                var b = new AccountBalance(accountId, account.Name, Math.Abs(totalCredit - totalDebit),
                    (totalCredit > totalDebit) ? AccountBalance.BalanceTypes.Credit : AccountBalance.BalanceTypes.Debit);
                return b;
            }
        }

        public static AccountBalance GetAccountBalance(this _DAL.Account account,int accountId)
        {
            return  GetAccountBalance(accountId);
        }

        public class AccountBalance
        {
            public AccountBalance(int id, string name, double amount, BalanceTypes balanceType)
            {
                Id = id;
                Name = name;
                BalanceAmount = amount;
                BalanceType = balanceType;
            }

            public int Id { get; }
            public string Name { get; }
            public double BalanceAmount { get; }
            public BalanceTypes BalanceType { get; }

            public string Balance
            {
                get
                {
                    return BalanceAmount.ToString() + " " + ((BalanceType == BalanceTypes.Credit) ? BalanceTypes.Credit.ToString() : BalanceTypes.Debit.ToString());
                }
            }

            public enum BalanceTypes
            {
                Credit = 1,
                Debit,
            }
        }
    }
}
