﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using MiltonXi._DAL;
using MiltonXi.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiltonXi.Class
{
    public static class Master
    {
        public enum UserType { SystemAdmin = 1, SystemUser = 2 }
        public static List<ValueAndID> UserTypeList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)UserType.SystemAdmin, Name = UserType.SystemAdmin.ToString() },
                new ValueAndID() { Id = (int)UserType.SystemUser, Name = UserType.SystemUser.ToString() } };

        public enum Actions { Show=1,Open=2,Add=3,Edit=4,Delete=5,Print=6}
        public class ValueAndID
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public enum WarningLevels { DoNotInterrupt=1, ShowWarning=2, Prevent=3 }
        public static List<ValueAndID> WarningLevelsList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)WarningLevels.DoNotInterrupt, Name = WarningLevels.DoNotInterrupt.ToString() },
                new ValueAndID() { Id = (int)WarningLevels.ShowWarning, Name = WarningLevels.ShowWarning.ToString() },
                new ValueAndID() { Id = (int)WarningLevels.Prevent, Name = WarningLevels.Prevent.ToString() } };
        public enum PaymentMethod { Credit=1, Cash}
        public static List<ValueAndID> PaymentMethodList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)PaymentMethod.Credit, Name = PaymentMethod.Credit.ToString() },
                new ValueAndID() { Id = (int)PaymentMethod.Cash, Name = PaymentMethod.Cash.ToString() } };

        public enum Currency { PLN, EUR }
        public static List<ValueAndID> CurrencyList = new List<ValueAndID>()
        {
            new ValueAndID(){Id=(int)Currency.PLN,Name = Currency.PLN.ToString() },
            new ValueAndID(){Id=(int)Currency.EUR,Name = Currency.EUR.ToString() },
        };
        public enum PrintMode
        {
            Direct,
            ShowPreview,
            ShowDialog,
        }
        public enum CostDistributionOptions
        {
            ByPrice,
            ByQty
        }
        public enum SourceType
        {
            Sales,
            Purchase,
            SalesReturn,
            PurchaseReturn
        }
        public enum InvoiceType
        {
            Sales = SourceType.Sales,
            Purchase = SourceType.Purchase,
            SalesReturn = SourceType.SalesReturn,
            PurchaseReturn = SourceType.PurchaseReturn
        }
        public static List<ValueAndID> InvoiceTypesList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)InvoiceType.Sales, Name = InvoiceType.Sales.ToString() },
                new ValueAndID() { Id = (int)InvoiceType.Purchase, Name = InvoiceType.Purchase.ToString() },
                new ValueAndID() { Id = (int)InvoiceType.SalesReturn, Name = InvoiceType.SalesReturn.ToString() },
                new ValueAndID() { Id = (int)InvoiceType.PurchaseReturn, Name = InvoiceType.PurchaseReturn.ToString() },};

        public enum ProductType { Service, Inventory }
        public static List<ValueAndID> ProductTypesList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)ProductType.Service, Name = ProductType.Service.ToString() },
                new ValueAndID() { Id = (int)ProductType.Inventory, Name = ProductType.Inventory.ToString() } };


        public enum PartyType { Customer, Vendor }
        public static List<ValueAndID> PartyTypesList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)PartyType.Customer, Name = PartyType.Customer.ToString() },
                new ValueAndID() { Id = (int)PartyType.Vendor, Name = PartyType.Vendor.ToString() } };


        #region Extension Methods

        public static void HideColumns(this BaseEdit edit)
        {
            var cols = ((LookUpEdit)edit).Properties.Columns.Where(x => x.FieldName != "Name");
                foreach (var col in cols)
                {
                    col.Visible = false;
                }
        }

        public static void InitializeData(this RepositoryItemLookUpEditBase repo, object dataSource, GridColumn column, DevExpress.XtraGrid.GridControl grid)
        {

            InitializeData(repo, dataSource, column, grid, "Name", "Id");
        }
        public static void InitializeData(this RepositoryItemLookUpEditBase repo, object dataSource, GridColumn column, DevExpress.XtraGrid.GridControl grid, string displayMember, string valueMember)
        {
            if (repo == null)
            {
                repo = new RepositoryItemLookUpEdit();
            }
            repo.DataSource = dataSource;
            column.ColumnEdit = repo;
            repo.ValueMember = valueMember;
            repo.DisplayMember = displayMember;
            repo.NullText = string.Empty;
            repo.BestFitMode = BestFitMode.BestFitResizePopup;
            if (grid != null)
                grid.RepositoryItems.Add(repo);

        }

        public static void InitializeData(this LookUpEdit lkp, object dataSource)
        {
            InitializeData(lkp, dataSource, "Name", "Id");
        }
        public static void InitializeData(this LookUpEdit lkp, object dataSource, string displayMember, string valueMember)
        {
            lkp.Properties.DataSource = dataSource;
            lkp.Properties.DisplayMember = displayMember;
            lkp.Properties.ValueMember = valueMember;
            lkp.Properties.ForceInitialize();
            lkp.Properties.PopulateColumns();
            lkp.Properties.ShowHeader = false;
            lkp.Properties.NullText = "";
            if (lkp.Properties.Columns.Count > 1)
            {
                HideColumns(lkp);
            }
        }
        public static void InitializeData(this LookUpEdit lkp, object dataSource, string displayMember)
        {
            InitializeData(lkp, dataSource, displayMember, "Id");
        }

        public static void InitializeData(this LookUpEdit lkp, object dataSource, string valueMember, object passNull)
        {
            InitializeData(lkp, dataSource, "Name", valueMember);
        }
        public static void InitializeData(this LookUpEdit lkp,List<ValueAndID> datasouce )
        {
            lkp.Properties.DataSource = datasouce;
            lkp.Properties.NullValuePrompt = "";
            lkp.Properties.DisplayMember = "Name";
            lkp.Properties.ValueMember = "Id";
            lkp.Properties.Columns.Clear();
            lkp.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo()
            {
                FieldName = "Name",

            });
            lkp.Properties.ShowHeader = false;

        }



        public static void InitializeData(this GridLookUpEdit lkp, object dataSource)
        {
            InitializeData(lkp, dataSource, "Name", "Id");
        }
        public static void InitializeData(this GridLookUpEdit lkp, object dataSource, string displayMember)
        {
            InitializeData(lkp, dataSource, displayMember, "Id");
        }
        public static void InitializeData(this GridLookUpEdit lkp, object dataSource, string valueMember, object passNull)
        {
            InitializeData(lkp, dataSource, "Name", valueMember);
        }
        public static void InitializeData(this GridLookUpEdit lkp, string displayMember, string valueMember)
        {
            lkp.Properties.DisplayMember = displayMember;
            lkp.Properties.ValueMember = valueMember;
        }
        public static void InitializeData(this GridLookUpEdit lkp, object dataSource, string displayMember, string valueMember)
        {

            lkp.Properties.DataSource = dataSource;
            lkp.Properties.DisplayMember = displayMember;
            lkp.Properties.ValueMember = valueMember;
            lkp.Properties.PopulateViewColumns();
            lkp.Properties.ValidateOnEnterKey = true;
            lkp.Properties.AllowNullInput = DefaultBoolean.False;
            lkp.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            lkp.Properties.ImmediatePopup = true;
            lkp.Properties.View.FocusRectStyle = DrawFocusRectStyle.RowFullFocus;
            lkp.Properties.View.OptionsSelection.UseIndicatorForSelection = true;
            lkp.Properties.View.OptionsView.ShowAutoFilterRow = true;
            lkp.Properties.View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            lkp.Properties.NullText = "";

        }

        public static void SetSpinEditPercentage(this SpinEdit spn)
        {
            spn.Properties.Increment = 0.01m;
            spn.Properties.Mask.EditMask = "p";
            spn.Properties.Mask.UseMaskAsDisplayFormat = true;
            spn.Properties.MaxValue = 1;
        }

        public static int FindRowHandleByRowObject(this GridView view, Object row)
        {
            if (row != null)
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (row.Equals(view.GetRow(i)))
                        return i;
                }
            }
            return DevExpress.XtraGrid.GridControl.InvalidRowHandle;
        }
        #endregion

        public static bool IsTextValid(this TextEdit txt)
        {
            if (txt.Text.Trim() == string.Empty)
            {
                txt.ErrorText = frm_Master.ErrorRequired;
                return false;
            }
            return true;
        }

        public static bool IsEditValueNumberAndNotZero(this LookUpEditBase lkp)
        {
            var val = lkp.EditValue;
            if ((val is DBNull))
            {
                lkp.ErrorText = frm_Master.ErrorRequired;
                return false;
            }
            if ((  val is int || val is byte || val is double || val is float || val is decimal|| 
                val is Master.WarningLevels|| val is Master.PaymentMethod|| val is Master.UserType ) & (Convert.ToInt32(val) != 0))
            {
                return true;
            }
            lkp.ErrorText = frm_Master.ErrorRequired;
            return false;

        }
        public static bool IsEditValueOfTypeInt(this LookUpEditBase lkp)
        {
           
            int num;
            if (lkp.EditValue != null && int.TryParse(lkp.EditValue.ToString(), out num))
            {
                return true;
            }
            return false;
        }
        public static bool IsDateValid(this DateEdit dt)
        {
            if (dt.DateTime.Year < 1950)
            {
                dt.ErrorText = frm_Master.ErrorRequired;
                return false;
            }
            return true;
        }

        public static string GetNextNumberInString(string Number)
        {
            if (Number == string.Empty || Number == null)
                return "1";
            string str1 = "";
            foreach (char c in Number)
                str1 = char.IsDigit(c) ? str1 + c.ToString() : "";
            if (str1 == string.Empty)
                return "1";
            string str2 = str1.Insert(0, "1");
            str2 = (Convert.ToInt64(str2) + 1).ToString();
            string str3 = str2[0] == '1' ? str2.Remove(0, 1) : str2.Remove(0, 1).Insert(0, "1");
            int idx = Number.LastIndexOf(str1);
            Number = Number.Remove(idx);
            Number = Number.Insert(idx, str3);
            return Number;
        }


        public static T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(data))
            {
                object obj = (T)formatter.Deserialize(stream);
                return (T)obj;
            }
        }
        public static byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                 formatter.Serialize(stream,obj);
                if (stream.Length ==0)
                    return new byte[1];
                return stream.ToArray();
            }
        }
        public static byte[] GetPropertyValue(string propertyName, int profileId)
        {
            using (var db = new Entities())
            {
                var prop = db.UserSetttingsProfileProperties.SingleOrDefault(x => x.ProfileId == profileId && x.PropertyName == propertyName);
                if (prop == null)
                    return null;
                return prop.PropertyValue.ToArray();
            }
        }

        public static string GetCallerName([CallerMemberName]string callerName = "")
        {
            return callerName;
        }

    }
}
