﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Class
{
    public class UserSettingsTemplate
    {
        int ProfileId { get; set; }

        public UserSettingsTemplate(int profileId)
        {
            ProfileId = profileId;
            General = new GeneralSettings(profileId);
            Invoice = new InvoiceSettings(profileId);
            Sales = new SalesSettings(profileId);
            Purchase = new PurchaseSettings(profileId);
        }

        public GeneralSettings General { get; set; }
        public InvoiceSettings Invoice { get; set; }
        public SalesSettings Sales { get; set; }
        public PurchaseSettings Purchase { get; set; }

        public static string GetPropCaption(string propName)
        {

            switch (propName)
            {

                case nameof(UserSettingsTemplate.General): return "General Settings";
                case nameof(UserSettingsTemplate.Invoice): return "Invoice Settings";
                case nameof(UserSettingsTemplate.Sales): return "Sales Settings";
                case nameof(UserSettingsTemplate.Purchase): return "Purchase Settings";
                case nameof(UserSettingsTemplate.General.CanChangeCustomer): return "Can Change Customer";
                case nameof(UserSettingsTemplate.General.CanChangeVendor): return "Can Change Vendor";
                case nameof(UserSettingsTemplate.General.CanChangeDrawer): return "Can Change Drawer";
                case nameof(UserSettingsTemplate.General.CanChangeBranch): return "Can Change Branch";
                case nameof(UserSettingsTemplate.General.CanViewDocumentHistory): return "Can View Document History";



                case nameof(UserSettingsTemplate.General.DefaultStore): return "Default Store";
                case nameof(UserSettingsTemplate.General.DefaultRawStore): return "Default Raw Store";
                case nameof(UserSettingsTemplate.General.DefaultDrawer): return "Default Drawer";
                case nameof(UserSettingsTemplate.General.DefaultCustomer): return "Default Customer";
                case nameof(UserSettingsTemplate.General.DefaultVendor): return "Default Vendor";

                case nameof(UserSettingsTemplate.Sales.DefaultPaymentMethodInSales): return "Default Payment Method";


                case nameof(UserSettingsTemplate.Sales.WhenSellingToACustomerExceedingMaxCredit): return "When Selling To A Customer Exceeding Max Credit";
                case nameof(UserSettingsTemplate.Sales.WhenSellingItemWithPriceLowerThanCostPrice): return "When Selling Item With Price Lower Than Cost Price";

                case nameof(UserSettingsTemplate.Sales.MaxDiscountInInvoice): return "Max Discount In Invoice";
                case nameof(UserSettingsTemplate.Sales.MaxDiscountPerItem): return "Max Discount Per Item";

                case nameof(UserSettingsTemplate.Sales.CanChangeItemPriceInSales): return "Can Change Item Price";
                case nameof(UserSettingsTemplate.Sales.CanChangePaidInInSales): return "Can Change Paid";
                case nameof(UserSettingsTemplate.Sales.CanChangeQtyInSales): return "Can Change Qty ";
                case nameof(UserSettingsTemplate.Sales.CanChangeSalesInvoiceDate): return "Can Change Invoice Date";
                case nameof(UserSettingsTemplate.Sales.CanNotPostToStoreInSales): return "Can Issue Unposted Invoice";
                case nameof(UserSettingsTemplate.Sales.CanSellToVendors): return "Can Sell To Vendors";

                case nameof(UserSettingsTemplate.Purchase.CanChangePurchaseInvoiceDate): return "Can Change Invoice Date";
                case nameof(UserSettingsTemplate.Purchase.CanChangeItemPriceInPurchase): return "Can Change Item Price";
                case nameof(UserSettingsTemplate.Purchase.CanBuyFromCustomers): return "Can Buy From Customers";

                case nameof(UserSettingsTemplate.Invoice.CanChangeTax): return "Can Change Tax";
                case nameof(UserSettingsTemplate.Invoice.CanDeleteItemInInvoices): return "Can Delete Item";
                case nameof(UserSettingsTemplate.Invoice.WhenSellingItemReachedReorderLevel): return "When Selling Item Reached Reorder Level";
                case nameof(UserSettingsTemplate.Invoice.WhenSellingItemWithQtyMoreThanAvailableQty): return "When Selling Item Qty more than Available Qty";

                default: return "$" + propName + "$";

            }
        }
        public static BaseEdit GetPropertyControl(string propName,object propertyValue)
        {
            BaseEdit edit = null;
            switch (propName)
            {
                case nameof(UserSettingsTemplate.General.CanChangeDrawer):
                case nameof(UserSettingsTemplate.General.CanViewDocumentHistory):
                case nameof(UserSettingsTemplate.General.CanChangeBranch):
                case nameof(UserSettingsTemplate.General.CanChangeVendor):
                case nameof(UserSettingsTemplate.General.CanChangeCustomer):
                case nameof(UserSettingsTemplate.Invoice.CanChangeTax):
                case nameof(UserSettingsTemplate.Invoice.CanDeleteItemInInvoices):
                case nameof(UserSettingsTemplate.Sales.CanChangeItemPriceInSales):
                case nameof(UserSettingsTemplate.Sales.CanChangePaidInInSales):
                case nameof(UserSettingsTemplate.Sales.CanChangeQtyInSales):
                case nameof(UserSettingsTemplate.Sales.CanChangeSalesInvoiceDate):
                case nameof(UserSettingsTemplate.Sales.CanNotPostToStoreInSales):
                case nameof(UserSettingsTemplate.Sales.CanSellToVendors):
                case nameof(UserSettingsTemplate.Sales.HideCostInSales):
                case nameof(UserSettingsTemplate.Purchase.CanBuyFromCustomers):
                case nameof(UserSettingsTemplate.Purchase.CanChangeItemPriceInPurchase):
                case nameof(UserSettingsTemplate.Purchase.CanChangePurchaseInvoiceDate):
                    edit = new ToggleSwitch();
                    ((ToggleSwitch)edit).Properties.OnText = "Allow";
                    ((ToggleSwitch)edit).Properties.OffText = "Deny";
                    break;
                case nameof(UserSettingsTemplate.General.DefaultStore):
                case nameof(UserSettingsTemplate.General.DefaultRawStore):
                    edit = new LookUpEdit();
                    ((LookUpEdit)edit).InitializeData(Session.Store);
                   ((LookUpEdit)edit).HideColumns();
                    break;
                case nameof(UserSettingsTemplate.General.DefaultDrawer):
                    edit = new LookUpEdit();
                    ((LookUpEdit)edit).InitializeData(Session.Drawer);
                    ((LookUpEdit)edit).HideColumns();
                    break;
                case nameof(UserSettingsTemplate.General.DefaultCustomer):
                    edit = new LookUpEdit();
                    ((LookUpEdit)edit).InitializeData(Session.Customers);
                    ((LookUpEdit)edit).HideColumns();
                    break;
                case nameof(UserSettingsTemplate.General.DefaultVendor):
                    edit = new LookUpEdit();
                    ((LookUpEdit)edit).InitializeData(Session.Vendors);
                    ((LookUpEdit)edit).HideColumns();
                    break;
                case nameof(UserSettingsTemplate.Sales.DefaultPaymentMethodInSales):
                    edit = new LookUpEdit();
                    ((LookUpEdit)edit).InitializeData(Master.PaymentMethodList);
                    ((LookUpEdit)edit).HideColumns();


                    break;
                case nameof(UserSettingsTemplate.Invoice.WhenSellingItemWithQtyMoreThanAvailableQty):
                case nameof(UserSettingsTemplate.Invoice.WhenSellingItemReachedReorderLevel):
                case nameof(UserSettingsTemplate.Sales.WhenSellingToACustomerExceedingMaxCredit):
                case nameof(UserSettingsTemplate.Sales.WhenSellingItemWithPriceLowerThanCostPrice):
                    edit = new LookUpEdit();
                  ((LookUpEdit)edit).InitializeData(Master.WarningLevelsList);
                    ((LookUpEdit)edit).HideColumns();

                    break;

                case nameof(UserSettingsTemplate.Sales.MaxDiscountInInvoice):
                case nameof(UserSettingsTemplate.Sales.MaxDiscountPerItem):
                    edit = new SpinEdit();
                    ((SpinEdit)edit).Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    ((SpinEdit)edit).SetSpinEditPercentage();

                    break;

                default:
                    break;
            }
            

            

            if (edit != null)
            {
                edit.Name = propName;
                edit.Properties.NullText = "";
                edit.EditValue = propertyValue;
            }
            return edit;
        }
       
    }
    
    public class InvoiceSettings
    {
        int ProfileId { get; set; }
        public InvoiceSettings(int profileId)
        {
            ProfileId = profileId;
        }
        public Master.WarningLevels WhenSellingItemReachedReorderLevel { get { return (Master.FromByteArray<Master.WarningLevels>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId))); } }
        public Master.WarningLevels WhenSellingItemWithQtyMoreThanAvailableQty { get { return Master.FromByteArray<Master.WarningLevels>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeTax { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanDeleteItemInInvoices { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }

    }

    public class PurchaseSettings
    {
        int ProfileId { get; set; }
        public PurchaseSettings(int profileId)
        {
            ProfileId = profileId;
        }
        public bool CanChangeItemPriceInPurchase { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanBuyFromCustomers { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangePurchaseInvoiceDate { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
    }

    public class SalesSettings
    {
        int ProfileId { get; set; }
        public SalesSettings(int profileId)
        {
            ProfileId = profileId;
        }
        public Master.PaymentMethod DefaultPaymentMethodInSales { get { return Master.FromByteArray<Master.PaymentMethod>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public Master.WarningLevels WhenSellingToACustomerExceedingMaxCredit { get { return Master.FromByteArray<Master.WarningLevels>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public Master.WarningLevels WhenSellingItemWithPriceLowerThanCostPrice { get { return Master.FromByteArray<Master.WarningLevels>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public decimal MaxDiscountInInvoice { get { return Master.FromByteArray<decimal>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public decimal MaxDiscountPerItem { get { return Master.FromByteArray<decimal>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangePaidInInSales { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanNotPostToStoreInSales { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeItemPriceInSales { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanSellToVendors { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeSalesInvoiceDate { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeQtyInSales { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool HideCostInSales { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
    }

    public class GeneralSettings
    {
        int ProfileId { get; set; }
        public GeneralSettings(int profileId)
        {
            ProfileId = profileId;
        }
        public int DefaultRawStore { get { return Master.FromByteArray<int>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public int DefaultStore { get { return Master.FromByteArray<int>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public int DefaultDrawer { get { return Master.FromByteArray<int>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public int DefaultCustomer { get { return Master.FromByteArray<int>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public int DefaultVendor { get { return Master.FromByteArray<int>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeBranch { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeDrawer { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeCustomer { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanChangeVendor { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }
        public bool CanViewDocumentHistory { get { return Master.FromByteArray<bool>(Master.GetPropertyValue(Master.GetCallerName(), ProfileId)); } }


    }
}
