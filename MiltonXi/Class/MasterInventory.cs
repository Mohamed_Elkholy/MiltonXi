﻿using MiltonXi._DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MiltonXi.Class.Master;

namespace MiltonXi.Class
{
    public static class MasterInventory
    {
        public enum CostCalculationMethod { FIFO = 1, LIFO = 2, WAVP = 3 }
        public static List<ValueAndID> CostCalculationMethodList = new List<ValueAndID>() {
                new ValueAndID() { Id = (int)CostCalculationMethod.FIFO, Name = "First in first out" },
                new ValueAndID() { Id = (int)CostCalculationMethod.LIFO, Name = "Last in first out" },
                new ValueAndID() { Id = (int)CostCalculationMethod.WAVP, Name = "Weighted average" },
        };
        public static double GetCostOfNextProduct(int productId, int storeId, double qty)
        {
            using (var db = new Entities())
            {

                var query = db.StoreLogs.Where(sl => sl.ProductId == productId && sl.StoreId == storeId).OrderBy(sl => sl.InsertDate);

                if (query.ToList().Count <= 0)
                {
                    return Session.ProductsView.Single(x => x.Id == productId).Units.OrderByDescending(x => x.Factor).First().BuyPrice;
                }

                double TotalQtyOut = (query.Where(q => q.IsInTransaction == false).Sum(q => (double?)q.Qty)) ?? 0;

                double Balance = GetProductBalanceInStore(productId, storeId);
                if (Balance <= 0)
                {
                    return 0;
                }
                var subQuery = query
                    .Where(q => query.Where(q1 => q1.IsInTransaction == true && q1.InsertDate <= q.InsertDate)
                    .Sum(q1 => q1.Qty) > TotalQtyOut && q.IsInTransaction == true).ToList();
                var subQueryBalance = (subQuery.Where(q => q.IsInTransaction).Sum(q => (double?)q.Qty)) ?? 0 - (subQuery.Where(q => q.IsInTransaction == false).Sum(q => (double?)q.Qty)) ?? 0;
                if (subQueryBalance > Balance)
                {
                    var diff = subQueryBalance - Balance;
                     if(subQuery.Count>0)
                    subQuery[0].Qty -= diff;
                }

                double Fifo;
                double Lifo;
                double WAC;

                if (subQuery.Count > 0 && subQuery[0].Qty < qty)
                {
                    int i = 0;
                    var qtyx = qty;
                    double SumPrice = 0;
                    while (qtyx > 0 && i < subQuery.Count)
                    {
                        var row = subQuery[i];
                        double qty1 = (qtyx > row.Qty) ? row.Qty : qtyx;
                        SumPrice += qty1 * row.CostValue;
                        qtyx -= qty1;
                        i++;
                    }
                    Fifo = SumPrice / (qty - qtyx);
                }
                else
                {
                    if (subQuery.Count > 0)
                        Fifo = subQuery.FirstOrDefault().CostValue;
                    else
                        return 0;
                }



                subQuery = subQuery.OrderByDescending(q => q.InsertDate).ToList();
                if (subQuery.Count > 0 && subQuery[0].Qty < qty)
                {
                    int i = 0;
                    var qtyx = qty;
                    double SumPrice = 0;
                    while (qtyx > 0 && i < subQuery.Count)
                    {
                        var row = subQuery[i];
                        double qty1 = (qtyx > row.Qty) ? row.Qty : qtyx;
                        SumPrice += qty1 * row.CostValue;
                        qtyx -= qty1;
                        i++;
                    }
                    Lifo = SumPrice / (qty - qtyx);
                }
                else
                {
                    Lifo = subQuery.First().CostValue;
                }

                WAC = subQuery.Select(q => q.Qty * q.CostValue).Sum(q => q) / Balance;

                var method = (CostCalculationMethod)Session.Products.Single(x => x.Id == productId).CostCalculationMethod;
                switch (method)
                {
                    case CostCalculationMethod.FIFO:
                        return Fifo;
                    case CostCalculationMethod.LIFO:
                        return Lifo;
                    case CostCalculationMethod.WAVP:
                        return WAC;
                    default:
                        return Fifo;
                }
            }
        }

        public static double GetProductBalanceInStore(int productId, int storeId)
        {
            using (var db = new Entities())
            {

                var query = db.StoreLogs.Where(sl => sl.ProductId == productId && sl.StoreId == storeId);
                double TotalQtyOut = query.Where(q => q.IsInTransaction == false).Sum(q => (double?)q.Qty) ?? 0;
                double TotalQtyIn = query.Where(q => q.IsInTransaction == true).Sum(q => (double?)q.Qty) ?? 0;
                double Balance = TotalQtyIn - TotalQtyOut;
                return Balance;
            }
        }
    }
}
