﻿using MiltonXi.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.Class
{
    public class ScreenAccessProfile
    {

        public static int MaxId = 1;
        public int ParentScreenId { get; set; }
        public int ScreenId { get; set; }
        public string ScreenName { get; set; }
        public string ScreenCaption { get; set; }
        public bool CanShow { get; set; }
        public bool CanOpen { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool CanPrint { get; set; }
        public List<Master.Actions> Actions { get; set; }
        public ScreenAccessProfile(string name, ScreenAccessProfile parent = null)
        {
            if (parent != null)
                ParentScreenId = parent.ScreenId;
            else
                ParentScreenId = 0;
            ScreenName = name;
            ScreenId = MaxId++;
            Actions = new List<Master.Actions> {
            Master.Actions.Add,
            Master.Actions.Edit,
            Master.Actions.Delete,
            Master.Actions.Print,
            Master.Actions.Show,
            Master.Actions.Open,
            };
        }
    }
        public static class Screens
        {
            public static ScreenAccessProfile MainSettings = new ScreenAccessProfile("elm_MainSettings")
            {
                Actions = new List<Master.Actions>() { Master.Actions.Show },
                ScreenCaption = "Company"
            };
        public static ScreenAccessProfile CompanyInfo = new ScreenAccessProfile(nameof(frm_CompanyInfo), MainSettings) {
           
                ScreenCaption ="Company Info"};

        public static ScreenAccessProfile Customers = new ScreenAccessProfile("elm_Customers") {
                ScreenCaption ="Cusomters",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
            };
            public static ScreenAccessProfile NewCustomer = new ScreenAccessProfile("frm_Customer", Customers) { ScreenCaption="New Customer"};
            public static ScreenAccessProfile CustomerList = new ScreenAccessProfile("frm_CustomerList", Customers) { ScreenCaption="Customers"};

        public static ScreenAccessProfile Vendors = new ScreenAccessProfile("elm_Vendors")
        {
            ScreenCaption = "Vendors",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile NewVendor = new ScreenAccessProfile("frm_Vendor", Vendors) { ScreenCaption = "New Vendor" };
        public static ScreenAccessProfile VendorList = new ScreenAccessProfile("frm_VendrosList", Vendors) { ScreenCaption = "Vendors List" };

        public static ScreenAccessProfile Branches = new ScreenAccessProfile("elm_Branches")
        {
            ScreenCaption = "Branches",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile NewBranch = new ScreenAccessProfile(nameof(frm_Store), Branches) { ScreenCaption = "New Branch" };
        public static ScreenAccessProfile BranchesList = new ScreenAccessProfile(nameof(frm_StoresList), Branches) { ScreenCaption = "Branches List" };


        public static ScreenAccessProfile Drawers = new ScreenAccessProfile("elm_Drawers")
        {
            ScreenCaption = "Drawers",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile NewDrawers = new ScreenAccessProfile(nameof(frm_Drawer), Drawers) { ScreenCaption = "New Drawer" };
        public static ScreenAccessProfile DrawersList = new ScreenAccessProfile(nameof(frm_DrawerList), Drawers) { ScreenCaption = "Drawer List" };

        public static ScreenAccessProfile Products = new ScreenAccessProfile("elm_Products")
        {
            ScreenCaption = "Products",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile NewProduct = new ScreenAccessProfile(nameof(frm_Product), Products) { ScreenCaption = "New Product" };
        public static ScreenAccessProfile ProductsList = new ScreenAccessProfile(nameof(frm_ProductList), Products) { ScreenCaption = "Products & Services" };
        public static ScreenAccessProfile ProductCategory = new ScreenAccessProfile(nameof(frm_ProductCategory), Products) { ScreenCaption = "Product Category" };


        public static ScreenAccessProfile SalesInvoices = new ScreenAccessProfile("elm_SalesInvoices")
        {
            ScreenCaption = "Sales",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile frm_NewSalesInvoice = new ScreenAccessProfile("frm_SalesInvoice", SalesInvoices) { ScreenCaption = "New Sales Invoice" };
        public static ScreenAccessProfile frm_SalesInvoiceList = new ScreenAccessProfile("frm_SalesInvoiceList", SalesInvoices) { ScreenCaption = "Sales Invoice List" };



        public static ScreenAccessProfile PurchasingInvoices = new ScreenAccessProfile("elm_PurchasingInvoices")
        {
            ScreenCaption = "Purchasing",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile frm_NewPurchaseInvoice = new ScreenAccessProfile("frm_PurchaseInvoice", PurchasingInvoices) { ScreenCaption = "New Purchase Invoice" };
        public static ScreenAccessProfile frm_PurchaseInvoiceList = new ScreenAccessProfile("frm_PurchaseInvoiceList", PurchasingInvoices) { ScreenCaption = "Purchase Invoice List" };




        public static ScreenAccessProfile UsersAndSettings = new ScreenAccessProfile("elm_UsersAndSettings")
        {
            ScreenCaption = "Users And Settings",
            Actions = new List<Master.Actions>() { Master.Actions.Show },
        };
        public static ScreenAccessProfile frm_RoleAccessPermissions = new ScreenAccessProfile("frm_RoleAccessPermissions", UsersAndSettings) { ScreenCaption = "New RoleAccessPermissions" };
        public static ScreenAccessProfile frm_RoleAccessPermissionList = new ScreenAccessProfile("frm_RoleAccessPermissionList", UsersAndSettings) { ScreenCaption = "Role Access Permissions List" };
        public static ScreenAccessProfile frm_UserRoleTemplate = new ScreenAccessProfile("frm_UserRoleTemplate", UsersAndSettings) { ScreenCaption = "New User Role Template" };
        public static ScreenAccessProfile frm_UserRoleTemplateList = new ScreenAccessProfile("frm_UserRoleTemplateList", UsersAndSettings) { ScreenCaption = "User Role Template List" };
        public static ScreenAccessProfile frm_User = new ScreenAccessProfile("frm_User", UsersAndSettings) { ScreenCaption = "New User" };
        public static ScreenAccessProfile frm_UserList = new ScreenAccessProfile("frm_UserList", UsersAndSettings) { ScreenCaption = "User List" };

        public static List<ScreenAccessProfile> GetScreens
            {
                get
                {
                    Type t = typeof(Screens);
                    FieldInfo[] fields = t.GetFields(BindingFlags.Public | BindingFlags.Static);
                    var list = new List<ScreenAccessProfile>();
                    foreach (var item in fields)
                    {
                        var obj = item.GetValue(null);
                        if (obj != null && obj.GetType() == typeof(ScreenAccessProfile))
                            list.Add((ScreenAccessProfile)obj);
                    }
                    return list;
                }
            }
        }
}
