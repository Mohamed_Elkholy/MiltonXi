﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using MiltonXi.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MiltonXi.Class.Master;

namespace MiltonXi.UserControls
{
    public class CostDistributionOption : XtraUserControl
    {
        public RadioGroup group = new RadioGroup();
        public CostDistributionOptions SelectedOption { get
            {
                if (group.EditValue != null)
                    return ((CostDistributionOptions)group.EditValue);
                else
                    return CostDistributionOptions.ByQty;
            } }
        public CostDistributionOption()
        {
            LayoutControl lc = new LayoutControl();
            group.Properties.Items.AddRange(
                new DevExpress.XtraEditors.Controls.RadioGroupItem[]
                {
                    new DevExpress.XtraEditors.Controls.RadioGroupItem(CostDistributionOptions.ByPrice,"By Price"),
                    new DevExpress.XtraEditors.Controls.RadioGroupItem(CostDistributionOptions.ByQty,"By Quantity")
                });
            this.Controls.Add(lc);
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            
            group.EditValue = CostDistributionOptions.ByQty; 
            lc.Dock = System.Windows.Forms.DockStyle.Fill;
            lc.AddItem("Method of allocating expenses to items", group).TextLocation= DevExpress.Utils.Locations.Top;
        }
    }
}
