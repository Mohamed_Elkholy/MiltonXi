﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using MiltonXi.Forms;
using System.Diagnostics;
using MiltonXi._Logger;
using MiltonXi.Properties;
using DevExpress.LookAndFeel;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace MiltonXi
{
    static class Program
    {
        //Log4Net
        // private static readonly log4net.ILog log = LogHelper.GetLogger();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (Properties.Settings.Default.UpgradeRequired)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpgradeRequired = false;
                Properties.Settings.Default.Save();
            }
            UserLookAndFeel.Default.SkinName = Settings.Default.SkinName;
            UserLookAndFeel.Default.SetSkinStyle(Settings.Default.SkinName, Settings.Default.PaletteName);
            frm_Login frm = new frm_Login();
                frm.Show();
            if (Debugger.IsAttached)
            {
                log4net.GlobalContext.Properties["UserName"] = LogHelper.UserName;
                log4net.GlobalContext.Properties["UserId"] = LogHelper.UserId;
                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", System.Configuration.ConfigurationManager.AppSettings["FirebaseJsonTest"]);
                Utils.FireStore.projectID = "milton-ed8ee";

                frm.txt_Username.Text = "admin";
                frm.txt_Password.Text = "admin";
                frm.btn_Login.PerformClick();
                Application.Run();


                //Application.Run(new frm_RoleAccessPermissions());
                //log.Debug("Debug from non debug mode");
                //log.Error("New Error from non debug mode");
                // log.Fatal("Fatal error ");
            }
            else
            {
                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", System.Configuration.ConfigurationManager.AppSettings["FirebaseJson"]);
                Utils.FireStore.projectID = "milton-prod-a4db0";
                Application.Run(new frm_MainForm());
            }

        }
    }
}
