﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    class DispatchFormsStatusModel
    {
        [FirestoreDocumentId]
        public string DocumentID { get; set; }

        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreDocumentCreateTimestamp]
        public Timestamp CreateTime { get; set; }
        [FirestoreDocumentUpdateTimestamp]
        public Timestamp UpdateTime { get; set; }
        [FirestoreDocumentReadTimestamp]
        public Timestamp ReadTime { get; set; }



        [FirestoreProperty]
        public int CurrentStatus { get; set; }
        /*
         0 New
         1 At the Load
         2 Loaded
         3 At the Unload
         4 Unloaded
             */
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Transport Order Number is required")]
        public string TransportOrderNumber { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck is required")]
        public string TruckID { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Name is required")]
        public string TruckName { get; set; }
        [FirestoreProperty]
        public string MultiStopPlanReference { get; set; }
    }
}
