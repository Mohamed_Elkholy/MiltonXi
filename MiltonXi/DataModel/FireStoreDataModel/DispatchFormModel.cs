﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;
using System.ComponentModel.DataAnnotations;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class DispatchFormModel 
    {


        [FirestoreDocumentId]
        public string DocumentID { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LastUpdateDate { get; set; }
        
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Created By is required")]
        public string CreatedBy { get; set; }

        [FirestoreProperty]
        public int CurrentStatus { get; set; }
        /*
         0 Nutral
         1 At the Load
         2 Loaded
         3 At the Unload
         4 Unloaded
             */

        // Dispatch Form
        [FirestoreProperty]
        public string TransportOrderNumber { get; set; }
        [FirestoreProperty]
        public string ExternalReference { get; set; }
        [FirestoreProperty]
        public string TruckID { get; set; }
        [FirestoreProperty]
        public string TruckName { get; set; }
        [FirestoreProperty]
        public string TrailerName { get; set; }
        [FirestoreProperty]
        public string DriverID { get; set; }
        // Load Details
        [FirestoreProperty]
        public string LoadingAddress { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LoadingDateTimeFrom { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LoadingDateTimeTo { get; set; }
        [FirestoreProperty]
        public string LoadingReferenceNumber { get; set; }
        // Unload Details
        [FirestoreProperty]
        public string UnloadingAddress { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? UnloadingDateTimeFrom { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? UnloadingDateTimeTo { get; set; }
        [FirestoreProperty]
        public string UnloadingReferenceNumber { get; set; }
        // Load Information
        [FirestoreProperty]
        public int NumberOfLoads { get; set; }
        [FirestoreProperty]
        public int TotalWeight { get; set; }
        [FirestoreProperty]
        public string LoadDetails { get; set; }
        // Route Details
        [FirestoreProperty]
        public string RouteInformation { get; set; }
        [FirestoreProperty]
        public bool TollRoads { get; set; }
        [FirestoreProperty]
        public bool VignettesNeeded { get; set; }
        [FirestoreProperty]
        public string FerryInformation { get; set; }
        [FirestoreProperty]
        public string ContactInformation { get; set; }
        [FirestoreProperty]
        public string MultiStopPlanReference { get; set; }
        [FirestoreProperty]
        public string Notes { get; set; }
        [FirestoreProperty]
        public bool Archived { get; set; }
        [FirestoreProperty]
        public bool ADRCategory21 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory22 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory23 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory3 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory41 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory42 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory43 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory51 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory52 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory61 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory62 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory8 { get; set; }
        [FirestoreProperty]
        public bool ADRCategory9 { get; set; }
        [FirestoreProperty]
        public bool NotifyDriver { get; set; }
        [FirestoreProperty]
        public bool Loaded { get; set; }
        //[FirestoreProperty]
        //public DateTimeOffset LoadedDateTime { get; set; }
        [FirestoreProperty]
        public Dictionary<string, string> LoadedImageURL { get; set; }
        [FirestoreProperty]
        public bool Unloaded { get; set; }
        //[FirestoreProperty]
        //public DateTimeOffset UnloadedDateTime { get; set; }
        [FirestoreProperty]
        public Dictionary<string, string> UnloadedImageURL { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? DriverLastReadDateTime { get; set; }
        [FirestoreProperty]
        public bool Deleted { get; set; }

        [FirestoreProperty]
        public string CustomsClearance { get; set; }

        [FirestoreProperty]
        public DateTimeOffset? AtTheLoadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? AtTheUnloadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LoadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? UnloadedDateTime { get; set; }



    }
}
