﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class InvoicingSheetsModel 
    {
        [FirestoreDocumentId]
        public string DocumentID { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LastUpdateDate { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        //[FirestoreDocumentCreateTimestamp]
        //public Timestamp CreateTime { get; set; }
        //[FirestoreDocumentUpdateTimestamp]
        //public Timestamp UpdateTime { get; set; }
        //[FirestoreDocumentReadTimestamp]
        //public Timestamp ReadTime { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck is required")]
        public string TruckID { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Name is required")]
        public string TruckName { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Transport Order Number is required")]
        public string TransportOrderNumber { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ExternalReference is required")]
        public string ExternalReference { get; set; }

        [FirestoreProperty]
        public DateTimeOffset? DateRecieved { get; set; }

        [FirestoreProperty]
        public string Currency { get; set; }
        [FirestoreProperty]
        public string CurrencyId { get; set; }

        [FirestoreProperty]
        public double VATPercentage { get; set; }
        [FirestoreProperty]
        public string NIPEUVATNumber { get; set; }
        [FirestoreProperty]
        public string BuyerAddress { get; set; }

        public string OriginalOrderLink { get; set; }

        [FirestoreProperty]
        public int DeadLine { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Departure Location is required")]
        public string DepartureLocation { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrival Location is required")]
        public string ArrivalLocation { get; set; }

        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Distance KM Value must be number")]
        [Range(0, double.MaxValue, ErrorMessage = "Distance KM must be Positive number")]
        public double DistanceKM { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Buyer is required")]
        public string Buyer { get; set; }

        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Total Order Worth NET Value must be number")]
        [Range(0, double.MaxValue, ErrorMessage = "Total Order Worth NET must be Positive number")]
        public double TotalOrderWorthNET { get; set; }

        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Total Order Worth GROSS Value must be number")]
        [Range(0, double.MaxValue, ErrorMessage = "Total Order Worth GROSS must be Positive number")]
        public double TotalOrderWorthGROSS { get; set; }


        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Rate per KM Value must be number")]
        [Range(0, double.MaxValue, ErrorMessage = "Rate per KM must be Positive number")]
        public double RatePerKM { get; set; }


        [FirestoreProperty]
        public DateTimeOffset? DateTimeCompleted { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? DateTimeCMRSent { get; set; }


        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invoice Number is required")]
        public string InvoiceNumber { get; set; }

        [FirestoreProperty]
        public DateTimeOffset? InvoiceIssuedDate { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invoice Link is required")]
        public string InvoiceLink { get; set; }


        [FirestoreProperty]
        public string PaymentDeadline { get; set; }

        [FirestoreProperty]
        public DateTimeOffset? PaymentDeadlineDate { get; set; }

        [FirestoreProperty]
        public DateTimeOffset? DatePaymentRecieved { get; set; }


        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Frieght Exchange is required")]
        public string FrieghtExchange { get; set; }

        [FirestoreProperty]
        public string Notes { get; set; }
        [FirestoreProperty]
        public bool Deleted { get; set; }
        [FirestoreProperty]
        public bool Archived { get; set; }


        [FirestoreProperty]
        [Display(Name = "Parent Invoice")]
        public string ParentInvoice { get; set; }

        [FirestoreProperty]
        public List<string> ChildInvoice { get; set; }

        [FirestoreProperty]
        [Display(Name = "Factoring Agent")]
        public string FactoringAgent { get; set; }
    }



}
