﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class ExpenseTypeModel
    {
        [FirestoreDocumentId]
        public string DocumentId { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        //[DisplayName("Expense Type")]
        public string Name { get; set; }
        [FirestoreProperty]
        [DisplayName("Index")]
        public int  Index { get; set; }
        [FirestoreProperty]
        [DisplayName("Archived")]
        public bool Archived { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Amount Of Liters")]
        public bool ShowAmountOfLiters { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Country Of Toll")]
        public bool ShowCountryOfToll { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Currency")]
        public bool ShowCurrency { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Expense Description")]
        public bool ShowExpenseDescription { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Paymenth Method")]
        public bool ShowPaymentMethod { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Receipt Number")]
        public bool ShowReceiptNumber { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Total Cost")]
        public bool ShowTotalCost { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Consumption")]
        public bool ShowConsumption { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Distance Travelled")]
        public bool ShowDistanceTravelled { get; set; }
        [FirestoreProperty]
        [DisplayName("Show Odometer")]
        public bool ShowOdometer { get; set; }
    }
}
