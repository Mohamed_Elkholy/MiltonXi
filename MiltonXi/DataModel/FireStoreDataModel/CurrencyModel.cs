﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class CurrencyModel
    {
        [FirestoreDocumentId]
        public string DocumentId { get; set; }

        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }

        [FirestoreProperty]
        [DisplayName("Currency")]
        [MaxLength(3)]
        public string Name { get; set; }

        [FirestoreProperty]
        [DisplayName("Index")]
        public int Index { get; set; }


        [FirestoreProperty]
        [DisplayName("Archived")]
        public bool Archived{ get; set; }

    
    }
}
