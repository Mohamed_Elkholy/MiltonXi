﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    public class InvoiceParentChild
    {
        private List<Children> childrenOrdersInternal = new List<Children>();

        private string parentInvoice;
        //  public List<string> childrenOrders;
        public InvoiceParentChild()
        {

        }
        public InvoiceParentChild(List<string> ChildrenOrders)
        {
            childrenOrdersInternal = new List<Children>();
            foreach (string item in ChildrenOrders)
            {
                this.childrenOrdersInternal.Add(new Children(item));
            }
        }
        public InvoiceParentChild(string ParentInvoice)
        {
            this.parentInvoice = ParentInvoice;
            
        }
        public InvoiceParentChild(string ParentInvoice, List<string> ChildrenOrders)
        {
            this.parentInvoice = ParentInvoice;
            childrenOrdersInternal = new List<Children>();
            foreach (string item in ChildrenOrders)
            {
                this.childrenOrdersInternal.Add(new Children(item));
            }
        }

        public string ParentInvoice
        {
            get { return parentInvoice; }
            set { parentInvoice = value; }
        }
        public List<Children> Chiledren
        {
            get { return childrenOrdersInternal; }
            set { childrenOrdersInternal = value; }
        }
            
    }

    public class Children
    {
        public string TransportOrderNumbers { get; set; }
        public Children(string TransportOrderNumbers)
        {
            this.TransportOrderNumbers = TransportOrderNumbers;
        }
    }
}
