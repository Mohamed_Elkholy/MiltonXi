﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    interface IMultiStopRoutePlanModel
    {
        string Address { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        string DispatchFormID { get; set; }
        string ExternalReference { get; set; }
        string ID { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }
        string Load_UnLoad { get; set; }
        string LoadDetails { get; set; }
        string MultiStopRoutePlanReference { get; set; }
        DocumentReference Reference { get; set; }
        DateTimeOffset TargetDateTime { get; set; }
        string TransportOrderNumber { get; set; }
        string TotalWeight { get; set; }

        int CurrentStatus { get; set; }
    }
}