﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    public interface ITruckModel
    {
        bool Archived { get; set; }
        bool AustriaLSticker { get; set; }
        int CargoHeight { get; set; }
        int CargoLength { get; set; }
        int CargoWidth { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        string EuroCateogry { get; set; }
        bool FranceCritAir { get; set; }
        int GrossWeight { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }
        int LoadingWeight { get; set; }
        bool LondonLowEmissionZone { get; set; }
        int MaximumTrailerGrossWeight { get; set; }
        string PhoneNumber { get; set; }
        DocumentReference Reference { get; set; }
        string TrailerFullName { get; set; }
        string TrailerID { get; set; }
        string TruckFullName { get; set; }
        string ID { get; set; }
        string TruckName { get; set; }
        string TruckRegistrationNumber { get; set; }
        DateTimeOffset TruckTechnicalInspectionExpiryDate { get; set; }
        bool ViennaLowEmissionZone { get; set; }
        Dictionary<string, DateTimeOffset> Vignettes { get; set; }
         string DriverID { get; set; }
         string DriverName { get; set; }
    }
}