﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
   public interface ITransportOrderInvoiceModel
    {
        //Timestamp CreateTime { get; set; }
        //Timestamp ReadTime { get; set; }
        //Timestamp UpdateTime { get; set; }
        bool Archived { get; set; }
        string ArrivalLocation { get; set; }
        string Buyer { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }
        DateTimeOffset CreatedDate { get; set; }


        DateTimeOffset? DatePaymentRecieved { get; set; }
        DateTimeOffset? DateRecieved { get; set; }
        DateTimeOffset? DateTimeCMRSent { get; set; }
        DateTimeOffset? DateTimeCompleted { get; set; }
        DateTimeOffset? PaymentDeadlineDate { get; set; }
        int DeadLine { get; set; }
        bool Deleted { get; set; }
        string DepartureLocation { get; set; }
        double DistanceKM { get; set; }
        string DocumentID { get; set; }
        string ExternalReference { get; set; }
        string FrieghtExchange { get; set; }
        DateTimeOffset? InvoiceIssuedDate { get; set; }
        string InvoiceLink { get; set; }
        string InvoiceNumber { get; set; }
        string Notes { get; set; }
        string PaymentDeadline { get; set; }
        double RatePerKM { get; set; }
        DocumentReference Reference { get; set; }
        double TotalOrderWorthGROSS { get; set; }
        double TotalOrderWorthNET { get; set; }
        string TransportOrderNumber { get; set; }
        string TruckID { get; set; }
        string TruckName { get; set; }
        double VATPercentage { get; set; }
        string NIPEUVATNumber { get; set; }
        string CurrencyId { get; set; }
        string Currency { get; set; }
        string BuyerAddress { get; set; }
        string ParentInvoice { get; set; }
        List<string> ChildInvoice { get; set; }
        string FactoringAgent { get; set; }
    }
}