﻿using System;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    interface ITrailerModel
    {
        string ID { get; set; }
        DocumentReference Reference { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }

        string TrailerName { get; set; }
        string TrailerFullName { get; set; }
        string TrailerRegistrationNumber { get; set; }
        DateTimeOffset TrailerTechnicalInspectionExpiryDate { get; set; }
        // Cargo Space Dimensions
        int CargoHeight { get; set; }
        int CargoWidth { get; set; }
        int CargoLength { get; set; }

        int GrossWeight { get; set; }
         int LoadingWeight { get; set; }



        bool Archived { get; set; }
    }
}