﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    public interface IDispatchFormModel
    {
        string CreatedBy { get; set; }
        bool ADRCategory21 { get; set; }
        bool ADRCategory22 { get; set; }
        bool ADRCategory23 { get; set; }
        bool ADRCategory3 { get; set; }
        bool ADRCategory41 { get; set; }
        bool ADRCategory42 { get; set; }
        bool ADRCategory43 { get; set; }
        bool ADRCategory51 { get; set; }
        bool ADRCategory52 { get; set; }
        bool ADRCategory61 { get; set; }
        bool ADRCategory62 { get; set; }
        bool ADRCategory8 { get; set; }
        bool ADRCategory9 { get; set; }
        bool Archived { get; set; }
        string ContactInformation { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        Timestamp CreateTime { get; set; }
        string DriverID { get; set; }
        string ExternalReference { get; set; }
        string FerryInformation { get; set; }
        string DocumentID { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }
        string LoadDetails { get; set; }



        //DateTimeOffset LoadedDateTime { get; set; }
        Dictionary<string, string> LoadedImageURL { get; set; }
        string LoadingAddress { get; set; }
        DateTimeOffset LoadingDateTimeFrom { get; set; }
        DateTimeOffset LoadingDateTimeTo { get; set; }
        string LoadingReferenceNumber { get; set; }
        string MultiStopPlanReference { get; set; }
        string Notes { get; set; }
        bool NotifyDriver { get; set; }
        int NumberOfLoads { get; set; }
        Timestamp ReadTime { get; set; }

        string RouteInformation { get; set; }
        bool TollRoads { get; set; }
        int TotalWeight { get; set; }
        string TrailerName { get; set; }
        string TrailerId { get; set; }
        string TransportOrderNumber { get; set; }
        string TruckID { get; set; }
        string TruckName { get; set; }
        string CustomsClearance { get; set; }


        //DateTimeOffset UnloadedDateTime { get; set; }
        Dictionary<string, string> UnloadedImageURL { get; set; }
        string UnloadingAddress { get; set; }
        DateTimeOffset UnloadingDateTimeFrom { get; set; }
        DateTimeOffset UnloadingDateTimeTo { get; set; }
        string UnloadingReferenceNumber { get; set; }
        Timestamp UpdateTime { get; set; }
        bool VignettesNeeded { get; set; }
        DateTimeOffset DriverLastReadDateTime { get; set; }
        bool Deleted { get; set; }
        int CurrentStatus { get; set; }
    }
}