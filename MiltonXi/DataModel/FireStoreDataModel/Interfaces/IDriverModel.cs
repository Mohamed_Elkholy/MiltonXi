﻿using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    public interface IDriverModel
    {
        DateTimeOffset ADRCertificateExpiryDate { get; set; }
        string ADRCertificateNumber { get; set; }
        bool Archived { get; set; }
        DateTimeOffset CPCCardExpiryDate { get; set; }
        string CPCCardNumber { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        Timestamp CreateTime { get; set; }
        Dictionary<string, DateTimeOffset> DriverDelegations { get; set; }
        string DriverName { get; set; }
        bool DrivingLicenseCategoryB { get; set; }
        bool DrivingLicenseCategoryBE { get; set; }
        bool DrivingLicenseCategoryC { get; set; }
        bool DrivingLicenseCategoryCE { get; set; }
        DateTimeOffset DrivingLicenseExpiryDate { get; set; }
        string DrivingLicenseNumber { get; set; }

        string ID { get; set; }
        DateTimeOffset LastUpdateDate { get; set; }
        DateTimeOffset PassportExpiryDate { get; set; }
        string PassportNumber { get; set; }

        DateTimeOffset? ResidenceCardExpiryDate { get; set; }
        string ResidenceCardNumber { get; set; }

        string PhoneNumber { get; set; }
        Timestamp ReadTime { get; set; }
        DocumentReference Reference { get; set; }
        DateTimeOffset TachoCardExpiryDate { get; set; }
        string TachoCardNumber { get; set; }
        Timestamp UpdateTime { get; set; }
      
    }
}