﻿using System;
using System.ComponentModel.DataAnnotations;
using Google.Cloud.Firestore;


namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class TrailerModel 
    {

        [FirestoreDocumentId]
        public string DocumentID { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset LastUpdateDate { get; set; }
        [FirestoreDocumentCreateTimestamp]
        public Timestamp CreateTime { get; set; }
        [FirestoreDocumentUpdateTimestamp]
        public Timestamp UpdateTime { get; set; }
        [FirestoreDocumentReadTimestamp]
        public Timestamp ReadTime { get; set; }



        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Trailer Number is required")]
        public string TrailerName { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Trailer Number is required")]
        public string TrailerFullName { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Trailer Registration Number is required")]
        public string TrailerRegistrationNumber { get; set; }

        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Trailer Height Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Trailer _Height must be Positive number")]
        public int CargoHeight { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Trailer Width Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Trailer Width must be Positive number")]
        public int CargoWidth { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Trailer Length Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Trailer Length must be Positive number")]
        public int CargoLength { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Trailer Length Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Gross Weight must be Positive number")]
        public int GrossWeight { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "Trailer Length Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Loading Weight must be Positive number")]
        public int LoadingWeight { get; set; }

        [FirestoreProperty]
        public DateTimeOffset TrailerTechnicalInspectionExpiryDate { get; set; }



        [FirestoreProperty]
        public bool Archived { get; set; }

    }
}
