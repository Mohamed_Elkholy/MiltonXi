﻿using Google.Cloud.Firestore;
using MiltonXi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    class DispatchFormTimeStamp
    {

        public DateTime? AtTheLoaded { get { return Utilities.setToLocalTime(AtTheLoadedDateTime); } set { AtTheLoaded = Utilities.setToLocalTime(value); } }
        public DateTime? AtTheUnloaded { get { return Utilities.setToLocalTime(AtTheUnloadedDateTime); } set { AtTheUnloaded = Utilities.setToLocalTime(value); } }
        public DateTime? Loaded { get { return Utilities.setToLocalTime(LoadedDateTime); } set { Loaded = Utilities.setToLocalTime(value); } }
        public DateTime? Unloaded { get { return Utilities.setToLocalTime(UnloadedDateTime); } set { Unloaded = Utilities.setToLocalTime(value); } }
        public DateTime? CreatedDateTime { get { return Utilities.setToLocalTime(CreatedDate); } set { CreatedDate = Utilities.setToLocalTime(value); } }

        [FirestoreProperty]
        public DateTimeOffset? AtTheLoadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? AtTheUnloadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? LoadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? UnloadedDateTime { get; set; }
        [FirestoreProperty]
        public DateTimeOffset? CreatedDate { get; set; }


    }
}
