﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    class DispatchFormCounter
    {
        [FirestoreDocumentId]
        public string DocumentId { get; set; }

        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }

        [FirestoreProperty]
        public int January { get; set; }

        [FirestoreProperty]
        public int February { get; set; }

        [FirestoreProperty]
        public int March { get; set; }

        [FirestoreProperty]
        public int April { get; set; }

        [FirestoreProperty]
        public int May { get; set; }

        [FirestoreProperty]
        public int Jun { get; set; }

        [FirestoreProperty]
        public int July { get; set; }

        [FirestoreProperty]
        public int August { get; set; }

        [FirestoreProperty]
        public int September { get; set; }

        [FirestoreProperty]
        public int October { get; set; }

        [FirestoreProperty]
        public int November { get; set; }

        [FirestoreProperty]
        public int December { get; set; }


        public void resetCounters()
        {
            January = 0;
            February = 0;
            March = 0;
            April = 0;
            May = 0;
            Jun = 0;
            July = 0;
            August = 0;
            September = 0;
            October = 0;
            November = 0;
            December = 0;
        }
    }
}
