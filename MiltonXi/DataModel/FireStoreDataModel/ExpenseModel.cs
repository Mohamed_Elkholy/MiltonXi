﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class ExpenseModel
    {
        [FirestoreDocumentId]
        public string DocumentId { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        [DisplayName("Expense Type")]
        public string expenseType { get; set; }
        [FirestoreProperty]
        [DisplayName("DateTime")]
        public DateTimeOffset? dateTime { get; set; }
        [FirestoreProperty]
        [DisplayName("User DateTime")]
        public DateTimeOffset? userInputDate { get; set; }
        [FirestoreProperty]
        [DisplayName("Location")]
        public string location { get; set; }
        [FirestoreProperty]
        [DisplayName("Geo Location")]
        public GeoPoint? locationGeo { get; set; }
        [FirestoreProperty]
        [DisplayName("Amount Of Liters")]
        public double amountOfLiters { get; set; }
        [FirestoreProperty]
        [DisplayName("Total Cost")]
        public double totalCost { get; set; }
        [FirestoreProperty]
        [DisplayName("Currency")]
        public string currency { get; set; }
        [FirestoreProperty]
        [DisplayName("Receipt Number")]
        public string receiptNumber { get; set; }
        [FirestoreProperty]
        [DisplayName("Payment Method")]
        public string paymentMethod { get; set; }
        [FirestoreProperty]
        [DisplayName("Expense Description")]
        public string expenseDescription { get; set; }
        [FirestoreProperty]
        [DisplayName("Country Of Toll")]
        public string countryOfToll { get; set; }
        [FirestoreProperty]
        [DisplayName("TruckName")]
        public string truckName { get; set; }
        [FirestoreProperty]
        [DisplayName("Comment")]
        public string comment { get; set; }

        [FirestoreProperty]
        [DisplayName("Odometer")]
        public string odometer { get; set; }

        [FirestoreProperty]
        [DisplayName("Distance Travelled")]
        public string distanceTravelled { get; set; }

        [FirestoreProperty]
        [DisplayName("Consumption")]
        public string consumption { get; set; }
        [FirestoreProperty]
        [DisplayName("UserID")]
        public string guid { get; set; }

    }
}
