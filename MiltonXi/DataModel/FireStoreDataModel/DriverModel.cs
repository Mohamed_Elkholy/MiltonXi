﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace MiltonXi.DataModel.FireStoreDataModel
{

    [FirestoreData]
    public class DriverModel 
    {

        [FirestoreDocumentId]
        public string DocumentID { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset LastUpdateDate { get; set; }
        [FirestoreDocumentCreateTimestamp]
        public Timestamp CreateTime { get; set; }
        [FirestoreDocumentUpdateTimestamp]
        public Timestamp UpdateTime { get; set; }
        [FirestoreDocumentReadTimestamp]
        public Timestamp ReadTime { get; set; }




        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Driver Name is required")]
        public string DriverName { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Passport Number is required")]
        public string PassportNumber { get; set; }
        [FirestoreProperty]

        public DateTimeOffset PassportExpiryDate { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Driving LicenseNumber is required")]
        public string DrivingLicenseNumber { get; set; }
        [FirestoreProperty]

        public DateTimeOffset DrivingLicenseExpiryDate { get; set; }


        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tacho Card Number is required")]
        public string TachoCardNumber { get; set; }
        [FirestoreProperty]

        public DateTimeOffset TachoCardExpiryDate { get; set; }

        [FirestoreProperty]
        public Dictionary<string, DateTimeOffset> DriverDelegations { get; set; }

        [FirestoreProperty]
        public string PhoneNumber { get; set; }

        [FirestoreProperty]
        public bool Archived { get; set; }





        [FirestoreProperty]
        public bool DrivingLicenseCategoryB { get; set; }
        [FirestoreProperty]
        public bool DrivingLicenseCategoryBE { get; set; }
        [FirestoreProperty]
        public bool DrivingLicenseCategoryC { get; set; }
        [FirestoreProperty]
        public bool DrivingLicenseCategoryCE { get; set; }

        [FirestoreProperty]

        public string ADRCertificateNumber { get; set; }
        [FirestoreProperty]
        public DateTimeOffset ADRCertificateExpiryDate { get; set; }
        [FirestoreProperty]
        public string CPCCardNumber { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CPCCardExpiryDate { get; set; }


    }
}
