﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Google.Cloud.Firestore;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
    public class TruckModel 
    {
        [FirestoreDocumentId]
        public string DocumentID { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset LastUpdateDate { get; set; }

        [FirestoreDocumentCreateTimestamp]
        public Timestamp CreateTime { get; set; }
        [FirestoreDocumentUpdateTimestamp]
        public Timestamp UpdateTime { get; set; }
        [FirestoreDocumentReadTimestamp]
        public Timestamp ReadTime { get; set; }

        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Name is required")]
        public string TruckName { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Full Name is required")]
        public string TruckFullName { get; set; }
        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Truck Registration Number is required")]
        public string TruckRegistrationNumber { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoHeight Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Cargo Height must be Positive number")]
        public int CargoHeight { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoWidth Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Cargo Width must be Positive number")]
        public int CargoWidth { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoDepth Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Cargo Length must be Positive number")]
        public int CargoLength { get; set; }
        [FirestoreProperty]
        public DateTimeOffset TruckTechnicalInspectionExpiryDate { get; set; }



        [FirestoreProperty]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Euro Cateogry is required")]
        public string EuroCateogry { get; set; }
        [FirestoreProperty]

        public Dictionary<string, DateTimeOffset> Vignettes { get; set; }

        [FirestoreProperty]
        public string PhoneNumber { get; set; }

        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoDepth Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Cargo Length must be Positive number")]
        public int GrossWeight { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoDepth Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Cargo Length must be Positive number")]
        public int LoadingWeight { get; set; }
        [FirestoreProperty]
        [RegularExpression("^[0-9]\\d*(\\.\\d+)?$", ErrorMessage = "CargoDepth Value must be number")]
        [Range(1, int.MaxValue, ErrorMessage = "Value for Maximum Trailer GrossWeight must be Positive number")]
        public int MaximumTrailerGrossWeight { get; set; }

        [FirestoreProperty]
        public bool LondonLowEmissionZone { get; set; }
        [FirestoreProperty]
        public bool FranceCritAir { get; set; }
        [FirestoreProperty]
        public bool AustriaLSticker { get; set; }
        [FirestoreProperty]
        public bool ViennaLowEmissionZone { get; set; }

        [FirestoreProperty]
        public string TrailerFullName { get; set; }
        [FirestoreProperty]
        public string TrailerID { get; set; }
        [FirestoreProperty]
        public bool Archived { get; set; }

        [FirestoreProperty]
        public string DriverID { get; set; }
        [FirestoreProperty]
        public string DriverName { get; set; }
    }
}
