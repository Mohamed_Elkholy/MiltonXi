﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]
  public  class MultiStopRoutePlanModel : IMultiStopRoutePlanModel
    {
        [FirestoreDocumentId]
        public string ID { get; set; }
        [FirestoreDocumentId]
        public DocumentReference Reference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset LastUpdateDate { get; set; }
        [FirestoreProperty]
        public string MultiStopRoutePlanReference { get; set; }
        [FirestoreProperty]
        public string TransportOrderNumber { get; set; }
        [FirestoreProperty]
        public string ExternalReference { get; set; }
        [FirestoreProperty]
        public DateTimeOffset TargetDateTime { get; set; }
        [FirestoreProperty]
        public string Address { get; set; }
        [FirestoreProperty]
        public string LoadDetails { get; set; }
        [FirestoreProperty]
        public string Load_UnLoad { get; set; }
        [FirestoreProperty]
        public string DispatchFormID { get; set; }
        [FirestoreProperty]
        public string DriverID { get; set; }
        [FirestoreProperty]
        public string TruckID { get; set; }
        [FirestoreProperty]
        public bool Deleted { get; set; }
        [FirestoreProperty]
        public string TotalWeight { get;  set; }


        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public int CurrentStatus { get; set; }

    }
}
