﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiltonXi.DataModel.FireStoreDataModel
{
    [FirestoreData]

  public  class MultiStopHeader
    {
        [FirestoreProperty]
        public bool Archived { get; set; }
        [FirestoreProperty]
        public DateTimeOffset CreatedDate { get; set; }
        [FirestoreProperty]
        public DateTimeOffset LastUpdateDate { get; set; }
        [FirestoreProperty]
        public List<string> TransportOrderNumbers { get; set; }
        [FirestoreProperty]
        public string ID { get; set; }
        [FirestoreProperty]
        public string DriverID { get; set; }
        [FirestoreProperty]
        public string TruckID { get; set; }
        [FirestoreProperty]
        public bool NotifyDriver { get; set; }
        [FirestoreProperty]
        public bool Deleted { get; set; }
        [FirestoreProperty]
        public Dictionary<string, bool> CompletedLeg { get; set; }

        [FirestoreProperty]
        public string CreatedBy { get; set; }
        [FirestoreProperty]
        public int CurrentStatus { get; set; }
    }

 
}
