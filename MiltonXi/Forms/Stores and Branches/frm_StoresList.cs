﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;

namespace MiltonXi.Forms
{
    public partial class frm_StoresList : frm_Master
    {
        public frm_StoresList()
        {
            InitializeComponent();
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            RefreshData();
            gridView1.OptionsBehavior.Editable = false;
            gridView1.Columns["Id"].Visible = false;
            gridView1.DoubleClick += GridView1_DoubleClick;
            btn_Save.Enabled = false;
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {
                int id = 0;
                id = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Id"));
                string name = gridView1.GetFocusedRowCellValue("Name").ToString();
                Class.Utilities.OpenFormInMain(new frm_Store(id) { Tag = name });
                //frm_Store frm = new frm_Store(id);
                //frm.ShowDialog();
        
            }
        }

        public override void New()
        {
            base.New();
            frm_Store frm = new frm_Store();

            frm.ShowDialog();
         
        }

        public override void RefreshData()
        {
            using (_DAL.Entities ctx = new _DAL.Entities())
            {
                storeBindingSource.DataSource = ctx.Stores.ToList();
            }
            base.Refresh();
        }
    }
}