﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using System.Data.Entity.Migrations;
using MiltonXi.Class;

namespace MiltonXi.Forms
{
    public partial class frm_Store : Forms.frm_Master
    {

        public frm_Store()
        {
            InitializeComponent();
            New();
        }

        public frm_Store(int id)
        {
            InitializeComponent();
            using (_DAL.Entities ctx = new Entities())
            {
                storeBindingSource.DataSource = ctx.Stores.Where(s => s.Id == id).FirstOrDefault();
                this.Text = $"Branch/Stores: {(storeBindingSource.DataSource as Store).Name}";
            }
        }

        public override void Save()
        {
            base.Save();
            int SavedChangesCount = 0;
            Account SalesAccount = new Account();
            Account SalesReturnAccount = new Account();
            Account InventoryAccount = new Account();
            Account CostOfSoldGoodsAccount = new Account();

            using (Entities ctx = new Entities())
            {
                _DAL.Store store = storeBindingSource.DataSource as _DAL.Store;
                if (store.Id == 0)
                {
                    SalesAccount.Name = store.Name + "-Sales";
                    SalesReturnAccount.Name = store.Name + "-SalesReturn";
                    InventoryAccount.Name = store.Name + "-Inventory";
                    CostOfSoldGoodsAccount.Name = store.Name + "-CostOfSoldGoods";

                }
                else
                {
                    SalesAccount = ctx.Accounts.SingleOrDefault(x => x.Id == store.SalesAccountId)?? new Account() { Name = store.Name + "-Sales" };
                    SalesReturnAccount = ctx.Accounts.SingleOrDefault(x => x.Id == store.SalesReturnAccountId)?? new Account() { Name= store.Name + "-SalesReturn" };
                    InventoryAccount = ctx.Accounts.SingleOrDefault(x => x.Id == store.InventoryAccountId)?? new Account() { Name = store.Name + "-Inventory" };
                    CostOfSoldGoodsAccount = ctx.Accounts.SingleOrDefault(x => x.Id == store.CostOfSoldGoodsAccountId) ?? new Account() { Name= store.Name + "-Inventory" };

                }
                ctx.Accounts.AddOrUpdate(SalesAccount, SalesReturnAccount, InventoryAccount, CostOfSoldGoodsAccount);
                SavedChangesCount += ctx.SaveChanges();


                store.SalesAccountId = SalesAccount.Id;
                store.SalesReturnAccountId = SalesReturnAccount.Id;
                store.InventoryAccountId = InventoryAccount.Id;
                store.CostOfSoldGoodsAccountId = CostOfSoldGoodsAccount.Id;
                store.DiscountAllowedAccountId = Session.Defaults.DiscountAllowedAccountId;
                store.DiscountReceivedAccountId = Session.Defaults.DiscountReceivedAccountId;
                ctx.Stores.AddOrUpdate(store);
                SavedChangesCount += ctx.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    this.Text = $"Branch/Stores: {store.Name}";

                    DataSavedMessage();
               
                }
            }
        }


        public override void New()
        {
            storeBindingSource.DataSource = new Store();
            this.Text = "New branch/stores";
            base.New();
        }
        public override void Delete()
        {
            base.Delete();
            int SavedChangesCount = 0;

            Store store = storeBindingSource.DataSource as Store;
            using (Entities ctx = new Entities())
            {
                var logCount = ctx.StoreLogs.Where(x => x.StoreId == store.Id).Count();
                List<Journal> accountInUse = ctx.Journals.Where(x => x.AccountId == store.SalesAccountId ||
                 x.AccountId == store.SalesReturnAccountId ||
                 x.AccountId == store.InventoryAccountId ||
                 x.AccountId == store.CostOfSoldGoodsAccountId).ToList();
                int accountLogCount = accountInUse.Count();
                if (logCount + accountLogCount > 0)
                {
                    XtraMessageBox.Show("Unable to delete (branch/sotre) already in use", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                IQueryable<Account> accountsToDelete = ctx.Accounts.Where(x => x.Id == store.SalesAccountId || x.Id == store.SalesReturnAccountId ||
                x.Id == store.InventoryAccountId || x.Id == store.CostOfSoldGoodsAccountId);
                ctx.Accounts.RemoveRange(accountsToDelete);
                ctx.Stores.Attach(store);
                ctx.Stores.Remove(store);
                SavedChangesCount += ctx.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    New();
                    DataSavedMessage();
                }
            }
        }

        public override bool IsDataValid()
        {
            var store = (storeBindingSource.DataSource as Store);

            var stringProperties = store.GetType().GetProperties()
                           .Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(store, null);
                if (currentValue == null) continue;
                stringProperty.SetValue(store, currentValue.Trim(), null);
            }


            using (Entities ctx = new Entities())
            {
                var result = ctx.Stores.Where(x => x.Name == store.Name
                && store.Id != x.Id
                    ).Count();
                if (result > 0)
                {
                    DuplicateMessage();
                    return false;
                }
            }
            return true;
        }

        private void frm_Store_Load(object sender, EventArgs e)
        {

        }
    }
}