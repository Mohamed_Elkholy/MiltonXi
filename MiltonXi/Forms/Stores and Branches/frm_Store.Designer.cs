﻿namespace MiltonXi.Forms
{
    partial class frm_Store
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.storeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SalesAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SalesReturnAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.InventoryAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostOfSoldGoodsAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DiscountReceivedAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DiscountAllowedAccountIdSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDiscountAllowedAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSalesAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSalesReturnAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInventoryAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostOfSoldGoodsAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDiscountReceivedAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesReturnAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InventoryAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostOfSoldGoodsAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountReceivedAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountAllowedAccountIdSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountAllowedAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalesAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalesReturnAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInventoryAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostOfSoldGoodsAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountReceivedAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SalesAccountIdSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SalesReturnAccountIdSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.InventoryAccountIdSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostOfSoldGoodsAccountIdSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DiscountReceivedAccountIdSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DiscountAllowedAccountIdSpinEdit);
            this.dataLayoutControl1.DataSource = this.storeBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(657, 279);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(387, 53);
            this.NameTextEdit.MaximumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.MenuManager = this._barManager;
            this.NameTextEdit.MinimumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.NameTextEdit.Size = new System.Drawing.Size(221, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 5;
            // 
            // storeBindingSource
            // 
            this.storeBindingSource.DataSource = typeof(MiltonXi._DAL.Store);
            // 
            // SalesAccountIdSpinEdit
            // 
            this.SalesAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "SalesAccountId", true));
            this.SalesAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SalesAccountIdSpinEdit.Location = new System.Drawing.Point(387, 77);
            this.SalesAccountIdSpinEdit.MenuManager = this._barManager;
            this.SalesAccountIdSpinEdit.Name = "SalesAccountIdSpinEdit";
            this.SalesAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SalesAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SalesAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SalesAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.SalesAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.SalesAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SalesAccountIdSpinEdit.Properties.ReadOnly = true;
            this.SalesAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.SalesAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.SalesAccountIdSpinEdit.TabIndex = 12;
            // 
            // SalesReturnAccountIdSpinEdit
            // 
            this.SalesReturnAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "SalesReturnAccountId", true));
            this.SalesReturnAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SalesReturnAccountIdSpinEdit.Location = new System.Drawing.Point(387, 101);
            this.SalesReturnAccountIdSpinEdit.MenuManager = this._barManager;
            this.SalesReturnAccountIdSpinEdit.Name = "SalesReturnAccountIdSpinEdit";
            this.SalesReturnAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SalesReturnAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SalesReturnAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SalesReturnAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.SalesReturnAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.SalesReturnAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SalesReturnAccountIdSpinEdit.Properties.ReadOnly = true;
            this.SalesReturnAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.SalesReturnAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.SalesReturnAccountIdSpinEdit.TabIndex = 13;
            // 
            // InventoryAccountIdSpinEdit
            // 
            this.InventoryAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "InventoryAccountId", true));
            this.InventoryAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.InventoryAccountIdSpinEdit.Location = new System.Drawing.Point(387, 125);
            this.InventoryAccountIdSpinEdit.MenuManager = this._barManager;
            this.InventoryAccountIdSpinEdit.Name = "InventoryAccountIdSpinEdit";
            this.InventoryAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.InventoryAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.InventoryAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InventoryAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.InventoryAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.InventoryAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.InventoryAccountIdSpinEdit.Properties.ReadOnly = true;
            this.InventoryAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.InventoryAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.InventoryAccountIdSpinEdit.TabIndex = 14;
            // 
            // CostOfSoldGoodsAccountIdSpinEdit
            // 
            this.CostOfSoldGoodsAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "CostOfSoldGoodsAccountId", true));
            this.CostOfSoldGoodsAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostOfSoldGoodsAccountIdSpinEdit.Location = new System.Drawing.Point(387, 149);
            this.CostOfSoldGoodsAccountIdSpinEdit.MenuManager = this._barManager;
            this.CostOfSoldGoodsAccountIdSpinEdit.Name = "CostOfSoldGoodsAccountIdSpinEdit";
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostOfSoldGoodsAccountIdSpinEdit.Properties.ReadOnly = true;
            this.CostOfSoldGoodsAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.CostOfSoldGoodsAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostOfSoldGoodsAccountIdSpinEdit.TabIndex = 15;
            // 
            // DiscountReceivedAccountIdSpinEdit
            // 
            this.DiscountReceivedAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "DiscountReceivedAccountId", true));
            this.DiscountReceivedAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DiscountReceivedAccountIdSpinEdit.Location = new System.Drawing.Point(387, 173);
            this.DiscountReceivedAccountIdSpinEdit.MenuManager = this._barManager;
            this.DiscountReceivedAccountIdSpinEdit.Name = "DiscountReceivedAccountIdSpinEdit";
            this.DiscountReceivedAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DiscountReceivedAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DiscountReceivedAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DiscountReceivedAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.DiscountReceivedAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.DiscountReceivedAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DiscountReceivedAccountIdSpinEdit.Properties.ReadOnly = true;
            this.DiscountReceivedAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.DiscountReceivedAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.DiscountReceivedAccountIdSpinEdit.TabIndex = 16;
            // 
            // DiscountAllowedAccountIdSpinEdit
            // 
            this.DiscountAllowedAccountIdSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.storeBindingSource, "DiscountAllowedAccountId", true));
            this.DiscountAllowedAccountIdSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DiscountAllowedAccountIdSpinEdit.Location = new System.Drawing.Point(387, 197);
            this.DiscountAllowedAccountIdSpinEdit.MenuManager = this._barManager;
            this.DiscountAllowedAccountIdSpinEdit.Name = "DiscountAllowedAccountIdSpinEdit";
            this.DiscountAllowedAccountIdSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DiscountAllowedAccountIdSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DiscountAllowedAccountIdSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DiscountAllowedAccountIdSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.DiscountAllowedAccountIdSpinEdit.Properties.Mask.EditMask = "N0";
            this.DiscountAllowedAccountIdSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DiscountAllowedAccountIdSpinEdit.Properties.ReadOnly = true;
            this.DiscountAllowedAccountIdSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.DiscountAllowedAccountIdSpinEdit.StyleController = this.dataLayoutControl1;
            this.DiscountAllowedAccountIdSpinEdit.TabIndex = 17;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(657, 279);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(637, 259);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForName,
            this.ItemForDiscountAllowedAccountId,
            this.ItemForSalesAccountId,
            this.ItemForSalesReturnAccountId,
            this.ItemForInventoryAccountId,
            this.ItemForCostOfSoldGoodsAccountId,
            this.ItemForDiscountReceivedAccountId});
            this.layoutControlGroup2.Location = new System.Drawing.Point(212, 29);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(400, 192);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(376, 24);
            this.ItemForName.Text = "Name";
            this.ItemForName.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForDiscountAllowedAccountId
            // 
            this.ItemForDiscountAllowedAccountId.Control = this.DiscountAllowedAccountIdSpinEdit;
            this.ItemForDiscountAllowedAccountId.Location = new System.Drawing.Point(0, 144);
            this.ItemForDiscountAllowedAccountId.Name = "ItemForDiscountAllowedAccountId";
            this.ItemForDiscountAllowedAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForDiscountAllowedAccountId.Text = "Discount Allowed Account Id";
            this.ItemForDiscountAllowedAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForSalesAccountId
            // 
            this.ItemForSalesAccountId.Control = this.SalesAccountIdSpinEdit;
            this.ItemForSalesAccountId.Location = new System.Drawing.Point(0, 24);
            this.ItemForSalesAccountId.Name = "ItemForSalesAccountId";
            this.ItemForSalesAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForSalesAccountId.Text = "Sales Account Id";
            this.ItemForSalesAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForSalesReturnAccountId
            // 
            this.ItemForSalesReturnAccountId.Control = this.SalesReturnAccountIdSpinEdit;
            this.ItemForSalesReturnAccountId.Location = new System.Drawing.Point(0, 48);
            this.ItemForSalesReturnAccountId.Name = "ItemForSalesReturnAccountId";
            this.ItemForSalesReturnAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForSalesReturnAccountId.Text = "Sales Return Account Id";
            this.ItemForSalesReturnAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForInventoryAccountId
            // 
            this.ItemForInventoryAccountId.Control = this.InventoryAccountIdSpinEdit;
            this.ItemForInventoryAccountId.Location = new System.Drawing.Point(0, 72);
            this.ItemForInventoryAccountId.Name = "ItemForInventoryAccountId";
            this.ItemForInventoryAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForInventoryAccountId.Text = "Inventory Account Id";
            this.ItemForInventoryAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForCostOfSoldGoodsAccountId
            // 
            this.ItemForCostOfSoldGoodsAccountId.Control = this.CostOfSoldGoodsAccountIdSpinEdit;
            this.ItemForCostOfSoldGoodsAccountId.Location = new System.Drawing.Point(0, 96);
            this.ItemForCostOfSoldGoodsAccountId.Name = "ItemForCostOfSoldGoodsAccountId";
            this.ItemForCostOfSoldGoodsAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForCostOfSoldGoodsAccountId.Text = "Cost Of Sold Goods Account Id";
            this.ItemForCostOfSoldGoodsAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // ItemForDiscountReceivedAccountId
            // 
            this.ItemForDiscountReceivedAccountId.Control = this.DiscountReceivedAccountIdSpinEdit;
            this.ItemForDiscountReceivedAccountId.Location = new System.Drawing.Point(0, 120);
            this.ItemForDiscountReceivedAccountId.Name = "ItemForDiscountReceivedAccountId";
            this.ItemForDiscountReceivedAccountId.Size = new System.Drawing.Size(376, 24);
            this.ItemForDiscountReceivedAccountId.Text = "Discount Received Account Id";
            this.ItemForDiscountReceivedAccountId.TextSize = new System.Drawing.Size(148, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(612, 29);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(25, 192);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 29);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(212, 192);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 221);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(637, 38);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(637, 29);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_Store
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 307);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_Store";
            this.Text = "Branch/Stores";
            this.Load += new System.EventHandler(this.frm_Store_Load);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesReturnAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InventoryAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostOfSoldGoodsAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountReceivedAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountAllowedAccountIdSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountAllowedAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalesAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalesReturnAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInventoryAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostOfSoldGoodsAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountReceivedAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private System.Windows.Forms.BindingSource storeBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraEditors.SpinEdit SalesAccountIdSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SalesReturnAccountIdSpinEdit;
        private DevExpress.XtraEditors.SpinEdit InventoryAccountIdSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostOfSoldGoodsAccountIdSpinEdit;
        private DevExpress.XtraEditors.SpinEdit DiscountReceivedAccountIdSpinEdit;
        private DevExpress.XtraEditors.SpinEdit DiscountAllowedAccountIdSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSalesAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSalesReturnAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInventoryAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostOfSoldGoodsAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDiscountReceivedAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDiscountAllowedAccountId;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}