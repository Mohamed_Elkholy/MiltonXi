﻿using DevExpress.XtraEditors;
using MiltonXi._DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiltonXi.Forms
{
    public partial class frm_CustomerVendor : Forms.frm_Master
    {
        private bool isCustomer;
        private CustomerAndVendor CusNVen;
        public frm_CustomerVendor(bool isCustomer)
        {
            InitializeComponent();
            New();
            this.isCustomer = isCustomer;
        }
        public frm_CustomerVendor(int Id)
        {
            InitializeComponent();
            using (Entities ctx = new Entities())
            {
      
                customerAndVendorBindingSource.DataSource = ctx.CustomerAndVendors.Single(x => x.Id == Id);
                CusNVen = customerAndVendorBindingSource.DataSource as CustomerAndVendor;
                isCustomer = CusNVen.IsCustomer;
                this.Text = ((isCustomer) ? "Customer: " : "Vendor: ") + CusNVen.Name;
            }
        }
    

        public override void New()
        {
            base.New();
            customerAndVendorBindingSource.DataSource = new CustomerAndVendor() { IsCustomer = isCustomer ,MaxCredit=10000};
            this.Text = (isCustomer) ? "New Customer" : "New Vendor";

        }
        public override void Save()
        {
            base.Save();
            

            using (Entities ctx = new Entities())
            {
                int SavedChangesCount = 0;

                CustomerAndVendor cav = customerAndVendorBindingSource.DataSource as CustomerAndVendor;
          //      ctx.CustomerAndVendors.Attach(cav);
                ctx.CustomerAndVendors.AddOrUpdate(cav);
                Account account = ctx.Accounts.Where(a => a.Id == cav.AccountId).FirstOrDefault() ?? new Account();
                account.Name = cav.Name;
                ctx.Accounts.AddOrUpdate(account);
                SavedChangesCount+= ctx.SaveChanges();
                cav.AccountId = account.Id;

                SavedChangesCount+= ctx.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    DataSavedMessage();
                    this.Text = ((cav.IsCustomer) ? "Customer: " : "Vendor: ") + cav.Name;

                }

            }
        }
        public override void Delete()
        {
            base.Delete();
            CustomerAndVendor cav = customerAndVendorBindingSource.DataSource as CustomerAndVendor;
            using (Entities ctx = new Entities())
            {
                ctx.CustomerAndVendors.Attach(cav);
                ctx.CustomerAndVendors.Remove(cav);
                if (ctx.SaveChanges() > 0)
                {
                    DataSavedMessage();
                    New();
                }
            }
        }

        public override bool IsDataValid()
        {
            var cav = (customerAndVendorBindingSource.DataSource as CustomerAndVendor);
            var stringProperties = cav.GetType().GetProperties()
                           .Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(cav, null);
                if (currentValue == null) continue;
                stringProperty.SetValue(cav, currentValue.Trim(), null);
            }
            using (Entities ctx = new Entities())
            {
                var result = ctx.CustomerAndVendors.Where(x => x.Name == cav.Name
                    && cav.Branch == x.Branch
                    && cav.IsCustomer == x.IsCustomer
                    && cav.Id != x.Id
                    ).Count();
                if (result > 0)
                {
                    DuplicateMessage();
                    return false;
                }
            }
            return true;
        }
    }
}
