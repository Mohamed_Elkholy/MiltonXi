﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using MiltonXi.Class;

namespace MiltonXi.Forms
{
    public partial class frm_CustomersAndVendorsList : frm_Master
    {
        bool isCustomer;
        public frm_CustomersAndVendorsList(bool IsCustomer)
        {
            InitializeComponent();
            isCustomer = IsCustomer;
            this.Text = (isCustomer) ? "Customers" : "Vendors";

        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {
                int id = 0;
                id = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Id"));

                string name = gridView1.GetFocusedRowCellValue("Name").ToString();
                Class.Utilities.OpenFormInMain(new frm_CustomerVendor(id) { Tag = name });
            }
        }
        public override void RefreshData()
        {
            
                customerAndVendorBindingSource.DataSource =(isCustomer) ?  Session.Customers: Session.Vendors;

            base.RefreshData();
        }
        public override void New()
        {
            base.New();
            frm_CustomerVendor frm = new frm_CustomerVendor(isCustomer);
            frm.Text = (isCustomer) ? "New Customer" : "New Vendor";
            frm.Name = (isCustomer) ? "frm_Customer" : "frm_Vendor";
           // frm.Show();
            Class.Utilities.OpenFormInMain(frm);

        }

        private void frm_CustomersAndVendorsList_Load(object sender, EventArgs e)
        {

            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            gridView1.OptionsBehavior.Editable = false;
            gridView1.Columns["Id"].Visible = false;
            gridView1.Columns["IsCustomer"].Visible = isCustomer;
            this.Text = (isCustomer) ? "Customers" : "Vendors";
            RefreshData();
            gridView1.DoubleClick += GridView1_DoubleClick; 
            btn_Save.Enabled = false;

           if(isCustomer)
                Session.Customers.ListChanged += Customer_ListChanged;
           else
                Session.Vendors.ListChanged += Vendors_ListChanged;
            
        }

        private void Customer_ListChanged(object sender, ListChangedEventArgs e)
        {
            RefreshData();
        }

        private void Vendors_ListChanged(object sender, ListChangedEventArgs e)
        {
            RefreshData();
        }
    }
}