﻿namespace MiltonXi.Forms
{
    partial class frm_CustomerVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.AccountIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.customerAndVendorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.IsCustomerCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CountryTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BranchTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.VAT_NIPTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WebsiteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MaxCreditSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForIsCustomer = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCountry = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVAT_NIP = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMaxCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccountIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerAndVendorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCustomerCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VAT_NIPTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCreditSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT_NIP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaxCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AccountIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IsCustomerCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CountryTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BranchTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.VAT_NIPTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebsiteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MaxCreditSpinEdit);
            this.dataLayoutControl1.DataSource = this.customerAndVendorBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIsCustomer});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(798, 486);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AccountIdTextEdit
            // 
            this.AccountIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "AccountId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AccountIdTextEdit.Location = new System.Drawing.Point(338, 293);
            this.AccountIdTextEdit.MenuManager = this._barManager;
            this.AccountIdTextEdit.Name = "AccountIdTextEdit";
            this.AccountIdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AccountIdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AccountIdTextEdit.Properties.Mask.EditMask = "N0";
            this.AccountIdTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AccountIdTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AccountIdTextEdit.Properties.ReadOnly = true;
            this.AccountIdTextEdit.Size = new System.Drawing.Size(221, 20);
            this.AccountIdTextEdit.StyleController = this.dataLayoutControl1;
            this.AccountIdTextEdit.TabIndex = 5;
            // 
            // customerAndVendorBindingSource
            // 
            this.customerAndVendorBindingSource.DataSource = typeof(MiltonXi._DAL.CustomerAndVendor);
            // 
            // IsCustomerCheckEdit
            // 
            this.IsCustomerCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "IsCustomer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.IsCustomerCheckEdit.Location = new System.Drawing.Point(12, 60);
            this.IsCustomerCheckEdit.MenuManager = this._barManager;
            this.IsCustomerCheckEdit.Name = "IsCustomerCheckEdit";
            this.IsCustomerCheckEdit.Properties.Caption = "Is Customer";
            this.IsCustomerCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.IsCustomerCheckEdit.Size = new System.Drawing.Size(776, 20);
            this.IsCustomerCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsCustomerCheckEdit.TabIndex = 6;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(338, 53);
            this.NameTextEdit.MaximumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.MenuManager = this._barManager;
            this.NameTextEdit.MinimumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.NameTextEdit.Size = new System.Drawing.Size(221, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 7;
            // 
            // PhoneTextEdit
            // 
            this.PhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Phone", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PhoneTextEdit.Location = new System.Drawing.Point(338, 125);
            this.PhoneTextEdit.MenuManager = this._barManager;
            this.PhoneTextEdit.Name = "PhoneTextEdit";
            this.PhoneTextEdit.Size = new System.Drawing.Size(221, 20);
            this.PhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PhoneTextEdit.TabIndex = 8;
            // 
            // MobileTextEdit
            // 
            this.MobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Mobile", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.MobileTextEdit.Location = new System.Drawing.Point(338, 149);
            this.MobileTextEdit.MenuManager = this._barManager;
            this.MobileTextEdit.Name = "MobileTextEdit";
            this.MobileTextEdit.Size = new System.Drawing.Size(221, 20);
            this.MobileTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTextEdit.TabIndex = 9;
            // 
            // CountryTextEdit
            // 
            this.CountryTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Country", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CountryTextEdit.Location = new System.Drawing.Point(338, 173);
            this.CountryTextEdit.MenuManager = this._barManager;
            this.CountryTextEdit.Name = "CountryTextEdit";
            this.CountryTextEdit.Size = new System.Drawing.Size(221, 20);
            this.CountryTextEdit.StyleController = this.dataLayoutControl1;
            this.CountryTextEdit.TabIndex = 10;
            // 
            // BranchTextEdit
            // 
            this.BranchTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Branch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BranchTextEdit.Location = new System.Drawing.Point(338, 197);
            this.BranchTextEdit.MenuManager = this._barManager;
            this.BranchTextEdit.Name = "BranchTextEdit";
            this.BranchTextEdit.Size = new System.Drawing.Size(221, 20);
            this.BranchTextEdit.StyleController = this.dataLayoutControl1;
            this.BranchTextEdit.TabIndex = 11;
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Address", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AddressTextEdit.Location = new System.Drawing.Point(338, 221);
            this.AddressTextEdit.MenuManager = this._barManager;
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Size = new System.Drawing.Size(221, 20);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 12;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.Location = new System.Drawing.Point(338, 245);
            this.EmailTextEdit.MenuManager = this._barManager;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Size = new System.Drawing.Size(221, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 13;
            // 
            // NotesMemoEdit
            // 
            this.NotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Notes", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NotesMemoEdit.Location = new System.Drawing.Point(338, 317);
            this.NotesMemoEdit.MenuManager = this._barManager;
            this.NotesMemoEdit.Name = "NotesMemoEdit";
            this.NotesMemoEdit.Size = new System.Drawing.Size(221, 109);
            this.NotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.NotesMemoEdit.TabIndex = 15;
            // 
            // VAT_NIPTextEdit
            // 
            this.VAT_NIPTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "VAT_NIP", true));
            this.VAT_NIPTextEdit.Location = new System.Drawing.Point(338, 77);
            this.VAT_NIPTextEdit.MenuManager = this._barManager;
            this.VAT_NIPTextEdit.Name = "VAT_NIPTextEdit";
            this.VAT_NIPTextEdit.Size = new System.Drawing.Size(221, 20);
            this.VAT_NIPTextEdit.StyleController = this.dataLayoutControl1;
            this.VAT_NIPTextEdit.TabIndex = 16;
            // 
            // WebsiteTextEdit
            // 
            this.WebsiteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "Website", true));
            this.WebsiteTextEdit.Location = new System.Drawing.Point(338, 269);
            this.WebsiteTextEdit.MenuManager = this._barManager;
            this.WebsiteTextEdit.Name = "WebsiteTextEdit";
            this.WebsiteTextEdit.Size = new System.Drawing.Size(221, 20);
            this.WebsiteTextEdit.StyleController = this.dataLayoutControl1;
            this.WebsiteTextEdit.TabIndex = 17;
            // 
            // MaxCreditSpinEdit
            // 
            this.MaxCreditSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customerAndVendorBindingSource, "MaxCredit", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.MaxCreditSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaxCreditSpinEdit.Location = new System.Drawing.Point(338, 101);
            this.MaxCreditSpinEdit.MenuManager = this._barManager;
            this.MaxCreditSpinEdit.Name = "MaxCreditSpinEdit";
            this.MaxCreditSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.MaxCreditSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MaxCreditSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaxCreditSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.MaxCreditSpinEdit.Properties.Mask.EditMask = "F";
            this.MaxCreditSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MaxCreditSpinEdit.Size = new System.Drawing.Size(221, 20);
            this.MaxCreditSpinEdit.StyleController = this.dataLayoutControl1;
            this.MaxCreditSpinEdit.TabIndex = 18;
            // 
            // ItemForIsCustomer
            // 
            this.ItemForIsCustomer.Control = this.IsCustomerCheckEdit;
            this.ItemForIsCustomer.Location = new System.Drawing.Point(0, 48);
            this.ItemForIsCustomer.Name = "ItemForIsCustomer";
            this.ItemForIsCustomer.Size = new System.Drawing.Size(780, 23);
            this.ItemForIsCustomer.Text = "Is Customer";
            this.ItemForIsCustomer.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForIsCustomer.TextVisible = false;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(798, 486);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(778, 466);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForName,
            this.ItemForPhone,
            this.ItemForMobile,
            this.ItemForCountry,
            this.ItemForBranch,
            this.ItemForAddress,
            this.ItemForEmail,
            this.ItemForNotes,
            this.ItemForAccountId,
            this.ItemForVAT_NIP,
            this.ItemForWebsite,
            this.ItemForMaxCredit});
            this.layoutControlGroup2.Location = new System.Drawing.Point(259, 29);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(304, 401);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(280, 24);
            this.ItemForName.Text = "Name";
            this.ItemForName.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.Control = this.PhoneTextEdit;
            this.ItemForPhone.Location = new System.Drawing.Point(0, 72);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(280, 24);
            this.ItemForPhone.Text = "Phone";
            this.ItemForPhone.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.MobileTextEdit;
            this.ItemForMobile.Location = new System.Drawing.Point(0, 96);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(280, 24);
            this.ItemForMobile.Text = "Mobile";
            this.ItemForMobile.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForCountry
            // 
            this.ItemForCountry.Control = this.CountryTextEdit;
            this.ItemForCountry.Location = new System.Drawing.Point(0, 120);
            this.ItemForCountry.Name = "ItemForCountry";
            this.ItemForCountry.Size = new System.Drawing.Size(280, 24);
            this.ItemForCountry.Text = "Country";
            this.ItemForCountry.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForBranch
            // 
            this.ItemForBranch.Control = this.BranchTextEdit;
            this.ItemForBranch.Location = new System.Drawing.Point(0, 144);
            this.ItemForBranch.Name = "ItemForBranch";
            this.ItemForBranch.Size = new System.Drawing.Size(280, 24);
            this.ItemForBranch.Text = "Branch";
            this.ItemForBranch.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.Location = new System.Drawing.Point(0, 168);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(280, 24);
            this.ItemForAddress.Text = "Address";
            this.ItemForAddress.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.Location = new System.Drawing.Point(0, 192);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(280, 24);
            this.ItemForEmail.Text = "Email";
            this.ItemForEmail.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesMemoEdit;
            this.ItemForNotes.Location = new System.Drawing.Point(0, 264);
            this.ItemForNotes.MaxSize = new System.Drawing.Size(0, 113);
            this.ItemForNotes.MinSize = new System.Drawing.Size(69, 113);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(280, 113);
            this.ItemForNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForAccountId
            // 
            this.ItemForAccountId.Control = this.AccountIdTextEdit;
            this.ItemForAccountId.Location = new System.Drawing.Point(0, 240);
            this.ItemForAccountId.Name = "ItemForAccountId";
            this.ItemForAccountId.Size = new System.Drawing.Size(280, 24);
            this.ItemForAccountId.Text = "Account Id";
            this.ItemForAccountId.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForVAT_NIP
            // 
            this.ItemForVAT_NIP.Control = this.VAT_NIPTextEdit;
            this.ItemForVAT_NIP.Location = new System.Drawing.Point(0, 24);
            this.ItemForVAT_NIP.Name = "ItemForVAT_NIP";
            this.ItemForVAT_NIP.Size = new System.Drawing.Size(280, 24);
            this.ItemForVAT_NIP.Text = "VAT/NIP";
            this.ItemForVAT_NIP.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForWebsite
            // 
            this.ItemForWebsite.Control = this.WebsiteTextEdit;
            this.ItemForWebsite.Location = new System.Drawing.Point(0, 216);
            this.ItemForWebsite.Name = "ItemForWebsite";
            this.ItemForWebsite.Size = new System.Drawing.Size(280, 24);
            this.ItemForWebsite.Text = "Website";
            this.ItemForWebsite.TextSize = new System.Drawing.Size(52, 13);
            // 
            // ItemForMaxCredit
            // 
            this.ItemForMaxCredit.Control = this.MaxCreditSpinEdit;
            this.ItemForMaxCredit.Location = new System.Drawing.Point(0, 48);
            this.ItemForMaxCredit.Name = "ItemForMaxCredit";
            this.ItemForMaxCredit.Size = new System.Drawing.Size(280, 24);
            this.ItemForMaxCredit.Text = "Max Credit";
            this.ItemForMaxCredit.TextSize = new System.Drawing.Size(52, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 29);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(259, 401);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(563, 29);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(215, 401);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 430);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(778, 36);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(778, 29);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_CustomerVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 514);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_CustomerVendor";
            this.Text = "Customer and vendor";
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AccountIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerAndVendorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCustomerCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VAT_NIPTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCreditSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT_NIP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaxCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private System.Windows.Forms.BindingSource customerAndVendorBindingSource;
        private DevExpress.XtraEditors.TextEdit AccountIdTextEdit;
        private DevExpress.XtraEditors.CheckEdit IsCustomerCheckEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit PhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit MobileTextEdit;
        private DevExpress.XtraEditors.TextEdit CountryTextEdit;
        private DevExpress.XtraEditors.TextEdit BranchTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressTextEdit;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.MemoEdit NotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsCustomer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCountry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBranch;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit VAT_NIPTextEdit;
        private DevExpress.XtraEditors.TextEdit WebsiteTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVAT_NIP;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebsite;
        private DevExpress.XtraEditors.SpinEdit MaxCreditSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaxCredit;
    }
}