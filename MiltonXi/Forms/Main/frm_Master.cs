﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MiltonXi.Class;

namespace MiltonXi.Forms
{
    public partial class frm_Master : XtraForm
    {
        public BarManager _barManager;
        public Bar _toolBar;
        public bool IsNew;
        public Color OddRowColor { get => Color.WhiteSmoke; }
        public Color EvenRowColor { get => Color.FromArgb(200, 255, 249, 196); }
        public static string ErrorRequired { get { return "Required field"; } }

        public frm_Master()
        {
            InitializeComponent();
            _barManager = barManager;
            _toolBar = ToolBar;
            this.KeyPreview = true;
            this.btn_Print.Visibility = BarItemVisibility.Never;

            this.KeyDown += ParentFrmEntryScreen_KeyDown;

        }

        private void ParentFrmEntryScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control && e.KeyCode == Keys.S) && btn_Save.Enabled)
            {
                btn_Save.PerformClick();
            }

            if ((e.Control && e.KeyCode == Keys.N) && btn_New.Enabled)
            {
                btn_New.PerformClick();
            }

            if ((e.Shift && e.KeyCode == Keys.Delete) && btn_Delete.Enabled)
            {
                btn_Delete.PerformClick();
            }

            if ((e.Control && e.KeyCode == Keys.P) && btn_Print.Enabled)
            {
                btn_New.PerformClick();
            }
        }



        public DialogResult QuestionUser(string titlepart)
        {
            return XtraMessageBox.Show("Are you sure?", $"{titlepart} confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
        public DialogResult QuestionUser(string msg, string titlepart)
        {
            return XtraMessageBox.Show($"Are you sure {msg}?", $"{titlepart} confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
        private void btn_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (CheckActionAuthorization(this.Name,IsNew? Master.Actions.Add:Master.Actions.Edit))
            {
                if (IsDataValid())
                {
                    if (QuestionUser("Saving") == DialogResult.Yes)
                    {
                        if (this.ValidateChildren())
                        {

                            Save();

                        }

                    }
                }
            }
        }

        private void btn_New_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            New();
           
        }

        private void btn_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (CheckActionAuthorization(this.Name, Master.Actions.Delete))
            {
                if (QuestionUser("Deleting") == DialogResult.Yes)
                {
                    Delete();
                }
            }
        }
        public virtual void SystemProtectedDelete()
        {
            XtraMessageBox.Show("Unable to delete system protected Item", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
        public virtual void DataSavedMessage()
        {
            XtraMessageBox.Show("Data Saved!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public virtual void DataSavedMessage(int total)
        {
            XtraMessageBox.Show($"{total} Data Saved!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public virtual void DuplicateMessage()
        {
            XtraMessageBox.Show(text: "Duplicate record found!", caption: "Error", icon: MessageBoxIcon.Error, buttons: MessageBoxButtons.OK);
        }

        public virtual void GetData()
        {

        }
        public virtual void SetData()
        {

        }
        public virtual void Save()
        {

            RefreshData();
            IsNew = false;
            btn_Delete.Enabled = true;

        }
        public virtual void New()
        {
            GetData();
            IsNew = true;
            btn_Delete.Enabled = false;
        }
        public virtual void Delete()
        {

        }
        public virtual void RefreshData()
        {

        }
        public virtual void Open()
        {

        }
        public virtual bool IsDataValid()
        {
            return false;
        }
        public bool ValidateGridDoubleClick(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            if (info.InRow || info.InRowCell)
            {
                return true;
            }
            return false;
        }

        private void btn_Print_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (CheckActionAuthorization(this.Name, Master.Actions.Print))
            {
                Print();
            }
        }
        public virtual void Print()
        {

        }
        public static bool CheckActionAuthorization(string FormName, Master.Actions actions, _DAL.User user = null)
        {
            if (user == null)
                user = Session.User;
            if (user.UserType == (byte)Master.UserType.SystemAdmin)
                return true;
            else
            {
                var screen = Session.ScreenAccess.SingleOrDefault(x => x.ScreenName == FormName);
                bool flag = true;
                if (screen != null)
                {
                    switch (actions)
                    {
                        case Master.Actions.Add:
                            flag = screen.CanAdd;
                            break;
                        case Master.Actions.Edit:
                            flag = screen.CanEdit;
                            break;
                        case Master.Actions.Delete:
                            flag = screen.CanDelete;
                            break;
                        case Master.Actions.Print:
                            flag = screen.CanPrint;
                            break;
                        default:
                            break;
                    }
                }
                if (flag == false)
                {
                    XtraMessageBox.Show("Action permission denied", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return flag;

            }
        }
        protected override void OnResizeBegin(EventArgs e)
        {
            SuspendLayout();
            base.OnResizeBegin(e);
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            ResumeLayout();
            base.OnResizeEnd(e);
        }
    }
}
