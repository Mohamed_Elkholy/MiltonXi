﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DashboardCommon;

namespace MiltonXi
{
    public partial class UCDashboard : DevExpress.XtraEditors.XtraUserControl
    {
        public UCDashboard()
        {
            InitializeComponent();


          
            
        }

        private void UCDashboard_Load(object sender, EventArgs e)
        {
            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                dashboardDesigner1.LoadDashboard(@"_Dashboards\DispatchFormDashboardEF.xml");
                Console.WriteLine("design mode ");
            }
        }
    }
}
