﻿namespace MiltonXi.Forms
{
    partial class frm_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            bool isDesignMode = DesignMode;
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if (--OpenFormCount == 0 && !isDesignMode)
            {
                System.Windows.Forms.Application.Exit();
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_MainForm));
            this.tabFormControl = new DevExpress.XtraBars.TabFormControl();
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinPaletteDropDownButtonItem1 = new DevExpress.XtraBars.SkinPaletteDropDownButtonItem();
            this.accordionControl = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionControlElement7 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement26 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement29 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement30 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement20 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement21 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement13 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement22 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement23 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement14 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement18 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement19 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement8 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement24 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement25 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement12 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement6 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement16 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement17 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement15 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement27 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement28 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement4 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_DriverDispatchFormx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllDriverDispatchFormx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllMultiStopRoutePlanx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_LoadStatusx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllDriverx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllTrucksx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllTrailerx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_InvoiceReport = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_ManageExpenses = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_PendingInvoices = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ElementExpiredDocument = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement3 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_ExpenseType = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_Currency = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_Country = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_AllUsersx = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_ApplicationSettings = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.Element_ApplicationUpdate = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement9 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement5 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement10 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement11 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabFormControl
            // 
            this.tabFormControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.skinDropDownButtonItem1,
            this.skinPaletteDropDownButtonItem1});
            this.tabFormControl.Location = new System.Drawing.Point(0, 0);
            this.tabFormControl.Name = "tabFormControl";
            this.tabFormControl.ShowAddPageButton = false;
            this.tabFormControl.ShowTabsInTitleBar = DevExpress.XtraBars.ShowTabsInTitleBar.True;
            this.tabFormControl.Size = new System.Drawing.Size(1429, 27);
            this.tabFormControl.TabForm = this;
            this.tabFormControl.TabIndex = 0;
            this.tabFormControl.TabLeftItemLinks.Add(this.skinDropDownButtonItem1);
            this.tabFormControl.TabLeftItemLinks.Add(this.skinPaletteDropDownButtonItem1);
            this.tabFormControl.TabStop = false;
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Id = 0;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // skinPaletteDropDownButtonItem1
            // 
            this.skinPaletteDropDownButtonItem1.Id = 1;
            this.skinPaletteDropDownButtonItem1.Name = "skinPaletteDropDownButtonItem1";
            // 
            // accordionControl
            // 
            this.accordionControl.Appearance.Item.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.accordionControl.Appearance.Item.Disabled.Options.UseFont = true;
            this.accordionControl.Appearance.Item.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.accordionControl.Appearance.Item.Hovered.Options.UseFont = true;
            this.accordionControl.Appearance.Item.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.accordionControl.Appearance.Item.Normal.Options.UseFont = true;
            this.accordionControl.Appearance.Item.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.accordionControl.Appearance.Item.Pressed.Options.UseFont = true;
            this.accordionControl.Appearance.ItemWithContainer.Normal.FontStyleDelta = System.Drawing.FontStyle.Italic;
            this.accordionControl.Appearance.ItemWithContainer.Normal.Options.UseFont = true;
            this.accordionControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement7,
            this.accordionControlElement26,
            this.accordionControlElement29,
            this.accordionControlElement20,
            this.accordionControlElement22,
            this.accordionControlElement18,
            this.accordionControlElement24,
            this.accordionControlElement6,
            this.accordionControlElement27,
            this.accordionControlElement4,
            this.accordionControlElement1,
            this.accordionControlElement3,
            this.accordionControlElement9});
            this.accordionControl.Location = new System.Drawing.Point(0, 27);
            this.accordionControl.Name = "accordionControl";
            this.accordionControl.Size = new System.Drawing.Size(250, 809);
            this.accordionControl.TabIndex = 9;
            this.accordionControl.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // accordionControlElement7
            // 
            this.accordionControlElement7.Name = "accordionControlElement7";
            this.accordionControlElement7.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement7.Tag = "frm_CompanyInfo";
            this.accordionControlElement7.Text = "Company info";
            // 
            // accordionControlElement26
            // 
            this.accordionControlElement26.Name = "accordionControlElement26";
            this.accordionControlElement26.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement26.Tag = "frm_Invoice";
            this.accordionControlElement26.Text = "New Invoice";
            // 
            // accordionControlElement29
            // 
            this.accordionControlElement29.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement30});
            this.accordionControlElement29.Expanded = true;
            this.accordionControlElement29.Name = "accordionControlElement29";
            this.accordionControlElement29.Text = "Users & Settings";
            // 
            // accordionControlElement30
            // 
            this.accordionControlElement30.Name = "accordionControlElement30";
            this.accordionControlElement30.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement30.Tag = "frm_UserRoleTemplateList";
            this.accordionControlElement30.Text = "User Role Templates";
            // 
            // accordionControlElement20
            // 
            this.accordionControlElement20.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement21,
            this.accordionControlElement13});
            this.accordionControlElement20.Name = "accordionControlElement20";
            this.accordionControlElement20.Text = "Customers";
            // 
            // accordionControlElement21
            // 
            this.accordionControlElement21.Name = "accordionControlElement21";
            this.accordionControlElement21.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement21.Tag = "frm_Customer";
            this.accordionControlElement21.Text = "New customer";
            // 
            // accordionControlElement13
            // 
            this.accordionControlElement13.Name = "accordionControlElement13";
            this.accordionControlElement13.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement13.Tag = "frm_CustomerList";
            this.accordionControlElement13.Text = "Customers";
            // 
            // accordionControlElement22
            // 
            this.accordionControlElement22.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement23,
            this.accordionControlElement14});
            this.accordionControlElement22.Name = "accordionControlElement22";
            this.accordionControlElement22.Text = "Vendors";
            // 
            // accordionControlElement23
            // 
            this.accordionControlElement23.Name = "accordionControlElement23";
            this.accordionControlElement23.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement23.Tag = "frm_Vendor";
            this.accordionControlElement23.Text = "New vendor";
            // 
            // accordionControlElement14
            // 
            this.accordionControlElement14.Name = "accordionControlElement14";
            this.accordionControlElement14.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement14.Tag = "frm_VendrosList";
            this.accordionControlElement14.Text = "Vendros";
            // 
            // accordionControlElement18
            // 
            this.accordionControlElement18.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement19,
            this.accordionControlElement8});
            this.accordionControlElement18.Name = "accordionControlElement18";
            this.accordionControlElement18.Text = "Branches";
            // 
            // accordionControlElement19
            // 
            this.accordionControlElement19.Name = "accordionControlElement19";
            this.accordionControlElement19.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement19.Tag = "frm_Store";
            this.accordionControlElement19.Text = "New branch";
            // 
            // accordionControlElement8
            // 
            this.accordionControlElement8.Name = "accordionControlElement8";
            this.accordionControlElement8.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement8.Tag = "frm_StoresList";
            this.accordionControlElement8.Text = "Branches & stores";
            // 
            // accordionControlElement24
            // 
            this.accordionControlElement24.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement25,
            this.accordionControlElement12});
            this.accordionControlElement24.Name = "accordionControlElement24";
            this.accordionControlElement24.Text = "Drawers";
            // 
            // accordionControlElement25
            // 
            this.accordionControlElement25.Name = "accordionControlElement25";
            this.accordionControlElement25.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement25.Tag = "frm_Drawer";
            this.accordionControlElement25.Text = "New drawer";
            // 
            // accordionControlElement12
            // 
            this.accordionControlElement12.Name = "accordionControlElement12";
            this.accordionControlElement12.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement12.Tag = "frm_DrawerList";
            this.accordionControlElement12.Text = "Drawers";
            // 
            // accordionControlElement6
            // 
            this.accordionControlElement6.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement16,
            this.accordionControlElement17,
            this.accordionControlElement15});
            this.accordionControlElement6.Expanded = true;
            this.accordionControlElement6.Name = "accordionControlElement6";
            this.accordionControlElement6.Text = "Product";
            // 
            // accordionControlElement16
            // 
            this.accordionControlElement16.Name = "accordionControlElement16";
            this.accordionControlElement16.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement16.Tag = "frm_Product";
            this.accordionControlElement16.Text = "New product";
            // 
            // accordionControlElement17
            // 
            this.accordionControlElement17.Name = "accordionControlElement17";
            this.accordionControlElement17.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement17.Tag = "frm_ProductList";
            this.accordionControlElement17.Text = "Products & services";
            // 
            // accordionControlElement15
            // 
            this.accordionControlElement15.Name = "accordionControlElement15";
            this.accordionControlElement15.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement15.Tag = "frm_ProductCategory";
            this.accordionControlElement15.Text = "Product category";
            // 
            // accordionControlElement27
            // 
            this.accordionControlElement27.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement28});
            this.accordionControlElement27.Expanded = true;
            this.accordionControlElement27.Name = "accordionControlElement27";
            this.accordionControlElement27.Text = "Purchasing";
            // 
            // accordionControlElement28
            // 
            this.accordionControlElement28.Name = "accordionControlElement28";
            this.accordionControlElement28.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement28.Tag = "frm_PurchaseInvoice";
            this.accordionControlElement28.Text = "New purchasing invoice";
            // 
            // accordionControlElement4
            // 
            this.accordionControlElement4.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.Element_DriverDispatchFormx,
            this.Element_AllDriverDispatchFormx,
            this.Element_AllMultiStopRoutePlanx,
            this.Element_LoadStatusx});
            this.accordionControlElement4.Name = "accordionControlElement4";
            this.accordionControlElement4.Text = "Dispatch";
            // 
            // Element_DriverDispatchFormx
            // 
            this.Element_DriverDispatchFormx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_DriverDispatchFormx.ImageOptions.Image")));
            this.Element_DriverDispatchFormx.Name = "Element_DriverDispatchFormx";
            this.Element_DriverDispatchFormx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_DriverDispatchFormx.Tag = "UC_DispatchForm";
            this.Element_DriverDispatchFormx.Text = "New Dispatch Form";
            // 
            // Element_AllDriverDispatchFormx
            // 
            this.Element_AllDriverDispatchFormx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllDriverDispatchFormx.ImageOptions.Image")));
            this.Element_AllDriverDispatchFormx.Name = "Element_AllDriverDispatchFormx";
            this.Element_AllDriverDispatchFormx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllDriverDispatchFormx.Tag = "UC_DispatchFormsALL";
            this.Element_AllDriverDispatchFormx.Text = "Dispatch Forms";
            // 
            // Element_AllMultiStopRoutePlanx
            // 
            this.Element_AllMultiStopRoutePlanx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllMultiStopRoutePlanx.ImageOptions.Image")));
            this.Element_AllMultiStopRoutePlanx.Name = "Element_AllMultiStopRoutePlanx";
            this.Element_AllMultiStopRoutePlanx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllMultiStopRoutePlanx.Tag = "UC_MultiStopALL";
            this.Element_AllMultiStopRoutePlanx.Text = "Multi Stops";
            // 
            // Element_LoadStatusx
            // 
            this.Element_LoadStatusx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_LoadStatusx.ImageOptions.Image")));
            this.Element_LoadStatusx.Name = "Element_LoadStatusx";
            this.Element_LoadStatusx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_LoadStatusx.Tag = "UC_LoadStatus";
            this.Element_LoadStatusx.Text = "Status";
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.Element_AllDriverx,
            this.Element_AllTrucksx,
            this.Element_AllTrailerx,
            this.Element_InvoiceReport,
            this.Element_ManageExpenses,
            this.Element_PendingInvoices,
            this.ElementExpiredDocument});
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Text = "Administration";
            // 
            // Element_AllDriverx
            // 
            this.Element_AllDriverx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllDriverx.ImageOptions.Image")));
            this.Element_AllDriverx.Name = "Element_AllDriverx";
            this.Element_AllDriverx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllDriverx.Tag = "frm_AllDriverx";
            this.Element_AllDriverx.Text = "Drivers";
            // 
            // Element_AllTrucksx
            // 
            this.Element_AllTrucksx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllTrucksx.ImageOptions.Image")));
            this.Element_AllTrucksx.Name = "Element_AllTrucksx";
            this.Element_AllTrucksx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllTrucksx.Tag = "frm_AllTruckx";
            this.Element_AllTrucksx.Text = "Trucks";
            // 
            // Element_AllTrailerx
            // 
            this.Element_AllTrailerx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllTrailerx.ImageOptions.Image")));
            this.Element_AllTrailerx.Name = "Element_AllTrailerx";
            this.Element_AllTrailerx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllTrailerx.Tag = "frm_AllTrailerx";
            this.Element_AllTrailerx.Text = "Trailers";
            // 
            // Element_InvoiceReport
            // 
            this.Element_InvoiceReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_InvoiceReport.ImageOptions.Image")));
            this.Element_InvoiceReport.Name = "Element_InvoiceReport";
            this.Element_InvoiceReport.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_InvoiceReport.Tag = "UC_InvoicingSheet";
            this.Element_InvoiceReport.Text = "Invoice Report";
            // 
            // Element_ManageExpenses
            // 
            this.Element_ManageExpenses.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_ManageExpenses.ImageOptions.Image")));
            this.Element_ManageExpenses.Name = "Element_ManageExpenses";
            this.Element_ManageExpenses.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_ManageExpenses.Tag = "frm_Expensex";
            this.Element_ManageExpenses.Text = "Expenses Report";
            // 
            // Element_PendingInvoices
            // 
            this.Element_PendingInvoices.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_PendingInvoices.ImageOptions.Image")));
            this.Element_PendingInvoices.Name = "Element_PendingInvoices";
            this.Element_PendingInvoices.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_PendingInvoices.Tag = "frm_PendingInvociesx";
            this.Element_PendingInvoices.Text = "Pending Invoices";
            // 
            // ElementExpiredDocument
            // 
            this.ElementExpiredDocument.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ElementExpiredDocument.ImageOptions.Image")));
            this.ElementExpiredDocument.Name = "ElementExpiredDocument";
            this.ElementExpiredDocument.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ElementExpiredDocument.Tag = "frm_DocumentExpiry";
            this.ElementExpiredDocument.Text = "Document Expiry";
            // 
            // accordionControlElement3
            // 
            this.accordionControlElement3.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.Element_ExpenseType,
            this.Element_Currency,
            this.Element_Country,
            this.Element_AllUsersx,
            this.Element_ApplicationSettings,
            this.Element_ApplicationUpdate});
            this.accordionControlElement3.Name = "accordionControlElement3";
            this.accordionControlElement3.Text = "Configuration";
            // 
            // Element_ExpenseType
            // 
            this.Element_ExpenseType.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_ExpenseType.ImageOptions.Image")));
            this.Element_ExpenseType.Name = "Element_ExpenseType";
            this.Element_ExpenseType.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_ExpenseType.Tag = "frm_ExpenseTypex";
            this.Element_ExpenseType.Text = "Expense Type";
            // 
            // Element_Currency
            // 
            this.Element_Currency.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_Currency.ImageOptions.Image")));
            this.Element_Currency.Name = "Element_Currency";
            this.Element_Currency.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_Currency.Tag = "frm_Currencyx";
            this.Element_Currency.Text = "Currency";
            // 
            // Element_Country
            // 
            this.Element_Country.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_Country.ImageOptions.Image")));
            this.Element_Country.Name = "Element_Country";
            this.Element_Country.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_Country.Tag = "frm_Countryx";
            this.Element_Country.Text = "Country";
            // 
            // Element_AllUsersx
            // 
            this.Element_AllUsersx.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_AllUsersx.ImageOptions.Image")));
            this.Element_AllUsersx.Name = "Element_AllUsersx";
            this.Element_AllUsersx.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_AllUsersx.Tag = "frm_AllUsersx";
            this.Element_AllUsersx.Text = "System Users";
            // 
            // Element_ApplicationSettings
            // 
            this.Element_ApplicationSettings.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("Element_ApplicationSettings.ImageOptions.SvgImage")));
            this.Element_ApplicationSettings.Name = "Element_ApplicationSettings";
            this.Element_ApplicationSettings.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_ApplicationSettings.Tag = "frm_ApplicationSettingsx";
            this.Element_ApplicationSettings.Text = "Application Settings";
            // 
            // Element_ApplicationUpdate
            // 
            this.Element_ApplicationUpdate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Element_ApplicationUpdate.ImageOptions.Image")));
            this.Element_ApplicationUpdate.Name = "Element_ApplicationUpdate";
            this.Element_ApplicationUpdate.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.Element_ApplicationUpdate.Text = "Update";
            // 
            // accordionControlElement9
            // 
            this.accordionControlElement9.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement5,
            this.accordionControlElement10,
            this.accordionControlElement2,
            this.accordionControlElement11});
            this.accordionControlElement9.Name = "accordionControlElement9";
            this.accordionControlElement9.Text = "Data Sync";
            // 
            // accordionControlElement5
            // 
            this.accordionControlElement5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionControlElement5.ImageOptions.Image")));
            this.accordionControlElement5.Name = "accordionControlElement5";
            this.accordionControlElement5.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement5.Tag = "frm_BackupFiles";
            this.accordionControlElement5.Text = "Backup files";
            // 
            // accordionControlElement10
            // 
            this.accordionControlElement10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionControlElement10.ImageOptions.Image")));
            this.accordionControlElement10.Name = "accordionControlElement10";
            this.accordionControlElement10.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement10.Tag = "UC_SyncDispatchForm";
            this.accordionControlElement10.Text = "Dispatch Forms";
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement2.Tag = "UC_SyncExpenses";
            this.accordionControlElement2.Text = "Expenses";
            // 
            // accordionControlElement11
            // 
            this.accordionControlElement11.Name = "accordionControlElement11";
            this.accordionControlElement11.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement11.Tag = "UC_SyncInvoicingSheets";
            this.accordionControlElement11.Text = "Invoicing Sheets";
            // 
            // separatorControl1
            // 
            this.separatorControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.separatorControl1.LineOrientation = System.Windows.Forms.Orientation.Vertical;
            this.separatorControl1.Location = new System.Drawing.Point(250, 27);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(20, 809);
            this.separatorControl1.TabIndex = 10;
            // 
            // frm_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1429, 836);
            this.Controls.Add(this.separatorControl1);
            this.Controls.Add(this.accordionControl);
            this.Controls.Add(this.tabFormControl);
            this.LookAndFeel.SkinName = "DevExpress Style";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "frm_MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TabFormControl = this.tabFormControl;
            this.Text = "XtraForm1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.TabFormControl tabFormControl;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_DriverDispatchFormx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllDriverDispatchFormx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllMultiStopRoutePlanx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_LoadStatusx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllDriverx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllTrucksx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllTrailerx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_InvoiceReport;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_ManageExpenses;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_PendingInvoices;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ElementExpiredDocument;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_ExpenseType;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_Currency;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_Country;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_AllUsersx;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_ApplicationSettings;
        private DevExpress.XtraBars.Navigation.AccordionControlElement Element_ApplicationUpdate;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement9;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement5;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement10;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement11;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.SkinPaletteDropDownButtonItem skinPaletteDropDownButtonItem1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement6;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement7;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement8;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement12;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement13;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement14;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement15;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement16;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement17;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement18;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement19;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement20;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement21;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement24;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement25;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement22;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement23;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement26;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement27;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement28;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement29;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement30;
    }
}

