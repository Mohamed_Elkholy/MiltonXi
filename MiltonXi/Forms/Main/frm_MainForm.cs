﻿using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;
using MiltonXi.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using MiltonXi.Properties;
using MiltonXi.Class;
using DevExpress.XtraEditors;

namespace MiltonXi.Forms
{
    public partial class frm_MainForm : DevExpress.XtraBars.TabForm
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static int OpenFormCount = 1;
        public static frm_MainForm _instance;
        public static frm_MainForm Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new frm_MainForm();
                }
                return _instance;
            }
        }
        public frm_MainForm()
        {
            InitializeComponent();
            //Override event to handle tab page next to accordion control
            tabFormControl.SelectedPageChanged += TabFormControl_SelectedPageChanged;
        
            accordionControl.ElementClick += AccordionControl1_ElementClick;
            accordionControl.ShowFilterControl = ShowFilterControl.Always;
            this.KeyPreview = true;
            this.FormClosing += Frm_MainForm_FormClosing;
        }



        private void Frm_MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default["SkinName"] = UserLookAndFeel.Default.SkinName;
            Settings.Default.Save();
            Application.Exit();
        }

        private void TabFormControl_SelectedPageChanged(object sender, TabFormSelectedPageChangedEventArgs e)
        {
            if (e.Page == null) return;
            BeginInvoke(new Action(() =>
            {
            e.Page.ContentContainer.SuspendLayout();
               
                // this.SuspendLayout();
                // ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).BeginInit();
                e.Page.ContentContainer.BringToFront();
                // ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).EndInit();
                // this.ResumeLayout(true);
            e.Page.ContentContainer.ResumeLayout();
            }));

        }

        private void AccordionControl1_ElementClick(object sender, ElementClickEventArgs e)
        {
            accordionControl.ClosePopupForm();

            var tag = e.Element.Tag as string;
            if (tag != string.Empty)
            {
                OpenFormByName(tag);
            }
        }
        TabFormContentContainer tabFormContentContainer;
        TabFormPage tabFormPage;

        public void OpenFormByName(string tag)
        {
            Form form;
            switch (tag)
            {
                case "frm_CustomerList":
                    form = new Forms.frm_CustomersAndVendorsList(true);
                    form.Name = Class.Screens.CustomerList.ScreenName;
                    OpenFormWithPermission(form);
                    break;
                case "frm_VendrosList":
                    form = new Forms.frm_CustomersAndVendorsList(false);
                    form.Name = Class.Screens.VendorList.ScreenName;
                    OpenFormWithPermission(form);
                    break;
                case "frm_Customer":
                    form = new Forms.frm_CustomerVendor(true);
                    form.Name = Class.Screens.NewCustomer.ScreenName;
                    form.Text = "New customer";
                    OpenFormWithPermission(form);
                    break;
                case "frm_Vendor":
                    form = new Forms.frm_CustomerVendor(true);
                    form.Name = Class.Screens.NewVendor.ScreenName;
                    form.Text = "New vendor";
                    OpenFormWithPermission(form);
                    break;
                case "frm_PurchaseInvoice":
                    form = new frm_Invoice(Class.Master.InvoiceType.Purchase);
                    form.Name = Class.Screens.frm_NewPurchaseInvoice.ScreenName;
                    form.Text = "Purchase invoice";
                    OpenFormWithPermission(form);
                    break;
                case "frm_PurchaseInvoiceList":
                    form = new frm_InvoiceList(Class.Master.InvoiceType.Purchase);
                    form.Name = Class.Screens.frm_PurchaseInvoiceList.ScreenName;
                    form.Text = "Purchase invoices";
                    OpenFormWithPermission(form);
                    break;
                case "frm_SalesInvoice":
                    form = new frm_Invoice(Class.Master.InvoiceType.Sales);
                    form.Name = Class.Screens.frm_NewSalesInvoice.ScreenName;
                    form.Text = "Sales invoice";
                    OpenFormWithPermission(form);
                    break;
                case "frm_SalesInvoiceList":
                    form = new frm_InvoiceList(Class.Master.InvoiceType.Sales);
                    form.Name = Class.Screens.frm_SalesInvoiceList.ScreenName;
                    form.Text = "Sales invoices";
                    OpenFormWithPermission(form);
                    break;
                default:
                    var ins = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(x => x.Name == tag);
                    if (ins != null)
                    {
                        form = Activator.CreateInstance(ins) as Form;
                        form.Name = tag;
                        OpenFormWithPermission(form);
                    }
                    break;
            }

        }
        public void OpenFormWithPermission(Form frm)
        {
            if (Session.User.UserType == (byte)Master.UserType.SystemAdmin)
            {
                OpenFormInTab( frm);
                return;
            }
            var screen = Session.ScreenAccess.SingleOrDefault(x => x.ScreenName == frm.Name);
            if (screen != null)
            {
                if (screen.CanOpen == true)
                {
                    OpenFormInTab(frm);
                    return;
                }
                else
                {
                    XtraMessageBox.Show("User does not have permission to access this screen", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void OpenFormInTab(Form form)
        {
            if (Application.OpenForms[form.Name] != null)
            {
                var page = TabFormControl.Pages.Where(x => x.Name == form.Name && x.Tag == form.Tag).FirstOrDefault();
                if (page != null)
                {
                    TabFormControl.SelectedPage = page;
                    return;
                }
            }
            form.Dock = DockStyle.Fill;
            form.TopLevel = false;
            form.FormBorderStyle = FormBorderStyle.None;
            this.tabFormContentContainer = new DevExpress.XtraBars.TabFormContentContainer();
            this.tabFormPage = new DevExpress.XtraBars.TabFormPage();
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).BeginInit();
            this.SuspendLayout();
            this.tabFormControl.Location = new System.Drawing.Point(0, 0);
            this.tabFormControl.Name = "tabFormControl";
            this.tabFormControl.Pages.Add(this.tabFormPage);
            this.tabFormControl.SelectedPage = this.tabFormPage;
            this.tabFormControl.Size = new System.Drawing.Size(284, 50);
            this.tabFormControl.TabForm = this;
            this.tabFormControl.TabIndex = 0;
            this.tabFormControl.TabStop = false;
            this.tabFormControl.PageClosing += TabFormControl_PageClosing;
            this.tabFormContentContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFormContentContainer.Location = new System.Drawing.Point(0, 50);
            this.tabFormContentContainer.Name = "tabFormContentContainer";
            this.tabFormContentContainer.Size = new System.Drawing.Size(284, 50);
            this.tabFormContentContainer.TabIndex = 1;
            this.tabFormPage.ContentContainer = this.tabFormContentContainer;
            this.tabFormPage.Name = form.Name;
            this.tabFormPage.Text = form.Text;
            this.tabFormPage.Tag = form.Tag;

            this.tabFormContentContainer.Controls.Add(form);
            try
            {

            form.Show();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Print($@"ClassName: {GetClassName(e)}");
                System.Diagnostics.Debug.Print($@"LineNumber: {GetLineNumber(e)}");
                log.Fatal($@"ClassName: {GetClassName(e)} LineNumber: {GetLineNumber(e)}");
                throw;
            }
            ((System.ComponentModel.ISupportInitialize)(this.tabFormControl)).EndInit();
            this.ResumeLayout(true);
        }

        private void TabFormControl_PageClosing(object sender, PageClosingEventArgs e)
        {

            var pageInfos = tabFormControl.ViewInfo.PageInfos.Where(x => x.Page.Name == tabFormControl.SelectedPage.Name && x.Page.Tag == tabFormControl.SelectedPage.Tag).FirstOrDefault();
            if (pageInfos != null)
            {
                var page = pageInfos.Page;
                Form frm = Application.OpenForms.Cast<Form>().Where(f => f.Name == page.Name && f.Tag == page.Tag).FirstOrDefault();
                if (frm != null)
                    frm.Close();
            }
        }


        //void OnOuterFormCreating(object sender, OuterFormCreatingEventArgs e)
        //{
        //    frm_MainForm form = new frm_MainForm();
        //    form.tabFormControl.Pages.Clear();
        //    e.Form = form;
        //    OpenFormCount++;
        //}

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //This should close the windows using Ctrl+W like google Chrome
            if (keyData == (Keys.W | Keys.Control))
            {
                var pageInfos = tabFormControl.ViewInfo.PageInfos.Where(x => x.Page.Name == tabFormControl.SelectedPage.Name && x.Page.Tag == tabFormControl.SelectedPage.Tag).FirstOrDefault();
                if (pageInfos != null)
                {
                    var page = pageInfos.Page;
                    if (page.Name == "tabFormHomePage") return true;
                    TabFormControl.ClosePage(page);
                }

                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void tabFormControl_Click(object sender, EventArgs e)
        {

        }

        private void frm_MainForm_Load(object sender, EventArgs e)
        {
            accordionControl.Elements.Clear();
            var Screens = Class.Session.ScreenAccess.Where(x=>x.CanShow==true);
            Screens.Where(s => s.ParentScreenId == 0).ToList().ForEach(s =>
            {
                AccordionControlElement elm = new AccordionControlElement()
                {
                    Text = s.ScreenCaption,
                    Tag = s.ScreenName,
                    Name = s.ScreenName,
                    Style = ElementStyle.Group
                };
                accordionControl.Elements.Add(elm);
                AddAccordionElement(elm, s.ScreenId);
            });
        }
        void AddAccordionElement(AccordionControlElement parent, int parentId)
        {

            var Screens = Class.Session.ScreenAccess.Where(x => x.CanShow == true || Class.Session.User.UserType == (byte)Class.Master.UserType.SystemAdmin);
            Screens.Where(s => s.ParentScreenId == parentId).ToList().ForEach(s =>
            {
                AccordionControlElement elm = new AccordionControlElement()
                {
                    Text = s.ScreenCaption,
                    Tag = s.ScreenName,
                    Name = s.ScreenName,
                    Style = ElementStyle.Item
                };
                parent.Elements.Add(elm);
            });


        }

        private string GetClassName(Exception e)
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(e, true);
            return trace.GetFrame(0).GetMethod().ReflectedType.FullName;
        }

        private int GetLineNumber(Exception e)
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(e, true);
            return trace.GetFrame(0).GetFileLineNumber();
        }
    }
}
