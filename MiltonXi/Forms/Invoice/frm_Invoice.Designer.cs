﻿namespace MiltonXi.Forms
{
    partial class frm_Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.invoiceHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DeliveryDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.BranchLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.PosteDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PostedToStoreCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TotalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ExpensesSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.NetSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DrawerLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.RemainingSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ShippingAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.NotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.PaidSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PartyIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.PartyTypeLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.DiscountValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DiscountRatioSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TaxValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TaxRatioSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TotalLocalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PartyAddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PartyPhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PartyBalanceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MaxCreditSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPartType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPartId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPost = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPosteDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostedToStore = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDiscountValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDiscounteRatio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpenses = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaxValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaxRatio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNet = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalLocal = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDrawer = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForShippingAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeliveryDate = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceHeaderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosteDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosteDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostedToStoreCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NetSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrawerLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemainingSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShippingAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyTypeLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountRatioSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxRatioSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLocalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyAddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyPhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyBalanceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCreditSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPosteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostedToStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscounteRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalLocal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDrawer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShippingAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeliveryDate)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.CodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DeliveryDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.BranchLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PosteDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PostedToStoreCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpensesSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.NetSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DrawerLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RemainingSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ShippingAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.NotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PartyIdGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PartyTypeLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DiscountValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DiscountRatioSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TaxValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TaxRatioSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalLocalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PartyAddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PartyPhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PartyBalanceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MaxCreditSpinEdit);
            this.dataLayoutControl1.DataSource = this.invoiceHeaderBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(988, 667);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(354, 49);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this._barManager;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(593, 453);
            this.gridControl1.TabIndex = 16;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // CodeTextEdit
            // 
            this.CodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Code", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CodeTextEdit.Location = new System.Drawing.Point(125, 291);
            this.CodeTextEdit.MenuManager = this._barManager;
            this.CodeTextEdit.Name = "CodeTextEdit";
            this.CodeTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CodeTextEdit.Size = new System.Drawing.Size(201, 20);
            this.CodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CodeTextEdit.TabIndex = 2;
            // 
            // invoiceHeaderBindingSource
            // 
            this.invoiceHeaderBindingSource.DataSource = typeof(MiltonXi._DAL.InvoiceHeader);
            // 
            // DateDateEdit
            // 
            this.DateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "IssueDate", true));
            this.DateDateEdit.EditValue = null;
            this.DateDateEdit.Location = new System.Drawing.Point(125, 315);
            this.DateDateEdit.MenuManager = this._barManager;
            this.DateDateEdit.Name = "DateDateEdit";
            this.DateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateDateEdit.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.DateDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.DateDateEdit.Size = new System.Drawing.Size(201, 20);
            this.DateDateEdit.StyleController = this.dataLayoutControl1;
            this.DateDateEdit.TabIndex = 3;
            // 
            // DeliveryDateDateEdit
            // 
            this.DeliveryDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "DeliveryDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DeliveryDateDateEdit.EditValue = null;
            this.DeliveryDateDateEdit.Location = new System.Drawing.Point(805, 555);
            this.DeliveryDateDateEdit.MenuManager = this._barManager;
            this.DeliveryDateDateEdit.Name = "DeliveryDateDateEdit";
            this.DeliveryDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DeliveryDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeliveryDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeliveryDateDateEdit.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.DeliveryDateDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.DeliveryDateDateEdit.Size = new System.Drawing.Size(142, 20);
            this.DeliveryDateDateEdit.StyleController = this.dataLayoutControl1;
            this.DeliveryDateDateEdit.TabIndex = 14;
            // 
            // BranchLookUpEdit
            // 
            this.BranchLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Branch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BranchLookUpEdit.Location = new System.Drawing.Point(125, 462);
            this.BranchLookUpEdit.MenuManager = this._barManager;
            this.BranchLookUpEdit.Name = "BranchLookUpEdit";
            this.BranchLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BranchLookUpEdit.Properties.NullText = "";
            this.BranchLookUpEdit.Size = new System.Drawing.Size(201, 20);
            this.BranchLookUpEdit.StyleController = this.dataLayoutControl1;
            this.BranchLookUpEdit.TabIndex = 6;
            // 
            // PosteDateDateEdit
            // 
            this.PosteDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "PosteDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PosteDateDateEdit.EditValue = null;
            this.PosteDateDateEdit.Location = new System.Drawing.Point(125, 438);
            this.PosteDateDateEdit.MenuManager = this._barManager;
            this.PosteDateDateEdit.Name = "PosteDateDateEdit";
            this.PosteDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.PosteDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PosteDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PosteDateDateEdit.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.PosteDateDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.PosteDateDateEdit.Size = new System.Drawing.Size(201, 20);
            this.PosteDateDateEdit.StyleController = this.dataLayoutControl1;
            this.PosteDateDateEdit.TabIndex = 5;
            // 
            // PostedToStoreCheckEdit
            // 
            this.PostedToStoreCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "PostedToStore", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PostedToStoreCheckEdit.Location = new System.Drawing.Point(125, 412);
            this.PostedToStoreCheckEdit.MenuManager = this._barManager;
            this.PostedToStoreCheckEdit.Name = "PostedToStoreCheckEdit";
            this.PostedToStoreCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.PostedToStoreCheckEdit.Properties.Caption = "Posted To Store";
            this.PostedToStoreCheckEdit.Size = new System.Drawing.Size(201, 22);
            this.PostedToStoreCheckEdit.StyleController = this.dataLayoutControl1;
            this.PostedToStoreCheckEdit.TabIndex = 4;
            // 
            // TotalSpinEdit
            // 
            this.TotalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Total", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalSpinEdit.Location = new System.Drawing.Point(125, 651);
            this.TotalSpinEdit.MenuManager = this._barManager;
            this.TotalSpinEdit.Name = "TotalSpinEdit";
            this.TotalSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TotalSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.TotalSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TotalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TotalSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSpinEdit.Properties.ReadOnly = true;
            this.TotalSpinEdit.Size = new System.Drawing.Size(201, 26);
            this.TotalSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalSpinEdit.TabIndex = 13;
            // 
            // ExpensesSpinEdit
            // 
            this.ExpensesSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Expenses", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExpensesSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpensesSpinEdit.Location = new System.Drawing.Point(125, 603);
            this.ExpensesSpinEdit.MenuManager = this._barManager;
            this.ExpensesSpinEdit.Name = "ExpensesSpinEdit";
            this.ExpensesSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ExpensesSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ExpensesSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpensesSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.ExpensesSpinEdit.Properties.Mask.EditMask = "c";
            this.ExpensesSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpensesSpinEdit.Properties.ReadOnly = true;
            this.ExpensesSpinEdit.Size = new System.Drawing.Size(201, 20);
            this.ExpensesSpinEdit.StyleController = this.dataLayoutControl1;
            this.ExpensesSpinEdit.TabIndex = 11;
            // 
            // NetSpinEdit
            // 
            this.NetSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Net", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NetSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NetSpinEdit.Location = new System.Drawing.Point(125, 555);
            this.NetSpinEdit.MenuManager = this._barManager;
            this.NetSpinEdit.Name = "NetSpinEdit";
            this.NetSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NetSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.NetSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.NetSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NetSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NetSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.NetSpinEdit.Properties.Mask.EditMask = "c";
            this.NetSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NetSpinEdit.Properties.ReadOnly = true;
            this.NetSpinEdit.Size = new System.Drawing.Size(201, 20);
            this.NetSpinEdit.StyleController = this.dataLayoutControl1;
            this.NetSpinEdit.TabIndex = 19;
            // 
            // DrawerLookUpEdit
            // 
            this.DrawerLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Drawer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DrawerLookUpEdit.Location = new System.Drawing.Point(455, 555);
            this.DrawerLookUpEdit.MenuManager = this._barManager;
            this.DrawerLookUpEdit.Name = "DrawerLookUpEdit";
            this.DrawerLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DrawerLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DrawerLookUpEdit.Properties.NullText = "";
            this.DrawerLookUpEdit.Size = new System.Drawing.Size(221, 20);
            this.DrawerLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DrawerLookUpEdit.TabIndex = 12;
            // 
            // RemainingSpinEdit
            // 
            this.RemainingSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Remaining", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RemainingSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RemainingSpinEdit.Location = new System.Drawing.Point(518, 649);
            this.RemainingSpinEdit.MenuManager = this._barManager;
            this.RemainingSpinEdit.Name = "RemainingSpinEdit";
            this.RemainingSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemainingSpinEdit.Properties.Appearance.ForeColor = System.Drawing.Color.OrangeRed;
            this.RemainingSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.RemainingSpinEdit.Properties.Appearance.Options.UseForeColor = true;
            this.RemainingSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.RemainingSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RemainingSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RemainingSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.RemainingSpinEdit.Properties.Mask.EditMask = "c";
            this.RemainingSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RemainingSpinEdit.Properties.ReadOnly = true;
            this.RemainingSpinEdit.Size = new System.Drawing.Size(158, 58);
            this.RemainingSpinEdit.StyleController = this.dataLayoutControl1;
            this.RemainingSpinEdit.TabIndex = 22;
            // 
            // ShippingAddressMemoEdit
            // 
            this.ShippingAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "ShippingAddress", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ShippingAddressMemoEdit.Location = new System.Drawing.Point(702, 593);
            this.ShippingAddressMemoEdit.MenuManager = this._barManager;
            this.ShippingAddressMemoEdit.Name = "ShippingAddressMemoEdit";
            this.ShippingAddressMemoEdit.Size = new System.Drawing.Size(247, 116);
            this.ShippingAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.ShippingAddressMemoEdit.TabIndex = 15;
            // 
            // NotesMemoEdit
            // 
            this.NotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Notes", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NotesMemoEdit.Location = new System.Drawing.Point(125, 486);
            this.NotesMemoEdit.MenuManager = this._barManager;
            this.NotesMemoEdit.Name = "NotesMemoEdit";
            this.NotesMemoEdit.Size = new System.Drawing.Size(201, 16);
            this.NotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.NotesMemoEdit.TabIndex = 7;
            // 
            // PaidSpinEdit
            // 
            this.PaidSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "Paid", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PaidSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PaidSpinEdit.Location = new System.Drawing.Point(354, 649);
            this.PaidSpinEdit.MenuManager = this._barManager;
            this.PaidSpinEdit.Name = "PaidSpinEdit";
            this.PaidSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaidSpinEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.PaidSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.PaidSpinEdit.Properties.Appearance.Options.UseForeColor = true;
            this.PaidSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PaidSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PaidSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Undo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "0", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PaidSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.PaidSpinEdit.Properties.Mask.EditMask = "c";
            this.PaidSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PaidSpinEdit.Size = new System.Drawing.Size(160, 58);
            this.PaidSpinEdit.StyleController = this.dataLayoutControl1;
            this.PaidSpinEdit.TabIndex = 13;
            // 
            // PartyIdGridLookUpEdit
            // 
            this.PartyIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "PartyId", true));
            this.PartyIdGridLookUpEdit.Location = new System.Drawing.Point(125, 73);
            this.PartyIdGridLookUpEdit.MenuManager = this._barManager;
            this.PartyIdGridLookUpEdit.Name = "PartyIdGridLookUpEdit";
            this.PartyIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)});
            this.PartyIdGridLookUpEdit.Properties.NullText = "";
            this.PartyIdGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.PartyIdGridLookUpEdit.Size = new System.Drawing.Size(201, 20);
            this.PartyIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PartyIdGridLookUpEdit.TabIndex = 1;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // PartyTypeLookUpEdit
            // 
            this.PartyTypeLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "PartyType", true));
            this.PartyTypeLookUpEdit.Location = new System.Drawing.Point(125, 49);
            this.PartyTypeLookUpEdit.MenuManager = this._barManager;
            this.PartyTypeLookUpEdit.Name = "PartyTypeLookUpEdit";
            this.PartyTypeLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PartyTypeLookUpEdit.Properties.NullText = "";
            this.PartyTypeLookUpEdit.Size = new System.Drawing.Size(201, 20);
            this.PartyTypeLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PartyTypeLookUpEdit.TabIndex = 0;
            // 
            // DiscountValueSpinEdit
            // 
            this.DiscountValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "DiscountValue", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DiscountValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DiscountValueSpinEdit.Location = new System.Drawing.Point(125, 627);
            this.DiscountValueSpinEdit.MenuManager = this._barManager;
            this.DiscountValueSpinEdit.Name = "DiscountValueSpinEdit";
            this.DiscountValueSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DiscountValueSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DiscountValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DiscountValueSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.DiscountValueSpinEdit.Properties.Mask.EditMask = "c";
            this.DiscountValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DiscountValueSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.DiscountValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.DiscountValueSpinEdit.TabIndex = 23;
            // 
            // DiscountRatioSpinEdit
            // 
            this.DiscountRatioSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "DiscountRatio", true));
            this.DiscountRatioSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DiscountRatioSpinEdit.Location = new System.Drawing.Point(226, 627);
            this.DiscountRatioSpinEdit.MenuManager = this._barManager;
            this.DiscountRatioSpinEdit.Name = "DiscountRatioSpinEdit";
            this.DiscountRatioSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DiscountRatioSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DiscountRatioSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DiscountRatioSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.DiscountRatioSpinEdit.Properties.Mask.EditMask = "p";
            this.DiscountRatioSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DiscountRatioSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.DiscountRatioSpinEdit.StyleController = this.dataLayoutControl1;
            this.DiscountRatioSpinEdit.TabIndex = 24;
            // 
            // TaxValueSpinEdit
            // 
            this.TaxValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "TaxValue", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TaxValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TaxValueSpinEdit.Location = new System.Drawing.Point(125, 579);
            this.TaxValueSpinEdit.MenuManager = this._barManager;
            this.TaxValueSpinEdit.Name = "TaxValueSpinEdit";
            this.TaxValueSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TaxValueSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TaxValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TaxValueSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TaxValueSpinEdit.Properties.Mask.EditMask = "c";
            this.TaxValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TaxValueSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.TaxValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.TaxValueSpinEdit.TabIndex = 25;
            // 
            // TaxRatioSpinEdit
            // 
            this.TaxRatioSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "TaxRatio", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TaxRatioSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TaxRatioSpinEdit.Location = new System.Drawing.Point(226, 579);
            this.TaxRatioSpinEdit.MenuManager = this._barManager;
            this.TaxRatioSpinEdit.Name = "TaxRatioSpinEdit";
            this.TaxRatioSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TaxRatioSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TaxRatioSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TaxRatioSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TaxRatioSpinEdit.Properties.Mask.EditMask = "p";
            this.TaxRatioSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TaxRatioSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.TaxRatioSpinEdit.StyleController = this.dataLayoutControl1;
            this.TaxRatioSpinEdit.TabIndex = 26;
            // 
            // DueDateDateEdit
            // 
            this.DueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "DueDate", true));
            this.DueDateDateEdit.EditValue = null;
            this.DueDateDateEdit.Location = new System.Drawing.Point(125, 339);
            this.DueDateDateEdit.MenuManager = this._barManager;
            this.DueDateDateEdit.Name = "DueDateDateEdit";
            this.DueDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DueDateDateEdit.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Fluent;
            this.DueDateDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.DueDateDateEdit.Size = new System.Drawing.Size(201, 20);
            this.DueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.DueDateDateEdit.TabIndex = 28;
            // 
            // TotalLocalSpinEdit
            // 
            this.TotalLocalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.invoiceHeaderBindingSource, "TotalLocal", true));
            this.TotalLocalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalLocalSpinEdit.Location = new System.Drawing.Point(125, 681);
            this.TotalLocalSpinEdit.MenuManager = this._barManager;
            this.TotalLocalSpinEdit.Name = "TotalLocalSpinEdit";
            this.TotalLocalSpinEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.TotalLocalSpinEdit.Properties.Appearance.Options.UseFont = true;
            this.TotalLocalSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalLocalSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TotalLocalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalLocalSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.TotalLocalSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalLocalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalLocalSpinEdit.Size = new System.Drawing.Size(201, 26);
            this.TotalLocalSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalLocalSpinEdit.TabIndex = 29;
            // 
            // PartyAddressTextEdit
            // 
            this.PartyAddressTextEdit.Location = new System.Drawing.Point(137, 134);
            this.PartyAddressTextEdit.MenuManager = this._barManager;
            this.PartyAddressTextEdit.Name = "PartyAddressTextEdit";
            this.PartyAddressTextEdit.Properties.ReadOnly = true;
            this.PartyAddressTextEdit.Size = new System.Drawing.Size(177, 20);
            this.PartyAddressTextEdit.StyleController = this.dataLayoutControl1;
            this.PartyAddressTextEdit.TabIndex = 30;
            // 
            // PartyPhoneTextEdit
            // 
            this.PartyPhoneTextEdit.Location = new System.Drawing.Point(137, 158);
            this.PartyPhoneTextEdit.MenuManager = this._barManager;
            this.PartyPhoneTextEdit.Name = "PartyPhoneTextEdit";
            this.PartyPhoneTextEdit.Properties.ReadOnly = true;
            this.PartyPhoneTextEdit.Size = new System.Drawing.Size(177, 20);
            this.PartyPhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PartyPhoneTextEdit.TabIndex = 31;
            // 
            // PartyBalanceTextEdit
            // 
            this.PartyBalanceTextEdit.Location = new System.Drawing.Point(137, 206);
            this.PartyBalanceTextEdit.MenuManager = this._barManager;
            this.PartyBalanceTextEdit.Name = "PartyBalanceTextEdit";
            this.PartyBalanceTextEdit.Properties.ReadOnly = true;
            this.PartyBalanceTextEdit.Size = new System.Drawing.Size(177, 20);
            this.PartyBalanceTextEdit.StyleController = this.dataLayoutControl1;
            this.PartyBalanceTextEdit.TabIndex = 33;
            // 
            // MaxCreditSpinEdit
            // 
            this.MaxCreditSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MaxCreditSpinEdit.Location = new System.Drawing.Point(137, 182);
            this.MaxCreditSpinEdit.MenuManager = this._barManager;
            this.MaxCreditSpinEdit.Name = "MaxCreditSpinEdit";
            this.MaxCreditSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.MaxCreditSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.MaxCreditSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MaxCreditSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.MaxCreditSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.MaxCreditSpinEdit.Properties.ReadOnly = true;
            this.MaxCreditSpinEdit.Size = new System.Drawing.Size(177, 20);
            this.MaxCreditSpinEdit.StyleController = this.dataLayoutControl1;
            this.MaxCreditSpinEdit.TabIndex = 32;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(971, 731);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup2,
            this.layoutControlGroup8,
            this.layoutControlGroupPost,
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.layoutControlGroup7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(951, 711);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDate,
            this.ItemForCode,
            this.ItemForDueDate});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 242);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(330, 121);
            this.layoutControlGroup3.Text = "Invoice info.";
            // 
            // ItemForDate
            // 
            this.ItemForDate.Control = this.DateDateEdit;
            this.ItemForDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForDate.Name = "ItemForDate";
            this.ItemForDate.Size = new System.Drawing.Size(306, 24);
            this.ItemForDate.Text = "Date";
            this.ItemForDate.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForCode
            // 
            this.ItemForCode.Control = this.CodeTextEdit;
            this.ItemForCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForCode.Name = "ItemForCode";
            this.ItemForCode.Size = new System.Drawing.Size(306, 24);
            this.ItemForCode.Text = "Invoice N.";
            this.ItemForCode.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForDueDate
            // 
            this.ItemForDueDate.Control = this.DueDateDateEdit;
            this.ItemForDueDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForDueDate.Name = "ItemForDueDate";
            this.ItemForDueDate.Size = new System.Drawing.Size(306, 24);
            this.ItemForDueDate.Text = "Due Date";
            this.ItemForDueDate.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPartType,
            this.ItemForPartId,
            this.layoutControlGroup5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(330, 242);
            this.layoutControlGroup2.Text = "Dealing party";
            // 
            // ItemForPartType
            // 
            this.ItemForPartType.Control = this.PartyTypeLookUpEdit;
            this.ItemForPartType.Location = new System.Drawing.Point(0, 0);
            this.ItemForPartType.Name = "ItemForPartType";
            this.ItemForPartType.Size = new System.Drawing.Size(306, 24);
            this.ItemForPartType.Text = "Party Type";
            this.ItemForPartType.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPartId
            // 
            this.ItemForPartId.Control = this.PartyIdGridLookUpEdit;
            this.ItemForPartId.Location = new System.Drawing.Point(0, 24);
            this.ItemForPartId.Name = "ItemForPartId";
            this.ItemForPartId.Size = new System.Drawing.Size(306, 24);
            this.ItemForPartId.Text = "Party";
            this.ItemForPartId.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.ExpandOnDoubleClick = true;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(306, 145);
            this.layoutControlGroup5.Text = "Party Information";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.PartyBalanceTextEdit;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem5.Text = "Current Balance";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.MaxCreditSpinEdit;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem4.Text = "Credit limit";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.PartyPhoneTextEdit;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem3.Text = "Phone";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.PartyAddressTextEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem2.Text = "Adress";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.layoutControlGroup8.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup8.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup8.Location = new System.Drawing.Point(330, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(621, 506);
            this.layoutControlGroup8.Text = "Products";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(597, 457);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupPost
            // 
            this.layoutControlGroupPost.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
            this.layoutControlGroupPost.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroupPost.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroupPost.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupPost.ExpandButtonVisible = true;
            this.layoutControlGroupPost.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPosteDate,
            this.ItemForPostedToStore,
            this.ItemForBranch,
            this.ItemForNotes});
            this.layoutControlGroupPost.Location = new System.Drawing.Point(0, 363);
            this.layoutControlGroupPost.Name = "layoutControlGroupPost";
            this.layoutControlGroupPost.Size = new System.Drawing.Size(330, 143);
            this.layoutControlGroupPost.Text = "Branch and store";
            // 
            // ItemForPosteDate
            // 
            this.ItemForPosteDate.Control = this.PosteDateDateEdit;
            this.ItemForPosteDate.Location = new System.Drawing.Point(0, 26);
            this.ItemForPosteDate.Name = "ItemForPosteDate";
            this.ItemForPosteDate.Size = new System.Drawing.Size(306, 24);
            this.ItemForPosteDate.Text = "Poste Date";
            this.ItemForPosteDate.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPostedToStore
            // 
            this.ItemForPostedToStore.Control = this.PostedToStoreCheckEdit;
            this.ItemForPostedToStore.Location = new System.Drawing.Point(0, 0);
            this.ItemForPostedToStore.Name = "ItemForPostedToStore";
            this.ItemForPostedToStore.Size = new System.Drawing.Size(306, 26);
            this.ItemForPostedToStore.Text = " ";
            this.ItemForPostedToStore.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForBranch
            // 
            this.ItemForBranch.Control = this.BranchLookUpEdit;
            this.ItemForBranch.Location = new System.Drawing.Point(0, 50);
            this.ItemForBranch.Name = "ItemForBranch";
            this.ItemForBranch.Size = new System.Drawing.Size(306, 24);
            this.ItemForBranch.Text = "Branch";
            this.ItemForBranch.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesMemoEdit;
            this.ItemForNotes.Location = new System.Drawing.Point(0, 74);
            this.ItemForNotes.MaxSize = new System.Drawing.Size(306, 0);
            this.ItemForNotes.MinSize = new System.Drawing.Size(306, 20);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(306, 20);
            this.ItemForNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNotes.StartNewLine = true;
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextLocation = DevExpress.Utils.Locations.Left;
            this.ItemForNotes.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDiscountValue,
            this.ItemForDiscounteRatio,
            this.ItemForExpenses,
            this.ItemForTaxValue,
            this.ItemForTaxRatio,
            this.ItemForTotal,
            this.ItemForNet,
            this.ItemForTotalLocal});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 506);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(330, 205);
            this.layoutControlGroup4.Text = "Total and payment";
            // 
            // ItemForDiscountValue
            // 
            this.ItemForDiscountValue.Control = this.DiscountValueSpinEdit;
            this.ItemForDiscountValue.Location = new System.Drawing.Point(0, 72);
            this.ItemForDiscountValue.Name = "ItemForDiscountValue";
            this.ItemForDiscountValue.Size = new System.Drawing.Size(202, 24);
            this.ItemForDiscountValue.Text = "Discount Value";
            this.ItemForDiscountValue.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForDiscounteRatio
            // 
            this.ItemForDiscounteRatio.Control = this.DiscountRatioSpinEdit;
            this.ItemForDiscounteRatio.Location = new System.Drawing.Point(202, 72);
            this.ItemForDiscounteRatio.Name = "ItemForDiscounteRatio";
            this.ItemForDiscounteRatio.Size = new System.Drawing.Size(104, 24);
            this.ItemForDiscounteRatio.Text = "Discounte Ratio";
            this.ItemForDiscounteRatio.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForDiscounteRatio.TextVisible = false;
            // 
            // ItemForExpenses
            // 
            this.ItemForExpenses.Control = this.ExpensesSpinEdit;
            this.ItemForExpenses.Location = new System.Drawing.Point(0, 48);
            this.ItemForExpenses.Name = "ItemForExpenses";
            this.ItemForExpenses.Size = new System.Drawing.Size(306, 24);
            this.ItemForExpenses.Text = "Additional expenses";
            this.ItemForExpenses.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForTaxValue
            // 
            this.ItemForTaxValue.Control = this.TaxValueSpinEdit;
            this.ItemForTaxValue.Location = new System.Drawing.Point(0, 24);
            this.ItemForTaxValue.Name = "ItemForTaxValue";
            this.ItemForTaxValue.Size = new System.Drawing.Size(202, 24);
            this.ItemForTaxValue.Text = "Tax Value";
            this.ItemForTaxValue.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForTaxRatio
            // 
            this.ItemForTaxRatio.Control = this.TaxRatioSpinEdit;
            this.ItemForTaxRatio.Location = new System.Drawing.Point(202, 24);
            this.ItemForTaxRatio.Name = "ItemForTaxRatio";
            this.ItemForTaxRatio.Size = new System.Drawing.Size(104, 24);
            this.ItemForTaxRatio.Text = "Tax Ratio";
            this.ItemForTaxRatio.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTaxRatio.TextVisible = false;
            // 
            // ItemForTotal
            // 
            this.ItemForTotal.Control = this.TotalSpinEdit;
            this.ItemForTotal.Location = new System.Drawing.Point(0, 96);
            this.ItemForTotal.Name = "ItemForTotal";
            this.ItemForTotal.Size = new System.Drawing.Size(306, 30);
            this.ItemForTotal.Text = "Total";
            this.ItemForTotal.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForNet
            // 
            this.ItemForNet.Control = this.NetSpinEdit;
            this.ItemForNet.Location = new System.Drawing.Point(0, 0);
            this.ItemForNet.Name = "ItemForNet";
            this.ItemForNet.Size = new System.Drawing.Size(306, 24);
            this.ItemForNet.Text = "Net";
            this.ItemForNet.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForTotalLocal
            // 
            this.ItemForTotalLocal.Control = this.TotalLocalSpinEdit;
            this.ItemForTotalLocal.Location = new System.Drawing.Point(0, 126);
            this.ItemForTotalLocal.Name = "ItemForTotalLocal";
            this.ItemForTotalLocal.Size = new System.Drawing.Size(306, 30);
            this.ItemForTotalLocal.Text = "Total Local Currency";
            this.ItemForTotalLocal.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup6.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDrawer,
            this.ItemForPaid,
            this.ItemForRemaining});
            this.layoutControlGroup6.Location = new System.Drawing.Point(330, 506);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(350, 205);
            this.layoutControlGroup6.Text = "Settlement details";
            // 
            // ItemForDrawer
            // 
            this.ItemForDrawer.Control = this.DrawerLookUpEdit;
            this.ItemForDrawer.Location = new System.Drawing.Point(0, 0);
            this.ItemForDrawer.Name = "ItemForDrawer";
            this.ItemForDrawer.Size = new System.Drawing.Size(326, 24);
            this.ItemForDrawer.Text = "Drawer";
            this.ItemForDrawer.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPaid
            // 
            this.ItemForPaid.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 20F);
            this.ItemForPaid.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPaid.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPaid.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ItemForPaid.Control = this.PaidSpinEdit;
            this.ItemForPaid.Location = new System.Drawing.Point(0, 24);
            this.ItemForPaid.Name = "ItemForPaid";
            this.ItemForPaid.Size = new System.Drawing.Size(164, 132);
            this.ItemForPaid.Text = "Paid";
            this.ItemForPaid.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForPaid.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForPaid.TextSize = new System.Drawing.Size(100, 65);
            this.ItemForPaid.TextToControlDistance = 5;
            // 
            // ItemForRemaining
            // 
            this.ItemForRemaining.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 20F);
            this.ItemForRemaining.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForRemaining.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForRemaining.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ItemForRemaining.Control = this.RemainingSpinEdit;
            this.ItemForRemaining.Location = new System.Drawing.Point(164, 24);
            this.ItemForRemaining.Name = "ItemForRemaining";
            this.ItemForRemaining.Size = new System.Drawing.Size(162, 132);
            this.ItemForRemaining.Text = "Remaining";
            this.ItemForRemaining.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForRemaining.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForRemaining.TextSize = new System.Drawing.Size(100, 65);
            this.ItemForRemaining.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.layoutControlGroup7.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForShippingAddress,
            this.ItemForDeliveryDate});
            this.layoutControlGroup7.Location = new System.Drawing.Point(680, 506);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(271, 205);
            this.layoutControlGroup7.Text = "Delivery and Shipping";
            // 
            // ItemForShippingAddress
            // 
            this.ItemForShippingAddress.Control = this.ShippingAddressMemoEdit;
            this.ItemForShippingAddress.Location = new System.Drawing.Point(0, 24);
            this.ItemForShippingAddress.Name = "ItemForShippingAddress";
            this.ItemForShippingAddress.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.ItemForShippingAddress.Size = new System.Drawing.Size(247, 132);
            this.ItemForShippingAddress.StartNewLine = true;
            this.ItemForShippingAddress.Text = "Shipping Address";
            this.ItemForShippingAddress.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForShippingAddress.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForDeliveryDate
            // 
            this.ItemForDeliveryDate.Control = this.DeliveryDateDateEdit;
            this.ItemForDeliveryDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForDeliveryDate.Name = "ItemForDeliveryDate";
            this.ItemForDeliveryDate.Size = new System.Drawing.Size(247, 24);
            this.ItemForDeliveryDate.Text = "Delivery Date";
            this.ItemForDeliveryDate.TextSize = new System.Drawing.Size(98, 13);
            // 
            // frm_Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 695);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_Invoice";
            this.Text = "Invoice";
            this.Load += new System.EventHandler(this.frm_Invoice_Load);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.invoiceHeaderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosteDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosteDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostedToStoreCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NetSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrawerLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemainingSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShippingAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyTypeLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountRatioSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxRatioSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLocalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyAddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyPhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyBalanceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCreditSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPartId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPosteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostedToStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscountValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscounteRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalLocal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDrawer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShippingAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeliveryDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource invoiceHeaderBindingSource;
        private DevExpress.XtraEditors.TextEdit CodeTextEdit;
        private DevExpress.XtraEditors.DateEdit DateDateEdit;
        private DevExpress.XtraEditors.DateEdit DeliveryDateDateEdit;
        private DevExpress.XtraEditors.LookUpEdit BranchLookUpEdit;
        private DevExpress.XtraEditors.DateEdit PosteDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit PostedToStoreCheckEdit;
        private DevExpress.XtraEditors.SpinEdit TotalSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ExpensesSpinEdit;
        private DevExpress.XtraEditors.SpinEdit NetSpinEdit;
        private DevExpress.XtraEditors.LookUpEdit DrawerLookUpEdit;
        private DevExpress.XtraEditors.SpinEdit RemainingSpinEdit;
        private DevExpress.XtraEditors.MemoEdit ShippingAddressMemoEdit;
        private DevExpress.XtraEditors.MemoEdit NotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShippingAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPosteDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostedToStore;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemaining;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDrawer;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpenses;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNet;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotal;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPartType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPartId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeliveryDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBranch;
        private DevExpress.XtraEditors.SpinEdit PaidSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaid;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.GridLookUpEdit PartyIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.LookUpEdit PartyTypeLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.SpinEdit DiscountValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit DiscountRatioSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TaxValueSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TaxRatioSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDiscountValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDiscounteRatio;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaxValue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaxRatio;
        private DevExpress.XtraEditors.DateEdit DueDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit TotalLocalSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalLocal;
        private DevExpress.XtraEditors.TextEdit PartyAddressTextEdit;
        private DevExpress.XtraEditors.TextEdit PartyPhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit PartyBalanceTextEdit;
        private DevExpress.XtraEditors.SpinEdit MaxCreditSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}