﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Class;
using MiltonXi._DAL;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using System.Data.Entity.Migrations;
using DevExpress.XtraReports.UI;
using MiltonXi.Reporting;
using RestSharp;
using Newtonsoft.Json.Linq;
using static MiltonXi.Class.MasterFinance;

namespace MiltonXi.Forms
{
    public partial class frm_Invoice : frm_Master
    {
        private bool IsDiscountValueFocused;
        private bool IsTaxValueFocused;
        private _DAL.InvoiceHeader invoiceHeader;
        private Master.InvoiceType type;
        private Entities generalDb;
        private BindingList<InvoiceDetail> invoiceDetails;
        private RepositoryItemGridLookUpEdit repoItems;
        private RepositoryItemLookUpEdit repoItemsAll;
        private RepositoryItemLookUpEdit repoUOM;
        private RepositoryItemLookUpEdit repoStore;

        public frm_Invoice()
        {
            InitializeComponent();
            type = Master.InvoiceType.Sales;
            PartyTypeLookUpEdit.EditValueChanged += PartyTypeLookUpEdit_EditValueChanged;
            PartyIdGridLookUpEdit.EditValueChanged += PartyIdGridLookUpEdit_EditValueChanged;

            RefreshData();
            New();
        }

        public frm_Invoice(Master.InvoiceType _type,int Id)
        {
            InitializeComponent();
            type = _type;
            InitializeEarlyEvents();
            RefreshData();
            using(var db = new Entities())
            {
                invoiceHeader = db.InvoiceHeaders.Single(x => x.Id ==Id);
                GetData();
                IsNew = false;
            }
        }
        public frm_Invoice(Master.InvoiceType _type)
        {
            InitializeComponent();
            type = _type;
            InitializeEarlyEvents();

            RefreshData();
            New();
        }
        void InitializeEarlyEvents()
        {
            PartyTypeLookUpEdit.EditValueChanged += PartyTypeLookUpEdit_EditValueChanged;
            PartyIdGridLookUpEdit.EditValueChanged += PartyIdGridLookUpEdit_EditValueChanged;
        }
        private void frm_Invoice_Load(object sender, EventArgs e)
        {
            switch (type)
            {
                case Master.InvoiceType.Purchase:
                    this.Text = "Purchase invoice";
                    this.Name = Class.Screens.frm_NewPurchaseInvoice.ScreenName;
                    PostedToStoreCheckEdit.Enabled = false;
                    PostedToStoreCheckEdit.Checked = true;
                    break;
                case Master.InvoiceType.Sales:
                    this.Name = Class.Screens.frm_NewSalesInvoice.ScreenName;
                    this.Name = "frm_SalesInvoice";
                    PostedToStoreCheckEdit.Checked = false;
                    break;
                case Master.InvoiceType.PurchaseReturn:
                case Master.InvoiceType.SalesReturn:
                default:
                    throw new NotImplementedException();
            }


            var d = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);



            this.btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            #region ToolTip
            //Configuring tooltip
            DevExpress.Utils.SuperToolTip _superToolTip = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem _superToolTipTitle = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem _superToolTipContent = new DevExpress.Utils.ToolTipItem();
            _superToolTip.Items.Add(_superToolTipTitle);
            _superToolTip.Items.Add(_superToolTipContent);

            //Tool Tip for Praty Type Customer and Vendro
            _superToolTipTitle.Text = $"New {((type == Master.InvoiceType.Purchase | type == Master.InvoiceType.Purchase) ? "vendor" : "customer") }";
            _superToolTipContent.Text = $"Adding new {((type == Master.InvoiceType.Purchase | type == Master.InvoiceType.Purchase) ? "vendor" : "customer") } " +
                "allowing you to use the new entry directly into this screen";
            PartyIdGridLookUpEdit.Properties.Buttons.Single(x => x.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus).SuperTip = _superToolTip;

            //Tool Tip for Paid value => reset to net value
            _superToolTipTitle.Text = "Reset";
            _superToolTipContent.Text = "Reset the value of this field back to the net value\r\nOnce this value is reset it " +
    "will auto respond to the changes of values in other fields";
            PaidSpinEdit.Properties.Buttons.Single(x => x.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Undo).SuperTip = _superToolTip;
            _superToolTipTitle.Text = "Reset";
            _superToolTipContent.Text = "Reset the value of this field to zero" +
    "NOTE:the field will not auto respond to the changes of values in other fields";
            PaidSpinEdit.Properties.Buttons.Single(x => x.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph).SuperTip = _superToolTip;
            #endregion



            invoiceHeader = invoiceHeaderBindingSource.DataSource as _DAL.InvoiceHeader;

            BranchLookUpEdit.Properties.Columns["Id"].Visible = false;

            DrawerLookUpEdit.Properties.Columns["Id"].Visible = false;
            DrawerLookUpEdit.Properties.Columns[nameof(Drawer.AccountId)].Visible = false;
            DrawerLookUpEdit.EditValueChanged += DrawerLookUpEdit_EditValueChanged;
            SetCurrencyMask();
            PartyTypeLookUpEdit.InitializeData(Class.Master.PartyTypesList);
            //  PartyTypeLookUpEdit.Properties.PopulateColumns();
            //    PartyTypeLookUpEdit.Properties.Columns[nameof(Product.Id)].Visible = false;
            PartyTypeLookUpEdit.HideColumns();

            //PartyIdGridLookUpEdit.Properties.PopulateViewColumns();
            //PartyIdGridLookUpEdit.Properties.ValidateOnEnterKey = true;
            //PartyIdGridLookUpEdit.Properties.AllowNullInput = DefaultBoolean.False;
            //PartyIdGridLookUpEdit.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            //PartyIdGridLookUpEdit.Properties.ImmediatePopup = true;
            //PartyIdGridLookUpEdit.Properties.View.FocusRectStyle = DrawFocusRectStyle.RowFullFocus;
            //PartyIdGridLookUpEdit.Properties.View.OptionsSelection.UseIndicatorForSelection = true;
            //PartyIdGridLookUpEdit.Properties.View.OptionsView.ShowAutoFilterRow = true;
            //PartyIdGridLookUpEdit.Properties.View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Id)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.IsCustomer)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.AccountId)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Notes)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Website)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Email)].Visible = false;




            repoItems = new RepositoryItemGridLookUpEdit();
            repoItems.InitializeData(Session.ProductsView.Where(x => x.Inactive == false), gridView1.Columns[nameof(InvoiceDetail.ItemId)], gridControl1);
            repoItems.PopulateViewColumns();
            repoItems.Name = "GridRepoItemForProductsActiveOnly";
            repoItems.ValidateOnEnterKey = true;
            repoItems.AllowNullInput = DefaultBoolean.False;
            repoItems.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            repoItems.ImmediatePopup = true;
            repoItems.View.FocusRectStyle = DrawFocusRectStyle.RowFullFocus;
            repoItems.View.OptionsSelection.UseIndicatorForSelection = true;
            repoItems.View.OptionsView.ShowAutoFilterRow = true;
            repoItems.View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            repoItems.View.Columns[nameof(Product.Id)].Visible = false;
            repoItems.View.Columns[nameof(Product.Inactive)].Visible = false;
            repoItems.View.Columns[nameof(Product.Type)].Visible = false;
            repoItems.Buttons.Add(new EditorButton(ButtonPredefines.Plus));
            repoItems.ButtonClick += RepoItems_ButtonClick;

            repoItemsAll = new RepositoryItemLookUpEdit();
            repoItemsAll.InitializeData(Session.ProductsView, gridView1.Columns[nameof(InvoiceDetail.ItemId)], gridControl1);
            repoItemsAll.Name = "LookupeditforAllProductsActive&Inactive";
            gridView1.Columns[nameof(InvoiceDetail.ItemId)].ColumnEdit = repoItemsAll;

            repoUOM = new RepositoryItemLookUpEdit();
            repoUOM.InitializeData(Session.UnitNames, gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)], gridControl1);
            repoUOM.PopulateColumns();
            repoUOM.Columns[nameof(UnitName.Id)].Visible = false;
            repoUOM.BestFitMode = BestFitMode.BestFitResizePopup;

            repoStore = new RepositoryItemLookUpEdit();
            repoStore.InitializeData(Session.Store, gridView1.Columns[nameof(InvoiceDetail.StoreId)], gridControl1);
            repoStore.PopulateColumns();
            repoStore.Columns["Id"].Visible = false;
            repoStore.BestFitMode = BestFitMode.BestFitResizePopup;



            gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            gridView1.Columns[nameof(InvoiceDetail.Id)].Visible = false;
            gridView1.Columns[nameof(InvoiceDetail.InvoiceId)].Visible = false;
            gridView1.Columns[nameof(InvoiceDetail.TotalPrice)].OptionsColumn.AllowFocus = false;
            gridView1.Columns[nameof(InvoiceDetail.ItemId)].Caption = "Item";
            gridView1.Columns[nameof(InvoiceDetail.Nontaxable)].Caption = "Nontaxable";
            gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)].Caption = "Unit";
            gridView1.Columns[nameof(InvoiceDetail.StoreId)].Caption = "Branch";
            gridView1.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmCode", FieldName = "Code", Caption = "Code", UnboundType = DevExpress.Data.UnboundColumnType.String });
            gridView1.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmIndex", FieldName = "Index", Caption = "#", UnboundType = DevExpress.Data.UnboundColumnType.Integer, MaxWidth = 25 });
            gridView1.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmBalance", FieldName = "Balance", Caption = "Balance", UnboundType = DevExpress.Data.UnboundColumnType.Decimal, MaxWidth = 75 });

            gridView1.Columns["Index"].OptionsColumn.AllowFocus = false;
            gridView1.Columns[nameof(InvoiceDetail.TotalCostValue)].OptionsColumn.AllowFocus = false;
            gridView1.Columns[nameof(InvoiceDetail.CostValue)].OptionsColumn.AllowFocus = false;
            gridView1.Columns["Index"].OptionsColumn.AllowFocus = false;
            gridView1.Columns["Index"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns["Index"].VisibleIndex = 0;
            gridView1.Columns["Code"].VisibleIndex = 1;
            gridView1.Columns[nameof(InvoiceDetail.ItemId)].MinWidth = 125;
            gridView1.Columns[nameof(InvoiceDetail.ItemId)].VisibleIndex = 2;
            gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)].VisibleIndex = 3;
            gridView1.Columns[nameof(InvoiceDetail.ItemQty)].VisibleIndex = 4;
            gridView1.Columns[nameof(InvoiceDetail.Price)].VisibleIndex = 5;
            gridView1.Columns[nameof(InvoiceDetail.Price)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.DiscountRatio)].VisibleIndex = 6;
            gridView1.Columns[nameof(InvoiceDetail.DiscountRatio)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.DiscountValue)].VisibleIndex = 7;
            gridView1.Columns[nameof(InvoiceDetail.DiscountValue)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.TotalPrice)].VisibleIndex = 8;
            gridView1.Columns[nameof(InvoiceDetail.TotalPrice)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.CostValue)].VisibleIndex = 9;
            gridView1.Columns[nameof(InvoiceDetail.CostValue)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.TotalCostValue)].VisibleIndex = 10;
            gridView1.Columns[nameof(InvoiceDetail.TotalCostValue)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.StoreId)].VisibleIndex = 11;
            gridView1.Columns[nameof(InvoiceDetail.Nontaxable)].Caption = "Non-Taxable";
            gridView1.Columns[nameof(InvoiceDetail.Nontaxable)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns[nameof(InvoiceDetail.Nontaxable)].VisibleIndex = 12;
            gridView1.Columns["Balance"].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
            gridView1.Columns["Balance"].VisibleIndex = 13;
            RepositoryItemButtonEdit deleteButton = new RepositoryItemButtonEdit();
            gridControl1.RepositoryItems.Add(deleteButton);
            deleteButton.Buttons.Clear();
            deleteButton.TextEditStyle = TextEditStyles.HideTextEditor;
            deleteButton.Buttons.Add(new EditorButton(ButtonPredefines.Delete));
            deleteButton.ButtonsStyle = BorderStyles.NoBorder;
            deleteButton.Buttons.SingleOrDefault().Appearance.Options.UseForeColor = true;
            deleteButton.Buttons.SingleOrDefault().Appearance.ForeColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;


            gridView1.Columns.Add(new GridColumn() { VisibleIndex = 100, Name = "clmDelete", FieldName = "Delete", Caption = "Del", Width = 15, ColumnEdit = deleteButton });
            gridView1.Columns["Delete"].VisibleIndex = 100;

            TaxRatioSpinEdit.Properties.Increment = 0.01m;
            TaxValueSpinEdit.Properties.Increment = 0.01m;
            DiscountValueSpinEdit.Properties.Increment = 0.01m;
            DiscountRatioSpinEdit.Properties.Increment = 0.01m;

            RepositoryItemSpinEdit spinEdit = new RepositoryItemSpinEdit();
            gridControl1.RepositoryItems.Add(spinEdit);
            gridView1.Columns[nameof(InvoiceDetail.DiscountValue)].ColumnEdit = spinEdit;
            gridView1.Columns[nameof(InvoiceDetail.Price)].ColumnEdit = spinEdit;
            gridView1.Columns[nameof(InvoiceDetail.ItemQty)].ColumnEdit = spinEdit;

            RepositoryItemSpinEdit spinEditRatio = new RepositoryItemSpinEdit();
            spinEditRatio.Increment = 0.01m;
            spinEditRatio.Mask.EditMask = "p";
            spinEditRatio.Mask.UseMaskAsDisplayFormat = true;
            spinEditRatio.MaxValue = 1;
            gridControl1.RepositoryItems.Add(spinEditRatio);
            gridView1.Columns[nameof(InvoiceDetail.DiscountRatio)].ColumnEdit = spinEditRatio;

            //  gridView1.Appearance.EvenRow.BackColor = EvenRowColor;
            //   gridView1.OptionsView.EnableAppearanceEvenRow = true;

            //    gridView1.Appearance.OddRow.BackColor = OddRowColor;
            gridView1.OptionsView.EnableAppearanceOddRow = true;

            ReadUsersettings();

            #region Events
            PartyIdGridLookUpEdit.ButtonClick += PartIdGridLookUpEdit_ButtonClick;
            DiscountValueSpinEdit.Enter += DiscountValueSpinEdit_Enter;
            DiscountValueSpinEdit.Leave += DiscountValueSpinEdit_Leave;
            TaxValueSpinEdit.Enter += TaxValueSpinEdit_Enter;
            TaxValueSpinEdit.Leave += TaxValueSpinEdit_Leave;

            DiscountValueSpinEdit.EditValueChanged += DiscountSpinEdit_EditValueChanged;
            DiscountRatioSpinEdit.EditValueChanged += DiscountSpinEdit_EditValueChanged;



            TaxValueSpinEdit.EditValueChanged += TaxSpinEdit_EditValueChanged;
            TaxRatioSpinEdit.EditValueChanged += TaxSpinEdit_EditValueChanged;



            TaxValueSpinEdit.EditValueChanged += CalcTotal_OnEditValueChanged;
            DiscountValueSpinEdit.EditValueChanged += CalcTotal_OnEditValueChanged;
            ExpensesSpinEdit.EditValueChanged += CalcTotal_OnEditValueChanged;
            NetSpinEdit.EditValueChanged += CalcTotal_OnEditValueChanged;
            NetSpinEdit.EditValueChanged += TaxSpinEdit_EditValueChanged;
            NetSpinEdit.EditValueChanged += DiscountSpinEdit_EditValueChanged;

            PaidSpinEdit.EditValueChanged += CalcRemaining_OnEditValueChanged;
            TotalSpinEdit.EditValueChanged += CalcRemaining_OnEditValueChanged;
            TotalSpinEdit.EditValueChanging += TotalSpinEdit_EditValueChanging;
            TotalSpinEdit.Validated += TotalSpinEdit_ValidatedAsync;
            PaidSpinEdit.ButtonPressed += PaidSpinEdit_ButtonPressed; ;
            gridView1.CustomRowCellEditForEditing += GridView1_CustomRowCellEditForEditing;
            gridView1.CellValueChanged += GridView1_CellValueChanged;
            gridView1.RowCountChanged += GridView1_RowCountChanged;
            gridView1.RowUpdated += GridView1_RowUpdated;
            BranchLookUpEdit.EditValueChanging += BranchLookUpEdit_EditValueChanging;
            // related to    gridView1.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmCode", FieldName = "Code", Caption = "BarCode" ,UnboundType= DevExpress.Data.UnboundColumnType.String});
            gridView1.CustomUnboundColumnData += GridView1_CustomUnboundColumnData;
            //
            gridControl1.ProcessGridKey += GridControl1_ProcessGridKey;
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            this.Activated += Frm_Invoice_Activated;
            deleteButton.ButtonClick += DeleteButton_ButtonClick;
            this.KeyPreview = true;
            this.KeyDown += Frm_Invoice_KeyDown;
            #endregion
            //  MoveFocusToGrid();



        }
        AccountBalance accountBalance;
        private void PartyIdGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            var id = Convert.ToInt32(PartyIdGridLookUpEdit.EditValue);
            if (id != 0)
            {
                CustomerAndVendor account;
                if (PartyTypeLookUpEdit.EditValue.Equals((byte)Master.PartyType.Vendor))
                {
                    account = Session.Vendors.Single(x => x.Id == id);
                }
                else
                {
                    account = Session.Customers.Single(x => x.Id == id);
                }
                PartyAddressTextEdit.Text = account.Address;
                PartyPhoneTextEdit.Text = account.Phone;
                accountBalance = GetAccountBalance(account.AccountId);
                PartyBalanceTextEdit.Text = accountBalance.Balance;
                MaxCreditSpinEdit.EditValue = account.MaxCredit;

            }
            else
            {
                PartyAddressTextEdit.Text = "";
                PartyPhoneTextEdit.Text = "";
                PartyBalanceTextEdit.Text = "";
                MaxCreditSpinEdit.EditValue = 0;
            }
        }

        private void DrawerLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {

            SetCurrencyMask();
        }
        public void SetCurrencyMask()
        {
            var row = DrawerLookUpEdit.GetSelectedDataRow() as Drawer;
            switch (row.Currency)
            {
                case "PLN":
                    TotalLocalSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    NetSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    TaxValueSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    DiscountValueSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    ExpensesSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    TotalSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    PaidSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    RemainingSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    break;
                case "EUR":
                    TotalLocalSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("pl");
                    NetSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    TaxValueSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    DiscountValueSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    ExpensesSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    TotalSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    PaidSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    RemainingSpinEdit.Properties.Mask.Culture = new System.Globalization.CultureInfo("de");
                    break;
                default:
                    break;
            }
        }
        private async void TotalSpinEdit_ValidatedAsync(object sender, EventArgs e)
        {
            //TODO move to seperate class
            invoiceHeader.Currency = (DrawerLookUpEdit.Properties.GetDataSourceRowByKeyValue(DrawerLookUpEdit.EditValue) as Drawer).Currency;
            if (Convert.ToInt32(TotalSpinEdit.EditValue) == 0 || Session.Defaults.Currency == invoiceHeader.Currency)
            {
                TotalLocalSpinEdit.EditValue = TotalSpinEdit.EditValue;
                return;
            }
            double Exrate = 1;
            //await  Task.Run(() =>
            // {
            //     //Provider https://free.currencyconverterapi.com/
            //     // other provider to check https://currencyfreaks.com/signup.html
            //     string FromToEx = $"{ invoiceHeader.Currency }_{ Session.Defaults.Currency}";
            //     string requestString = $@"https://free.currconv.com/api/v7/convert?q={FromToEx}&compact=ultra&apiKey={Session.Defaults.CurrencyExchangeAPIKey}";
            //     var client = new RestClient(requestString);
            //     client.Timeout = -1;
            //     var request = new RestRequest(Method.GET);
            //     IRestResponse response = client.Execute(request);

            //     Console.WriteLine(response.Content);
            //     string data = JObject.Parse(response.Content)[FromToEx].ToString();
            //      Exrate = Convert.ToDouble(data);

            // });

            TotalLocalSpinEdit.EditValue = await CurrencyConverter.FromApi.CurrConv(invoiceHeader.Currency, Session.Defaults.Currency, Convert.ToDouble(TotalSpinEdit.EditValue), Session.Defaults.CurrencyExchangeAPIKey);

            // TotalLocalSpinEdit.EditValue = Convert.ToDouble(TotalSpinEdit.EditValue) * Exrate;
        }

        private void RepoItems_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == ButtonPredefines.Plus)
            {
                Class.Utilities.OpenFormInMain(new frm_Product());
            }
        }

        void MoveFocusToGrid(bool FocusToItem = true)
        {
            //gridView1.Focus();
            //gridView1.FocusedColumn = FocusToItem ? gridView1.Columns["Code"] : gridView1.Columns[nameof(InvoiceDetail.ItemId)];
            //gridView1.AddNewRow();

            // gridView1.FocusedColumn = gridView1.Columns["Code"];
        }

        private void Frm_Invoice_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F5)
            //    MoveFocusToGrid(e.Modifiers == Keys.Shift);
            //else if (e.KeyCode == Keys.F6)
            //{ /*TODO go to product by index*/}
            //else if (e.KeyCode == Keys.F7)
            //    PartyIdGridLookUpEdit.Focus();
            //else if (e.KeyCode == Keys.F8)
            //    CodeTextEdit.Focus();
            //else if (e.KeyCode == Keys.F9)
            //    DiscountValueSpinEdit.Focus();
            //else if (e.KeyCode == Keys.F10)
            //    TaxValueSpinEdit.Focus();
            //else if (e.KeyCode == Keys.F11)
            //    ExpensesSpinEdit.Focus();
            //else if (e.KeyCode == Keys.F12)
            //    PaidSpinEdit.Focus();
        }
        private void GridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl control = sender as GridControl;
            if (control == null)
                return;
            GridView view = control.FocusedView as GridView;
            if (view == null)
                return;
            if (view.FocusedColumn == null) return;
            if (e.KeyCode == Keys.Return)
            {
                string focusedColumn = view.FocusedColumn.FieldName;
                if (view.FocusedColumn.FieldName == "Code" || view.FocusedColumn.FieldName == nameof(InvoiceDetail.ItemId))
                {
                    GridControl1_ProcessGridKey(sender, new KeyEventArgs(Keys.Tab));
                }
                if (view.FocusedRowHandle < 0)
                {
                    view.AddNewRow();
                    view.FocusedColumn = view.Columns[focusedColumn];
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Tab)
            {
                view.FocusedColumn = view.VisibleColumns[view.FocusedColumn.VisibleIndex - 1];
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Delete)
            {
                if (DialogResult.Yes == QuestionUser("you want to delete", "Delete"))
                {
                    view.DeleteSelectedRows();
                    e.Handled = true;

                }
            }


        }

        #region GridView

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            if (e.Row == null || (e.Row as InvoiceDetail).ItemId == 0)
            {
                gridView1.DeleteRow(e.RowHandle);
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.Ignore;
            }
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = e.Row as InvoiceDetail;
            if (row == null || row.ItemId == 0)
            {
                e.Valid = false;
                return;
            }

            switch (type)
            {
                case Master.InvoiceType.Purchase:

                    break;
                case Master.InvoiceType.Sales:
                    if (row.CostValue > 0)
                    {
                        switch (Session.UserSettings.Sales.WhenSellingItemWithPriceLowerThanCostPrice)
                        {
                            case Master.WarningLevels.DoNotInterrupt:
                                break;
                            case Master.WarningLevels.ShowWarning:
                                e.Valid = false;
                                gridView1.SetColumnError(gridView1.Columns[nameof(row.Price)], "Sell price less than the cost");

                                break;
                            case Master.WarningLevels.Prevent:
                                if (XtraMessageBox.Show(
                                    text: "Sell price less than the cost, Do you want to continue?",
                                    caption: "Sell confirmation",
                                    icon: MessageBoxIcon.Question,
                                    buttons: MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    e.Valid = false;
                                    gridView1.SetColumnError(gridView1.Columns[nameof(row.Price)], "Sell price less than the cost");
                                }
                                break;
                            default:
                                break;
                        }


                    }
                    if (Convert.ToDecimal(row.DiscountRatio) > Session.UserSettings.Sales.MaxDiscountPerItem)
                    {
                        e.Valid = false;
                        gridView1.SetColumnError(gridView1.Columns[nameof(row.DiscountRatio)], "Discount per item exceeded the limite");

                    }
                    break;

                case Master.InvoiceType.SalesReturn:
                    break;
                case Master.InvoiceType.PurchaseReturn:
                    break;
                default:
                    break;
            }

        }

        string enteredCode;
        private void GridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Code")
            {
                if (e.IsSetData)
                    enteredCode = e.Value.ToString();
                else if (e.IsGetData)
                    e.Value = enteredCode;

            }
            else if (e.Column.FieldName == "Index")
            {
                e.Value = gridView1.GetVisibleRowHandle(e.ListSourceRowIndex) + 1;
            }
            else if (e.Column.FieldName == "Balance")
            {
                var row = e.Row as InvoiceDetail;
                if (row == null || row.ItemId == 0 || row.StoreId == 0 || row.ItemUnitId==0)
                {
                    e.Value = null;
                    return;
                }
                var proBalance = Session.ProductsBalance.FirstOrDefault(x => x.ProductId == row.ItemId && x.StoreId == row.StoreId);
                if (proBalance == null)
                {
                    e.Value = 0;
                    return;
                }
                var balance = proBalance.Balance;
                var product = Session.ProductsView.Single(x => x.Id == row.ItemId);
                var factor = product.Units.Single(x => x.UnitId == row.ItemUnitId).Factor;
                e.Value = balance / factor;
            }
        }

        private void GridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            var items = gridView1.DataSource as BindingList<InvoiceDetail>;
            if (items == null)
                NetSpinEdit.EditValue = 0;
            else
            {
                ExpensesSpinEdit.EditValue = items.Where(x => x.Nontaxable == true).Sum(x => x.TotalPrice);
                NetSpinEdit.EditValue = items.Where(x => x.Nontaxable == false).Sum(x => x.TotalPrice);
            }

        }

        int CurrentGridRowCount = 0;
        private void GridView1_RowCountChanged(object sender, EventArgs e)
        {
            if (CurrentGridRowCount < gridView1.RowCount)
            {
                var rows = gridView1.DataSource as BindingList<InvoiceDetail>;
                var lastRow = rows.Last();
                var row = rows.FirstOrDefault(x => x.ItemId == lastRow.ItemId && x.ItemUnitId == lastRow.ItemUnitId && x != lastRow);
                if (row != null)
                {
                    row.ItemQty += lastRow.ItemQty;
                    GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(gridView1.FindRowHandleByRowObject(row),
                        gridView1.Columns[nameof(row.ItemQty)], row.ItemQty));
                    rows.Remove(lastRow);
                }
            }
            CurrentGridRowCount = gridView1.RowCount;
            GridView1_RowUpdated(sender, null);
        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as InvoiceDetail;
            if (row == null)
                return;
            Session.ProductViewClass itemV = null;
            Session.ProductViewClass.ProductUOMView unitV = null;
            // Handle the use of the barcode to auto select the product 
            if (e.Column.FieldName == "Code")
            {
                string ItemCode = e.Value.ToString();
                if (Session.GlobalSettings.ReadFromScaleBarcode &&
                    ItemCode.Length == Session.GlobalSettings.BarcodeLength &&
                   ItemCode.ToString().StartsWith(Session.GlobalSettings.ScaleBarcodePrefix))
                {
                    var itemCodeString = ItemCode.ToString().Substring(Session.GlobalSettings.ScaleBarcodePrefix.Length, Session.GlobalSettings.ProductCodeLength);
                    ItemCode = Convert.ToInt32(itemCodeString).ToString();
                    string readValue = ItemCode.ToString().Substring(
                        Session.GlobalSettings.ScaleBarcodePrefix.Length +
                        Session.GlobalSettings.ProductCodeLength);
                    if (Session.GlobalSettings.IgnoreCheckDigit)
                        readValue = readValue.Remove(readValue.Length - 1, 1);
                    double value = Convert.ToDouble(readValue);
                    value = value / (Math.Pow(10, Session.GlobalSettings.DivideValueBy));
                    if (Session.GlobalSettings.ReadMode == Session.GlobalSettings.ReadValueMode.Weight)
                    { row.ItemQty = value; }
                    else if (Session.GlobalSettings.ReadMode == Session.GlobalSettings.ReadValueMode.Price)
                    {
                        itemV = Session.ProductsView.FirstOrDefault(x => x.Units.Select(u => u.Barcode.ToLower()).Contains(ItemCode.ToLower()));
                        if (itemV != null)
                        {
                            unitV = itemV.Units.First(x => x.Barcode.ToLower() == ItemCode.ToString().ToLower());
                            switch (type)
                            {
                                case Master.InvoiceType.Sales:
                                case Master.InvoiceType.SalesReturn:
                                    row.ItemQty = value / unitV.SellPrice;
                                    break;
                                case Master.InvoiceType.Purchase:
                                case Master.InvoiceType.PurchaseReturn:
                                    row.ItemQty = value / unitV.BuyPrice;
                                    break;

                                default:
                                    break;
                            }
                        }

                    }
                }
                if (itemV == null)
                    itemV = Session.ProductsView.FirstOrDefault(x => x.Units.Select(u => u.Barcode.ToLower()).Contains(ItemCode.ToLower()));
                if (itemV != null)
                {
                    row.ItemId = itemV.Id;
                    if (unitV == null)
                        unitV = itemV.Units.First(x => x.Barcode.ToLower() == ItemCode.ToString().ToLower());
                    row.ItemUnitId = unitV.UnitId;
                    GridView1_CellValueChanged(sender,
                        new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(
                            e.RowHandle,
                            gridView1.Columns[nameof(InvoiceDetail.ItemId)], row.ItemId));

                    GridView1_CellValueChanged(sender,
                        new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(
                            e.RowHandle,
                            gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)], row.ItemUnitId));
                    enteredCode = string.Empty;
                    return;
                }
                enteredCode = string.Empty;

            }

            //////////////////////////////////////////
            if (row.ItemId == 0)
                return;
            itemV = Session.ProductsView.SingleOrDefault(x => x.Id == row.ItemId);
            if (row.ItemUnitId == 0)
            {
                row.ItemUnitId = itemV.Units.First().UnitId;
                GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)], row.ItemUnitId));
            }
            unitV = itemV.Units.SingleOrDefault(x => x.UnitId == row.ItemUnitId);
            //////////////////////////////////////////
            switch (e.Column.FieldName)
            {
                case nameof(InvoiceDetail.ItemId):
                    var item = Session.ProductsView.Single(x => x.Id == row.ItemId);
                    row.ItemUnitId = item.Units.FirstOrDefault().UnitId;
                    row.Nontaxable = item.NonTaxable;
                    if (row.StoreId == 0 && BranchLookUpEdit.IsEditValueNumberAndNotZero())
                        row.StoreId = ((int?)BranchLookUpEdit.EditValue) ?? 0;
                    GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(InvoiceDetail.ItemUnitId)], row.ItemUnitId));
                    break;
                case nameof(InvoiceDetail.ItemUnitId):
                    switch (type)
                    {
                        case Master.InvoiceType.Purchase:
                            row.Price = unitV.BuyPrice;
                            break;
                        case Master.InvoiceType.Sales:
                            row.Price = unitV.SellPrice;
                            break;
                        case Master.InvoiceType.PurchaseReturn:
                        case Master.InvoiceType.SalesReturn:
                            break;
                    }

                    if (row.ItemQty == 0)
                        row.ItemQty = 1;
                    GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(InvoiceDetail.Price)], row.Price));
                    // TODO Caluclate current item balance
                    break;

                case nameof(InvoiceDetail.Price):
                case nameof(InvoiceDetail.DiscountRatio):
                case nameof(InvoiceDetail.ItemQty):
                    //TODO Should get cost price from the store when change the Qty of the item or the item itself
                    row.DiscountValue = row.DiscountRatio * (row.ItemQty * row.Price);
                    GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(InvoiceDetail.DiscountValue)], row.DiscountValue));
                    break;

                case nameof(InvoiceDetail.DiscountValue):
                    if (gridView1.FocusedColumn.FieldName == nameof(InvoiceDetail.DiscountValue))
                    {
                        row.DiscountRatio = row.DiscountValue / (row.ItemQty * row.Price);
                    }
                    row.TotalPrice = (row.ItemQty * row.Price) - row.DiscountValue;
                    switch (type)
                    {
                        // TODO ممكن دمج حساب سعر الصرف زي الشراء عشان يبقي اللي داخل و اللي طالع زي بعض و ساعتها مش حنحسب هامش ربح
                        case Master.InvoiceType.Purchase:
                            // حساب تكلفه سعر الشراء 
                            // عشان تختلف عن سعر البيع
                            row.CostValue = row.TotalPrice / row.ItemQty;
                            row.TotalCostValue = row.TotalPrice;
                            break;
                        case Master.InvoiceType.Sales:
                            // حساب تكلفه البيع
                            var store = (row.StoreId == 0) ? Convert.ToInt32(BranchLookUpEdit.EditValue) : row.StoreId;
                            var costPerMainUnit = MasterInventory.GetCostOfNextProduct(row.ItemId, row.StoreId, row.ItemQty);
                            row.CostValue = costPerMainUnit * unitV.Factor;
                            row.TotalCostValue = row.CostValue * row.ItemQty;
                            break;
                        case Master.InvoiceType.PurchaseReturn:
                        case Master.InvoiceType.SalesReturn:
                        default:
                            throw new NotImplementedException();

                    }


                    break;

                case nameof(InvoiceDetail.Nontaxable):
                    var itemTx = Session.ProductsView.Single(x => x.Id == row.ItemId);

                    row.Nontaxable = itemTx.NonTaxable;
                    break;
                default:
                    break;
            }

        }

        private void GridView1_CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {

            switch (e.Column.FieldName)
            {
                case nameof(InvoiceDetail.ItemUnitId):
                    RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                    repo.Name = "Item Unit ID";
                    repo.NullText = "";
                    e.RepositoryItem = repo;
                    var row = gridView1.GetRow(e.RowHandle) as InvoiceDetail;
                    if (row == null)
                        return;
                    var item = Session.ProductsView.SingleOrDefault(x => x.Id == row.ItemId);
                    if (item == null)
                        return;
                    repo.DataSource = item.Units;
                    repo.ValueMember = nameof(Session.ProductViewClass.ProductUOMView.UnitId);
                    repo.DisplayMember = nameof(Session.ProductViewClass.ProductUOMView.UnitName);
                    repo.PopulateColumns();
                    repo.Columns[nameof(Session.ProductViewClass.ProductUOMView.UnitId)].Visible = false;
                    repo.BestFitMode = BestFitMode.BestFitResizePopup;
                    break;

                case nameof(InvoiceDetail.ItemId):
                    e.RepositoryItem = repoItems;
                    break;
                default:
                    break;
            }

        }

        private void DeleteButton_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridControl1_ProcessGridKey(gridControl1, new KeyEventArgs(Keys.Delete));
        }

        private void Frm_Invoice_Activated(object sender, EventArgs e)
        {
            MoveFocusToGrid();
        }
        #endregion

        #region SpinEdit

        private void PaidSpinEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            SpinEdit spn = sender as SpinEdit;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph:
                    spn.EditValue = 0;
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Undo:
                    if (spn.Name == nameof(PaidSpinEdit))
                    {
                        PaidSpinEdit.EditValue = TotalSpinEdit.EditValue;
                    }

                    break;
                default:
                    break;
            }
        }

        

        private void TotalSpinEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {


            if (type == Master.InvoiceType.Sales && Session.UserSettings.Sales.DefaultPaymentMethodInSales == Master.PaymentMethod.Cash)
            {
                if (Convert.ToDouble(e.OldValue) == (Convert.ToDouble(PaidSpinEdit.EditValue)))
                {
                    PaidSpinEdit.EditValue = e.NewValue;
                }

            }
            else if (type == Master.InvoiceType.Sales && Session.UserSettings.Sales.DefaultPaymentMethodInSales == Master.PaymentMethod.Credit)
            {
                PaidSpinEdit.EditValue = 0;
            }
        }

        private void TaxSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            var Net = Convert.ToDouble(NetSpinEdit.EditValue);
            var TaxVal = Convert.ToDouble(TaxValueSpinEdit.EditValue);
            var TaxRat = Convert.ToDouble(TaxRatioSpinEdit.EditValue);

            if (IsTaxValueFocused)
                TaxRatioSpinEdit.EditValue = (TaxVal / Net);
            else
                TaxValueSpinEdit.EditValue = (TaxRat * Net);
        }

        private void TaxValueSpinEdit_Leave(object sender, EventArgs e)
        {
            IsTaxValueFocused = false;
        }

        private void TaxValueSpinEdit_Enter(object sender, EventArgs e)
        {
            IsTaxValueFocused = true;
        }

        private void DiscountSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            var total = Convert.ToDouble(NetSpinEdit.EditValue);
            var discountVal = Convert.ToDouble(DiscountValueSpinEdit.EditValue);
            var discountRat = Convert.ToDouble(DiscountRatioSpinEdit.EditValue);

            if (IsDiscountValueFocused)
                DiscountRatioSpinEdit.EditValue = (discountVal / total);
            else
                DiscountValueSpinEdit.EditValue = (discountRat * total);
        }

        private void DiscountValueSpinEdit_Leave(object sender, EventArgs e)
        {
            IsDiscountValueFocused = false;
        }

        private void DiscountValueSpinEdit_Enter(object sender, EventArgs e)
        {
            IsDiscountValueFocused = true;
        }

        #endregion


        #region Calculations
        private void CalcRemaining_OnEditValueChanged(object sender, EventArgs e)
        {
            var net = Convert.ToDouble(TotalSpinEdit.EditValue);
            var paid = Convert.ToDouble(PaidSpinEdit.EditValue);
            RemainingSpinEdit.EditValue = net - paid;
        }

        private void CalcTotal_OnEditValueChanged(object sender, EventArgs e)
        {
            var total = Convert.ToDouble(NetSpinEdit.EditValue);
            var TaxVal = Convert.ToDouble(TaxValueSpinEdit.EditValue);
            var discountVal = Convert.ToDouble(DiscountValueSpinEdit.EditValue);
            var expense = Convert.ToDouble(ExpensesSpinEdit.EditValue);
            TotalSpinEdit.EditValue = (total + TaxVal + expense - discountVal);
        }

        #endregion

        #region LookUpEdit
        private void BranchLookUpEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            var items = gridView1.DataSource as BindingList<InvoiceDetail>;
            if (e.OldValue is int && e.NewValue is int)
                foreach (var row in items)
                    if (row.StoreId == Convert.ToInt32(e.OldValue))
                        row.StoreId = Convert.ToInt32(e.NewValue);
        }

        private void PartyTypeLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (PartyTypeLookUpEdit.IsEditValueOfTypeInt())
            {
                int partyType = Convert.ToInt32(PartyTypeLookUpEdit.EditValue);
                if (partyType == (int)Master.PartyType.Customer)
                    PartyIdGridLookUpEdit.InitializeData(Session.Customers);
                else if (partyType == (int)Master.PartyType.Vendor)
                    PartyIdGridLookUpEdit.InitializeData(Session.Vendors);
            }
            PartyIdGridLookUpEdit.Refresh();


        }

        private void PartIdGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                using (var frm = new frm_CustomerVendor(Convert.ToInt32(PartyTypeLookUpEdit.EditValue) == (int)Master.PartyType.Customer))
                {
                    Class.Utilities.OpenFormInMain(frm);


                    RefreshData();
                }
            }
        }

        #endregion


        string GetNewInvoiceCode()
        {
            string MaxCode;
            using (Entities ctx = new Entities())
            {
                MaxCode = ctx.InvoiceHeaders.Where(x => x.InvoiceType == (int)type).Select(x => x.Code).Max();
            }
            return Master.GetNextNumberInString(MaxCode);
        }




        public override void New()
        {
            invoiceHeader = new _DAL.InvoiceHeader()
            {
                Drawer = Session.Defaults.Drawer,
                IssueDate = DateTime.Now,
                DueDate = DateTime.Now,
                DeliveryDate = DateTime.Now,
                PosteDate = DateTime.Now,
                Code = GetNewInvoiceCode(),
            };
            switch (type)
            {
                case Master.InvoiceType.SalesReturn:
                case Master.InvoiceType.Sales:
                    invoiceHeader.PartyType = (int)Master.PartyType.Customer;
                    invoiceHeader.PartyId = Session.Defaults.Customer;
                    invoiceHeader.Branch = Session.Defaults.Store;
                    break;
                case Master.InvoiceType.PurchaseReturn:
                case Master.InvoiceType.Purchase:
                    invoiceHeader.PartyType = (int)Master.PartyType.Vendor;
                    invoiceHeader.PartyId = Session.Defaults.Vendor;
                    invoiceHeader.Branch = Session.Defaults.RawStore;
                    break;
                default:
                    throw new NotImplementedException();
            }

            base.New();
            MoveFocusToGrid();
        }

        public override void RefreshData()
        {
            BranchLookUpEdit.InitializeData(Session.Store);
            DrawerLookUpEdit.InitializeData(Session.Drawer);
            base.RefreshData();
        }

        public override void GetData()
        {

            invoiceHeaderBindingSource.DataSource = invoiceHeader;
            generalDb = new Entities();
            invoiceDetails = new BindingList<InvoiceDetail>(generalDb.InvoiceDetails.Where(x => x.InvoiceId == invoiceHeader.Id).ToList());
            gridControl1.DataSource = invoiceDetails;

            base.GetData();
        }

        public override void SetData()
        {
            invoiceHeader.InvoiceType = (byte)type;
            invoiceHeader.Currency = (DrawerLookUpEdit.Properties.GetDataSourceRowByKeyValue(DrawerLookUpEdit.EditValue) as Drawer).Currency;
            invoiceHeaderBindingSource.DataSource = invoiceHeader;
            (invoiceHeaderBindingSource.DataSource as InvoiceHeader).InvoiceType = (byte)type;

            base.SetData();
        }

        public override bool IsDataValid()
        {
            int NumberOfErrors = 0;
            if (gridView1.RowCount == 0)
            {
                NumberOfErrors++;
                XtraMessageBox.Show(text: "Please select at least one Item", caption: "Error", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error);
            }
            NumberOfErrors += CodeTextEdit.IsTextValid() ? 0 : 1;
            NumberOfErrors += PartyTypeLookUpEdit.IsEditValueOfTypeInt() ? 0 : 1;
            NumberOfErrors += PartyIdGridLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            NumberOfErrors += DrawerLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            NumberOfErrors += BranchLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            NumberOfErrors += DateDateEdit.IsDateValid() ? 0 : 1;
            if (PostedToStoreCheckEdit.Checked)
            {
                NumberOfErrors += PosteDateDateEdit.IsDateValid() ? 0 : 1;
                layoutControlGroupPost.Expanded = true;
            }

            switch (type)
            {
                case Master.InvoiceType.Sales:
                    if ((invoiceHeader.DiscountRatio != Convert.ToDouble(DiscountRatioSpinEdit.EditValue)) &&
                        (Session.UserSettings.Sales.MaxDiscountInInvoice < Convert.ToDecimal(DiscountRatioSpinEdit.EditValue)))
                    {
                        NumberOfErrors++;
                        DiscountRatioSpinEdit.ErrorText = "Discount not allowed";
                    }

                    if (invoiceHeader.Id == 0)
                    {
                        var id = Convert.ToInt32(PartyIdGridLookUpEdit.EditValue);
                        if (id != 0)
                        {

                            CustomerAndVendor account;
                            if (PartyTypeLookUpEdit.EditValue.Equals((byte)Master.PartyType.Vendor))
                            {
                                account = Session.Vendors.Single(x => x.Id == id);
                            }
                            else
                            {
                                account = Session.Customers.Single(x => x.Id == id);
                            }
                            if ((account.MaxCredit <= accountBalance.BalanceAmount) && accountBalance.BalanceType == AccountBalance.BalanceTypes.Debit)
                            {
                                switch (Session.UserSettings.Sales.WhenSellingToACustomerExceedingMaxCredit)
                                {
                                    case Master.WarningLevels.DoNotInterrupt:
                                        break;
                                    case Master.WarningLevels.ShowWarning:
                                        NumberOfErrors++;
                                        XtraMessageBox.Show("Cannot sell to customer exceeded credit limite!");
                                        break;
                                    case Master.WarningLevels.Prevent:
                                        if (XtraMessageBox.Show(
                                            text: "This customer exceeded the credit limite, Do you want to continue?",
                                            caption: "Sell confirmation",
                                            icon: MessageBoxIcon.Question,
                                            buttons: MessageBoxButtons.YesNo) == DialogResult.No)
                                        {
                                            NumberOfErrors++;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    break;
                case Master.InvoiceType.Purchase:
                    break;
                case Master.InvoiceType.SalesReturn:
                    break;
                case Master.InvoiceType.PurchaseReturn:
                    break;
                default:
                    break;
            }

            return (NumberOfErrors == 0);
        }

        public override void Save()
        {
            //gridView1.UpdateCurrentRow();

            SetData();


            int SavedChangesCount = 0;

            //close the editing mode and confirm 
            if (gridView1.FocusedRowHandle < 0)
                GridView1_ValidateRow(gridView1, new DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs(gridView1.FocusedRowHandle, gridView1.GetRow(gridView1.FocusedRowHandle)));
            DialogResult CostDistributionOptionsDialogResult = DialogResult.OK;
            var items = gridView1.DataSource as BindingList<InvoiceDetail>;
            switch (type)
            {
                case Master.InvoiceType.Purchase:
                    if (invoiceHeader.Expenses > 0)
                    {
                        var totalPrice = items.Sum(x => x.TotalPrice);
                        var totalQty = items.Sum(x => x.ItemQty);
                        // Price per each currency unit
                        var ByPriceUnit = invoiceHeader.Expenses / totalPrice;
                        // price per each item qty on each selected items
                        var ByQtyUnit = invoiceHeader.Expenses / totalQty;

                        XtraDialogArgs args = new XtraDialogArgs();
                        UserControls.CostDistributionOption distributionOption = new UserControls.CostDistributionOption();
                        args.Caption = "";
                        args.Content = distributionOption;
                        ((XtraBaseArgs)args).Buttons = new DialogResult[]
                        {
                DialogResult.OK,DialogResult.Cancel,DialogResult.Abort
                        };
                        args.Showing += Args_Showing;
                        CostDistributionOptionsDialogResult = XtraDialog.Show(args);
                        foreach (var row in items)
                        {
                            if (distributionOption.SelectedOption == Master.CostDistributionOptions.ByPrice)
                            {
                                row.CostValue = (row.TotalPrice / row.ItemQty) + (ByPriceUnit * row.Price); //devide by price
                                row.TotalCostValue = row.ItemQty * row.CostValue;
                            }
                            else if (distributionOption.SelectedOption == Master.CostDistributionOptions.ByQty)
                            {
                                row.CostValue = (row.TotalPrice / row.ItemQty) + (ByQtyUnit); //devide by qty
                                row.TotalCostValue = row.ItemQty * row.CostValue;

                            }

                        }
                        gridView1.RefreshData();
                    }
                    else
                    {
                        int index = 0;
                        foreach (var row in items)
                        {
                            GridView1_ValidateRow(gridView1, new DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs(gridView1.GetRowHandle(index), row));
                            row.CostValue = row.TotalPrice / row.ItemQty;
                            row.TotalCostValue = row.TotalPrice;
                            index++;
                        }
                    }
                    break;
                case Master.InvoiceType.Sales:
                    break;
                case Master.InvoiceType.SalesReturn:
                case Master.InvoiceType.PurchaseReturn:
                default:
                    break;
            }


            if (CostDistributionOptionsDialogResult == DialogResult.Cancel)
                return;



            var ctx = new Entities();
            if (invoiceHeader.Id == 0)
            {
                ctx.InvoiceHeaders.AddOrUpdate(invoiceHeader);
            }
            else
            {

                //ctx.InvoiceHeaders.Attach(invoiceHeader);
                ctx.InvoiceHeaders.AddOrUpdate(invoiceHeader);
            }

            SavedChangesCount += ctx.SaveChanges();



            #region Jornals
            // بيع



            // شراء
            ctx.Journals.RemoveRange(ctx.Journals.Where(x => x.SourceType == (byte)type && x.SourceId == invoiceHeader.Id));
            SavedChangesCount += ctx.SaveChanges();

            var store = ctx.Stores.Single(x => x.Id == invoiceHeader.Branch);
            var drawer = ctx.Drawers.Single(x => x.Id == invoiceHeader.Drawer);

            int StoreAccount;
            string msg;
            int TaxAccountId;
            int PartyAccountId = ctx.CustomerAndVendors.Single(x => x.Id == invoiceHeader.PartyId).AccountId;
            int DiscountAccontId;
            bool IsPartyCredit;
            bool InsertCostOfSoldGoodsJornal;
            bool IsIn;

            switch (type)
            {
                case Master.InvoiceType.Purchase:
                    StoreAccount = store.InventoryAccountId;
                    TaxAccountId = Session.Defaults.PurchaseTaxId;
                    DiscountAccontId = Session.Defaults.DiscountReceivedAccountId;
                    IsPartyCredit = true;
                    IsIn = true;
                    InsertCostOfSoldGoodsJornal = false;
                    msg = $"Purchase invocice {invoiceHeader.Id} in favor of {PartyIdGridLookUpEdit.Text}";

                    break;
                case Master.InvoiceType.Sales:
                    StoreAccount = store.SalesAccountId;
                    TaxAccountId = Session.Defaults.SalesTaxId;
                    DiscountAccontId = Session.Defaults.DiscountAllowedAccountId;
                    IsPartyCredit = false;
                    IsIn = false;
                    InsertCostOfSoldGoodsJornal = true;
                    msg = $"Sales invocice {invoiceHeader.Id} in favor of {PartyIdGridLookUpEdit.Text}";

                    break;
                case Master.InvoiceType.SalesReturn:
                case Master.InvoiceType.PurchaseReturn:
                default:
                    throw new NotImplementedException();
            }

            //Supplier Party Jornal
            ctx.Journals.AddOrUpdate(new Journal()
            {
                AccountId = PartyAccountId,
                Code = 54545,
                Credit = (IsPartyCredit) ? invoiceHeader.Total + invoiceHeader.TaxValue + invoiceHeader.Expenses : 0,
                Debit = (!IsPartyCredit) ? invoiceHeader.Total + invoiceHeader.TaxValue + invoiceHeader.Expenses : 0,
                DateCreated = invoiceHeader.IssueDate,
                Notes = msg,
                SourceId = invoiceHeader.Id,
                SourceType = (byte)type
            });
            // Store Inventory
            ctx.Journals.AddOrUpdate(new Journal()
            {
                AccountId = store.InventoryAccountId,
                Code = 54545,
                Credit = (!IsPartyCredit) ? invoiceHeader.Total + invoiceHeader.Expenses : 0,
                Debit = (IsPartyCredit) ? invoiceHeader.Total + invoiceHeader.Expenses : 0,
                DateCreated = invoiceHeader.IssueDate,
                Notes = msg,
                SourceId = invoiceHeader.Id,
                SourceType = (byte)type
            });

            // Store Tax
            if (invoiceHeader.TaxValue > 0)
                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = Session.Defaults.PurchaseTaxId,
                    Code = 54545,
                    Credit = (!IsPartyCredit) ? invoiceHeader.TaxValue : 0,
                    Debit = (IsPartyCredit) ? invoiceHeader.TaxValue : 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - VAT Tax",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });

            //// Store Expense
            //if (invoiceHeader.Expenses > 0)
            //    ctx.Journals.AddOrUpdate(new Journal()
            //    {
            //        AccountId = Session.Defaults.PurchaseExpenseId,
            //        Code = 54545,
            //        Credit = 0,
            //        Debit = invoiceHeader.Expenses,
            //        DateCreated = invoiceHeader.Date,
            //        Notes = msg + " - Purchase Expense",
            //        SourceId = invoiceHeader.Id,
            //        SourceType = (byte)type
            //    });

            if (invoiceHeader.DiscountValue > 0)
            {
                //  Discount

                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = Session.Defaults.DiscountReceivedAccountId,
                    Code = 54545,
                    Credit = (!IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    Debit = (IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Purchase Discount",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });

                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = PartyAccountId,
                    Code = 54545,
                    Credit = (IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    Debit = (!IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Purchase Discount",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });
            }
            if (InsertCostOfSoldGoodsJornal)
            {
                var TotalCost = items.Sum(x => x.TotalCostValue);
                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = store.InventoryAccountId,
                    Code = 54545,
                    //Credit = (!IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    //Debit = (IsPartyCredit) ? invoiceHeader.DiscountValue : 0,
                    Credit = (!IsPartyCredit) ? TotalCost : 0,
                    Debit = (IsPartyCredit) ? TotalCost : 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Cost of sales",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });

                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = store.CostOfSoldGoodsAccountId,
                    Code = 54545,
                    Credit = (IsPartyCredit) ? TotalCost : 0,
                    Debit = (!IsPartyCredit) ? TotalCost : 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Cost of salest",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });
            }
            if (invoiceHeader.Paid > 0)
            {
                //  Payment
                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = drawer.AccountId,
                    Code = 54545,
                    Credit = invoiceHeader.Paid,
                    Debit = 0,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Payment",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type

                });



                ctx.Journals.AddOrUpdate(new Journal()
                {
                    AccountId = PartyAccountId,
                    Code = 54545,
                    Credit = 0,
                    Debit = invoiceHeader.Paid,
                    DateCreated = invoiceHeader.IssueDate,
                    Notes = msg + " - Payment",
                    SourceId = invoiceHeader.Id,
                    SourceType = (byte)type
                });
            }
            #endregion

            foreach (var row in items)
                row.InvoiceId = invoiceHeader.Id;

            ctx.InvoiceDetails.AddOrUpdate(items.ToArray<InvoiceDetail>());
            SavedChangesCount += ctx.SaveChanges();

            foreach (var row in items)
            {
                ctx.StoreLogs.RemoveRange(ctx.StoreLogs.Where(x => x.SourceType == (byte)type && x.SourceId == row.Id));
            }

            //      ctx.StoreLogs.RemoveRange(ctx.StoreLogs.Where(x => x.SourceType == (byte)type && x.SourceId == invoiceHeader.Id));
            SavedChangesCount += ctx.SaveChanges();
            if (invoiceHeader.PostedToStore)
            {
                foreach (var row in items)
                {
                    var unitView = Session.ProductsView.Single(x => x.Id == row.ItemId).Units.Single(x => x.UnitId == row.ItemUnitId);
                    ctx.StoreLogs.AddOrUpdate(new StoreLog()
                    {
                        ProductId = row.ItemId,
                        InsertDate = invoiceHeader.PosteDate,
                        SourceId = row.Id,
                        SourceType = (byte)type,
                        Notes = msg,
                        IsInTransaction = IsIn,
                        StoreId = row.StoreId,
                        Qty = row.ItemQty * unitView.Factor,
                        CostValue = row.CostValue / unitView.Factor
                    });
                }
                SavedChangesCount += ctx.SaveChanges();
            }
            if (SavedChangesCount > 0)
            {
                DataSavedMessage();
            }


            base.Save();
        }

        public override void Print()
        {
            using (var db = new Entities())
            {
                var invoice = (from inv in db.InvoiceHeaders
                               join str in db.Stores on inv.Branch equals str.Id
                               // join part in db.CustomerAndVendors on Inv.PartyId equals part.Id
                               from prt in db.CustomerAndVendors.Where(x => x.Id == inv.PartyId).DefaultIfEmpty()
                               from drw in db.Drawers.Where(x => x.Id == inv.Drawer).DefaultIfEmpty()
                               where inv.Id == invoiceHeader.Id
                               select new
                               {
                                   inv.Id,
                                   Store = str.Name,
                                   Drawer = drw.Name,
                                   drw.BankAccount,
                                   drw.Swift,
                                   drw.LegalNote,
                                   inv.Code,
                                   PartyName = prt.Name,
                                   PartyAddress = prt.Address,
                                   PartyVATNIP = prt.VAT_NIP,
                                   PartyEmail = prt.Email,
                                   PartyPhone = prt.Phone,
                                   PartyWebsite = prt.Website,
                                   inv.IssueDate,
                                   inv.DiscountValue,
                                   DiscounteRatio = (inv.DiscountRatio * 100),
                                   inv.Currency,
                                   inv.Expenses,
                                   InvoiceType =
                                (inv.InvoiceType == (byte)Master.InvoiceType.Sales) ? "Salse Invoice" :
                                (inv.InvoiceType == (byte)Master.InvoiceType.SalesReturn) ? "Sales Return Invoice" :
                                (inv.InvoiceType == (byte)Master.InvoiceType.Purchase) ? "Purchase Invoice" :
                                (inv.InvoiceType == (byte)Master.InvoiceType.PurchaseReturn) ? "Purchase Return Invoice" : "Undefined",
                                   inv.Net,
                                   inv.Notes,
                                   inv.Paid,
                                   PartyType =
                                (inv.PartyType == (byte)Master.PartyType.Customer) ? "Customer" :
                                (inv.PartyType == (byte)Master.PartyType.Vendor) ? "Vendor" : "Undefined",
                                   inv.PosteDate,
                                   inv.DeliveryDate,
                                   inv.PostedToStore,
                                   inv.DueDate,
                                   inv.Remaining,
                                   inv.TaxValue,
                                   inv.TaxRatio,
                                   TaxRatioSolid = (inv.TaxRatio * 100),
                                   inv.Total,
                                   ProductCount = db.InvoiceDetails.Where(x => x.InvoiceId == inv.Id).Count(),
                                   Products = (
                                from d in db.InvoiceDetails.Where(x => x.InvoiceId == inv.Id)
                                from p in db.Products.Where(x => x.Id == d.ItemId)
                                from u in db.UnitNames.Where(x => x.Id == d.ItemUnitId).DefaultIfEmpty()
                                select new
                                {
                                    ProductName = p.Name,
                                    UnitName = u.Name,
                                    d.ItemQty,
                                    d.Price,
                                    d.TotalPrice,
                                    d.CostValue,
                                    d.Nontaxable,

                                }).ToList()
                               }).ToList();
                Reporting.rpt_Invoice.Print(invoice);
            }
            base.Print();
        }

        private void Args_Showing(object sender, XtraMessageShowingArgs e)
        {
            e.Form.Text = "Additional Expense Distribution Options";
            e.Form.CloseBox = false;
            e.Form.Height = 100;
            e.Buttons[DialogResult.OK].Text = "Save and continue";
            e.Buttons[DialogResult.OK].Width = 100;
            e.Buttons[DialogResult.Cancel].Text = "Preview";
            e.Buttons[DialogResult.Abort].Text = "Abort";
        }
        public override void Delete()
        {
            int SavedChangesCount = 0;
            using (var ctx = new Entities())
            {
                ctx.StoreLogs.RemoveRange(
                    ctx.StoreLogs.Where(x => x.SourceType == (byte)type &&
                    ctx.InvoiceDetails.Where(y => y.InvoiceId == invoiceHeader.Id).Select(y => y.Id).Contains(x.SourceId))
              );
                //    SavedChangesCount += ctx.SaveChanges();
                ctx.InvoiceDetails.RemoveRange(ctx.InvoiceDetails.Where(y => y.InvoiceId == invoiceHeader.Id));
                //   SavedChangesCount += ctx.SaveChanges();

                ctx.InvoiceHeaders.Remove(ctx.InvoiceHeaders.Single(x => x.Id == invoiceHeader.Id));
                SavedChangesCount += ctx.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    DataSavedMessage();
                }
            }
            base.Delete();
        }
        void ReadUsersettings()
        {
            switch (type)
            {
                case Master.InvoiceType.Purchase:
                    break;
                case Master.InvoiceType.Sales:
                    PaidSpinEdit.Enabled = Session.UserSettings.Sales.CanChangePaidInInSales;
                    PostedToStoreCheckEdit.Enabled = Session.UserSettings.Sales.CanNotPostToStoreInSales;
                    gridView1.Columns[nameof(InvoiceDetail.Price)].OptionsColumn.AllowEdit = Session.UserSettings.Sales.CanChangeItemPriceInSales;
                    gridView1.Columns[nameof(InvoiceDetail.ItemQty)].OptionsColumn.AllowEdit = Session.UserSettings.Sales.CanChangeQtyInSales;

                    gridView1.Columns[nameof(InvoiceDetail.CostValue)].Visible =
                    gridView1.Columns[nameof(InvoiceDetail.CostValue)].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns[nameof(InvoiceDetail.TotalCostValue)].Visible = 
                    gridView1.Columns[nameof(InvoiceDetail.TotalCostValue)].OptionsColumn.ShowInCustomizationForm = !Session.UserSettings.Sales.HideCostInSales;


                    PartyTypeLookUpEdit.Enabled = Session.UserSettings.Sales.CanSellToVendors;
                    DateDateEdit.Enabled = Session.UserSettings.Sales.CanChangeSalesInvoiceDate;
                    break;
                case Master.InvoiceType.SalesReturn:
                case Master.InvoiceType.PurchaseReturn:
                    throw new NotImplementedException();
            }
        }
    }
}