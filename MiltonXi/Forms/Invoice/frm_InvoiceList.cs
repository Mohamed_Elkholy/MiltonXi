﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Class;
using MiltonXi._DAL;
using System.Data.Entity;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;

namespace MiltonXi.Forms
{
    public partial class frm_InvoiceList : frm_MasterList
    {
        Master.InvoiceType Type;
        RepositoryItemLookUpEdit repoPartType;
        RepositoryItemGridLookUpEdit repoPart;
        RepositoryItemLookUpEdit repoBranch;
        RepositoryItemLookUpEdit repoDrawer;
        RepositoryItemLookUpEdit repoCurrency;
        public frm_InvoiceList(Master.InvoiceType _type)
        {
            InitializeComponent();
            gridView1.AddButtonToGroupHeader(Properties.Resources.editfilter, OpenFilterPanel);
            flyoutPanel1.Options.CloseOnOuterClick = true;
            flyoutPanel1.Options.CloseOnHidingOwner = true;
            flyoutPanel1.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.Center;
            flyoutPanel1.Options.AnimationType = DevExpress.Utils.Win.PopupToolWindowAnimation.Slide;
            dateEditFrom.EditValue =
            dateEditTo.EditValue = DateTime.Today;
            PartyTypeLookUpEdit.EditValueChanged += PartyTypeLookUpEdit_EditValueChanged;
            Type = _type;
        }
        private void OpenFilterPanel(object sender, EventArgs e)
        {
            flyoutPanel1.ShowPopup();
        }
        public override void OpenForm(int id, string name)
        {
            if (name == string.Empty)
                switch (Type)
                {
                    case Master.InvoiceType.Sales:
                        name = "frm_SalesInvoiceList";
                        break;
                    case Master.InvoiceType.Purchase:
                        name = "frm_PurchaseInvoiceList";
                        break;
                    case Master.InvoiceType.SalesReturn:
                        name = "Sales Return";
                        break;
                    case Master.InvoiceType.PurchaseReturn:
                        name = "Purchase Return";
                        break;
                    default:
                        break;
                }

            Class.Utilities.OpenFormInMain(new frm_Invoice(Type, id) { Name = name, Tag = name });

            base.OpenForm(id, name);
        }
        public override void RefreshData()
        {
            DateTime? fromDate =null;
            DateTime? toDate =null;
            
            if (dateEditFrom.DateTime.Year > 1950)
                fromDate = dateEditFrom.DateTime;
            if (dateEditTo.DateTime.Year > 1950)
                toDate = dateEditTo.DateTime;
            using (var db = new Entities())
            {
                var query = from inv in db.InvoiceHeaders.Where(x => x.InvoiceType == (byte)Type)

                            select new
                            {
                                inv.Id,
                                inv.Code,
                                inv.IssueDate,
                                Party=  inv.PartyId,
                                 inv.PartyType,
                                inv.Branch,
                                inv.Drawer,
                                //Drawer = drw.Name,
                                //drw.Currency,
                                Currency ="",
                                ItemsCount = db.InvoiceDetails.Where(x => x.InvoiceId == inv.Id).Count(),
                                inv.PostedToStore,
                                inv.Total,
                                inv.TaxRatio,
                                inv.TaxValue,
                                inv.DiscountRatio,
                                inv.DiscountValue,
                                inv.Expenses,
                                inv.Net,
                                inv.Paid,
                                inv.Remaining,
                                PayStatus = "",
                                Products = (from itm in db.InvoiceDetails.Where(x => x.InvoiceId == inv.Id)
                                            from pro in db.Products.Where(x => x.Id == itm.ItemId).DefaultIfEmpty()
                                            from unt in db.UnitNames.Where(x => x.Id == itm.ItemUnitId).DefaultIfEmpty()
                                            select new
                                            {
                                                Product = pro.Name,
                                                Unit = unt.Name,
                                                itm.Price,
                                                itm.ItemQty,
                                                itm.DiscountValue,
                                                itm.TotalPrice,

                                            }).ToList(),
                            };
                if (fromDate != null)
                    query = query.Where(x => DbFunctions.TruncateTime(x.IssueDate) >= fromDate.Value);
                if (toDate != null)
                    query = query.Where(x => DbFunctions.TruncateTime(x.IssueDate) <= toDate.Value);

                if (PartyIdGridLookUpEdit.IsEditValueOfTypeInt())
                    query = query.Where(x => x.Party == (int)(PartyIdGridLookUpEdit.EditValue));

                if (PartyTypeLookUpEdit.IsEditValueOfTypeInt())
                    query = query.Where(x => x.PartyType == (int)(PartyTypeLookUpEdit.EditValue));

                if (DrawerLookUpEdit.IsEditValueOfTypeInt())
                    query = query.Where(x => x.Drawer == (int)(DrawerLookUpEdit.EditValue));

                if (BranchLookUpEdit.IsEditValueOfTypeInt())
                    query = query.Where(x => x.Branch == (int)BranchLookUpEdit.EditValue);

                gridControl1.DataSource = query.ToList() ;
                gridView1.Columns["Id"].Visible = false;
                //gridControl1.RepositoryItems.AddRange(new RepositoryItem[]
                //{
                //    repoBranch, repoDrawer,repoPart,repoPartType,
                //});
                repoBranch.InitializeData( Session.Store,gridView1.Columns[nameof(InvoiceHeader.Branch)],gridControl1);
                repoDrawer.InitializeData( Session.Drawer,gridView1.Columns[nameof(InvoiceHeader.Drawer)],gridControl1);
                repoCurrency.InitializeData( Session.Drawer,gridView1.Columns["Currency"],gridControl1,nameof(Drawer.Currency),"Id");
                repoPartType.InitializeData( Master.PartyTypesList,gridView1.Columns[nameof(InvoiceHeader.PartyType)],gridControl1);
                
                var parties = new List<CustomerAndVendor>();
                parties.AddRange(Session.Customers);
                parties.AddRange(Session.Vendors);
                repoPart.InitializeData(parties, gridView1.Columns["Party"],gridControl1);

                RepositoryItemSpinEdit spinEditPercent = new RepositoryItemSpinEdit();
                spinEditPercent.Increment = 0.01m;
                spinEditPercent.Mask.EditMask = "p";
                spinEditPercent.Mask.UseMaskAsDisplayFormat = true;
                spinEditPercent.MaxValue = 1;
                gridControl1.RepositoryItems.Add(spinEditPercent);
                gridView1.Columns[nameof(InvoiceHeader.DiscountRatio)].ColumnEdit = spinEditPercent;
                gridView1.Columns[nameof(InvoiceHeader.TaxRatio)].ColumnEdit = spinEditPercent;
            }

            base.RefreshData();
        }
        private void frm_InvoiceList_Load(object sender, EventArgs e)
        {

            switch (Type)
            {
                case Master.InvoiceType.Purchase:
                    this.Text = "Purchase invoice List";
                    this.Name = Class.Screens.frm_PurchaseInvoiceList.ScreenName;
                    break;
                case Master.InvoiceType.Sales:
                    this.Text = "Sales invoice List";
                    this.Name = Class.Screens.frm_SalesInvoiceList.ScreenName;
                    break;
                case Master.InvoiceType.PurchaseReturn:
                case Master.InvoiceType.SalesReturn:
                default:
                    throw new NotImplementedException();
            }
            PartyTypeLookUpEdit.InitializeData(Class.Master.PartyTypesList);
            BranchLookUpEdit.InitializeData(Session.Store);
            DrawerLookUpEdit.InitializeData(Session.Drawer);
            PartyIdGridLookUpEdit.Properties.NullText = "";

            dateEditFrom.AddClearValueButton();
            dateEditTo.AddClearValueButton();
            BranchLookUpEdit.AddClearValueButton();
            DrawerLookUpEdit.AddClearValueButton();
            PartyTypeLookUpEdit.AddClearValueButton();
            PartyIdGridLookUpEdit.AddClearValueButton();


            RefreshData();


        }

        public override void New()
        {
            Class.Utilities.OpenFormInMain(new frm_Invoice(Type) { Tag = Type.ToString() });

            base.New();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            RefreshData();
            flyoutPanel1.HidePopup();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dateEditFrom.EditValue=
            dateEditTo.EditValue =
            DrawerLookUpEdit.EditValue =
            PartyTypeLookUpEdit.EditValue =
            PartyIdGridLookUpEdit.EditValue=
            BranchLookUpEdit.EditValue = null;
            RefreshData();
            flyoutPanel1.HidePopup();
        }
        private void PartyTypeLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (PartyTypeLookUpEdit.IsEditValueOfTypeInt())
            {
                int partyType = Convert.ToInt32(PartyTypeLookUpEdit.EditValue);
                if (partyType == (int)Master.PartyType.Customer)
                {
                    lycPartyIdGridLookUpEdit.Text = "Customer";
                    PartyIdGridLookUpEdit.InitializeData(Session.Customers);

                }
                else if (partyType == (int)Master.PartyType.Vendor)
                {
                    lycPartyIdGridLookUpEdit.Text = "Vendor";
                    PartyIdGridLookUpEdit.InitializeData(Session.Vendors);
                }
            }
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Id)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.IsCustomer)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.AccountId)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Notes)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Website)].Visible = false;
            PartyIdGridLookUpEdit.Properties.View.Columns[nameof(CustomerAndVendor.Email)].Visible = false;
            
            PartyIdGridLookUpEdit.Refresh();


        }
    }
}