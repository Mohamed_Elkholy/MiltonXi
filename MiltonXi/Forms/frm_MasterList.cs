﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MiltonXi.Forms
{
    public partial class frm_MasterList : frm_Master
    {
        public frm_MasterList()
        {
            InitializeComponent();

        }

        private void frm_MasterList_Load(object sender, EventArgs e)
        {
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            gridView1.OptionsBehavior.Editable = false;
            gridView1.DoubleClick += GridView1_DoubleClick;
            gridView1.OptionsView.ShowAutoFilterRow = true;
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {
                string name = "";
                int id = 0;
                id = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Id"));
                if (gridView1.GetFocusedRowCellValue("Name") != null)
                    name = gridView1.GetFocusedRowCellValue("Name").ToString();
                else
                    name = "";
                OpenForm(id, name);
            }
        }

        public virtual void OpenForm(int id,string name)
        {

        }
        public override void Save()
        {
            
        }
        public override void Print()
        {
            
        }
        public override void Delete()
        {
        
        }
    }
}