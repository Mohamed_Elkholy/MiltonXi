﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using DevExpress.XtraSplashScreen;
using System.Diagnostics;

namespace MiltonXi.Forms
{
    public partial class frm_Login : XtraForm
    {
        public frm_Login()
        {
            InitializeComponent();
            if (Debugger.IsAttached)
            {
                //txt_Username.Text = "admin";
                //txt_Password.Text = "admin";
                //btn_Login_Click(null,null);
            }
            this.FormClosed += Frm_Login_FormClosed;
        }

        private void Frm_Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            using (var db = new Entities())
            {
                var username = txt_Username.Text;
                var password = txt_Password.Text;

                var user = db.Users.SingleOrDefault(x => x.Username.ToLower() == username.ToLower());
                if (user == null)
                {
                    goto LoginFailed;
                }
                else
                {
                    if (user.Inactive)
                    {
                        lbl_Message.Text = "This account is disabled!";
                        return;
                    }
                    var passwordHash = user.Password;
                    var hasher = new Liphsoft.Crypto.Argon2.PasswordHasher();
                    if (hasher.Verify(passwordHash, password))
                    {
                        // Successful Login 
                        this.Hide();
                        SplashScreenManager.ShowForm(frm_MainForm.Instance, typeof(frm_BootSplashScreen));
                        // heavy load here
                        Class.Session.SetUser(user);

                        Type t = typeof(Class.Session);
                        var properties = t.GetProperties(System.Reflection.BindingFlags.Public| System.Reflection.BindingFlags.Static);
                        foreach (var item in properties)
                        {
                            var obj = item.GetValue(null);

                        }
                        //

                        frm_MainForm.Instance.Show();

                        SplashScreenManager.CloseForm();
                        return ;
                    }
                }
            }
        LoginFailed:
            lbl_Message.Text = "Username or password incrrect!";
        }

        private void frm_Login_Load(object sender, EventArgs e)
        {
           
        }
    }
}