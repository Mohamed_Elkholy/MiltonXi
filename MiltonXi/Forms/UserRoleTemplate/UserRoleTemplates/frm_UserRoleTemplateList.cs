﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;

namespace MiltonXi.Forms
{
    public partial class frm_UserRoleTemplateList : frm_Master
    {
        public frm_UserRoleTemplateList()
        {
            InitializeComponent();
            gridView1.OptionsBehavior.Editable = false;
            gridView1.DoubleClick += GridView1_DoubleClick;
            RefreshData();
            gridView1.Columns["Id"].Visible = false;
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        }
        public override void RefreshData()
        {
            using (var db = new Entities())
            {
                gridControl1.DataSource = db.UserSettingsProfiles.ToList();
            }
            base.RefreshData();
        }
        public override void New()
        {
            Class.Utilities.OpenFormInMain(new frm_UserRoleTemplate());
            base.New();
        }
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {

                int id = 0;
                id = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Id"));

                string name = gridView1.GetFocusedRowCellValue("Name").ToString();
                Class.Utilities.OpenFormInMain(new frm_UserRoleTemplate(id) { Tag = name });
            }
        }
    }
}