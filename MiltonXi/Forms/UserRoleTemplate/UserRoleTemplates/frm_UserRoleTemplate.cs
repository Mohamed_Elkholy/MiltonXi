﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Class;
using DevExpress.XtraTab;
using DevExpress.XtraLayout;
using MiltonXi._DAL;
using System.Data.Entity.Migrations;
using DevExpress.Utils.Animation;

namespace MiltonXi.Forms
{
    public partial class frm_UserRoleTemplate : frm_Master
    {
        UserSettingsProfile profile;
        List<BaseEdit> editors;
        public frm_UserRoleTemplate()
        {
            InitializeComponent();
            accordionControl1.ElementClick += AccordionControl1_ElementClick;

            New();
            GetData();
        }
        public frm_UserRoleTemplate(int Id)
        {
            InitializeComponent();
            accordionControl1.ElementClick += AccordionControl1_ElementClick;
            using (var db = new Entities())
            {
                profile = db.UserSettingsProfiles.Single(x => x.Id == Id);
                NametextEdit.Text = profile.Name;
                GetData();
            }
        }
        private void frm_UserSettingsProfile_Load(object sender, EventArgs e)
        {
            accordionControl1.AnimationType = DevExpress.XtraBars.Navigation.AnimationType.Simple;
            accordionControl1.Appearance.Item.Hovered.Font = new System.Drawing.Font(
                accordionControl1.Appearance.Item.Hovered.Font, FontStyle.Bold
                );
            accordionControl1.Appearance.Item.Hovered.Options.UseFont = true;
            accordionControl1.Appearance.Item.Pressed.Font = new System.Drawing.Font(
        accordionControl1.Appearance.Item.Pressed.Font, FontStyle.Bold
        );
            accordionControl1.Appearance.Item.Pressed.Options.UseFont = true;
            accordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Hidden;
            accordionControl1.OptionsMinimizing.AllowMinimizeMode = DevExpress.Utils.DefaultBoolean.False;
            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            xtraTabControl1.Transition.AllowTransition = DevExpress.Utils.DefaultBoolean.True;
            xtraTabControl1.Transition.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            SlideFadeTransition trans = new SlideFadeTransition();
            trans.Parameters.EffectOptions = PushEffectOptions.FromBottom;
            xtraTabControl1.Transition.TransitionType = trans;
            xtraTabControl1.SelectedPageChanging += XtraTabControl1_SelectedPageChanging;
        }

        private void XtraTabControl1_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            SlideFadeTransition trans = new SlideFadeTransition();
            var currentPage = xtraTabControl1.TabPages.IndexOf(e.Page);
            var previusPage = xtraTabControl1.TabPages.IndexOf(e.PrevPage);
            if (currentPage > previusPage)
                trans.Parameters.EffectOptions = PushEffectOptions.FromBottom;
            else
                trans.Parameters.EffectOptions = PushEffectOptions.FromTop;

            xtraTabControl1.Transition.TransitionType = trans;
        }

        private void AccordionControl1_ElementClick(object sender, DevExpress.XtraBars.Navigation.ElementClickEventArgs e)
        {
            var index = accordionControl1.Elements.IndexOf(e.Element);
            xtraTabControl1.SelectedTabPageIndex = index;
        }
        public override void New()
        {
            profile = new UserSettingsProfile();
            NametextEdit.Text = profile.Name;
            
        }

        public override void GetData()
        {

            editors = new List<BaseEdit>();
            UserSettingsTemplate settings = new UserSettingsTemplate(profile.Id);
            // UserSettingsTemplate settings = Class.UserSettingsTemplate(1);

            accordionControl1.AllowItemSelection = true;
            accordionControl1.Elements.Clear();

            xtraTabControl1.SuspendLayout();
            xtraTabControl1.TabPages.Clear();
            var catalog = settings.GetType().GetProperties();
            foreach (var item in catalog)
            {
                accordionControl1.Elements.Add(new DevExpress.XtraBars.Navigation.AccordionControlElement()
                {
                    Name = nameof(item),
                    Text = UserSettingsTemplate.GetPropCaption(item.Name),
                    Style = DevExpress.XtraBars.Navigation.ElementStyle.Item,
                });
                var page = new XtraTabPage() { Name = item.Name, Text = UserSettingsTemplate.GetPropCaption(item.Name) };
               
                LayoutControl lc = new LayoutControl();
                EmptySpaceItem empty1 = new EmptySpaceItem();
                EmptySpaceItem empty2 = new EmptySpaceItem();
                lc.AddItem(empty1);
                lc.AddItem(empty2, empty1, DevExpress.XtraLayout.Utils.InsertType.Left);
                var props = item.GetValue(settings).GetType().GetProperties();
                foreach (var prop in props)
                {
                    BaseEdit edit = UserSettingsTemplate.GetPropertyControl(prop.Name, prop.GetValue(item.GetValue(settings)));
                    if (edit != null)
                    {
                        var layoutItem = lc.AddItem(" ", edit, empty2, DevExpress.XtraLayout.Utils.InsertType.Top);
                        layoutItem.TextVisible = true;
                        layoutItem.Text = UserSettingsTemplate.GetPropCaption(prop.Name);
                        layoutItem.SizeConstraintsType = SizeConstraintsType.Custom;
                        layoutItem.MaxSize = new Size(700, 30);
                        layoutItem.MinSize = new Size(250, 30);
                        editors.Add(edit);
                    }

                }

                lc.Dock = DockStyle.Fill;
                page.Controls.Add(lc);
                xtraTabControl1.TabPages.Add(page);
            }
         //var index=   xtraTabControl1.TabPages.IndexOf(xtraTabControl1.TabPages.FirstOrDefault());
         //       xtraTabControl1.SelectedTabPageIndex =index;
            base.GetData();
        }
        public override bool IsDataValid()
        {
            int flag = 0;
            if (NametextEdit.Text.Trim() == string.Empty)
            {
                NametextEdit.ErrorText = ErrorRequired;
                flag++;
            }
            editors.ForEach(e =>
            {
                if (e.GetType() == typeof(LookUpEdit) && ((LookUpEdit)e).Properties.DataSource.GetType() != typeof(List<Master.ValueAndID>))
                {
                 //   if (((LookUpEdit)e).IsEditValueNumberAndNotZero() == false)
                        flag += ((LookUpEdit)e).IsEditValueNumberAndNotZero() ? 0 : 1;
                }

            });
            return (flag == 0);


        }
        public override void Save()
        {
            int SavedChangesCount = 0;

            var db = new Entities();
            if (profile.Id == 0)
            {
                db.UserSettingsProfiles.AddOrUpdate(profile);
            }
            else
            {
                db.UserSettingsProfiles.Attach(profile);
            }
            profile.Name = NametextEdit.Text;
            SavedChangesCount += db.SaveChanges();
            db.UserSetttingsProfileProperties.RemoveRange(db.UserSetttingsProfileProperties.Where(x => x.ProfileId == profile.Id));
            SavedChangesCount += db.SaveChanges();
            editors.ForEach(e =>
            {
                db.UserSetttingsProfileProperties.AddOrUpdate(new UserSetttingsProfileProperty()
                {
                    ProfileId = profile.Id,
                    PropertyName = e.Name,
                    PropertyValue = Master.ToByteArray<object>(e.EditValue)
                });
            });
            SavedChangesCount += db.SaveChanges();
            if (SavedChangesCount > 0)
            {
                DataSavedMessage();
            }
            base.Save();
        }
    }
}