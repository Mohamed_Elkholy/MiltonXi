﻿namespace MiltonXi.Forms
{
    partial class frm_User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UsernameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UserTypeLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.SettingsProfileIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.ScreenProfileIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.InactiveToggleSwitch = new DevExpress.XtraEditors.ToggleSwitch();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUsername = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSettingsProfileId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForScreenProfileId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForUserType = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsernameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserTypeLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsProfileIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenProfileIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveToggleSwitch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSettingsProfileId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForScreenProfileId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserType)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UsernameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UserTypeLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SettingsProfileIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ScreenProfileIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.InactiveToggleSwitch);
            this.dataLayoutControl1.DataSource = this.userBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(810, 237);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(325, 22);
            this.NameTextEdit.MenuManager = this._barManager;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Size = new System.Drawing.Size(199, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 4;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(MiltonXi._DAL.User);
            // 
            // UsernameTextEdit
            // 
            this.UsernameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Username", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UsernameTextEdit.Location = new System.Drawing.Point(325, 46);
            this.UsernameTextEdit.MenuManager = this._barManager;
            this.UsernameTextEdit.Name = "UsernameTextEdit";
            this.UsernameTextEdit.Size = new System.Drawing.Size(302, 20);
            this.UsernameTextEdit.StyleController = this.dataLayoutControl1;
            this.UsernameTextEdit.TabIndex = 5;
            // 
            // PasswordTextEdit
            // 
            this.PasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Password", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PasswordTextEdit.Location = new System.Drawing.Point(325, 70);
            this.PasswordTextEdit.MenuManager = this._barManager;
            this.PasswordTextEdit.Name = "PasswordTextEdit";
            this.PasswordTextEdit.Properties.UseSystemPasswordChar = true;
            this.PasswordTextEdit.Size = new System.Drawing.Size(302, 20);
            this.PasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.PasswordTextEdit.TabIndex = 6;
            // 
            // UserTypeLookUpEdit
            // 
            this.UserTypeLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "UserType", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.UserTypeLookUpEdit.Location = new System.Drawing.Point(325, 118);
            this.UserTypeLookUpEdit.MenuManager = this._barManager;
            this.UserTypeLookUpEdit.Name = "UserTypeLookUpEdit";
            this.UserTypeLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UserTypeLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.UserTypeLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UserTypeLookUpEdit.Properties.NullText = "";
            this.UserTypeLookUpEdit.Size = new System.Drawing.Size(302, 20);
            this.UserTypeLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserTypeLookUpEdit.TabIndex = 7;
            // 
            // SettingsProfileIdLookUpEdit
            // 
            this.SettingsProfileIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "SettingsProfileId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SettingsProfileIdLookUpEdit.Location = new System.Drawing.Point(325, 94);
            this.SettingsProfileIdLookUpEdit.MenuManager = this._barManager;
            this.SettingsProfileIdLookUpEdit.Name = "SettingsProfileIdLookUpEdit";
            this.SettingsProfileIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SettingsProfileIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SettingsProfileIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SettingsProfileIdLookUpEdit.Properties.NullText = "";
            this.SettingsProfileIdLookUpEdit.Size = new System.Drawing.Size(302, 20);
            this.SettingsProfileIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SettingsProfileIdLookUpEdit.TabIndex = 8;
            // 
            // ScreenProfileIdLookUpEdit
            // 
            this.ScreenProfileIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "ScreenProfileId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ScreenProfileIdLookUpEdit.Location = new System.Drawing.Point(325, 142);
            this.ScreenProfileIdLookUpEdit.MenuManager = this._barManager;
            this.ScreenProfileIdLookUpEdit.Name = "ScreenProfileIdLookUpEdit";
            this.ScreenProfileIdLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ScreenProfileIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ScreenProfileIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ScreenProfileIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ScreenProfileIdLookUpEdit.Properties.NullText = "";
            this.ScreenProfileIdLookUpEdit.Size = new System.Drawing.Size(302, 20);
            this.ScreenProfileIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ScreenProfileIdLookUpEdit.TabIndex = 9;
            // 
            // InactiveToggleSwitch
            // 
            this.InactiveToggleSwitch.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Inactive", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.InactiveToggleSwitch.EditValue = true;
            this.InactiveToggleSwitch.Location = new System.Drawing.Point(528, 22);
            this.InactiveToggleSwitch.MenuManager = this._barManager;
            this.InactiveToggleSwitch.Name = "InactiveToggleSwitch";
            this.InactiveToggleSwitch.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.InactiveToggleSwitch.Properties.OffText = "Inactive";
            this.InactiveToggleSwitch.Properties.OnText = "Active";
            this.InactiveToggleSwitch.Size = new System.Drawing.Size(99, 20);
            this.InactiveToggleSwitch.StyleController = this.dataLayoutControl1;
            this.InactiveToggleSwitch.TabIndex = 10;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(810, 237);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForName,
            this.ItemForUsername,
            this.ItemForPassword,
            this.ItemForSettingsProfileId,
            this.ItemForScreenProfileId,
            this.ItemForInactive,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.ItemForUserType});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(790, 217);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(200, 10);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(316, 24);
            this.ItemForName.Text = "Name";
            this.ItemForName.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForUsername
            // 
            this.ItemForUsername.Control = this.UsernameTextEdit;
            this.ItemForUsername.Location = new System.Drawing.Point(200, 34);
            this.ItemForUsername.Name = "ItemForUsername";
            this.ItemForUsername.Size = new System.Drawing.Size(419, 24);
            this.ItemForUsername.Text = "Username";
            this.ItemForUsername.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForPassword
            // 
            this.ItemForPassword.Control = this.PasswordTextEdit;
            this.ItemForPassword.Location = new System.Drawing.Point(200, 58);
            this.ItemForPassword.Name = "ItemForPassword";
            this.ItemForPassword.Size = new System.Drawing.Size(419, 24);
            this.ItemForPassword.Text = "Password";
            this.ItemForPassword.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSettingsProfileId
            // 
            this.ItemForSettingsProfileId.Control = this.SettingsProfileIdLookUpEdit;
            this.ItemForSettingsProfileId.Location = new System.Drawing.Point(200, 82);
            this.ItemForSettingsProfileId.Name = "ItemForSettingsProfileId";
            this.ItemForSettingsProfileId.Size = new System.Drawing.Size(419, 24);
            this.ItemForSettingsProfileId.Text = "Roll Entity Permission";
            this.ItemForSettingsProfileId.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForScreenProfileId
            // 
            this.ItemForScreenProfileId.Control = this.ScreenProfileIdLookUpEdit;
            this.ItemForScreenProfileId.Location = new System.Drawing.Point(200, 130);
            this.ItemForScreenProfileId.Name = "ItemForScreenProfileId";
            this.ItemForScreenProfileId.Size = new System.Drawing.Size(419, 24);
            this.ItemForScreenProfileId.Text = "Role Access Permission";
            this.ItemForScreenProfileId.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForInactive
            // 
            this.ItemForInactive.Control = this.InactiveToggleSwitch;
            this.ItemForInactive.Location = new System.Drawing.Point(516, 10);
            this.ItemForInactive.Name = "ItemForInactive";
            this.ItemForInactive.Size = new System.Drawing.Size(103, 24);
            this.ItemForInactive.Text = "Inactive";
            this.ItemForInactive.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForInactive.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(200, 154);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(419, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(419, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(419, 63);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(200, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(419, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(200, 217);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(619, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(171, 217);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForUserType
            // 
            this.ItemForUserType.Control = this.UserTypeLookUpEdit;
            this.ItemForUserType.Location = new System.Drawing.Point(200, 106);
            this.ItemForUserType.Name = "ItemForUserType";
            this.ItemForUserType.Size = new System.Drawing.Size(419, 24);
            this.ItemForUserType.Text = "User Type";
            this.ItemForUserType.TextSize = new System.Drawing.Size(110, 13);
            // 
            // frm_User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 265);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_User";
            this.Text = "User";
            this.Load += new System.EventHandler(this.frm_User_Load);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsernameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserTypeLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsProfileIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenProfileIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveToggleSwitch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSettingsProfileId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForScreenProfileId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraEditors.TextEdit UsernameTextEdit;
        private DevExpress.XtraEditors.TextEdit PasswordTextEdit;
        private DevExpress.XtraEditors.LookUpEdit UserTypeLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit SettingsProfileIdLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit ScreenProfileIdLookUpEdit;
        private DevExpress.XtraEditors.ToggleSwitch InactiveToggleSwitch;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUsername;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPassword;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSettingsProfileId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForScreenProfileId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInactive;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}