﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using MiltonXi.Class;
using System.Data.Entity.Migrations;
using Liphsoft.Crypto.Argon2;
namespace MiltonXi.Forms
{
    public partial class frm_User : frm_Master
    {
        _DAL.User user;
        public frm_User()
        {
            InitializeComponent();
            New();
            RefreshData();
        }
        public frm_User(int Id)
        {
            InitializeComponent();
            RefreshData();

            using (var db = new Entities())
            {
                user = db.Users.SingleOrDefault(x => x.Id == Id);
                GetData();
            }
        }
        public override void New()
        {
            user = new User();

            base.New();
        }
        public override void GetData()
        {

            UsernameTextEdit.Text = user.Username;
            NameTextEdit.Text = user.Name;
            PasswordTextEdit.Text = user.Password;
            SettingsProfileIdLookUpEdit.EditValue = user.SettingsProfileId;
            UserTypeLookUpEdit.EditValue = Convert.ToInt32( user.UserType);
            ScreenProfileIdLookUpEdit.EditValue = user.ScreenProfileId;
            InactiveToggleSwitch.IsOn = user.Inactive;
            base.GetData();
        }
        public override void SetData()
        {
            if (user.Password != PasswordTextEdit.Text)
            {
                var hasher = new PasswordHasher();
                string myhash = hasher.Hash(PasswordTextEdit.Text);
                PasswordTextEdit.Text = myhash;
            }
            user.Username = UsernameTextEdit.Text;
            user.Name = NameTextEdit.Text;
            user.SettingsProfileId = Convert.ToInt32(SettingsProfileIdLookUpEdit.EditValue);
            user.Password = PasswordTextEdit.Text;
            user.UserType = Convert.ToByte(UserTypeLookUpEdit.EditValue);
            user.ScreenProfileId = Convert.ToInt32(ScreenProfileIdLookUpEdit.EditValue);
            user.Inactive = InactiveToggleSwitch.IsOn;

            base.SetData();
        }
        public override void RefreshData()
        {
            using (var db = new Entities())
            {
                ScreenProfileIdLookUpEdit.InitializeData(db.UserAccessProfiles.Select(x => new { x.Id, x.Name }).ToList());
                SettingsProfileIdLookUpEdit.InitializeData(db.UserSettingsProfiles.Select(x => new { x.Id, x.Name }).ToList());
                UserTypeLookUpEdit.InitializeData(Master.UserTypeList);
            }
            base.RefreshData();
        }
        public override bool IsDataValid()
        {
            int NumberOfErrors = 0;
            using (var db = new Entities())
            {
                if (db.Users.Where(x => x.Username.Trim() == UsernameTextEdit.Text.Trim()
                 && x.Id != user.Id).Count() > 0)
                {
                    UsernameTextEdit.ErrorText = "This user already in use";
                    NumberOfErrors++;
                }
                if (db.Users.Where(x => x.Name.Trim() == NameTextEdit.Text.Trim()
                && x.Id != user.Id).Count() > 0)
                {
                    NameTextEdit.ErrorText = "This name already in use";
                    NumberOfErrors++;
                }
            }
            NumberOfErrors += NameTextEdit.IsTextValid() ? 0 : 1;
            NumberOfErrors += PasswordTextEdit.IsTextValid() ? 0 : 1;
            NumberOfErrors += UsernameTextEdit.IsTextValid() ? 0 : 1;
            NumberOfErrors += UserTypeLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            NumberOfErrors += ScreenProfileIdLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            NumberOfErrors += SettingsProfileIdLookUpEdit.IsEditValueNumberAndNotZero() ? 0 : 1;
            if (NumberOfErrors > 0)
                return false;
            else
                return true;
        }
        public override void Save()
        {
            int SavedChangesCount = 0;

            using (var db = new Entities())
            {
                //     db.Users.Attach(user);
                SetData();
          //      db.Users.Attach(user);
                db.Users.AddOrUpdate(user);
                SavedChangesCount += db.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    DataSavedMessage();
                    base.Save();
                }
            }

        }

        private void frm_User_Load(object sender, EventArgs e)
        {


        }





    }
}