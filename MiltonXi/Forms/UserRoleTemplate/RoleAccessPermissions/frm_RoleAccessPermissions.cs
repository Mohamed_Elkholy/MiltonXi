﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Class;
using MiltonXi._DAL;
using System.Data.Entity.Migrations;
using DevExpress.XtraEditors.Repository;

namespace MiltonXi.Forms
{
    public partial class frm_RoleAccessPermissions : frm_Master
    {
        _DAL.UserAccessProfile profile;
        public frm_RoleAccessPermissions()
        {
            InitializeComponent();
            New();
            GetData();
            NametextEdit.Text = profile.Name;

           

        }
        RepositoryItemCheckEdit repoCheck;
        private void Frm_RoleAccessPermissions_Load(object sender, EventArgs e)
        {
            treeList1.CustomNodeCellEdit += TreeList1_CustomNodeCellEdit;
            treeList1.OptionsView.ShowIndicator = false;
            treeList1.KeyFieldName = nameof(ScreenAccessProfile.ScreenId);
            treeList1.ParentFieldName = nameof(ScreenAccessProfile.ParentScreenId);
            treeList1.Columns[nameof(ScreenAccessProfile.ScreenName)].Visible = false;
            treeList1.Columns[nameof(ScreenAccessProfile.ScreenName)].OptionsColumn.AllowEdit = false;
            treeList1.BestFitColumns();
            repoCheck = new RepositoryItemCheckEdit();
            repoCheck.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgRadio2;
            treeList1.Columns[nameof(ScreenAccessProfile.CanAdd)].ColumnEdit = repoCheck;
            treeList1.Columns[nameof(ScreenAccessProfile.CanDelete)].ColumnEdit = repoCheck;
            treeList1.Columns[nameof(ScreenAccessProfile.CanEdit)].ColumnEdit = repoCheck;
            treeList1.Columns[nameof(ScreenAccessProfile.CanOpen)].ColumnEdit = repoCheck;
            treeList1.Columns[nameof(ScreenAccessProfile.CanPrint)].ColumnEdit = repoCheck;
            treeList1.Columns[nameof(ScreenAccessProfile.CanShow)].ColumnEdit = repoCheck;

            treeList1.Columns[nameof(ScreenAccessProfile.CanAdd)].Width = 25;
          treeList1.Columns[nameof(ScreenAccessProfile.CanDelete)].Width=25;
            treeList1.Columns[nameof(ScreenAccessProfile.CanEdit)].Width = 25;
            treeList1.Columns[nameof(ScreenAccessProfile.CanOpen)].Width = 25;
           treeList1.Columns[nameof(ScreenAccessProfile.CanPrint)].Width = 25;
            treeList1.Columns[nameof(ScreenAccessProfile.CanShow)].Width = 25;

            treeList1.ExpandAll();
        }

        public frm_RoleAccessPermissions(int Id )
        {
            InitializeComponent();
            using(var db = new Entities())
            {
                profile = db.UserAccessProfiles.SingleOrDefault(x => x.Id == Id);
            }
            NametextEdit.Text = profile.Name;
            GetData();

        }
        public override void New()
        {
            profile = new _DAL.UserAccessProfile();
            NametextEdit.Text = profile.Name;
        }

        private void TreeList1_CustomNodeCellEdit(object sender, DevExpress.XtraTreeList.GetCustomNodeCellEditEventArgs e)
        {
            if (e.Node.Id >= 0)
            {
                var row = treeList1.GetRow(e.Node.Id) as ScreenAccessProfile;
                if (row != null)
                {
                    if (e.Column.FieldName == nameof(ScreenAccessProfile.CanAdd) && row.Actions.Contains(Master.Actions.Add) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();
                    }
                    else if (e.Column.FieldName == nameof(ScreenAccessProfile.CanDelete) && row.Actions.Contains(Master.Actions.Delete) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();

                    }
                    else if (e.Column.FieldName == nameof(ScreenAccessProfile.CanEdit) && row.Actions.Contains(Master.Actions.Edit) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();

                    }
                    else if (e.Column.FieldName == nameof(ScreenAccessProfile.CanOpen) && row.Actions.Contains(Master.Actions.Open) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();

                    }
                    else if (e.Column.FieldName == nameof(ScreenAccessProfile.CanPrint) && row.Actions.Contains(Master.Actions.Print) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();

                    }
                    else if (e.Column.FieldName == nameof(ScreenAccessProfile.CanShow) && row.Actions.Contains(Master.Actions.Show) == false)
                    {
                        e.RepositoryItem = new DevExpress.XtraEditors.Repository.RepositoryItem();

                    }
                }
            }
        }

        public override void GetData()
        {
            List<ScreenAccessProfile> data;
            using (var db = new Entities())
            {
                data = (from s in Screens.GetScreens
                        from d in db.UserAccessProfileDetails
                        .Where(x => x.UserAccessProfileId == profile.Id && x.ScreenId == s.ScreenId).DefaultIfEmpty()
                        select new ScreenAccessProfile(s.ScreenName)
                        {
                            ScreenName = s.ScreenName,
                            ParentScreenId = s.ParentScreenId,
                            ScreenCaption = s.ScreenCaption,
                            ScreenId = s.ScreenId,
                            Actions = s.Actions,
                            CanAdd = (d == null) ? true : d.CanAdd,
                            CanDelete = (d == null) ? true : d.CanDelete,
                            CanOpen = (d == null) ? true : d.CanOpen,
                            CanPrint = (d == null) ? true : d.CanPrint,
                            CanEdit = (d == null) ? true : d.CanEdit,
                            CanShow = (d == null) ? true : d.CanShow,
                        }).ToList();

            }
            treeList1.DataSource = data;

        }

        public override bool IsDataValid()
        {
            return true;
        }
        public override void Save()
        {
            int SavedChangesCount = 0;

            var db = new Entities();
            if (profile.Id == 0)
            {
                db.UserAccessProfiles.AddOrUpdate(profile);
            }
            else
            {
                db.UserAccessProfiles.Attach(profile);
            }
            profile.Name = NametextEdit.Text;
            SavedChangesCount += db.SaveChanges();
            db.UserAccessProfileDetails.RemoveRange(db.UserAccessProfileDetails.Where(x => x.UserAccessProfileId == profile.Id));
            SavedChangesCount += db.SaveChanges();
            var data = treeList1.DataSource as List<ScreenAccessProfile>;
            var dbData = data.Select(s => new UserAccessProfileDetail
            {

                CanAdd = s.CanAdd,
                CanDelete  = s.CanDelete,
                CanOpen =  s.CanOpen,
                CanPrint = s.CanPrint,
                CanEdit =  s.CanEdit,
                CanShow =  s.CanShow,
                UserAccessProfileId=profile.Id,
                ScreenId = s.ScreenId
            }).ToList();
            db.UserAccessProfileDetails.AddRange(dbData);
            SavedChangesCount += db.SaveChanges();
            if (SavedChangesCount > 0)
            {
                DataSavedMessage();
            }
            base.Save();
        }
    }
}