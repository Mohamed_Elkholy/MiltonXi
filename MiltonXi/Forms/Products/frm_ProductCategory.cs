﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiltonXi._DAL;
using DevExpress.XtraEditors;
using System.Data.Entity.Migrations;
using MiltonXi.Class;

namespace MiltonXi.Forms
{
    public partial class frm_ProductCategory : Forms.frm_Master
    {
        ProductCategory category;
        bool firstfocus = false;
        public frm_ProductCategory()
        {
            InitializeComponent();
            RefreshData();

        }


        private void frm_ProductCategory_Load(object sender, EventArgs e)
        {
            treeList1.OptionsBehavior.Editable = false;
            treeList1.ParentFieldName = nameof(category.ParentId);
            treeList1.KeyFieldName = nameof(category.Id);
            treeList1.Columns[nameof(category.Number)].Visible = false;
            treeList1.FocusedNodeChanged += TreeList1_FocusedNodeChanged;
            lookUpEditCategory.InitializeData(nameof(category.Name), nameof(category.Id));
            New();
        }
        private void TreeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (!firstfocus)
            { firstfocus = true; return; }

            int Id = 0;
            if (int.TryParse(e.Node.GetValue(nameof(category.Id)).ToString(), out Id))
            {
                using (Entities ctx = new Entities())
                {
                    category = ctx.ProductCategories.Single(x => x.Id == Id) ?? new ProductCategory();
                    GetData();
                }
            }
        }

        public override void New()
        {
            category = new _DAL.ProductCategory();
            GetData();
            base.New();
        }

        public override void Save()
        {
            SetData();
            using (Entities ctx = new Entities())
            {

                ctx.ProductCategories.AddOrUpdate(category);
                if (ctx.SaveChanges() > 0)
                {
                    DataSavedMessage();
                }
            }
            base.Save();
        }
        public override void GetData()
        {
            textEditName.Text = category.Name;
            lookUpEditCategory.EditValue = category.ParentId;
            base.GetData();
        }
        public override void SetData()
        {
            category.Name = textEditName.Text;
            category.ParentId = (lookUpEditCategory.EditValue as int?) ?? 0;
            category.Number = "0";
            base.SetData();
        }
        public override void RefreshData()
        {
            using (Entities ctx = new Entities())
            {
                var parentCategories = ctx.ProductCategories.ToList(); ;
                lookUpEditCategory.InitializeData(parentCategories);
                treeList1.DataSource = parentCategories;
                treeList1.ExpandAll();
            }
            base.RefreshData();
        }
        public override bool IsDataValid()
        {
            if (!textEditName.IsTextValid())
                return false;

            var stringProperties = category.GetType().GetProperties()
                           .Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(category, null);
                if (currentValue == null) continue;
                stringProperty.SetValue(category, currentValue.Trim(), null);
            }
            using (Entities ctx = new Entities())
            {
                var result = ctx.Stores.Where(x => x.Name == category.Name
                && category.Id != x.Id
                    ).Count();
                if (result > 0)
                {
                    DuplicateMessage();
                    return false;
                }
            }
            return true;
        }

        public override void Delete()
        {
            using (Entities ctx = new Entities())
            {
                ctx.ProductCategories.Attach(category);
                ctx.ProductCategories.Remove(category);
                if (ctx.SaveChanges() > 0)
                {
                    DataSavedMessage();
                    RefreshData();
                }
            }
            base.Delete();
        }


    }
}