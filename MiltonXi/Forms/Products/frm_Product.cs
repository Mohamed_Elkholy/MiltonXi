﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using static MiltonXi.Class.Master;
using System.Collections.ObjectModel;
using MiltonXi.Class;

namespace MiltonXi.Forms
{
    public partial class frm_Product : Forms.frm_Master
    {
        private Product product;
        private Entities entities = new Entities();
        private RepositoryItemCalcEdit calcEdit = new RepositoryItemCalcEdit();
        private RepositoryItemLookUpEdit repoUOM = new RepositoryItemLookUpEdit();
        private RepositoryItemCheckEdit repoCheckEdit = new RepositoryItemCheckEdit();
        private BindingList<ProductUnit> productunitList;

        public frm_Product()
        {
            InitializeComponent();
            RefreshData();
            New();
        }

        public frm_Product(int Id)
        {
            InitializeComponent();
            RefreshData();
            LoadProduct(Id);
        }

        void LoadProduct(int Id)
        {
            productBindingSource.DataSource = entities.Products.Single(x => x.Id == Id);
            this.Text = $"Product: {(productBindingSource.DataSource as Product).Name}";
            GetData();
        }

        private void frm_Products_Load(object sender, EventArgs e)
        {
            repoCheckEdit.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgRadio2;
            TypeLookUpEdit.InitializeData(nameof(ValueAndID.Name), nameof(ValueAndID.Id));
            CategoryIdLookUpEdit.InitializeData(nameof(_DAL.ProductCategory.Name), nameof(_DAL.ProductCategory.Id));
            CategoryIdLookUpEdit.ProcessNewValue += CategoryIdLookUpEdit_ProcessNewValue;
            CategoryIdLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            CategoryIdLookUpEdit.ItemIndex = CategoryIdLookUpEdit.ItemIndex == -1 ? 0 : CategoryIdLookUpEdit.ItemIndex;
            gridView1.OptionsView.ShowGroupPanel = false;
            gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            gridControl1.RepositoryItems.Add(calcEdit);
            gridControl1.RepositoryItems.Add(repoCheckEdit);
            gridControl1.RepositoryItems.Add(repoUOM);
            gridView1.Columns[nameof(_DAL.ProductUnit.Id)].Visible = false;
            gridView1.Columns[nameof(_DAL.ProductUnit.Product)].Visible = false;
            gridView1.Columns[nameof(_DAL.ProductUnit.ProductId)].Visible = false;
            gridView1.Columns[nameof(_DAL.ProductUnit.UnitId)].Caption = "Unit";
            gridView1.Columns[nameof(_DAL.ProductUnit.BuyPrice)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(_DAL.ProductUnit.SellPrice)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(_DAL.ProductUnit.SellDiscount)].ColumnEdit = calcEdit;
            gridView1.Columns[nameof(_DAL.ProductUnit.Factor)].ColumnEdit = calcEdit;

            gridView1.Columns[nameof(_DAL.ProductUnit.UnitId)].ColumnEdit = repoUOM;
            repoUOM.ValueMember = nameof(_DAL.UnitName.Id);
            repoUOM.DisplayMember = nameof(_DAL.UnitName.Name);
            repoUOM.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            //repoUOM.ProcessNewValue += RepoUOM_ProcessNewValue;
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            gridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            gridView1.CustomRowCellEditForEditing += GridView1_CustomRowCellEditForEditing;
            this.FormClosed += Frm_Products_FormClosed;
        }

        private void GridView1_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == nameof(ProductUnit.UnitId))
            {
                var ids = ((Collection<ProductUnit>)gridView1.DataSource).Select(x => x.UnitId).ToList();
                RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                using (var db = new Entities())
                {
                    var currentId = (Int32?)e.CellValue;
                    ids.Remove(currentId ?? 0);
                    repo.DataSource = db.UnitNames.Where(x => ids.Contains(x.Id) == false).ToList();
                    repo.ValueMember = nameof(_DAL.UnitName.Id);
                    repo.DisplayMember = nameof(_DAL.UnitName.Name);
                    repo.PopulateColumns();
                    e.RepositoryItem = repo;
                    repo.Columns["Id"].Visible = false;
                    repo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard; 
                    repo.ProcessNewValue += RepoUOM_ProcessNewValue;
                }

            }
        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //dont need it but keeping it for future 
            // gridView1.Columns[nameof(ProductUnit.Factor)].OptionsColumn.AllowEdit = !(e.FocusedRowHandle == 0);
        }

        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = e.Row as ProductUnit;
            var view = sender as GridView;
            if (row == null)
                return;
            //if (row.Factor < 1 && e.RowHandle != 0)
            if (row.Factor < 1)
            {
                e.Valid = false;
                gridView1.SetColumnError(view.Columns[nameof(row.Factor)], "Value should be greater than zero");
            }
            if (row.UnitId <= 0)
            {
                e.Valid = false;
                gridView1.SetColumnError(view.Columns[nameof(row.UnitId)], ErrorRequired);
            }
            if (CheckIfBarcodeExist(row.Barcode, product.Id))
            {
                e.Valid = false;
                gridView1.SetColumnError(view.Columns[nameof(row.Barcode)], "This code already in use");
            }

        }

        private void RepoUOM_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            if (e.DisplayValue is string value && value.Trim() != string.Empty)
            {
                var newObject = new _DAL.UnitName() { Name = value.ToLower().Trim() };
                using (Entities ctx = new Entities())
                {
                    if (DialogResult.Yes == XtraMessageBox.Show($"Do you want to create '{value}' as Unit and add it to the list?", "Confirmation Save", MessageBoxButtons
                       .YesNo, MessageBoxIcon.Question))
                    {
                        ctx.UnitNames.AddOrUpdate(newObject);
                        if (ctx.SaveChanges() > 0)
                        {
                            ((List<UnitName>)repoUOM.DataSource).Add(newObject);
                            ((List<UnitName>)((LookUpEdit)sender).Properties.DataSource).Add(newObject);
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        private void CategoryIdLookUpEdit_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            if (e.DisplayValue is string value && value.Trim() != string.Empty)
            {
                var newObject = new _DAL.ProductCategory() { Name = value.ToLower().Trim(), ParentId = 0, Number = "0" };
                using (Entities ctx = new Entities())
                {
                    if (DialogResult.Yes == XtraMessageBox.Show($"Do you want to create '{value}' as category and add it to the list?", "Confirmation Save", MessageBoxButtons
                        .YesNo, MessageBoxIcon.Question))
                    {
                        newObject = ctx.ProductCategories.Where(x => x.Name.ToLower() == newObject.Name.ToLower()).FirstOrDefault();
                        ctx.ProductCategories.AddOrUpdate(newObject);
                        if (ctx.SaveChanges() > 0)
                        {
                            ((List<ProductCategory>)CategoryIdLookUpEdit.Properties.DataSource).Add(newObject);
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        public override void Save()
        {
       
            using (Entities ctx = new Entities())
            {
                int SavedChangesCount = 0;
                product.ProductUnits = productunitList;
                ctx.Products.AddOrUpdate(product);
                SavedChangesCount += ctx.SaveChanges();
                foreach (var item in productunitList)
                {
                    item.ProductId = product.Id;
                    ctx.ProductUnits.AddOrUpdate(item);
                }
                SavedChangesCount += ctx.SaveChanges();
                if (SavedChangesCount > 0)
                {
                    DataSavedMessage();
                    this.Text =  $"Product: {product.Name}";
                    Class.Utilities.SetPageName(this.Name, (string)this.Tag, this.Text);
                 //   Class.Utilities.RefreshGrid(nameof(frm_ProductList));
                }
            }
            base.Save();
        }


        string GetNewProductCode()
        {
            string MaxCode;
            using (Entities ctx = new Entities())
            {
                MaxCode = ctx.Products.Select(x => x.Code).Max();
            }
            return GetNextNumberInString(MaxCode);

        }

        string GetNewBarCode()
        {
            string MaxCode;
            using (Entities ctx = new Entities())
            {
                MaxCode = ctx.ProductUnits.Select(x => x.Barcode).Max();
            }
            return GetNextNumberInString(MaxCode);
        }


        public override void GetData()
        {
            product = productBindingSource.DataSource as Product;
            var q = (product).ProductUnits.ToList();
            productunitList = new BindingList<ProductUnit>(q);
            gridControl1.DataSource = productunitList;
            base.GetData();
        }

        public override bool IsDataValid()
        {
            if (CategoryIdLookUpEdit.EditValue is int == false || Convert.ToInt32(CategoryIdLookUpEdit.EditValue) <= 0)
            {
                CategoryIdLookUpEdit.ErrorText = "This field is required";
                return false;
            }
            if (TypeLookUpEdit.EditValue is byte == false || Convert.ToByte(CategoryIdLookUpEdit.EditValue) <= 0)
            {
                CategoryIdLookUpEdit.ErrorText = "This field is required";
                return false;
            }

            var prod = (productBindingSource.DataSource as _DAL.Product);
            var stringProperties = prod.GetType().GetProperties()
                           .Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(prod, null);
                if (currentValue == null) continue;
                stringProperty.SetValue(prod, currentValue.Trim(), null);
            }


            using (Entities ctx = new Entities())
            {
                if (ctx.Products.Where(x => x.Name == prod.Name
                  && x.Id != prod.Id
                      ).Count() > 0)
                {
                    NameTextEdit.ErrorText = "This name alreay exist";
                    return false;
                }

                if (ctx.Products.Where(x => x.Name == prod.Name
                 && x.Code != prod.Code
                     ).Count() > 0)
                {
                    CodeTextEdit.ErrorText = "This code alreay exist";
                    return false;
                }

            }
            return true;
        }

        public override void RefreshData()
        {
            using (Entities ctx = new Entities())
            {
                var categories = ctx.ProductCategories
                    .Where(x => ctx.ProductCategories.Where(y => y.ParentId == x.Id).Count() == 0).ToList();
                CategoryIdLookUpEdit.Properties.DataSource = categories;
                TypeLookUpEdit.Properties.DataSource = new List<ValueAndID>() { new ValueAndID() { Id = (int)ProductType.Service, Name = ProductType.Service.ToString() }, new ValueAndID() { Id = (int)ProductType.Inventory, Name = ProductType.Inventory.ToString() } };
                repoUOM.DataSource = ctx.UnitNames.ToList();
            }
            CostCalculationMethodLookUpEdit.InitializeData(MasterInventory.CostCalculationMethodList);
            base.RefreshData();
        }

        public override void New()
        {
            this.Text = "New Product";
            Class.Utilities.SetPageName(this.Name, this.Tag == null ? "" : this.Tag.ToString(), this.Text);
            productBindingSource.DataSource = new Product()
            {
                Code = GetNewProductCode()
            };
            if (entities.UnitNames.Count() == 0)
            {
                entities.UnitNames.Add(new UnitName() { Name = "Kilogram" });
                entities.SaveChanges();
            }
            base.New();

            productunitList.Add(new ProductUnit() { Barcode = GetNewBarCode(), UnitId = entities.UnitNames.First().Id });

        }

        Boolean CheckIfBarcodeExist(string barcode, int productId)
        {
            using (Entities ctx = new Entities())
            {
                return ctx.ProductUnits.Where(x => x.Barcode == barcode && x.ProductId != productId).Count() > 0 ? true : false;
            }
        }

        private void frm_Products_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void Frm_Products_FormClosed(object sender, FormClosedEventArgs e)
        {
            entities.Dispose();
            Class.Utilities.RefreshGrid("frm_ProductList");
        }




    }
}

