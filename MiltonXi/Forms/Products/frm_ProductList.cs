﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi._DAL;
using static MiltonXi.Class.Master;
using MiltonXi.Class;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace MiltonXi.Forms
{
    public partial class frm_ProductList : frm_Master
    {

        public frm_ProductList()
        {
            InitializeComponent();
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        }

        private void frm_ProductList_Load(object sender, EventArgs e)
        {
            RefreshData();
            gridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
            gridView1.OptionsBehavior.Editable = false;
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            gridView1.DoubleClick += GridView1_DoubleClick;
            gridControl1.ViewRegistered += GridControl1_ViewRegistered;
            gridView1.OptionsDetail.ShowDetailTabs = false;
        }

        private void GridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {
            GridView view = e.View as GridView;
            if (e.View.LevelName == "UMO")
            {
                view.OptionsView.ShowGroupedColumns = false;
                view.OptionsView.ShowViewCaption = true;
                e.View.ViewCaption = "Unit Of Measure";
            }
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {
                int id = 0;
                if (int.TryParse(gridView1.GetFocusedRowCellValue("Id").ToString(), out id) && id > 0)
                {
                    string name = gridView1.GetFocusedRowCellValue("Name").ToString();
                    Class.Utilities.OpenFormInMain(new frm_Product(id) { Tag = name });
                }
            }
        }

        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == nameof(_DAL.Product.Type))
            {
                e.DisplayText = Master.ProductTypesList.Single(x => x.Id == Convert.ToInt32(e.Value)).Name;
            }
        }


        public override void RefreshData()
        {
            base.RefreshData();
            gridControl1.DataSource = Session.ProductsView;
            gridView1.Columns["Id"].Visible = false;
        }

       
        public override void New()
        {
            Class.Utilities.OpenFormInMain(new frm_Product());
            base.New();
        }

    }
}