﻿namespace MiltonXi.Forms
{
    partial class frm_Product
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.CodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TypeLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.DescreptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CategoryIdLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.CostCalculationMethodLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBarcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NontaxableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.InactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCategoryId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCalculationMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDescreption = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForProductUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNontaxable = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescreptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CategoryIdLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationMethodLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NontaxableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCategoryId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescreption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProductUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNontaxable)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TypeLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DescreptionMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CategoryIdLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCalculationMethodLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NontaxableCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.InactiveCheckEdit);
            this.dataLayoutControl1.DataSource = this.productBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1062, 666);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CodeTextEdit
            // 
            this.CodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Code", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CodeTextEdit.Location = new System.Drawing.Point(403, 77);
            this.CodeTextEdit.MenuManager = this._barManager;
            this.CodeTextEdit.Name = "CodeTextEdit";
            this.CodeTextEdit.Size = new System.Drawing.Size(319, 20);
            this.CodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CodeTextEdit.TabIndex = 3;
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(MiltonXi._DAL.Product);
            // 
            // TypeLookUpEdit
            // 
            this.TypeLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Type", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TypeLookUpEdit.Location = new System.Drawing.Point(403, 101);
            this.TypeLookUpEdit.MenuManager = this._barManager;
            this.TypeLookUpEdit.Name = "TypeLookUpEdit";
            this.TypeLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TypeLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TypeLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TypeLookUpEdit.Properties.NullText = "";
            this.TypeLookUpEdit.Size = new System.Drawing.Size(407, 20);
            this.TypeLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TypeLookUpEdit.TabIndex = 4;
            // 
            // DescreptionMemoEdit
            // 
            this.DescreptionMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Descreption", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DescreptionMemoEdit.Location = new System.Drawing.Point(403, 173);
            this.DescreptionMemoEdit.MenuManager = this._barManager;
            this.DescreptionMemoEdit.Name = "DescreptionMemoEdit";
            this.DescreptionMemoEdit.Size = new System.Drawing.Size(407, 101);
            this.DescreptionMemoEdit.StyleController = this.dataLayoutControl1;
            this.DescreptionMemoEdit.TabIndex = 6;
            // 
            // CategoryIdLookUpEdit
            // 
            this.CategoryIdLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "CategoryId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CategoryIdLookUpEdit.Location = new System.Drawing.Point(403, 125);
            this.CategoryIdLookUpEdit.MenuManager = this._barManager;
            this.CategoryIdLookUpEdit.Name = "CategoryIdLookUpEdit";
            this.CategoryIdLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CategoryIdLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CategoryIdLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CategoryIdLookUpEdit.Properties.NullText = "";
            this.CategoryIdLookUpEdit.Size = new System.Drawing.Size(407, 20);
            this.CategoryIdLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CategoryIdLookUpEdit.TabIndex = 5;
            // 
            // CostCalculationMethodLookUpEdit
            // 
            this.CostCalculationMethodLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "CostCalculationMethod", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CostCalculationMethodLookUpEdit.Location = new System.Drawing.Point(403, 149);
            this.CostCalculationMethodLookUpEdit.MenuManager = this._barManager;
            this.CostCalculationMethodLookUpEdit.Name = "CostCalculationMethodLookUpEdit";
            this.CostCalculationMethodLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CostCalculationMethodLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CostCalculationMethodLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostCalculationMethodLookUpEdit.Properties.NullText = "";
            this.CostCalculationMethodLookUpEdit.Size = new System.Drawing.Size(407, 20);
            this.CostCalculationMethodLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CostCalculationMethodLookUpEdit.TabIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(296, 315);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this._barManager;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(502, 284);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colProductId,
            this.colUnitId,
            this.colFactor,
            this.colBuyPrice,
            this.colSellPrice,
            this.colSellDiscount,
            this.colBarcode,
            this.colProduct});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            // 
            // colProductId
            // 
            this.colProductId.FieldName = "ProductId";
            this.colProductId.Name = "colProductId";
            this.colProductId.Visible = true;
            this.colProductId.VisibleIndex = 1;
            // 
            // colUnitId
            // 
            this.colUnitId.FieldName = "UnitId";
            this.colUnitId.Name = "colUnitId";
            this.colUnitId.Visible = true;
            this.colUnitId.VisibleIndex = 2;
            // 
            // colFactor
            // 
            this.colFactor.FieldName = "Factor";
            this.colFactor.Name = "colFactor";
            this.colFactor.Visible = true;
            this.colFactor.VisibleIndex = 3;
            // 
            // colBuyPrice
            // 
            this.colBuyPrice.FieldName = "BuyPrice";
            this.colBuyPrice.Name = "colBuyPrice";
            this.colBuyPrice.Visible = true;
            this.colBuyPrice.VisibleIndex = 4;
            // 
            // colSellPrice
            // 
            this.colSellPrice.FieldName = "SellPrice";
            this.colSellPrice.Name = "colSellPrice";
            this.colSellPrice.Visible = true;
            this.colSellPrice.VisibleIndex = 5;
            // 
            // colSellDiscount
            // 
            this.colSellDiscount.FieldName = "SellDiscount";
            this.colSellDiscount.Name = "colSellDiscount";
            this.colSellDiscount.Visible = true;
            this.colSellDiscount.VisibleIndex = 6;
            // 
            // colBarcode
            // 
            this.colBarcode.FieldName = "Barcode";
            this.colBarcode.Name = "colBarcode";
            this.colBarcode.Visible = true;
            this.colBarcode.VisibleIndex = 7;
            // 
            // colProduct
            // 
            this.colProduct.FieldName = "Product";
            this.colProduct.Name = "colProduct";
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 8;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(403, 53);
            this.NameTextEdit.MenuManager = this._barManager;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Size = new System.Drawing.Size(319, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 9;
            // 
            // NontaxableCheckEdit
            // 
            this.NontaxableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Nontaxable", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NontaxableCheckEdit.Location = new System.Drawing.Point(726, 77);
            this.NontaxableCheckEdit.MenuManager = this._barManager;
            this.NontaxableCheckEdit.Name = "NontaxableCheckEdit";
            this.NontaxableCheckEdit.Properties.Caption = "Non-Taxable";
            this.NontaxableCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.NontaxableCheckEdit.Size = new System.Drawing.Size(84, 20);
            this.NontaxableCheckEdit.StyleController = this.dataLayoutControl1;
            this.NontaxableCheckEdit.TabIndex = 10;
            // 
            // InactiveCheckEdit
            // 
            this.InactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productBindingSource, "Inactive", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.InactiveCheckEdit.Location = new System.Drawing.Point(726, 53);
            this.InactiveCheckEdit.MenuManager = this._barManager;
            this.InactiveCheckEdit.Name = "InactiveCheckEdit";
            this.InactiveCheckEdit.Properties.Caption = "Inactive";
            this.InactiveCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.InactiveCheckEdit.Size = new System.Drawing.Size(84, 20);
            this.InactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.InactiveCheckEdit.TabIndex = 11;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1062, 666);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1042, 646);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(814, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(228, 586);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 29);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(260, 586);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1042, 29);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 615);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1042, 31);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCode,
            this.ItemForType,
            this.ItemForCategoryId,
            this.ItemForCostCalculationMethod,
            this.ItemForDescreption,
            this.layoutControlGroup2,
            this.ItemForName,
            this.ItemForInactive,
            this.ItemForNontaxable});
            this.layoutControlGroup3.Location = new System.Drawing.Point(260, 29);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(554, 586);
            this.layoutControlGroup3.Text = " ";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // ItemForCode
            // 
            this.ItemForCode.Control = this.CodeTextEdit;
            this.ItemForCode.Location = new System.Drawing.Point(0, 24);
            this.ItemForCode.Name = "ItemForCode";
            this.ItemForCode.Size = new System.Drawing.Size(442, 24);
            this.ItemForCode.Text = "Code";
            this.ItemForCode.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForType
            // 
            this.ItemForType.Control = this.TypeLookUpEdit;
            this.ItemForType.Location = new System.Drawing.Point(0, 48);
            this.ItemForType.Name = "ItemForType";
            this.ItemForType.Size = new System.Drawing.Size(530, 24);
            this.ItemForType.Text = "Type";
            this.ItemForType.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForCategoryId
            // 
            this.ItemForCategoryId.Control = this.CategoryIdLookUpEdit;
            this.ItemForCategoryId.Location = new System.Drawing.Point(0, 72);
            this.ItemForCategoryId.Name = "ItemForCategoryId";
            this.ItemForCategoryId.Size = new System.Drawing.Size(530, 24);
            this.ItemForCategoryId.Text = "Category Id";
            this.ItemForCategoryId.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForCostCalculationMethod
            // 
            this.ItemForCostCalculationMethod.Control = this.CostCalculationMethodLookUpEdit;
            this.ItemForCostCalculationMethod.Location = new System.Drawing.Point(0, 96);
            this.ItemForCostCalculationMethod.Name = "ItemForCostCalculationMethod";
            this.ItemForCostCalculationMethod.Size = new System.Drawing.Size(530, 24);
            this.ItemForCostCalculationMethod.Text = "Cost Calculation Method";
            this.ItemForCostCalculationMethod.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForDescreption
            // 
            this.ItemForDescreption.Control = this.DescreptionMemoEdit;
            this.ItemForDescreption.Location = new System.Drawing.Point(0, 120);
            this.ItemForDescreption.MaxSize = new System.Drawing.Size(530, 105);
            this.ItemForDescreption.MinSize = new System.Drawing.Size(530, 105);
            this.ItemForDescreption.Name = "ItemForDescreption";
            this.ItemForDescreption.Size = new System.Drawing.Size(530, 105);
            this.ItemForDescreption.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDescreption.Text = "Descreption";
            this.ItemForDescreption.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForProductUnits});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 225);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(530, 337);
            this.layoutControlGroup2.Text = "Unit of measurements";
            // 
            // ItemForProductUnits
            // 
            this.ItemForProductUnits.Control = this.gridControl1;
            this.ItemForProductUnits.Location = new System.Drawing.Point(0, 0);
            this.ItemForProductUnits.MaxSize = new System.Drawing.Size(0, 288);
            this.ItemForProductUnits.MinSize = new System.Drawing.Size(104, 288);
            this.ItemForProductUnits.Name = "ItemForProductUnits";
            this.ItemForProductUnits.Size = new System.Drawing.Size(506, 288);
            this.ItemForProductUnits.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForProductUnits.StartNewLine = true;
            this.ItemForProductUnits.Text = "Product Units";
            this.ItemForProductUnits.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForProductUnits.TextVisible = false;
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(442, 24);
            this.ItemForName.Text = "Name";
            this.ItemForName.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForInactive
            // 
            this.ItemForInactive.Control = this.InactiveCheckEdit;
            this.ItemForInactive.Location = new System.Drawing.Point(442, 0);
            this.ItemForInactive.Name = "ItemForInactive";
            this.ItemForInactive.Size = new System.Drawing.Size(88, 24);
            this.ItemForInactive.Text = "Inactive";
            this.ItemForInactive.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForInactive.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForInactive.TextToControlDistance = 0;
            this.ItemForInactive.TextVisible = false;
            // 
            // ItemForNontaxable
            // 
            this.ItemForNontaxable.Control = this.NontaxableCheckEdit;
            this.ItemForNontaxable.Location = new System.Drawing.Point(442, 24);
            this.ItemForNontaxable.Name = "ItemForNontaxable";
            this.ItemForNontaxable.Size = new System.Drawing.Size(88, 24);
            this.ItemForNontaxable.Text = "Nontaxable";
            this.ItemForNontaxable.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNontaxable.TextVisible = false;
            // 
            // frm_Product
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 694);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_Product";
            this.Text = "Product";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Products_FormClosing);
            this.Load += new System.EventHandler(this.frm_Products_Load);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescreptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CategoryIdLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCalculationMethodLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NontaxableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCategoryId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescreption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProductUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNontaxable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit CodeTextEdit;
        private System.Windows.Forms.BindingSource productBindingSource;
        private DevExpress.XtraEditors.LookUpEdit TypeLookUpEdit;
        private DevExpress.XtraEditors.MemoEdit DescreptionMemoEdit;
        private DevExpress.XtraEditors.LookUpEdit CategoryIdLookUpEdit;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProductUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colProductId;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitId;
        private DevExpress.XtraGrid.Columns.GridColumn colFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSellDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colBarcode;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraEditors.LookUpEdit CostCalculationMethodLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCategoryId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCalculationMethod;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescreption;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.CheckEdit NontaxableCheckEdit;
        private DevExpress.XtraEditors.CheckEdit InactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNontaxable;
    }
}