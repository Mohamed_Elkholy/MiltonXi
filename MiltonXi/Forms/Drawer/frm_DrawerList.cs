﻿using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MiltonXi._DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiltonXi.Forms
{
    public partial class frm_DrawerList : frm_Master
    {
        public frm_DrawerList()
        {
            InitializeComponent();
            RefreshData();
            btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            gridView1.OptionsBehavior.Editable = false;
            gridView1.Columns["Id"].Visible = false;
            gridView1.Columns[nameof(Drawer.AccountId)].Visible = false;
            gridView1.Columns[nameof(Drawer.AccountId)].AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
          
            gridView1.DoubleClick += GridView1_DoubleClick; ;
            btn_Save.Enabled = false;

        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (ValidateGridDoubleClick(sender, e))
            {
           
                int id = 0;
                id = Convert.ToInt32(gridView1.GetFocusedRowCellValue("Id"));

                string name = gridView1.GetFocusedRowCellValue("Name").ToString();
                Class.Utilities.OpenFormInMain(new frm_Drawer(id) { Tag = name });
            }
        }

        public override void New()
        {
            base.New();
   
            Class.Utilities.OpenFormInMain(new frm_Drawer());
            RefreshData();
        }

        public override void RefreshData()
        {
            using (_DAL.Entities ctx = new _DAL.Entities())
            {
                drawerBindingSource.DataSource = ctx.Drawers.ToList();
            }
            base.RefreshData();
        }

    }
}
