﻿namespace MiltonXi.Forms
{
    partial class frm_Drawer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.drawerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SwiftTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CurrencyLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.LegalNoteMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.BankAccountMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLegalNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCurrency = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSwift = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwiftTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LegalNoteMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankAccountMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSwift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SwiftTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CurrencyLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.LegalNoteMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.BankAccountMemoEdit);
            this.dataLayoutControl1.DataSource = this.drawerBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 28);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(932, 618);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.drawerBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NameTextEdit.Location = new System.Drawing.Point(338, 53);
            this.NameTextEdit.MaximumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.MenuManager = this._barManager;
            this.NameTextEdit.MinimumSize = new System.Drawing.Size(221, 0);
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.NameTextEdit.Size = new System.Drawing.Size(221, 20);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 5;
            // 
            // drawerBindingSource
            // 
            this.drawerBindingSource.DataSource = typeof(MiltonXi._DAL.Drawer);
            // 
            // SwiftTextEdit
            // 
            this.SwiftTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.drawerBindingSource, "Swift", true));
            this.SwiftTextEdit.Location = new System.Drawing.Point(338, 406);
            this.SwiftTextEdit.MenuManager = this._barManager;
            this.SwiftTextEdit.Name = "SwiftTextEdit";
            this.SwiftTextEdit.Size = new System.Drawing.Size(221, 20);
            this.SwiftTextEdit.StyleController = this.dataLayoutControl1;
            this.SwiftTextEdit.TabIndex = 8;
            // 
            // CurrencyLookUpEdit
            // 
            this.CurrencyLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.drawerBindingSource, "Currency", true));
            this.CurrencyLookUpEdit.Location = new System.Drawing.Point(338, 77);
            this.CurrencyLookUpEdit.MenuManager = this._barManager;
            this.CurrencyLookUpEdit.Name = "CurrencyLookUpEdit";
            this.CurrencyLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CurrencyLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CurrencyLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CurrencyLookUpEdit.Properties.NullText = "";
            this.CurrencyLookUpEdit.Size = new System.Drawing.Size(221, 20);
            this.CurrencyLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CurrencyLookUpEdit.TabIndex = 10;
            // 
            // LegalNoteMemoEdit
            // 
            this.LegalNoteMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.drawerBindingSource, "LegalNote", true));
            this.LegalNoteMemoEdit.Location = new System.Drawing.Point(338, 190);
            this.LegalNoteMemoEdit.MenuManager = this._barManager;
            this.LegalNoteMemoEdit.Name = "LegalNoteMemoEdit";
            this.LegalNoteMemoEdit.Size = new System.Drawing.Size(221, 212);
            this.LegalNoteMemoEdit.StyleController = this.dataLayoutControl1;
            this.LegalNoteMemoEdit.TabIndex = 11;
            // 
            // BankAccountMemoEdit
            // 
            this.BankAccountMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.drawerBindingSource, "BankAccount", true));
            this.BankAccountMemoEdit.Location = new System.Drawing.Point(338, 101);
            this.BankAccountMemoEdit.MenuManager = this._barManager;
            this.BankAccountMemoEdit.Name = "BankAccountMemoEdit";
            this.BankAccountMemoEdit.Size = new System.Drawing.Size(221, 85);
            this.BankAccountMemoEdit.StyleController = this.dataLayoutControl1;
            this.BankAccountMemoEdit.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(932, 618);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlGroup3,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(912, 598);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(912, 29);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForName,
            this.ItemForBankAccount,
            this.ItemForLegalNote,
            this.ItemForCurrency,
            this.ItemForSwift});
            this.layoutControlGroup3.Location = new System.Drawing.Point(246, 29);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(317, 401);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.Location = new System.Drawing.Point(0, 0);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(293, 24);
            this.ItemForName.Text = "Name";
            this.ItemForName.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForBankAccount
            // 
            this.ItemForBankAccount.Control = this.BankAccountMemoEdit;
            this.ItemForBankAccount.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankAccount.MaxSize = new System.Drawing.Size(0, 89);
            this.ItemForBankAccount.MinSize = new System.Drawing.Size(82, 89);
            this.ItemForBankAccount.Name = "ItemForBankAccount";
            this.ItemForBankAccount.Size = new System.Drawing.Size(293, 89);
            this.ItemForBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForBankAccount.Text = "Bank Account";
            this.ItemForBankAccount.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForLegalNote
            // 
            this.ItemForLegalNote.Control = this.LegalNoteMemoEdit;
            this.ItemForLegalNote.Location = new System.Drawing.Point(0, 137);
            this.ItemForLegalNote.MaxSize = new System.Drawing.Size(0, 216);
            this.ItemForLegalNote.MinSize = new System.Drawing.Size(82, 216);
            this.ItemForLegalNote.Name = "ItemForLegalNote";
            this.ItemForLegalNote.Size = new System.Drawing.Size(293, 216);
            this.ItemForLegalNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLegalNote.Text = "Legal Note";
            this.ItemForLegalNote.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForCurrency
            // 
            this.ItemForCurrency.Control = this.CurrencyLookUpEdit;
            this.ItemForCurrency.Location = new System.Drawing.Point(0, 24);
            this.ItemForCurrency.Name = "ItemForCurrency";
            this.ItemForCurrency.Size = new System.Drawing.Size(293, 24);
            this.ItemForCurrency.Text = "Currency";
            this.ItemForCurrency.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ItemForSwift
            // 
            this.ItemForSwift.Control = this.SwiftTextEdit;
            this.ItemForSwift.Location = new System.Drawing.Point(0, 353);
            this.ItemForSwift.Name = "ItemForSwift";
            this.ItemForSwift.Size = new System.Drawing.Size(293, 24);
            this.ItemForSwift.Text = "Swift";
            this.ItemForSwift.TextSize = new System.Drawing.Size(65, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(563, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(349, 401);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 29);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(246, 401);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 430);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 34);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(912, 168);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_Drawer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 646);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_Drawer";
            this.Text = "Drawer";
            this.Load += new System.EventHandler(this.frm_Drawer_Load);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwiftTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LegalNoteMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankAccountMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSwift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource drawerBindingSource;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.TextEdit SwiftTextEdit;
        private DevExpress.XtraEditors.LookUpEdit CurrencyLookUpEdit;
        private DevExpress.XtraEditors.MemoEdit LegalNoteMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSwift;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLegalNote;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrency;
        private DevExpress.XtraEditors.MemoEdit BankAccountMemoEdit;
    }
}