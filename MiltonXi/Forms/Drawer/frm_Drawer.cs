﻿using DevExpress.XtraEditors;
using MiltonXi._DAL;
using MiltonXi.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiltonXi.Forms
{
    public partial class frm_Drawer : Forms.frm_Master
    {
        public frm_Drawer()
        {
            InitializeComponent();
            New();
        }
        public frm_Drawer(int Id)
        {
            InitializeComponent();
            using (Entities ctx = new Entities())
            {
                drawerBindingSource.DataSource = ctx.Drawers.Single(d => d.Id == Id);
                this.Text = $"Drawer: {(drawerBindingSource.DataSource as Drawer).Name}";
                if((drawerBindingSource.DataSource as Drawer).SystemProtected)
                {
                    CurrencyLookUpEdit.Properties.ReadOnly = true;
                    NameTextEdit.ReadOnly = true;
                }
            }
        }

        public override void New()
        {
            base.New();
            this.Text = "New drawer";
            drawerBindingSource.DataSource = new _DAL.Drawer();
        }

        public override void Save()
        {
            base.Save();
            _DAL.Drawer drawer = drawerBindingSource.DataSource as _DAL.Drawer;
            using (Entities ctx = new Entities())
            {
                ctx.Drawers.Attach(drawer);
                ctx.Drawers.AddOrUpdate(drawer);
                Account account = ctx.Accounts.Where(a => a.Id == drawer.AccountId).FirstOrDefault() ?? new Account();
                account.Name = drawer.Name;
                ctx.Accounts.AddOrUpdate(account);
                ctx.SaveChanges();
                drawer.AccountId = account.Id;
                if (ctx.SaveChanges()>0)
                {
                    DataSavedMessage();
                    this.Text = $"Drawer: {drawer.Name}";
                }
            }
            
        }
        public override void Delete()
        {
            base.Delete();
            _DAL.Drawer drawer = drawerBindingSource.DataSource as _DAL.Drawer;
            if (drawer.SystemProtected)
            { SystemProtectedDelete(); return; }

            using (Entities ctx = new Entities())
            {
                ctx.Drawers.Attach(drawer);
                ctx.Drawers.Remove(drawer);
                if (ctx.SaveChanges() > 0)
                {
                    DataSavedMessage();
                }
            }
        }

        public override bool IsDataValid()
        {
            var darwer = (drawerBindingSource.DataSource as _DAL.Drawer);

            var stringProperties = darwer.GetType().GetProperties()
                           .Where(p => p.PropertyType == typeof(string));
            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(darwer, null);
                if (currentValue == null) continue;
                stringProperty.SetValue(darwer, currentValue.Trim(), null);
            }


            using (Entities ctx = new Entities())
            {
                var result = ctx.Drawers.Where(x => x.Name == darwer.Name
                &&  x.Id!= darwer.Id
                    ).Count();
                if (result > 0)
                {
                    DuplicateMessage();
                    return false;
                }
            }
            return true;
        }

        private void frm_Drawer_Load(object sender, EventArgs e)
        {
           // CurrencyLookUpEdit.InitializeData(Master.CurrencyList, "Name", "Name");
            CurrencyLookUpEdit.Properties.DataSource = Master.CurrencyList;
            CurrencyLookUpEdit.Properties.ValueMember = "Name";
            CurrencyLookUpEdit.Properties.DisplayMember = "Name";
            CurrencyLookUpEdit.Properties.PopulateColumns();
            CurrencyLookUpEdit.Properties.Columns["Id"].Visible = false;

        }







    }
}
