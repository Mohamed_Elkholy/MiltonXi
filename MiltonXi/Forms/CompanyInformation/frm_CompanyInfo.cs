﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MiltonXi.Forms;

using System.Data.Entity.Migrations;
using MiltonXi._DAL;

namespace MiltonXi.Forms
{
    public partial class frm_CompanyInfo : frm_Master
    {
        public frm_CompanyInfo()
        {
           
            InitializeComponent();
          //  btn_Delete.Enabled = false;
            GetData();
        }

        public override void GetData()
        {
            base.GetData();
            using (Entities ctx = new Entities())
            {
                companyInfoBindingSource.DataSource = ctx.CompanyInfoes.FirstOrDefault() ?? new CompanyInfo();
            }
        }
        public override void Save()
        {

            using (Entities ctx = new Entities())
            {
                CompanyInfo company = companyInfoBindingSource.Current as CompanyInfo;
                ctx.CompanyInfoes.AddOrUpdate(company);
                if (ctx.SaveChanges() > 0)
                {
                    DataSavedMessage();
                }
            }
            base.Save();
        }
        //public override void Save()
        //{
        //   // this.Validate();
        //    using(Entities ctx = new Entities())
        //    {
        //        CompanyInfo company = companyInfoBindingSource.Current as CompanyInfo;
        //        ctx.CompanyInfoes.AddOrUpdate(company);
        //        if (ctx.SaveChanges() > 0)
        //        {
        //            DataSavedMessage();
        //        }
        //    }
        //    base.Save();

        //}
        public override void Delete()
        {
            base.Delete();
        }
        public override void New()
        {
            base.New();
            companyInfoBindingSource.DataSource = new CompanyInfo() { Id = 1 };
        }
        public override bool IsDataValid()
        {
            return true;
        }
    }
}