﻿namespace MiltonXi.Forms
{
    partial class frm_CompanyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.CompanyNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.companyInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BranchTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CountryTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccountIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCompanyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBranch = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCountry = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccountId = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.VAT_ID_NIPTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForVAT_ID_NIP = new DevExpress.XtraLayout.LayoutControlItem();
            this.WebsiteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VAT_ID_NIPTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT_ID_NIP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CompanyNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BranchTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CountryTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccountIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NotesMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.VAT_ID_NIPTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebsiteTextEdit);
            this.dataLayoutControl1.DataSource = this.companyInfoBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 24);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(739, 315);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CompanyNameTextEdit
            // 
            this.CompanyNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "CompanyName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CompanyNameTextEdit.Location = new System.Drawing.Point(90, 12);
            this.CompanyNameTextEdit.MenuManager = this._barManager;
            this.CompanyNameTextEdit.Name = "CompanyNameTextEdit";
            this.CompanyNameTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CompanyNameTextEdit.Size = new System.Drawing.Size(251, 20);
            this.CompanyNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CompanyNameTextEdit.TabIndex = 4;
            // 
            // companyInfoBindingSource
            // 
            this.companyInfoBindingSource.DataSource = typeof(MiltonXi._DAL.CompanyInfo);
            // 
            // PhoneTextEdit
            // 
            this.PhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Phone", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PhoneTextEdit.Location = new System.Drawing.Point(90, 36);
            this.PhoneTextEdit.MenuManager = this._barManager;
            this.PhoneTextEdit.Name = "PhoneTextEdit";
            this.PhoneTextEdit.Size = new System.Drawing.Size(251, 20);
            this.PhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.PhoneTextEdit.TabIndex = 5;
            // 
            // MobileTextEdit
            // 
            this.MobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Mobile", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.MobileTextEdit.Location = new System.Drawing.Point(90, 60);
            this.MobileTextEdit.MenuManager = this._barManager;
            this.MobileTextEdit.Name = "MobileTextEdit";
            this.MobileTextEdit.Size = new System.Drawing.Size(251, 20);
            this.MobileTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTextEdit.TabIndex = 6;
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Address", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AddressTextEdit.Location = new System.Drawing.Point(90, 84);
            this.AddressTextEdit.MenuManager = this._barManager;
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Size = new System.Drawing.Size(251, 20);
            this.AddressTextEdit.StyleController = this.dataLayoutControl1;
            this.AddressTextEdit.TabIndex = 7;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.Location = new System.Drawing.Point(90, 108);
            this.EmailTextEdit.MenuManager = this._barManager;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Size = new System.Drawing.Size(251, 20);
            this.EmailTextEdit.StyleController = this.dataLayoutControl1;
            this.EmailTextEdit.TabIndex = 8;
            // 
            // BranchTextEdit
            // 
            this.BranchTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Branch", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BranchTextEdit.Location = new System.Drawing.Point(90, 132);
            this.BranchTextEdit.MenuManager = this._barManager;
            this.BranchTextEdit.Name = "BranchTextEdit";
            this.BranchTextEdit.Size = new System.Drawing.Size(251, 20);
            this.BranchTextEdit.StyleController = this.dataLayoutControl1;
            this.BranchTextEdit.TabIndex = 9;
            // 
            // CityTextEdit
            // 
            this.CityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "City", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CityTextEdit.Location = new System.Drawing.Point(90, 156);
            this.CityTextEdit.MenuManager = this._barManager;
            this.CityTextEdit.Name = "CityTextEdit";
            this.CityTextEdit.Size = new System.Drawing.Size(251, 20);
            this.CityTextEdit.StyleController = this.dataLayoutControl1;
            this.CityTextEdit.TabIndex = 10;
            // 
            // CountryTextEdit
            // 
            this.CountryTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Country", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CountryTextEdit.Location = new System.Drawing.Point(90, 180);
            this.CountryTextEdit.MenuManager = this._barManager;
            this.CountryTextEdit.Name = "CountryTextEdit";
            this.CountryTextEdit.Size = new System.Drawing.Size(251, 20);
            this.CountryTextEdit.StyleController = this.dataLayoutControl1;
            this.CountryTextEdit.TabIndex = 11;
            // 
            // AccountIdTextEdit
            // 
            this.AccountIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "AccountId", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.AccountIdTextEdit.Location = new System.Drawing.Point(90, 252);
            this.AccountIdTextEdit.MenuManager = this._barManager;
            this.AccountIdTextEdit.Name = "AccountIdTextEdit";
            this.AccountIdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AccountIdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AccountIdTextEdit.Properties.Mask.EditMask = "N0";
            this.AccountIdTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AccountIdTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AccountIdTextEdit.Size = new System.Drawing.Size(251, 20);
            this.AccountIdTextEdit.StyleController = this.dataLayoutControl1;
            this.AccountIdTextEdit.TabIndex = 12;
            // 
            // NotesMemoEdit
            // 
            this.NotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Notes", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.NotesMemoEdit.Location = new System.Drawing.Point(423, 12);
            this.NotesMemoEdit.MenuManager = this._barManager;
            this.NotesMemoEdit.Name = "NotesMemoEdit";
            this.NotesMemoEdit.Size = new System.Drawing.Size(304, 260);
            this.NotesMemoEdit.StyleController = this.dataLayoutControl1;
            this.NotesMemoEdit.TabIndex = 14;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(739, 315);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCompanyName,
            this.ItemForPhone,
            this.ItemForMobile,
            this.ItemForAddress,
            this.ItemForEmail,
            this.ItemForBranch,
            this.ItemForCity,
            this.ItemForCountry,
            this.ItemForNotes,
            this.ItemForAccountId,
            this.emptySpaceItem1,
            this.ItemForVAT_ID_NIP,
            this.ItemForWebsite});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(719, 295);
            // 
            // ItemForCompanyName
            // 
            this.ItemForCompanyName.Control = this.CompanyNameTextEdit;
            this.ItemForCompanyName.Location = new System.Drawing.Point(0, 0);
            this.ItemForCompanyName.Name = "ItemForCompanyName";
            this.ItemForCompanyName.Size = new System.Drawing.Size(333, 24);
            this.ItemForCompanyName.Text = "Company Name";
            this.ItemForCompanyName.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.Control = this.PhoneTextEdit;
            this.ItemForPhone.Location = new System.Drawing.Point(0, 24);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(333, 24);
            this.ItemForPhone.Text = "Phone";
            this.ItemForPhone.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.MobileTextEdit;
            this.ItemForMobile.Location = new System.Drawing.Point(0, 48);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(333, 24);
            this.ItemForMobile.Text = "Mobile";
            this.ItemForMobile.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(333, 24);
            this.ItemForAddress.Text = "Address";
            this.ItemForAddress.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.Location = new System.Drawing.Point(0, 96);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(333, 24);
            this.ItemForEmail.Text = "Email";
            this.ItemForEmail.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForBranch
            // 
            this.ItemForBranch.Control = this.BranchTextEdit;
            this.ItemForBranch.Location = new System.Drawing.Point(0, 120);
            this.ItemForBranch.Name = "ItemForBranch";
            this.ItemForBranch.Size = new System.Drawing.Size(333, 24);
            this.ItemForBranch.Text = "Branch";
            this.ItemForBranch.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForCity
            // 
            this.ItemForCity.Control = this.CityTextEdit;
            this.ItemForCity.Location = new System.Drawing.Point(0, 144);
            this.ItemForCity.Name = "ItemForCity";
            this.ItemForCity.Size = new System.Drawing.Size(333, 24);
            this.ItemForCity.Text = "City";
            this.ItemForCity.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForCountry
            // 
            this.ItemForCountry.Control = this.CountryTextEdit;
            this.ItemForCountry.Location = new System.Drawing.Point(0, 168);
            this.ItemForCountry.Name = "ItemForCountry";
            this.ItemForCountry.Size = new System.Drawing.Size(333, 24);
            this.ItemForCountry.Text = "Country";
            this.ItemForCountry.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesMemoEdit;
            this.ItemForNotes.Location = new System.Drawing.Point(333, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(386, 264);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ItemForAccountId
            // 
            this.ItemForAccountId.Control = this.AccountIdTextEdit;
            this.ItemForAccountId.Location = new System.Drawing.Point(0, 240);
            this.ItemForAccountId.Name = "ItemForAccountId";
            this.ItemForAccountId.Size = new System.Drawing.Size(333, 24);
            this.ItemForAccountId.Text = "Account Id";
            this.ItemForAccountId.TextSize = new System.Drawing.Size(75, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 264);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(719, 31);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // VAT_ID_NIPTextEdit
            // 
            this.VAT_ID_NIPTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "VAT_ID_NIP", true));
            this.VAT_ID_NIPTextEdit.Location = new System.Drawing.Point(90, 204);
            this.VAT_ID_NIPTextEdit.MenuManager = this._barManager;
            this.VAT_ID_NIPTextEdit.Name = "VAT_ID_NIPTextEdit";
            this.VAT_ID_NIPTextEdit.Size = new System.Drawing.Size(251, 20);
            this.VAT_ID_NIPTextEdit.StyleController = this.dataLayoutControl1;
            this.VAT_ID_NIPTextEdit.TabIndex = 15;
            // 
            // ItemForVAT_ID_NIP
            // 
            this.ItemForVAT_ID_NIP.Control = this.VAT_ID_NIPTextEdit;
            this.ItemForVAT_ID_NIP.Location = new System.Drawing.Point(0, 192);
            this.ItemForVAT_ID_NIP.Name = "ItemForVAT_ID_NIP";
            this.ItemForVAT_ID_NIP.Size = new System.Drawing.Size(333, 24);
            this.ItemForVAT_ID_NIP.Text = "VAT ID/NIP";
            this.ItemForVAT_ID_NIP.TextSize = new System.Drawing.Size(75, 13);
            // 
            // WebsiteTextEdit
            // 
            this.WebsiteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyInfoBindingSource, "Website", true));
            this.WebsiteTextEdit.Location = new System.Drawing.Point(90, 228);
            this.WebsiteTextEdit.MenuManager = this._barManager;
            this.WebsiteTextEdit.Name = "WebsiteTextEdit";
            this.WebsiteTextEdit.Size = new System.Drawing.Size(251, 20);
            this.WebsiteTextEdit.StyleController = this.dataLayoutControl1;
            this.WebsiteTextEdit.TabIndex = 16;
            // 
            // ItemForWebsite
            // 
            this.ItemForWebsite.Control = this.WebsiteTextEdit;
            this.ItemForWebsite.Location = new System.Drawing.Point(0, 216);
            this.ItemForWebsite.Name = "ItemForWebsite";
            this.ItemForWebsite.Size = new System.Drawing.Size(333, 24);
            this.ItemForWebsite.Text = "Website";
            this.ItemForWebsite.TextSize = new System.Drawing.Size(75, 13);
            // 
            // frm_CompanyInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 339);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "frm_CompanyInfo";
            this.Text = "Company info";
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CompanyNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VAT_ID_NIPTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT_ID_NIP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit CompanyNameTextEdit;
        private System.Windows.Forms.BindingSource companyInfoBindingSource;
        private DevExpress.XtraEditors.TextEdit PhoneTextEdit;
        private DevExpress.XtraEditors.TextEdit MobileTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompanyName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress;
        private DevExpress.XtraEditors.TextEdit EmailTextEdit;
        private DevExpress.XtraEditors.TextEdit BranchTextEdit;
        private DevExpress.XtraEditors.TextEdit CityTextEdit;
        private DevExpress.XtraEditors.TextEdit CountryTextEdit;
        private DevExpress.XtraEditors.TextEdit AccountIdTextEdit;
        private DevExpress.XtraEditors.MemoEdit NotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBranch;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCountry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccountId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit VAT_ID_NIPTextEdit;
        private DevExpress.XtraEditors.TextEdit WebsiteTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVAT_ID_NIP;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebsite;
    }
}