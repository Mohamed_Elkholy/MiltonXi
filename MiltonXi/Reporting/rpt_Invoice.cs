﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using MiltonXi.Class;
using MiltonXi._DAL;

namespace MiltonXi.Reporting
{
    public partial class rpt_Invoice : DevExpress.XtraReports.UI.XtraReport
    {
        Master.InvoiceType Type;

        public rpt_Invoice()
        {
            InitializeComponent();
            lbl_SellerName.Text = Session.CompanyInfo.CompanyName;
            lbl_SellerAddress.Text = Session.CompanyInfo.Address;
            lbl_SellerVATNIP.Text = Session.CompanyInfo.VAT_ID_NIP;
            if (lbl_SellerVATNIP.Text == string.Empty)
                lbl_SellerVATNIPLable.Text = string.Empty;
            lbl_SellerEmail.Text = Session.CompanyInfo.Email;
            lbl_SellerWebsite.Text = Session.CompanyInfo.Website;
            lbl_SellerPhone.Text = Session.CompanyInfo.Phone;
            if (lbl_SellerPhone.Text == string.Empty)
                lbl_SellerPhoneLabel.Text = string.Empty;
            lbl_BuyerVATNIP.BeforePrint += Lbl_BuyerVATNIP_BeforePrint;
        }

        private void Lbl_BuyerVATNIP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if((sender as XRTableCell).Text == string.Empty)
            {
                switch ((sender as XRTableCell).Name)
                {
                    case "lbl_BuyerVATNIP":
                        lbl_BuyerVATNIPLabel.Text = string.Empty;
                        break;
                    case "lbl_BuyerPhone":
                        lbl_BuyerPhoneLabel.Text = string.Empty;
                        break;
                }
            }
        }

        private void BindData()
        {
            // Invoice ID
            lblInvoiceNumber.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Code));
            xrBarCode.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Code));
            lblDate.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.IssueDate));

            //TODO Seller Details

            // Buyer Details
            lbl_BuyerName.DataBindings.Add("Text", this.DataSource, "PartyName");
            lbl_BuyerAddress.DataBindings.Add("Text", this.DataSource, "PartyAddress");
            lbl_BuyerVATNIP.DataBindings.Add("Text", this.DataSource, "PartyVATNIP");

            lbl_BuyerEmail.DataBindings.Add("Text", this.DataSource, "PartyEmail");
            lbl_BuyerPhone.DataBindings.Add("Text", this.DataSource, "PartyPhone");

            lbl_BuyerWebsite.DataBindings.Add("Text", this.DataSource, "PartyWebsite");

            //total labels down
            lblTotalNetPrice.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Net));
            lblTotalNetPriceCurrency.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Currency));
            lblTotalVATAmount.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.TaxValue));
            lblTotalVATAmountCurrency.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Currency));

            lblTotalGorssAmount.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Total));
            lblTotalGorssAmountCurrency.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Currency));

            // total lables in grid
            lblCellNet.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Net));
            lblCellVATValue.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.TaxValue));
            lblCellVATRatio.DataBindings.Add("Text", this.DataSource, "TaxRatioSolid");
            lblCellTotal.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Total));

            //lables on the left side
            //TODO Due date
            lblDueDate.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.DueDate));
            lblTotalDue.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Total));
            lblTotalDueCurrency.DataBindings.Add("Text", this.DataSource, nameof(InvoiceHeader.Currency));

            lblLegalNote.DataBindings.Add("Text", this.DataSource, nameof(Drawer.LegalNote));
            lblBankAccount.DataBindings.Add("Text", this.DataSource, nameof(Drawer.BankAccount));
            lblSwift.DataBindings.Add("Text", this.DataSource, nameof(Drawer.Swift));

            cellIndex.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "#"));
            cellProduct.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "ProductName"));
            cellQty.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", nameof(InvoiceDetail.ItemQty)));
            cellUnit.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "UnitName"));
            cellTotalGross.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "(TotalPrice * TaxRatio)+TotalPrice"));
            cellTotalNet.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", nameof(InvoiceDetail.TotalPrice)));
            cellVATAmount.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "(TotalPrice * TaxRatio)"));
            cellVATRatio.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", "TaxRatioSolid"));
        }
        public static void Print(object ds)
        {
            Reporting.rpt_Invoice rpt = new Reporting.rpt_Invoice();
            rpt.DataSource = ds;
            rpt.DetailReport.DataSource = rpt.DataSource;
            rpt.DetailReport.DataMember = "Products";
            rpt.BindData();

            switch (Session.CurrentSettings.InvoicePrintMode)
            {
                case Master.PrintMode.Direct:rpt.Print(); break;
                case Master.PrintMode.ShowPreview:rpt.ShowPreview();break;
                case Master.PrintMode.ShowDialog:rpt.PrintDialog();break;
            }
        }
        int index = 1;
        private void cellIndex_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            cellIndex.Text = (index++).ToString();
        }
    }
}
