﻿namespace MiltonXi.Reporting
{
    partial class rpt_Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblInvoiceNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerVATNIPLable = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_SellerVATNIP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerWebsite = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_SellerPhoneLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_SellerPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerAddress = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerVATNIPLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_BuyerVATNIP = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerEmail = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerWebsite = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_BuyerPhoneLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_BuyerPhone = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.lblTotalDueCurrency = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrBarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.lblSwift = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBankAccount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLegalNote = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDueDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTotalDue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalNetPriceCurrency = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalNetPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalVATAmountCurrency = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalVATAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalGorssAmountCurrency = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTotalGorssAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellIndex = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellProduct = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellQty = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellUnit = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTotalNet = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellVATRatio = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellVATAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellTotalGross = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellNet = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellVATRatio = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellVATValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCellTotal = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 50F;
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 50F;
            this.BottomMargin.Name = "BottomMargin";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrTable1,
            this.xrLabel1,
            this.xrTable2,
            this.xrTable3,
            this.xrLabel2,
            this.xrLine2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 907.979F;
            this.Detail.HierarchyPrintOptions.Indent = 50.8F;
            this.Detail.Name = "Detail";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource(global::MiltonXi.Properties.Resources.Milton_Logo_for_invoices, true);
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(572.9824F, 340.5721F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(1245.937F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(754.0625F, 190.5F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Invoice / Faktura No. / numer";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblInvoiceNumber});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // lblInvoiceNumber
            // 
            this.lblInvoiceNumber.Dpi = 254F;
            this.lblInvoiceNumber.Font = new System.Drawing.Font("Arial", 18F);
            this.lblInvoiceNumber.Multiline = true;
            this.lblInvoiceNumber.Name = "lblInvoiceNumber";
            this.lblInvoiceNumber.StylePriority.UseFont = false;
            this.lblInvoiceNumber.StylePriority.UseTextAlignment = false;
            this.lblInvoiceNumber.Text = "InvouiceNumber\r";
            this.lblInvoiceNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblInvoiceNumber.Weight = 1D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.lblDate});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "Issue date / Data wystawienia:";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.75263157894736843D;
            // 
            // lblDate
            // 
            this.lblDate.Dpi = 254F;
            this.lblDate.Multiline = true;
            this.lblDate.Name = "lblDate";
            this.lblDate.StylePriority.UseTextAlignment = false;
            this.lblDate.Text = "....-..-..";
            this.lblDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDate.TextFormatString = "{0:dd-MM-yyyy}";
            this.lblDate.Weight = 0.24736842105263154D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 340.5721F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(820.2083F, 58.42001F);
            this.xrLabel1.Text = "Seller / Sprzedawca: ";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 412.1154F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable2.SizeF = new System.Drawing.SizeF(820.2083F, 462.6429F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UsePadding = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerName});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // lbl_SellerName
            // 
            this.lbl_SellerName.Dpi = 254F;
            this.lbl_SellerName.Multiline = true;
            this.lbl_SellerName.Name = "lbl_SellerName";
            this.lbl_SellerName.StylePriority.UseTextAlignment = false;
            this.lbl_SellerName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerName.Weight = 1D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerAddress});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // lbl_SellerAddress
            // 
            this.lbl_SellerAddress.Dpi = 254F;
            this.lbl_SellerAddress.Multiline = true;
            this.lbl_SellerAddress.Name = "lbl_SellerAddress";
            this.lbl_SellerAddress.StylePriority.UseTextAlignment = false;
            this.lbl_SellerAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerAddress.Weight = 1D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerVATNIPLable,
            this.lbl_SellerVATNIP});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // lbl_SellerVATNIPLable
            // 
            this.lbl_SellerVATNIPLable.Dpi = 254F;
            this.lbl_SellerVATNIPLable.Multiline = true;
            this.lbl_SellerVATNIPLable.Name = "lbl_SellerVATNIPLable";
            this.lbl_SellerVATNIPLable.StylePriority.UseTextAlignment = false;
            this.lbl_SellerVATNIPLable.Text = "VAT ID / NIP";
            this.lbl_SellerVATNIPLable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerVATNIPLable.Weight = 0.27096782307098083D;
            // 
            // lbl_SellerVATNIP
            // 
            this.lbl_SellerVATNIP.Dpi = 254F;
            this.lbl_SellerVATNIP.Multiline = true;
            this.lbl_SellerVATNIP.Name = "lbl_SellerVATNIP";
            this.lbl_SellerVATNIP.StylePriority.UseTextAlignment = false;
            this.lbl_SellerVATNIP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerVATNIP.Weight = 0.72903217692901912D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerEmail});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // lbl_SellerEmail
            // 
            this.lbl_SellerEmail.Dpi = 254F;
            this.lbl_SellerEmail.Multiline = true;
            this.lbl_SellerEmail.Name = "lbl_SellerEmail";
            this.lbl_SellerEmail.StylePriority.UseTextAlignment = false;
            this.lbl_SellerEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerEmail.Weight = 1D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerWebsite});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // lbl_SellerWebsite
            // 
            this.lbl_SellerWebsite.Dpi = 254F;
            this.lbl_SellerWebsite.Multiline = true;
            this.lbl_SellerWebsite.Name = "lbl_SellerWebsite";
            this.lbl_SellerWebsite.StylePriority.UseTextAlignment = false;
            this.lbl_SellerWebsite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerWebsite.Weight = 1D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_SellerPhoneLabel,
            this.lbl_SellerPhone});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // lbl_SellerPhoneLabel
            // 
            this.lbl_SellerPhoneLabel.Dpi = 254F;
            this.lbl_SellerPhoneLabel.Multiline = true;
            this.lbl_SellerPhoneLabel.Name = "lbl_SellerPhoneLabel";
            this.lbl_SellerPhoneLabel.StylePriority.UseTextAlignment = false;
            this.lbl_SellerPhoneLabel.Text = "Phone / Telefon:";
            this.lbl_SellerPhoneLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerPhoneLabel.Weight = 0.33548390409406909D;
            // 
            // lbl_SellerPhone
            // 
            this.lbl_SellerPhone.Dpi = 254F;
            this.lbl_SellerPhone.Multiline = true;
            this.lbl_SellerPhone.Name = "lbl_SellerPhone";
            this.lbl_SellerPhone.StylePriority.UseTextAlignment = false;
            this.lbl_SellerPhone.Text = "lbl_SellerPhone";
            this.lbl_SellerPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_SellerPhone.Weight = 0.66451609590593086D;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(1154.792F, 412.1154F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15});
            this.xrTable3.SizeF = new System.Drawing.SizeF(820.2083F, 462.6429F);
            this.xrTable3.StylePriority.UseBackColor = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerName});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // lbl_BuyerName
            // 
            this.lbl_BuyerName.Dpi = 254F;
            this.lbl_BuyerName.Multiline = true;
            this.lbl_BuyerName.Name = "lbl_BuyerName";
            this.lbl_BuyerName.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerName.Weight = 1D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerAddress});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // lbl_BuyerAddress
            // 
            this.lbl_BuyerAddress.Dpi = 254F;
            this.lbl_BuyerAddress.Multiline = true;
            this.lbl_BuyerAddress.Name = "lbl_BuyerAddress";
            this.lbl_BuyerAddress.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerAddress.Weight = 1D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerVATNIPLabel,
            this.lbl_BuyerVATNIP});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // lbl_BuyerVATNIPLabel
            // 
            this.lbl_BuyerVATNIPLabel.CanShrink = true;
            this.lbl_BuyerVATNIPLabel.Dpi = 254F;
            this.lbl_BuyerVATNIPLabel.Multiline = true;
            this.lbl_BuyerVATNIPLabel.Name = "lbl_BuyerVATNIPLabel";
            this.lbl_BuyerVATNIPLabel.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerVATNIPLabel.Text = "VAT ID / NIP";
            this.lbl_BuyerVATNIPLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerVATNIPLabel.Weight = 0.27096767424255447D;
            // 
            // lbl_BuyerVATNIP
            // 
            this.lbl_BuyerVATNIP.Dpi = 254F;
            this.lbl_BuyerVATNIP.Multiline = true;
            this.lbl_BuyerVATNIP.Name = "lbl_BuyerVATNIP";
            this.lbl_BuyerVATNIP.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerVATNIP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerVATNIP.Weight = 0.72903232575744559D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerEmail});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // lbl_BuyerEmail
            // 
            this.lbl_BuyerEmail.Dpi = 254F;
            this.lbl_BuyerEmail.Multiline = true;
            this.lbl_BuyerEmail.Name = "lbl_BuyerEmail";
            this.lbl_BuyerEmail.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerEmail.Weight = 1D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerWebsite});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // lbl_BuyerWebsite
            // 
            this.lbl_BuyerWebsite.Dpi = 254F;
            this.lbl_BuyerWebsite.Multiline = true;
            this.lbl_BuyerWebsite.Name = "lbl_BuyerWebsite";
            this.lbl_BuyerWebsite.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerWebsite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerWebsite.Weight = 1D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_BuyerPhoneLabel,
            this.lbl_BuyerPhone});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // lbl_BuyerPhoneLabel
            // 
            this.lbl_BuyerPhoneLabel.CanShrink = true;
            this.lbl_BuyerPhoneLabel.Dpi = 254F;
            this.lbl_BuyerPhoneLabel.Multiline = true;
            this.lbl_BuyerPhoneLabel.Name = "lbl_BuyerPhoneLabel";
            this.lbl_BuyerPhoneLabel.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerPhoneLabel.Text = "Phone / Telefon:";
            this.lbl_BuyerPhoneLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerPhoneLabel.Weight = 0.34193546754785D;
            // 
            // lbl_BuyerPhone
            // 
            this.lbl_BuyerPhone.Dpi = 254F;
            this.lbl_BuyerPhone.Multiline = true;
            this.lbl_BuyerPhone.Name = "lbl_BuyerPhone";
            this.lbl_BuyerPhone.StylePriority.UseTextAlignment = false;
            this.lbl_BuyerPhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl_BuyerPhone.Weight = 0.65806453245215D;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1154.792F, 340.5721F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(820.2083F, 58.42001F);
            this.xrLabel2.Text = "Buyer / Nabywca:";
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineWidth = 2F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 891.1954F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1950F, 5.291687F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.lblTotalDueCurrency,
            this.xrLine1,
            this.xrBarCode,
            this.lblSwift,
            this.lblBankAccount,
            this.xrLabel11,
            this.lblLegalNote,
            this.xrLabel9,
            this.xrLabel7,
            this.xrLabel8,
            this.lblDueDate,
            this.xrLabel6,
            this.lblTotalDue,
            this.xrLabel3,
            this.xrTable4});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 954.5341F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1750.104F, 883.0964F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(232.8334F, 58.41998F);
            this.xrPageInfo1.TextFormatString = "Page {0} of {1}";
            // 
            // lblTotalDueCurrency
            // 
            this.lblTotalDueCurrency.Dpi = 254F;
            this.lblTotalDueCurrency.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.lblTotalDueCurrency.LocationFloat = new DevExpress.Utils.PointFloat(407.5874F, 109.4549F);
            this.lblTotalDueCurrency.Multiline = true;
            this.lblTotalDueCurrency.Name = "lblTotalDueCurrency";
            this.lblTotalDueCurrency.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblTotalDueCurrency.SizeF = new System.Drawing.SizeF(148.0617F, 95.46172F);
            this.lblTotalDueCurrency.StylePriority.UseFont = false;
            this.lblTotalDueCurrency.StylePriority.UseTextAlignment = false;
            this.lblTotalDueCurrency.Text = "PLN";
            this.lblTotalDueCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 2F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(13.49998F, 7.666683F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1965.854F, 5F);
            // 
            // xrBarCode
            // 
            this.xrBarCode.AutoModule = true;
            this.xrBarCode.Dpi = 254F;
            this.xrBarCode.LocationFloat = new DevExpress.Utils.PointFloat(1471.354F, 652.0624F);
            this.xrBarCode.Module = 5.08F;
            this.xrBarCode.Name = "xrBarCode";
            this.xrBarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(26, 26, 0, 0, 254F);
            this.xrBarCode.SizeF = new System.Drawing.SizeF(508F, 190.5F);
            this.xrBarCode.StylePriority.UseTextAlignment = false;
            this.xrBarCode.Symbology = code128Generator1;
            this.xrBarCode.Text = "00000000";
            this.xrBarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSwift
            // 
            this.lblSwift.Dpi = 254F;
            this.lblSwift.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 827.3224F);
            this.lblSwift.Multiline = true;
            this.lblSwift.Name = "lblSwift";
            this.lblSwift.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblSwift.SizeF = new System.Drawing.SizeF(820.2083F, 58.41998F);
            this.lblSwift.Text = "Swift BIGBPLPW";
            // 
            // lblBankAccount
            // 
            this.lblBankAccount.Dpi = 254F;
            this.lblBankAccount.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 710.4825F);
            this.lblBankAccount.Multiline = true;
            this.lblBankAccount.Name = "lblBankAccount";
            this.lblBankAccount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblBankAccount.SizeF = new System.Drawing.SizeF(820.2083F, 116.8399F);
            this.lblBankAccount.Text = "PLN 54116022151037606762556868\r\nEUR PL16116022151037706762556868";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 652.0624F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(820.2083F, 58.41998F);
            this.xrLabel11.Text = "Wpłaty proszę kierować na rachunek cesjonariusza:";
            // 
            // lblLegalNote
            // 
            this.lblLegalNote.Dpi = 254F;
            this.lblLegalNote.LocationFloat = new DevExpress.Utils.PointFloat(24.99993F, 431.7409F);
            this.lblLegalNote.Multiline = true;
            this.lblLegalNote.Name = "lblLegalNote";
            this.lblLegalNote.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblLegalNote.SizeF = new System.Drawing.SizeF(1950F, 207.0923F);
            this.lblLegalNote.StylePriority.UseTextAlignment = false;
            this.lblLegalNote.Text = "...................................";
            this.lblLegalNote.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 373.3209F);
            this.xrLabel9.Multiline = true;
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(439.2083F, 58.41998F);
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Notes / Uwagi:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(424.5208F, 293.7342F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(366.0416F, 58.41998F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Transfer / Przelew";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 293.7342F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(399.5208F, 58.41995F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Payment type / Płatność:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDueDate
            // 
            this.lblDueDate.Dpi = 254F;
            this.lblDueDate.LocationFloat = new DevExpress.Utils.PointFloat(464.2083F, 215.5F);
            this.lblDueDate.Multiline = true;
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblDueDate.SizeF = new System.Drawing.SizeF(366.0416F, 58.41998F);
            this.lblDueDate.StylePriority.UseTextAlignment = false;
            this.lblDueDate.Text = "....-..-..";
            this.lblDueDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDueDate.TextFormatString = "{0:dd-MM-yyyy}";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 215.5F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(439.2083F, 58.41998F);
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Due date / Termin płatności:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.Dpi = 254F;
            this.lblTotalDue.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.lblTotalDue.LocationFloat = new DevExpress.Utils.PointFloat(555.649F, 109.4549F);
            this.lblTotalDue.Multiline = true;
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblTotalDue.SizeF = new System.Drawing.SizeF(366.0416F, 95.46172F);
            this.lblTotalDue.StylePriority.UseFont = false;
            this.lblTotalDue.StylePriority.UseTextAlignment = false;
            this.lblTotalDue.Text = "00.00";
            this.lblTotalDue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 141.2049F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(357.1874F, 63.71172F);
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Total due / Do zapłaty:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(1178.604F, 25F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16,
            this.xrTableRow18,
            this.xrTableRow17});
            this.xrTable4.SizeF = new System.Drawing.SizeF(804.3336F, 190.5F);
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.lblTotalNetPriceCurrency,
            this.lblTotalNetPrice});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Total net price / Wartość netto";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 1.3644051939796589D;
            // 
            // lblTotalNetPriceCurrency
            // 
            this.lblTotalNetPriceCurrency.CanShrink = true;
            this.lblTotalNetPriceCurrency.Dpi = 254F;
            this.lblTotalNetPriceCurrency.Multiline = true;
            this.lblTotalNetPriceCurrency.Name = "lblTotalNetPriceCurrency";
            this.lblTotalNetPriceCurrency.StylePriority.UseTextAlignment = false;
            this.lblTotalNetPriceCurrency.Text = "PLN";
            this.lblTotalNetPriceCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTotalNetPriceCurrency.Weight = 0.30190439299335636D;
            // 
            // lblTotalNetPrice
            // 
            this.lblTotalNetPrice.CanShrink = true;
            this.lblTotalNetPrice.Dpi = 254F;
            this.lblTotalNetPrice.Multiline = true;
            this.lblTotalNetPrice.Name = "lblTotalNetPrice";
            this.lblTotalNetPrice.Text = "00.00";
            this.lblTotalNetPrice.Weight = 0.44480211152805127D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.lblTotalVATAmountCurrency,
            this.lblTotalVATAmount});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "VAT amount / Wartość VAT";
            this.xrTableCell20.Weight = 1.3644051939796589D;
            // 
            // lblTotalVATAmountCurrency
            // 
            this.lblTotalVATAmountCurrency.CanShrink = true;
            this.lblTotalVATAmountCurrency.Dpi = 254F;
            this.lblTotalVATAmountCurrency.Multiline = true;
            this.lblTotalVATAmountCurrency.Name = "lblTotalVATAmountCurrency";
            this.lblTotalVATAmountCurrency.StylePriority.UseTextAlignment = false;
            this.lblTotalVATAmountCurrency.Text = "PLN";
            this.lblTotalVATAmountCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTotalVATAmountCurrency.Weight = 0.30190439299335636D;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.CanShrink = true;
            this.lblTotalVATAmount.Dpi = 254F;
            this.lblTotalVATAmount.Multiline = true;
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Text = "00.00";
            this.lblTotalVATAmount.Weight = 0.44480211152805127D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.lblTotalGorssAmountCurrency,
            this.lblTotalGorssAmount});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Total gross price / Wartość brutto";
            this.xrTableCell16.Weight = 1.3644051939796589D;
            // 
            // lblTotalGorssAmountCurrency
            // 
            this.lblTotalGorssAmountCurrency.CanShrink = true;
            this.lblTotalGorssAmountCurrency.Dpi = 254F;
            this.lblTotalGorssAmountCurrency.Multiline = true;
            this.lblTotalGorssAmountCurrency.Name = "lblTotalGorssAmountCurrency";
            this.lblTotalGorssAmountCurrency.StylePriority.UseTextAlignment = false;
            this.lblTotalGorssAmountCurrency.Text = "PLN";
            this.lblTotalGorssAmountCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTotalGorssAmountCurrency.Weight = 0.30190439299335636D;
            // 
            // lblTotalGorssAmount
            // 
            this.lblTotalGorssAmount.CanShrink = true;
            this.lblTotalGorssAmount.Dpi = 254F;
            this.lblTotalGorssAmount.Multiline = true;
            this.lblTotalGorssAmount.Name = "lblTotalGorssAmount";
            this.lblTotalGorssAmount.Text = "00.00";
            this.lblTotalGorssAmount.Weight = 0.44480211152805127D;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 97.89583F;
            this.Detail1.HierarchyPrintOptions.Indent = 50.8F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable6
            // 
            this.xrTable6.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.BorderWidth = 2F;
            this.xrTable6.Dpi = 254F;
            this.xrTable6.Font = new System.Drawing.Font("Arial", 9.75F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(24.99993F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1950F, 97.89583F);
            this.xrTable6.StylePriority.UseBorderDashStyle = false;
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseBorderWidth = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellIndex,
            this.cellProduct,
            this.cellQty,
            this.cellUnit,
            this.cellTotalNet,
            this.cellVATRatio,
            this.cellVATAmount,
            this.cellTotalGross});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // cellIndex
            // 
            this.cellIndex.Dpi = 254F;
            this.cellIndex.Multiline = true;
            this.cellIndex.Name = "cellIndex";
            this.cellIndex.Weight = 0.32481004469874625D;
            this.cellIndex.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cellIndex_BeforePrint);
            // 
            // cellProduct
            // 
            this.cellProduct.Dpi = 254F;
            this.cellProduct.Multiline = true;
            this.cellProduct.Name = "cellProduct";
            this.cellProduct.Weight = 2.642283268039066D;
            // 
            // cellQty
            // 
            this.cellQty.Dpi = 254F;
            this.cellQty.Multiline = true;
            this.cellQty.Name = "cellQty";
            this.cellQty.Weight = 0.46709455232280128D;
            // 
            // cellUnit
            // 
            this.cellUnit.Dpi = 254F;
            this.cellUnit.Multiline = true;
            this.cellUnit.Name = "cellUnit";
            this.cellUnit.Weight = 1.2195672245757656D;
            // 
            // cellTotalNet
            // 
            this.cellTotalNet.Dpi = 254F;
            this.cellTotalNet.Multiline = true;
            this.cellTotalNet.Name = "cellTotalNet";
            this.cellTotalNet.Text = "00.00";
            this.cellTotalNet.Weight = 0.95712859986101884D;
            // 
            // cellVATRatio
            // 
            this.cellVATRatio.Dpi = 254F;
            this.cellVATRatio.Multiline = true;
            this.cellVATRatio.Name = "cellVATRatio";
            this.cellVATRatio.Text = "00.00";
            this.cellVATRatio.Weight = 0.46413713507854915D;
            // 
            // cellVATAmount
            // 
            this.cellVATAmount.Dpi = 254F;
            this.cellVATAmount.Multiline = true;
            this.cellVATAmount.Name = "cellVATAmount";
            this.cellVATAmount.Text = "00.00";
            this.cellVATAmount.Weight = 1.0750211954224438D;
            // 
            // cellTotalGross
            // 
            this.cellTotalGross.Dpi = 254F;
            this.cellTotalGross.Multiline = true;
            this.cellTotalGross.Name = "cellTotalGross";
            this.cellTotalGross.Text = "00.00";
            this.cellTotalGross.Weight = 0.84995798000160838D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.HeightF = 176.7292F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable5
            // 
            this.xrTable5.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable5.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.BorderWidth = 2F;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(24.99993F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1950F, 176.7292F);
            this.xrTable5.StylePriority.UseBorderDashStyle = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseBorderWidth = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell25,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell28,
            this.xrTableCell26});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "No. / LP";
            this.xrTableCell22.Weight = 0.32481004469874625D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "Item / Nazwa towaru / usługi";
            this.xrTableCell25.Weight = 2.642283518439676D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "Qty / Ilość";
            this.xrTableCell23.Weight = 0.46709430192219176D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "Unit net price / Cena netto";
            this.xrTableCell24.Weight = 1.2195672245757658D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "Total net / Wartość netto";
            this.xrTableCell27.Weight = 0.95712859986101884D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "VAT %";
            this.xrTableCell29.Weight = 0.46413713507854915D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Multiline = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "VAT amount / Wartość VAT";
            this.xrTableCell28.Weight = 1.0750211954224438D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "Total gross / Wartość brutto";
            this.xrTableCell26.Weight = 0.84995798000160838D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.HeightF = 122.8958F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable7
            // 
            this.xrTable7.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.BorderWidth = 2F;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.Font = new System.Drawing.Font("Arial", 9.75F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(862.0832F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable7.SizeF = new System.Drawing.SizeF(1112.917F, 97.89582F);
            this.xrTable7.StylePriority.UseBorderDashStyle = false;
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseBorderWidth = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.lblCellNet,
            this.lblCellVATRatio,
            this.lblCellVATValue,
            this.lblCellTotal});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Total";
            this.xrTableCell6.Weight = 1.1958381662310937D;
            // 
            // lblCellNet
            // 
            this.lblCellNet.Dpi = 254F;
            this.lblCellNet.Multiline = true;
            this.lblCellNet.Name = "lblCellNet";
            this.lblCellNet.Text = "00.00";
            this.lblCellNet.Weight = 0.93850788856458356D;
            // 
            // lblCellVATRatio
            // 
            this.lblCellVATRatio.Dpi = 254F;
            this.lblCellVATRatio.Multiline = true;
            this.lblCellVATRatio.Name = "lblCellVATRatio";
            this.lblCellVATRatio.Text = "00.00";
            this.lblCellVATRatio.Weight = 0.45510606830910738D;
            // 
            // lblCellVATValue
            // 
            this.lblCellVATValue.Dpi = 254F;
            this.lblCellVATValue.Multiline = true;
            this.lblCellVATValue.Name = "lblCellVATValue";
            this.lblCellVATValue.Text = "00.00";
            this.lblCellVATValue.Weight = 1.0541047602335574D;
            // 
            // lblCellTotal
            // 
            this.lblCellTotal.Dpi = 254F;
            this.lblCellTotal.Multiline = true;
            this.lblCellTotal.Name = "lblCellTotal";
            this.lblCellTotal.Text = "00.00";
            this.lblCellTotal.Weight = 0.83342100926952511D;
            // 
            // rpt_Invoice
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.Detail,
            this.PageFooter,
            this.DetailReport});
            this.DisplayName = "Invoice Print";
            this.Dpi = 254F;
            this.DrawWatermark = true;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPreviewMarginLines = false;
            this.SnapGridSize = 25F;
            this.Version = "20.1";
            this.Watermark.Font = new System.Drawing.Font("Verdana", 66F, System.Drawing.FontStyle.Bold);
            this.Watermark.ShowBehind = false;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel lblSwift;
        private DevExpress.XtraReports.UI.XRLabel lblBankAccount;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lblLegalNote;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblDueDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblTotalDue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalNetPrice;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalVATAmount;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalGorssAmount;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell cellIndex;
        private DevExpress.XtraReports.UI.XRTableCell cellProduct;
        private DevExpress.XtraReports.UI.XRTableCell cellQty;
        private DevExpress.XtraReports.UI.XRTableCell cellUnit;
        private DevExpress.XtraReports.UI.XRTableCell cellTotalNet;
        private DevExpress.XtraReports.UI.XRTableCell cellVATRatio;
        private DevExpress.XtraReports.UI.XRTableCell cellVATAmount;
        private DevExpress.XtraReports.UI.XRTableCell cellTotalGross;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblInvoiceNumber;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerName;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerAddress;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerVATNIPLable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerEmail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerWebsite;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerPhoneLabel;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerName;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerAddress;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerVATNIPLabel;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerEmail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerWebsite;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerPhoneLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblCellNet;
        private DevExpress.XtraReports.UI.XRTableCell lblCellVATRatio;
        private DevExpress.XtraReports.UI.XRTableCell lblCellVATValue;
        private DevExpress.XtraReports.UI.XRTableCell lblCellTotal;
        private DevExpress.XtraReports.UI.XRLabel lblTotalDueCurrency;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalNetPriceCurrency;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalVATAmountCurrency;
        private DevExpress.XtraReports.UI.XRTableCell lblTotalGorssAmountCurrency;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerVATNIP;
        private DevExpress.XtraReports.UI.XRTableCell lbl_BuyerPhone;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerVATNIP;
        private DevExpress.XtraReports.UI.XRTableCell lbl_SellerPhone;
    }
}
